#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 13:31:54 2020

@author: dreizler

"""

import numpy as np
import transit
import yaml

from config import *


class Star:
    def __init__(self, **kwargs):
        self.Mstar  = kwargs.get("Mstar", 1.)   # mass of star in solar masses!
        self.Rstar  = kwargs.get("Rstar", 1.)   # radius of star in solar radii!
        self.MstarE = kwargs.get("MstarE", 0.)   # uncertainty mass of star in solar masses!
        self.RstarE = kwargs.get("RstarE", 0.)   # uncertainty radius of star in solar radii!
        self.Teff   = kwargs.get("Teff", 5780.)

class Planet(Star):
    #def __str__(self):
        # Todo
    def __init__(self, **kwargs):
        #print("kwargs",kwargs.get("Rstar", 1.))
        Mstar = kwargs.get("Mstar", 1.)
        Rstar = kwargs.get("Rstar", 1.)
        Teff  = kwargs.get("Teff", 5780.)

        #print("Rstar in Planet",Rstar)
        
        Star.__init__(self, Mstar=Mstar, Rstar=Rstar, Teff=Teff)
        #print("Rstar in Planet after Star",self.Rstar)

        self.t      = kwargs.get("t", 0.)                       # np.array of times
        self.t0     = kwargs.get("t0", 0.)                      # reference time

        self.K      = kwargs.get("K", None)                     # velocity semi-amplitude in m/s
        self.P      = kwargs.get("P", 365.25)                   # orbital period in days
        self.e      = np.abs(kwargs.get("e", 0.))               # eccenticity
        self.om     = kwargs.get("om", 0.5*np.pi)               # longitude of periastron in rad
        self.tperi  = kwargs.get("tperi", 0.)                   # time of periastron in days
        self.inc    = kwargs.get("inc", 0.5*np.pi)              # inclination in rad
        self.Om     = kwargs.get("Om", 0.)                      # longitude ascending node in rad
        self.ttra   = kwargs.get("ttra", 0.)                    # transit time in days
        self.Rp2Rs  = kwargs.get("Rp2Rs", 0.01)                 # planet to star ratio
        self.a2Rs   = kwargs.get("a2Rs", 214.85135514389316)    # semi major axis to star ratio
        self.m      = kwargs.get("m", None)                     # mass in M_earth
        self.lam    = kwargs.get("lam", 0.5*np.pi)              # Mean longitude in rad
        self.Ma     = kwargs.get("Ma", 0.)                      # Mean anomaly in rad
        self.albedo = kwargs.get("albedo", 0.)                  # planetary albedo
        a           = kwargs.get("a", 1.)*au
        
        self.orbit      = kwargs.get("orbit", "Circular")          # "Kepler", "Dynamic", "Cricular", "None"
        self.Transit    = kwargs.get("Transit", False)             # calculate transit? 
        self.Coplanar   = kwargs.get("Coplanar", True)             # co-planar orbits?
        self.TransitOnly = kwargs.get("TransitOnly", False)        # not RV data, fit Mandel Agol model only

        self.predict_a2Rs= kwargs.get("predict_a2Rs", False)       # calulate a2Rs from P (3.rd Kepler) using Stellar and Planetary mass from yaml file
        if self.predict_a2Rs: self.fixa2Rs = True

        self.fixK       = kwargs.get("fixK", False)                    # fix RV amplitude
        self.fixm       = kwargs.get("fixm", False)                    # fix planet mass
        self.fixP       = kwargs.get("fixP", False)                    # fix planet period
        self.fixe       = kwargs.get("fixe", False)                    # fix eccentricity
        self.fixom      = kwargs.get("fixom", False)                    # fix longitude of periastron
        self.fixtperi   = kwargs.get("fixtperi", False)                    # fix time of periastron
        self.fixInc     = kwargs.get("fixInc", False)              # fix inclination
        self.fixOm      = kwargs.get("fixOm", True)                # fix node line
        self.fixRp2Rs   = kwargs.get("fixRp2Rs", False)                    # fix planet radius to star radius
        self.fixa2Rs    = kwargs.get("fixa2Rs", False)                    # fix semi major axis to star radius
        self.fixRstar   = kwargs.get("fixRstar", False)
        
        if self.TransitOnly: self.fixRstar = True

        self.firstcall  = kwargs.get("firstcall", False)           #
        self.varfix = [0]*10        # 0 for free parameter, 1 for fixed parameter

        self.varind = []   # provide index list for free parameters
        self.varadd = [0]*9   # additional paramters calculated for corner plot


        # tperi handling:
        if self.tperi > 2e6:
            self.tperi = (self.tperi-self.t0)%self.P
            if self.firstcall: 
                print(f"tperi reduced and Modulo P: {self.tperi}")
        # sys.exit()    
        
        if "dynamic" in self.orbit.lower() or 'ttv' in self.orbit.lower():
            self.allTransits = []
        
        Planet.set_parameters(self,self.firstcall)
        
        #print(kwargs)
        #sys.eit()
        
    def set_parameters(self,firstcall):
        if self.Coplanar and self.fixInc:
            raise ValueError('conflicting keywords "Coplanar" and "fixInc"',self.Coplanar ,self.fixInc)

        if self.orbit == 'Dynamic' or self.orbit == 'TTV': 
            Planet.get_K(self)
        elif 'kepler' in self.orbit.lower():
            if self.K == None:
                Planet.get_K(self)
            elif self.m == None:
                Planet.get_mass(self)
            else:
                pass #raise ValueError('orbit: Kepler but neither K nor m given')
        else: 
            Planet.get_mass(self)
        Planet.get_a(self)
        Planet.get_lam(self)
        Planet.get_ttra(self)
        Planet.get_meananomaly(self)
        if not self.TransitOnly or self.predict_a2Rs:
            Planet.get_a2Rs(self)
        #print("Planet.a2Rs", self.a2Rs)

        Planet.get_Rp(self)
        Planet.get_teq(self)

        if firstcall:
            self.varind = []
            # default settings for specific cases
            if self.orbit == 'Circular':
                self.e      = 0.
                self.om     = 0.5*np.pi
            if not self.Transit:
                self.Rp2Rs  = 0.
                if not (self.orbit == 'Dynamic' or self.orbit == 'TTV') and (not self.Coplanar): 
                    self.inc    = np.pi*0.5
            #else:
            #    if self.fixInc:
            #        self.inc    = np.pi*0.5
            if self.fixOm:
                self.Om     = 0.
            Ppar = 0
            #print("varind",self.varind)
            if self.orbit == 'Circular':
                if not self.fixK and not self.TransitOnly:
                    Ppar += 1
                    self.varfix[0] = 0
                    self.varind.append(0)
                else:
                    self.varfix[0] = 1

                if not self.fixP:
                    Ppar += 1
                    self.varfix[2] = 0
                    self.varind.append(2)
                else:
                    self.varfix[2] = 1

                if not self.fixtperi:
                    Ppar += 1
                    self.varfix[5] = 0
                    self.varind.append(5)
                else:
                    self.varfix[5] = 1

                if not self.fixRp2Rs and self.Transit:
                    Ppar += 1
                    self.varfix[6] = 0
                    self.varind.append(6)
                else:
                    self.varfix[6] = 1

                if not self.fixInc and self.Transit:
                    Ppar += 1
                    self.varfix[7] = 0
                    self.varind.append(7)
                else:
                    self.varfix[7] = 1

                if not self.fixa2Rs and self.TransitOnly:
                    Ppar += 1
                    self.varfix[9] = 0
                    self.varind.append(9)
                else:
                    self.varfix[9] = 1

                self.varfix[1] = 1
                self.varfix[3] = 1
                self.varfix[4] = 1
                self.varfix[8] = 1

            # Ppar = 3

            if self.orbit == 'Kepler':
                if not self.fixK and not self.TransitOnly:
                    Ppar += 1
                    self.varfix[0] = 0
                    self.varind.append(0)
                else:
                    self.varfix[0] = 1

                if not self.fixP:
                    Ppar += 1
                    self.varfix[2] = 0
                    self.varind.append(2)
                else:
                    self.varfix[2] = 1

                if not self.fixe:
                    Ppar += 1
                    self.varfix[3] = 0
                    self.varind.append(3)
                else:
                    self.varfix[3] = 1

                if not self.fixom:
                    Ppar += 1
                    self.varfix[4] = 0
                    self.varind.append(4)
                else:
                    self.varfix[4] = 1

                if not self.fixtperi:
                    Ppar += 1
                    self.varfix[5] = 0
                    self.varind.append(5)
                else:
                    self.varfix[5] = 1

                if not self.fixRp2Rs and self.Transit:
                    Ppar += 1
                    self.varfix[6] = 0
                    self.varind.append(6)
                else:
                    self.varfix[6] = 1

                if not self.fixInc and self.Transit:
                    Ppar += 1
                    self.varfix[7] = 0
                    self.varind.append(7)
                else:
                    self.varfix[7] = 1

                if not self.fixa2Rs and self.TransitOnly:
                    Ppar += 1
                    self.varfix[9] = 0
                    self.varind.append(9)
                else:
                    self.varfix[9] = 1

                self.varfix[1] = 1
                self.varfix[8] = 1

                #Ppar = 5
            if self.orbit == 'Dynamic' or self.orbit == 'TTV':
                if not self.fixm and not self.TransitOnly:
                    Ppar += 1
                    self.varfix[1] = 0
                    self.varind.append(1)
                else:
                    self.varfix[1] = 1

                if not self.fixP:
                    Ppar += 1
                    self.varfix[2] = 0
                    self.varind.append(2)
                else:
                    self.varfix[2] = 1

                if not self.fixe:
                    Ppar += 1
                    self.varfix[3] = 0
                    self.varind.append(3)
                else:
                    self.varfix[3] = 1

                if not self.fixom:
                    Ppar += 1
                    self.varfix[4] = 0
                    self.varind.append(4)
                else:
                    self.varfix[4] = 1

                if not self.fixtperi:
                    Ppar += 1
                    self.varfix[5] = 0
                    self.varind.append(5)
                else:
                    self.varfix[5] = 1

                if not self.fixRp2Rs and self.Transit:
                    Ppar += 1
                    self.varfix[6] = 0
                    self.varind.append(6)
                else:
                    self.varfix[6] = 1

                if not self.fixInc:
                    Ppar += 1
                    self.varfix[7] = 0
                    self.varind.append(7)
                else:
                    self.varfix[7] = 1

                if not self.fixOm:
                    Ppar += 1
                    self.varfix[8] = 0
                    self.varind.append(8)
                else:
                    self.varfix[8] = 1

                if not self.fixa2Rs and self.TransitOnly:
                    Ppar += 1
                    self.varfix[9] = 0
                    self.varind.append(9)
                else:
                    self.varfix[9] = 1

                self.varfix[0] = 1

            #print("varind",self.varind)
            self.Ppar = Ppar
            self.ind  = 0      # correctly set in ModelingClass
    
            print("                  ")
            print("Param. Fixed Value")
            print("K     ",self.varfix[0],"   ",self.K)
            print("m     ",self.varfix[1],"   ",self.m)
            print("P     ",self.varfix[2],"   ",self.P)
            print("e     ",self.varfix[3],"   ",self.e)
            print("om    ",self.varfix[4],"   ",self.om)
            print("tperi ",self.varfix[5],"   ",self.tperi)
            print("Rp2Rs ",self.varfix[6],"   ",self.Rp2Rs)
            print("inc   ",self.varfix[7],"   ",self.inc)
            print("Om    ",self.varfix[8],"   ",self.Om)
            print("a2Rs  ",self.varfix[9],"   ",self.a2Rs)
            print(f"\n\tRstar {self.Rstar}")

            #print(self.varfix,self.varind,self.varval,Ppar)
            print("                  ")
            #print()
            #print("fix_a2Rs",self.fixa2Rs)
            #print()

    def set_TransitOnly(self,TransitOnly):
        self.TransitOnly = TransitOnly
        Planet.set_parameters(self,firstcall=True)
        
    def set_Transit(self,Transit):
        self.Transit = Transit
        Planet.set_parameters(self,firstcall=True)
        
    def set_Coplanar(self,Coplanar):
        self.Coplanar = Coplanar
        Planet.set_parameters(self,firstcall=True)
        
    def set_fixInc(self,fixInc):
        self.fixInc = fixInc
        Planet.set_parameters(self,firstcall=True)
        
    def set_fixOm(self,fixOm):
        self.fixOm = fixOm
        Planet.set_parameters(self,firstcall=True)
        
    def set_orbit(self,orbit):
        self.orbit = orbit
        Planet.set_parameters(self,firstcall=True)
        
    def set_tperi(self,tperi):
        self.tperi = tperi
        Planet.set_parameters(self,firstcall=False)
        
        
    def get_mass(self):
        if not self.m:
            mplanet = m_earth
            diff    = 1.
            count   = 0
            while (diff > 1.e-3) & (count< 10):
                try:
                    mnew = (self.P*d2s)**(1./3.)*tau13/G13*self.K*(self.Mstar*M_sun+mplanet)**(2./3.) *np.sqrt(1-self.e**2)/np.sin(self.inc)
                except ValueError:
                    mnew = mplanet
                    print('PlanetClass: problematic parameters:',self.P*d2s,self.K,self.Mstar)
                diff = np.abs(mnew-mplanet)/mplanet
                #print(diff,mnew,mplanet)
                mplanet = mnew
                count += 1
            self.m = mplanet/m_earth
        return self.m
    
    def get_K(self):
        self.K = G13/tau13/np.sqrt(1.-self.e*self.e) * self.m*m_earth*np.sin(self.inc) \
            / np.sqrt(1-self.e**2)  / ((self.Mstar*M_sun+self.m*m_earth)**(2./3.)) / ((self.P*d2s)**(1./3.)) 
        return self.K  
         
    def get_a(self):
        try:
            # print(f"{self.P} ({type(self.P)})")
            # print(f"{self.Mstar} ({type(self.Mstar)})")
            # print(f"{self.m} ({type(self.m)})")
            self.a = (self.P*d2s)**(2./3.)*G13*tau23*(self.Mstar*M_sun+self.m*m_earth)**(1./3.)/au
            #print("a",self.a)
        except ValueError:
            self.a = 1.*au
            print('PlanetClass: problematic parameters:',self.P*d2s,self.Mstar,self.m)
        return self.a
        
    def get_a2Rs(self):
        #print("a2Rs",self.a,self.Rstar)
        self.a2Rs = self.a*au/self.Rstar/R_sun
        return self.a2Rs

    def get_trad(self):
        """
        returns the transit duration
        
        Parameters:
            self
        """
        self.trad = self.P/Planet.get_a2Rs(self)/np.pi
        #self.trad = self.P/Planet.get_a2Rs(self)/np.pi * np.sqrt((1+self.Rp2Rs)**2-(self.a2Rs*np.cos(self.inc))**2)
        return self.trad 

    def get_Rp(self):
        self.Rp = self.Rp2Rs*self.Rstar*R_sun/r_earth
        return self.Rp
        
    def get_teq(self):
        self.teq = self.Teff*(1.-self.albedo)**0.25*np.sqrt(0.5*self.Rstar*R_sun/self.a/au)
        return self.teq 
        
    def get_lam(self):
        self.lam = (pi2/self.P*(self.t-self.tperi)+self.om) % pi2
        return self.lam
        
    def get_meananomaly(self):
        #print(self.P,self.t,self.tperi)
        self.Ma  = pi2/self.P*(self.t-self.tperi)
        #print("ma",self.Ma)
        return self.Ma 

    def get_anomaly(self):
        Ma   = Planet.get_meananomaly(self)
        Ea   = Ma
        diff = 1.
        count = 0
        while (diff > 1.e-6) and (count < 100):
            Enew = Ea - (Ea - np.abs(self.e)*np.sin(Ea) -Ma)/(1. - np.abs(self.e)*np.cos(Ea))
            #print(f'{Ea}')
            diff   = np.max(np.abs(Enew-Ea)/np.pi)
            #print(f'\r diff: {diff}', end='')
            Ea     = Enew
            count += 1
           
        Ta  = 2.*np.arctan(np.sqrt((1.+np.abs(self.e))/(1.-np.abs(self.e)))*np.tan(0.5*Ea))
        return Ta
        
    def get_ttra(self):
        f_tra = (0.5 * np.pi - self.om) % pi2

        tanE2 = np.tan(0.5 * f_tra) * np.sqrt((1. - np.abs(self.e)) / (1. + np.abs(self.e)))
        E = 2. * np.arctan(tanE2)
        Ma = E - np.abs(self.e) * np.sin(E)
        self.ttra = Ma * self.P / pi2 + self.tperi
        return self.ttra
    
    def get_tperi(self,ttra=0):
        """ Return the time of periastron passage
        
        args:
            ttra (float, optional): time of transit 
        """
        
        if ttra != 0:
            f_tra = (0.5 * np.pi - self.om) % pi2
    
            tanE2 = np.tan(0.5 * f_tra) * np.sqrt((1. - np.abs(self.e)) / (1. + np.abs(self.e)))
            E = 2. * np.arctan(tanE2)
            Ma = E - np.abs(self.e) * np.sin(E)
            tperi = ttra - Ma * self.P / pi2 
            return tperi
        else:
            return self.tperi
            

    def Kepler(self,t=0.):
        self.t = t
        #print("KEPLER",self.K)
        vkep= self.K\
            *(np.cos(self.om+Planet.get_anomaly(self))+np.abs(self.e)*np.cos(self.om)) 
            #*np.sin(self.inc)\
        #print("KEPLER",np.max(vkep),np.min(vkep))
        #plt.plot(t,vkep)
        #plt.show()
        return vkep

    def MandelAgolRV(self,t=0.,u1=0.,u2=0.):
        #print(self.Transit)
        if not self.Transit: return np.ones(len(t))
        self.t = t
        self.u1= u1
        self.u2= u2
                   
        #print("MA",self.t)
        
        Ta     = Planet.get_anomaly(self)
        self.m = Planet.get_mass(self)
        self.a = Planet.get_a(self)
        
        z = self.a*au*(1.-self.e*self.e)/(1.+self.e*np.cos(Ta))/self.Rstar/R_sun \
             *np.sqrt(1.-np.sin(self.om+Ta)**2*np.sin(self.inc)**2)
        sign = np.sign(np.sin(self.om+Ta)*np.sin(self.inc))
        Photo = transit.occultquad(z+1.-sign,self.Rp2Rs,[self.u1,self.u2]) #* 1.5e3
        #plt.plot(t,z)
        #plt.title('z')
        #plt.show()
        return Photo
    def MandelAgol(self,t=0.,u1=0.,u2=0.,t0shift=0.):
        #print(self.Transit)
        if not self.Transit: return np.ones(len(t))
        self.t = t + t0shift
        self.u1= u1
        self.u2= u2
                   
        #print("MA",self.t)
        #print("MA",self.a2Rs)
        
        Ta     = Planet.get_anomaly(self)
        z = self.a2Rs*(1.-self.e*self.e)/(1.+self.e*np.cos(Ta)) \
             *np.sqrt(1.-np.sin(self.om+Ta)**2*np.sin(self.inc)**2)
        sign = np.sign(np.sin(self.om+Ta)*np.sin(self.inc))
        Photo = transit.occultquad(z+1.-sign,self.Rp2Rs,[self.u1,self.u2]) #* 1.5e3
        #print(np.min(z))
        #import matplotlib.pyplot as plt
        #plt.plot(t,z)
        #plt.title('z')
        #plt.show()
        return Photo
