# Exoplanet Flexi Fit

Stefan's fit routine for radial velocity and transit data

20.05.2020 Paul:
	some routines updated and input file added
	starting point for multi-processing layed out
	Output filename handling for existing output files
04.07.2020 Paul:
    Input using the YAML file completely implemented
    and usable
	

ToDo:

    as class with:
	__init__ (read and setup)
	RV/AC/TR subclasses

	TTV?
	PlanetPlanet VS Mercury?
