#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 13:31:54 2020

@author: dreizler
"""

#import astropy
from astropy.io import fits
import corner
#import celerite
#from celerite import terms
#from celerite.modeling import Model
import emcee
#import gls
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
#import matplotlib.cm as cm
import numpy as np
import pprint,pickle
from scipy.optimize import minimize
import time
#import transit
import copy
import sys
import shutil

from multiprocessing import Pool, cpu_count
import multiprocessing as mp
from optimparallel import minimize_parallel

from DataClass import *
from PlanetClass import *
from ModelingClass import *
from InputFunction import *

from config import *

global input_dir



def fit_TransitTimes(**kwargs):
    output_dir = kwargs.get('output_dir', "Outputs/")
    input_dir  = kwargs.get('input_dir',"InputTests/")
    savename   = kwargs.get('savename', 'test')
    savenamein = kwargs.get('savenamein', savename)
    inputData  = kwargs.get('inputData','InputData.yaml')
    numBurn    = kwargs.get('numBurn', 2000)
    numMCMC    = kwargs.get('numMCMC', 10000)
    numWalkers = kwargs.get('numWalkers', 200)
    numThread  = kwargs.get('numThread', 1)
    overwrite  = kwargs.get('overwrite', True)
    showInput  = kwargs.get('showInput', False)
    
    plotBurnIn = kwargs.get('plotBurnIn', False)        # plots the BurnIn walker traces
    plotMCMC   = kwargs.get('plotMCMC', False)          # plots the MCMC walker traces
    
    start_from_sample = kwargs.get('start_from_sample', False)
    append_sample     = kwargs.get('append_sample', False)
    t0                = kwargs.get('t0', 2450000)
    numt              = kwargs.get('numt', 10000)

    if SPOCK: numThread = 1
    
    if noShow:
        plotBurnIn = False
        plotMCMC = False
    
    count = 0
    p0    = []
    
    
    ###############################################################################     
    # some basic settings
    
    
    #numt       = 10000     # number of points for model plot
    
    DATAList   = []        # list for all observational data
    PlanetList = []        # list for all planets
    
    #savename  += str(numProc)          # prefix of files to be saved
    #savenamein = savename              # prefix of files to be saved
    
    ReStart    = False                 # re-start from previous result?



    ###############################################################################
    # get t0corr
    print("#######################")
          
    if autoT0:
        t0corr = np.inf
        minTTR = np.inf
        minTRV = np.inf
        
        RVgeneral, RVdata, boundsgpRV, boundsJRV, ReStartDataRV = fileInput( "RV", inputData, input_dir)
        ACdata, boundsgpAC, boundsJAC, ReStartDataAC = fileInput("AC", inputData, input_dir)
        TRdata, boundsgpTR, boundsJTR, boundsTR, ReStartDataTR = fileInput("TR", inputData, input_dir)
                
        #TODO: rethink restart (read from pickle and set t0corr accordingly)
        if not ReStartDataRV:
            for i, RV in enumerate(RVdata):
                RV_input = RVTimeSeries(**RVgeneral, **RV, **boundsgpRV,
                                         t0=t0, t0corr= t0corr,
                                         plot = False,
                                         ReadTheData = True,
                                         color=INSTCOLORS[i])
                minTRV = min([item for s in [[minTRV],RV_input.time] for item in s])
        else:
            pkl_file = open(output_dir+savenamein+'RV.pkl', 'rb')
            #Stern    = pickle.load(pkl_file)
            #Planets  = pickle.load(pkl_file)
            Data     = pickle.load(pkl_file)
            pprint.pprint(Data)
            pkl_file.close()
            for i in range(len(RVdata)):
                minTRV = min([item for s in [[minTRV],Data[i].time] for item in s])
                print(f"i: {i}")
                print(f"min(Data[i].time: {min(Data[i].time)})")
        print(f"minTRV: {minTRV}")
            
        if not ReStartDataTR:
            for i, TR in enumerate(TRdata):
                TR_input = TRTimeSeries(**TR, **boundsgpTR[i], **boundsJTR[i],
                                            t0= t0, ttras= [], plot= False,
                                            #PlanetList= PlanetList[j], 
                                            data = 'TR', 
                                            ReadTheData = True)
                minTTR = min([item for s in [[minTTR],TR_input.time] for item in s])
        else:
            pkl_file = open(output_dir+savenamein+'TR.pkl', 'rb')
            #Stern    = pickle.load(pkl_file)
            #Planets  = pickle.load(pkl_file)
            Data     = pickle.load(pkl_file)
            pprint.pprint(Data)
            pkl_file.close()
            TRstart = len(RVdata)+len(ACdata)
            for i in range(len(TRdata)):
                minTTR = min([item for s in [[minTTR],Data[TRstart+i].time] for item in s])
        print(f"minTTR: {minTTR}")
            
            
        if np.isfinite(minTRV) and not np.isfinite(minTTR):
            minT = minTRV
        elif np.isfinite(minTTR): # != np.inf or not np.isnan(minTTR):
            minT = minTTR
        else:
            raise Exception("no RV or TR input data found")
            
        if minT < t0corr:
            t0corr = minT
            
            
        t0 += t0corr
    else:
        t0corr=0
        
    print(f"# t0: {t0} \n#\tt0corr: {t0corr}")
    print("#######################\n")
          
    # sys.exit()

   
    ###############################################################################
    # define planets and star, parameters need to be adapted
    #
    # read the input file of the parameters of the stellar system
    #
    ###############################################################################
    

    stellarParameter = {}
    planetParameter  = []
    bounds = {}
    
    stellarParameter, planetParameter, bounds, ReStart = planetInput(inputData, input_dir, PrintAll = False) 
    print("planetParameter",planetParameter)
    print(f"ReStart: {ReStart}")
    print('bounds', bounds)
    # sys.exit()
    
    ###############################################################################
    if ReStart:
        pkl_file = open(output_dir+savenamein+'System.pkl', 'rb')
        Stern    = pickle.load(pkl_file)
        Planets  = pickle.load(pkl_file)
        #Data     = pickle.load(pkl_file)
        #pprint.pprint(Stern)
        #pprint.pprint(Planets)
        #print(Stern.Mstar)
        #print(Stern.MstarE)
        #print(Stern.Rstar)
        #print(Stern.RstarE)
        #print(Planets[0].Mstar)
        #print(Planets[0].Rstar)
        #print(Planets[0].P)
        #print(Planets[0].e)
        #print(Planets[0].om)
        #print(Planets[0].tperi)
        #print(Planets[0].Ma)
        #pprint.pprint(Data)
        pkl_file.close()
        # decide, which planet and star parameters to be taken from previous solution
        # planet index 0 from previous solution
        
    ###############################################################################    
    
    Rstar = stellarParameter.get('Rstar',1)
    RstarE = stellarParameter.get('RstarE',1)
    Mstar = stellarParameter.get('Mstar',1)
    MstarE = stellarParameter.get('MstarE',1)
    fixRstar= stellarParameter.get('fixRstar',1)

    #print("Rstar",Rstar)
    
    if stellarParameter.get('restart',False):
        STAR  = Star(Mstar=Stern.Mstar, Rstar=Stern.Rstar,
                     MstarE=Stern.MstarE,RstarE=Stern.RstarE)
    else:
        STAR  = Star(**stellarParameter)
        
    if ReStart:
        print('**********************************************')
        print(f'STAR.Rstar: {STAR.Rstar}')
        print(f'STAR.RstarE: {STAR.RstarE}')
        print(f'STAR.RMstar: {STAR.Mstar}')
        print(f'STAR.RMstarE: {STAR.MstarE}')
        print('**********************************************')
        # sys.exit()
        
    #Rstar = STAR.Rstar
    #RstarE = STAR.RstarE
    #Mstar = STAR.Mstar
    #MstarE = STAR.MstarE
    #stellarParameter['Rstar'] = STAR.Rstar
    #stellarParameter['RstarE'] = STAR.RstarE
    #stellarParameter['Mstar'] = STAR.Mstar
    #stellarParameter['MstarE'] = STAR.MstarE
    #print("M,R input",STAR.Mstar,STAR.Rstar)
    #print(Rstar,Mstar)
    
    
    for i, planet in enumerate(planetParameter):
        try:
            if ReStart and planet.get('restart',False):
                planet.pop('restart')
                print(f'Restart Planet{i}')
                print("Updated parameters:")
                print(planet)
                for key, value in planet.items():
                    exec(f'Planets[i].{key} = "{value}"')
                Planets[i].set_parameters(True)
                
                PlanetList.append(Planets[i])
            
            else:
                PlanetList.append(Planet(t0 = t0, firstcall = True, 
                                     **stellarParameter, **planet))
        except IndexError:
            raise IndexError("More Planets set to Restart than present in save file")
        
    
    try:
        print("######################################################################")
        for i, planet in enumerate(PlanetList):
            if i == 0:
                print(f"STAR.Mstar: {STAR.Mstar}")
                print(f"STAR.MstarE: {STAR.MstarE}")
                print(f"STAR.Rstar: {STAR.Rstar}")
                print(f"STAR.RstarE: {STAR.RstarE}")
            #print(f"parameter vector: {len(dir(PlanetList[i]))}")
            #print(dir(PlanetList[i]))
            print("  ")
            print(f"orbit: {PlanetList[i].orbit}")
            print(f"P: {PlanetList[i].P}")
            print(f"K: {PlanetList[i].K}")
            print(f"m: {PlanetList[i].m}")
            print(f"e: {PlanetList[i].e}")
            print(f"om: {PlanetList[i].om}")
            print(f"tperi: {PlanetList[i].tperi}")
            print(f"inc: {PlanetList[i].inc}")
            print(f"Om: {PlanetList[i].Om}")
            print(f"ttra: {PlanetList[i].ttra}")
            print(f"Rp2Rs: {PlanetList[i].Rp2Rs}")
            print(f"a2Rs: {PlanetList[i].a2Rs}")
            
    except:
        print("######################################################################")
        print("no Planet Input")
              
    try:
        minPer = np.min([l.P for l in PlanetList if (l.P > 0.) & (l.orbit == 'Dynamic')])
    except:
        minPer = minPER
    
    fitRstar = False
    for Plan in PlanetList:
        #print(Plan.fixRstar)
        #if Plan.Transit & (not Plan.TransitOnly): fitRstar = True
        if (not Plan.fixRstar) & Plan.Transit & (not Plan.TransitOnly): fitRstar = True
        else: break
    
    print("")
    print("finished defining host star and planet(s)")
    print("")

    ###############################################################################
    
    # first file is "lead" data set
    RVstart= 0
    tminmax  = []
    All_time = []
    All_value= []
    All_index= []
    All_error= []
    All_index= []
    ttras    = []
    
    
    RVgeneral, RVdata, boundsgpRV, boundsJRV, ReStartData = fileInput( "RV", inputData, input_dir)
    #print(RVgeneral.get('GP'))
    #stop
    if ReStartData:
        pkl_file = open(output_dir+savenamein+'RV.pkl', 'rb')
        #Stern    = pickle.load(pkl_file)
        #Planets  = pickle.load(pkl_file)
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
    
    for i,RV in enumerate(RVdata):
        if RVgeneral.get('restartGeneral', False) or RV.get('restart', False):
            DATAList.append(Data[RVstart+i])
        else:
            DATAList.append(RVTimeSeries(**RVgeneral, **RV, **boundsgpRV,
                                         t0=t0, t0corr= t0corr,
                                         color=INSTCOLORS[i]))
        tminmax.append([np.min(DATAList[i].time),np.max(DATAList[i].time)])
        All_index= np.append(All_index,np.ones(len(DATAList[i].time))*i)
        All_time = np.append(All_time, DATAList[i].time)
        All_value= np.append(All_value,DATAList[i].value)
        All_error= np.append(All_error,DATAList[i].error)
        
    for i,RV in enumerate(RVdata): DATAList[i].set_Alltimes(All_time, All_value, All_error, All_index)

    ###############################################################################
    ACstart= len(RVdata)
    ACdata, boundsgpAC, boundsJAC, ReStartData = fileInput("AC", inputData, input_dir)
    if ReStartData:
        pkl_file = open(output_dir+savenamein+'AC.pkl', 'rb')
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
    
    for i,AC in enumerate(ACdata):
        if AC.get('restart', False):
            DATAList.append(Data[ACstart+i])
        else:
            DATAList.append(ACTimeSeries(**AC, **boundsgpAC[i],  **boundsJAC[i], 
                                         t0=t0, t0corr= t0corr,
                                         color=INSTCOLORS[i]))
            
    ###############################################################################
    TRstart = ACstart + len(ACdata)
    TRdata, boundsgpTR, boundsJTR, boundsTR, ReStartData = fileInput("TR", inputData, input_dir)
    if ReStartData:
        pkl_file = open(output_dir+savenamein+'TR.pkl', 'rb')
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
        
    #print(TRdata)
    bounds.update(boundsTR)
    
    for i,Plan in enumerate(PlanetList):
        if Plan.Transit: 
            trad = Plan.get_trad()
            print(f"Transit Time and Duration: {Plan.ttra}, {trad}")
            if i==0:
                ttras.append((Plan.ttra,Plan.P, 1.*trad/Plan.P,i ))
            else:
                ttras.append((Plan.ttra,17.05, 1.*trad/Plan.P,i ))
    # SD: needs to be made a planet property, or better: calulate the transit duration from planet parameters

    print(f"transit time parameters {ttras}")

    All_timeTR  = []
    All_valueTR = []
    All_indexTR = []
    All_TT_time = []
    All_TT_Stime = []
    All_TT_Etime = []
    All_TT_indx = []
    All_TT_planet = []
    TTV = False
    count = 0
    
    noTT = True##############################################################################################################
    if noTT:
        data = np.genfromtxt(output_dir+savename+"_TransitTimes",delimiter=',',names=['Time','Indx','Plan','File'])
        All_TT_time_in  = data['Time']
        All_TT_indx_in  = data['Indx'].astype(int)
        All_TT_planet_in= data['Plan'].astype(int)
        All_TT_file_in = data['File'].astype(int)
        print("All_TT_time",All_TT_time_in)
    
    noTT = False##############################################################################################################
    for i, TR in enumerate(TRdata):
        if TR.get('restart', False):
            DATAList.append(Data[TRstart+i])
        else:
            for j,Plan in enumerate(PlanetList):
                if Plan.Transit: 
                    TR_input = TRTimeSeries(**TR, **boundsgpTR[i], **boundsJTR[i],
                                            t0= t0, t0corr= t0corr,
                                            ttras= [ttras[j]], plot= False,
                                            PlanetList= PlanetList[j], 
                                            data = 'TR', noTT=noTT, 
                                            All_TT_time = All_TT_time_in, 
                                            All_TT_planet = All_TT_planet_in,
                                            All_TT_file=All_TT_file_in)

                    #print(f"Dynamic: {i, TR_input}")
                    #print(TR_input.TransitTimes)
                    if (Plan.orbit == 'Dynamic') and (not noTT):
                        TTV = True
                        for k,nTr in enumerate(TR_input.TransitTimes):
                            TR_store = copy.copy(TR_input)
                            #print(TR_input.time,nTr,2*ttras[j][2]*ttras[j][1])
                            ind = np.where((TR_input.time > nTr - 2*ttras[j][2]*ttras[j][1]) & \
                                           (TR_input.time < nTr + 2*ttras[j][2]*ttras[j][1]) )
                            print("ind[0]",ind[0])
                            if ind[0] != [] and i==TR_input.TransitFiles[k] :
                                print("cutting out transits",TR_input.name,j,count,nTr,2*ttras[j][2]*ttras[j][1])
                                #print("ind[0]",ind[0])
                                #print(TR_input.value[ind])
                                #errfac = 1.-0.99*np.abs((TR_input.time[ind] - nTr)/(ttras[j][2]*ttras[j][1]))
                                plt.plot(TR_input.time[ind],TR_input.value[ind])
                                #mod = Plan.MandelAgol(TR_input.time[ind],TR_input.u1,TR_input.u2)
                                #plt.plot(TR_input.time[ind],mod)
                                plt.show()
                                plt.close()
                                #stop
 
                                xhi2 = []
                                ttmin = np.min(TR_input.time[ind]) - 0.2*ttras[j][2]*ttras[j][1] 
                                ttmax = np.max(TR_input.time[ind]) + 0.2*ttras[j][2]*ttras[j][1] 
                                #print(ttmin,ttmax,30./86400)
                                for tt in np.arange(ttmin,ttmax,30./86400):
                                    #print(tt)
                                    PP = Planet(Mstar=Plan.Mstar,Rstar=Plan.Rstar,K=Plan.K,m=Plan.m,P=Plan.P,\
                                                e=Plan.e,om=0.5*np.pi,tperi=tt,inc=Plan.inc,\
                                                Rp2Rs=Plan.Rp2Rs,Om=Plan.Om,a2Rs=Plan.a2Rs,
                                                orbit=Plan.orbit,Transit=Plan.Transit)
                                    if Plan.TransitOnly:
                                        mod = PP.MandelAgol(np.arange(ttmin-2,ttmax+2,30./86400),TR_input.u1,TR_input.u2)
                                    else:
                                        mod = PP.MandelAgolRV(np.arange(ttmin-2,ttmax+2,30./86400),TR_input.u1,TR_input.u2)                            
                                    mod = mod + TR_input.thirdL
                                    mod = mod/(1.+TR_input.thirdL) + 0.005   # use to compensate poor normalization
                                    #mod = mod*(1+ TR_input.thirdL) - TR_input.thirdL + 0.001
                                    #print(TR_input.thirdL,TR_input.off)
                                    #plt.plot(TR_input.time[ind],TR_input.value[ind])
                                    #plt.plot(np.arange(ttmin-2,ttmax+2,300./86400),mod)
                                    #plt.show()
                                    xhi2.append(np.sum((TR_input.value[ind]-np.interp(TR_input.time[ind],np.arange(ttmin-2,ttmax+2,30./86400),mod))**2))
                                xhi2 = np.array(xhi2)
                                ii = np.arange(np.argmin(xhi2)-70,np.argmin(xhi2)+70)
                                #print("1",TR_input.time[ind][ii])
                                #print("2",xhi2[ii])
                                plt.plot(np.arange(ttmin,ttmax,30./86400),xhi2)
                                plt.plot(np.arange(ttmin,ttmax,30./86400)[ii],xhi2[ii])
                                plt.show()
                                plt.close()
                                try:
                                    pf = np.polyfit(np.arange(ttmin,ttmax,30./86400)[ii],xhi2[ii],2)
                                    tperi_tt = -0.5*pf[1]/pf[0]
                                    #tperi_tt = 733.3527530898477#491.94#319.2607623468152#181.02
                                    print("Transit time found:",tperi_tt)
                                    '''
                                    xhi2 = []
                                    ttmin = tperi_tt-9500./86400
                                    ttmax = tperi_tt+9500./86400
                                    for tt in np.arange(ttmin,ttmax,10./86400):
                                        PP = Planet(Mstar=Plan.Mstar,Rstar=Plan.Rstar,K=Plan.K,m=Plan.m,P=Plan.P,\
                                                    e=Plan.e,om=0.5*np.pi,tperi=tt,inc=Plan.inc,\
                                                    Rp2Rs=Plan.Rp2Rs,Om=Plan.Om,a2Rs=Plan.a2Rs,
                                                    orbit=Plan.orbit,Transit=Plan.Transit)
                                        if Plan.TransitOnly:
                                            mod = PP.MandelAgol(np.arange(ttmin-2,ttmax+2,10./86400),TR_input.u1,TR_input.u2)
                                        else:
                                            mod = PP.MandelAgolRV(np.arange(ttmin-2,ttmax+2,10./86400),TR_input.u1,TR_input.u2)                            
                                        mod = mod*(1+ TR_input.thirdL) - TR_input.thirdL
                                        if tt == ttmin:
                                            plt.plot()
                                        xhi2.append(np.sum((TR_input.value[ind]-np.interp(TR_input.time[ind],np.arange(ttmin-2,ttmax+2,10./86400),mod))**2))
                                    xhi2 = np.array(xhi2)
                                    plt.plot(np.arange(ttmin,ttmax,10./86400),xhi2)
                                    plt.show()
                                    plt.close()
                                    pf = np.polyfit(np.arange(ttmin,ttmax,10./86400),xhi2,2)
                                    tperi_tt = -0.5*pf[1]/pf[0]
                                    print("refined Transit time found:",tperi_tt)
                                    '''
                                except:
                                    tperi_tt = np.max(TR_input.time[ind]-1.)
                                    print("determination of transit time failed, use dummy time")

                                PP = Planet(Mstar=Plan.Mstar,Rstar=Plan.Rstar,K=Plan.K,m=Plan.m,P=Plan.P,\
                                            e=Plan.e,om=0.5*np.pi,tperi=tperi_tt,inc=Plan.inc,\
                                            Rp2Rs=Plan.Rp2Rs,Om=Plan.Om,a2Rs=Plan.a2Rs,
                                            orbit=Plan.orbit,Transit=Plan.Transit)
                                if Plan.TransitOnly:
                                    mod = PP.MandelAgol(TR_input.time,TR_input.u1,TR_input.u2)
                                else:
                                    mod = PP.MandelAgolRV(TR_input.time,TR_input.u1,TR_input.u2) 
                                mod = mod + TR_input.thirdL
                                mod = mod/(1.+TR_input.thirdL)
                                #mod = mod*(1+ TR_input.thirdL) - TR_input.thirdL - TR_input.off

                                leastsq_model = np.zeros(len(TR_input.time))
                                if TR_input.detrend == 'Linear':
                                    leastsq_coef  = np.dot(mod-TR_input.value,np.linalg.pinv(TR_input.vector))
                                    leastsq_model = np.dot(leastsq_coef,TR_input.vector)
                                else:
                                    vecs=np.ones(len(TR_input.time))    # polynomial regression
                                    #vecs=np.append(vecs,TR_input.time)
                                    #vecs=np.append(vecs,TR_input.time*TR_input.time)
                
                                    vecs=np.asarray(vecs).reshape(1,len(TR_input.time))
 
                                    leastsq_coef  = np.dot(mod-TR_input.value,np.linalg.pinv(vecs))
                                    leastsq_model = np.dot(leastsq_coef,vecs)

                                fmed = np.median(TR_input.value[ind] + leastsq_model[ind]-mod[ind])

                                #mod = mod - leastsq_model  # leastsq needs to be provided

                                if TR_input.detrend == 'Linear':
                                    np.savetxt("Data/"+TR_input.name+"_"+str(k)+"_"+str(j)+"_detrended.dat",\
                                               list(zip(TR_input.time_in[ind],TR_input.value[ind]+leastsq_model[ind]-fmed,\
                                               TR_input.error[ind],TR_input.vector[0,ind].reshape(-1))),fmt='%15.6f')
                                else:
                                    np.savetxt("Data/"+TR_input.name+"_"+str(k)+"_"+str(j)+".dat",list(zip(TR_input.time[ind],TR_input.value[ind]+leastsq_model[ind]-fmed,TR_input.error[ind])),fmt='%15.6f')
                                f = open("Data/"+TR_input.name+"_"+str(k)+"_"+str(j)+"_TransitTime.dat","w")
                                f.write("TransitTime: {0} {1}\n".format(TR_input.name,tperi_tt))
                                f.close()
                                
                                plt.plot(TR_input.time[ind],TR_input.value[ind] + leastsq_model[ind]-fmed)

                                if Plan.TransitOnly:
                                    mod = PP.MandelAgol(np.arange(ttmin,ttmax,3./86400),TR_input.u1,TR_input.u2)
                                else:
                                    mod = PP.MandelAgolRV(np.arange(ttmin,ttmax,3./86400),TR_input.u1,TR_input.u2) 
                                mod = mod + TR_input.thirdL
                                mod = mod/(1.+TR_input.thirdL)

                                plt.plot(np.arange(ttmin,ttmax,3./86400),mod)    
                                plt.show()
                                plt.close()

                                #plt.plot(TR_input.time[ind],TR_input.error[ind]*errfac)
                                #plt.show()
                                #plt.close()
                                #plt.plot(TR_input.time[ind],errfac)
                                #plt.show()
                                #plt.close()
                                if TR_input.vector != np.array([]): TR_store.set_Vecs(TR_input.vector[:,ind[0]])
                                TR_store.set_Times(TR_input.time[ind],TR_input.value[ind],TR_input.error[ind])#*errfac)
                                TR_store.set_TransitTimes(nTr,j)
                                DATAList.append(TR_store)
                                #print(DATAList[-1].Planet)
                                DATAList[-1].ParFileNr = i
                                #All_index= np.append(All_index,np.ones(len(ind[0])*(TRstart+k+\
                                #                     j*len(TR_input.TransitTimes)+i*len(PlanetList))))                
                                All_TT_time.append(nTr)
                                All_TT_Stime.append(np.min(TR_input.time[ind]))
                                All_TT_Etime.append(np.max(TR_input.time[ind]))
                                All_TT_indx.append(count)
                                All_TT_planet.append(j)
                                count += 1
                                #plt.plot(DATAList[-1].time,DATAList[-1].value-0.01)
                                #plt.plot(TR_store.time,TR_store.value)
                                #plt.show()
                                All_indexTR += [i+TRstart+j*len(TR_input.TransitTimes)+k]*len(ind[0])
                                All_timeTR  += list(TR_store.time)
                                All_valueTR += list(TR_store.value)
                    else:
                        DATAList.append(TR_input)
                        #All_index= np.append(All_index,np.ones(len(DATAList[TRstart+i].time))*(i+TRstart))
                        DATAList[-1].ParFileNr = i
                        #DATAList.append(TR_input)
                        #DATAList[-1].ParFileNr = i
    stop
    ###############################################################################
    # provide the information that only transit light curves are fitted
    TransitOnly = False
    if (RVdata == []) & (not TTV): TransitOnly = True
    for Plan in PlanetList:
        Plan.set_TransitOnly(TransitOnly)
    
    print("")
    print("")
    print("finished reading data files")
    print("")

    ###############################################################################
    # define model
    
    #print("All-TT",All_TT_time)
    inds = np.argsort(All_time)
    if inds != []: 
        All_time  = All_time[inds]
        All_index = All_index[inds]
    noTT = True##############################################################################################################
    #if noTT:
    #    data = np.genfromtxt(output_dir+savename+"_TransitTimes",delimiter=',',names=['Time','Indx','Plan'])
    #    All_TT_time  = data['Time']
    #    All_TT_indx  = data['Indx'].astype(int)
    #    All_TT_planet= data['Plan'].astype(int)
    #    #print(All_TT_planet)
    inds = np.argsort(All_TT_time)
    if inds != []:
        All_TT_time  = np.array(All_TT_time)[inds]
        #All_TT_Stime = np.array(All_TT_Stime)[inds]
        #All_TT_Etime = np.array(All_TT_Etime)[inds]
        All_TT_indx  = np.array(All_TT_indx)[inds]
        All_TT_planet= np.array(All_TT_planet)[inds]
        if not noTT: np.savetxt(output_dir+savename+"_TransitTimes",list(zip(All_TT_time,All_TT_indx,All_TT_planet)),fmt='%15.6f,%4i,%4i')
        for j,Plan in enumerate(PlanetList):
            ind = np.where(All_TT_planet == j)
            TranNr = np.round((All_TT_time[ind]-(All_TT_time[ind[0][0]])) / Plan.P)
            polcoef = np.polyfit(TranNr,All_TT_time[ind],1)
            polcoef[0] = Plan.P
            polynom  = np.poly1d(polcoef)
            print("ephemeris: {0}, {1}".format(polynom[0],polynom[1]))
            pp = PdfPages(output_dir+savename+"_palnet"+str(j)+"_TTV.pdf")
            fig = plt.figure()
            plt.plot(TranNr,All_TT_time[ind]-polynom(TranNr),'o')
            plt.xlabel("Transit Number")
            plt.ylabel("O-C [d]")
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()                
            plt.close()
            pp.close()
             
       
    #print("All_TT_indx",All_TT_indx)  
    #stop
    mod  = ModelingClass(PlanetList,DATAList,All_TT_time,All_TT_Stime,All_TT_Etime,All_TT_indx,All_TT_planet,\
                        All_time,All_index,\
                        All_timeTR,All_indexTR,TTV,\
                        bounds,boundsgpRV=boundsgpRV,boundsgpAC=boundsgpAC,\
                        boundsgpTR=boundsgpTR,boundsJRV=boundsJRV,\
                        boundsJAC=boundsJAC,boundsJTR=boundsJTR,\
                        lin=0.0)
    
    # set time arrays for model output and plot input model
    try: 
        timesRV = np.linspace(np.min(tminmax)- 0., np.max(tminmax)+10, numt)
    except:
        timesRV = np.linspace(0,1)
    try:
        timesTR = np.linspace(Plan.ttra-Plan.trad*Plan.P,Plan.ttra+Plan.trad*Plan.P, numt)
    except:
        timesTR = np.linspace(0,1)

    if showInput: mod.plot_input(savename,output_dir,RVstart,ACstart,TRstart,t0corr=t0corr)
    
    print("#######################")
    #print(mod.gpAll.get_parameter_vector())
    #print(mod.gpAll.get_parameter_names())
    #stop
    #sys.exit()
    
    #print("M,R plot_input",Mstar,Rstar,STAR.Rstar,STAR.RstarE)
    STARResult  = Star(Mstar=STAR.Mstar,Rstar=STAR.Rstar,MstarE=STAR.MstarE,RstarE=STAR.RstarE)
    #print("Rstar",STAR.Rstar)
    PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),STAR.Rstar)

    #print("M,R plot input",STARResult.Mstar,STARResult.Rstar,STARResult.RstarE,PlanetListResult[0].Rstar)

    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' Start',minPer)
    
    ###############################################################################
    initial_params = mod.gpAll.get_parameter_vector()
    bounds         = mod.gpAll.get_parameter_bounds()
    
    # initialize all parameters
    x = DATAList[0].time
    y = DATAList[0].value
    e = DATAList[0].error
    for i in range(1,len(DATAList)):
        x = np.concatenate((x,DATAList[i].time),axis=None)
        y = np.concatenate((y,DATAList[i].value),axis=None)
        e = np.concatenate((e,DATAList[i].error),axis=None)
        
    inds = np.argsort(x)
    x    = x[inds]
    y    = y[inds]
    e    = e[inds]
    
    mod.gpAll.compute(x, e)
    
    print("##############################")
    print(mod.gpAll.get_parameter_dict())
    
    if checkStart:
        sys.exit() 
    
    if start_from_sample:
        reader           = emcee.backends.HDFBackend(output_dir+savenamein+'.h5')
        p_sample         = reader.get_last_sample()
        samples          = reader.get_chain(flat=True)
        #print(samples[mod.Soff+0,:])
        #samples[:,mod.Soff+1] = np.abs(samples[:,mod.Soff+1])
        #samples[:,mod.Soff+2] = np.abs(samples[:,mod.Soff+2])
        #samples[:,mod.Soff+3] = np.abs(samples[:,mod.Soff+3])
        #samples[:,mod.Soff+4] = np.abs(samples[:,mod.Soff+4])
        log_prob_samples = reader.get_log_prob(flat=True)
        best             = samples[np.argmax(log_prob_samples)]
        ndim             = len(best)
        DATAList         = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
        print('######################################################################')
        print("Best fit from sample: {0}".format(best))
        print("ln L RV only:",mod.log_probability_RV(best))
        print("ln L TR only:",mod.log_probability_TR(best))
        print('######################################################################')
        ###############################################################################
        # save and show results

        if fitRstar: Rstar = best[mod.Soff]
        PlanetListResult = mod.SaveThePlanet(best,Rstar)
        #BIC_1 = mod.plot_result(timesRV,timesTR,savename,output_dir,RVstart,ACstart,TRstart,mod,'Start',minPer)

    else:
        print("")
        print("... start minimzation...")
        startTime = time.clock()
        soln = minimize_parallel(mod.neg_log_like, initial_params, 
                        bounds=bounds, args=(y,mod.gpAll),\
                        options={'ftol': 2.220446049250313e-09, 'gtol': 1e-08, \
                                 'eps': 1e-08, 'maxfun': 25000, 'maxiter': 800})
        minimizeParallelTime = time.clock() - startTime
        # print(soln2)
        print(f"Time minimizeParallel: {minimizeParallelTime}")
        #soln = minimize(mod.neg_log_like, initial_params, 
        #                method="L-BFGS-B", bounds=bounds, args=(y,mod.gpAll),\
        #                #method="Nelder-Mead", args=(y,mod.gpAll),\
        #                options={'ftol': 2.220446049250313e-09, 'gtol': 1e-08, \
        #                         'eps': 1e-08, 'maxfun': 25000, 'maxiter': 800})
        
        
        print("... end minimzation...")
        print(mod.gpAll.get_parameter_dict())
      
        ftol  = 2.220446049250313e-09
        tmp_i = np.zeros(len(soln.x))
        uncertainty_i = np.zeros(len(soln.x))
        for i in range(len(soln.x)):
            tmp_i[i] = 1.0
            uncertainty_i[i] = np.sqrt(max(1, abs(soln.fun))*ftol*soln.hess_inv(tmp_i)[i])
            tmp_i[i] = 0.0
            print('{0:12.4e} ± {1:.1e}'.format(soln.x[i], uncertainty_i[i]))
    
        DATAList = mod.SaveTheDatafiles(soln.x,RVstart,ACstart,TRstart)    
        print('######################################################################')
        print("Result:",soln)
        print("Final log-likelihood optimization: {0}".format(soln.fun))
        print("ln L RV only:",mod.log_probability_RV(soln.x))
        print("ln L TR only:",mod.log_probability_TR(soln.x))
        print('######################################################################')
        f = open(output_dir+savename+'output.txt','a')
        f.write("Final log-likelihood optimization: {0}\n".format(soln.fun))
        f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(soln.x)))
        f.write("ln L TR only: {0}\n".format(mod.log_probability_TR(soln.x)))
        f.close()
        ###############################################################################
        # save and show results
        #print("fitRstar",fitRstar,Rstar,STAR.Rstar,soln.x[mod.Soff])
        if fitRstar:
            Rstar = soln.x[mod.Soff]
            #print(fitRstar,Rstar)
        else:
            Rstar = STAR.Rstar
            #print(Rstar)
        if fitRstar: Rstar = soln.x[mod.Soff]
        PlanetListResult = mod.SaveThePlanet(soln.x,Rstar)
        BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' LM',minPer)
        print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(soln.x))
        #stop
        ###############################################################################
        
        initial = np.array(soln.x)
        #initial = np.array(initial_params)
        ndim    = len(initial)
    

        while (len(p0) != numWalkers):
            params = (initial + ufact * uncertainty_i * np.random.randn(1, ndim)).reshape(-1)
            #print(params.reshape(-1))
            mod.gpAll.set_parameter_vector(params)
            lnL = mod.gpAll.log_prior()
            if np.isfinite(lnL):
                p0.append(params)
            count += 1
            if count > initlim:
                print("WARNING: only {0} of {1} initial parameter sets found: reduce parameter ufact OR increase initlim in config.py and rerun".format(len(p0),numWalkers))
                stop

        print("{0} initializations needed to create start distribution\n".format(count))
        p0 =  np.array(p0).reshape(numWalkers,ndim) 
        
    print("")
    print('######################################################################')
    print("... start MCMC...")
    
    strt = time.time()
    if start_from_sample:
        shutil.copy(output_dir+savenamein+'.h5',output_dir+savename+'burnin.h5')
    
    backend = emcee.backends.HDFBackend(output_dir+savename+'burnin.h5')
    if not start_from_sample: 
        print("resetting sample")
        backend.reset(numWalkers, ndim)
    print(f"Running burn-in with {ndim} dimensions")
    
    startPars = mod.gpAll.get_parameter_dict().items()
    
    samplers = []
    if numThread == 1:
        for samp in range(10):
            sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                      backend=backend) ## or fraction of CPUs??(cpu_count()//4)),
            start = time.time()
            if start_from_sample: 
                p0,lp,state = sampler.run_mcmc(None, numBurn/10, progress=True)
            else:
                p0,lp,state = sampler.run_mcmc(p0, numBurn/10, progress=True)
            end = time.time()
            multi_time = end - start

            print("Multiprocessing took {0:.1f} seconds".format(multi_time))
            print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))

            if plotBurnIn or showAll:
                s = copy.copy(sampler)
                samplers.append(s)
            
    else:
        for samp in range(10):
            with Pool() as pool:
                sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                          pool=mp.Pool(processes=numThread),backend=backend) ## or fraction of CPUs??(cpu_count()//4)),
                start = time.time()
                if start_from_sample: 
                    p0,lp,state = sampler.run_mcmc(None, numBurn/10, progress=True)
                else:
                    p0,lp,state = sampler.run_mcmc(p0, numBurn/10, progress=True)
                end = time.time()
                multi_time = end - start
                                
                print("Multiprocessing took {0:.1f} seconds".format(multi_time))    
                print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))

                if plotBurnIn or showAll:
                    s = copy.copy(sampler)
                    samplers.append(s)
                    
    print('')
    print('######################################################################')
    print('')
    
    if plotBurnIn or showAll:
        for par, (key, startVal) in enumerate(startPars):
            parameter = key.split(':')[1]
            print(f'Plotting {nPlotWalker} walkers of: {parameter}  ({par+1} of {len(startPars)})')
            
            for s in samplers:
                numWalker = len(s.chain)
                walkerChoice = np.arange(0, numWalker, int(np.ceil(numWalker/nPlotWalker)), dtype = int)
                for walker in s.chain[walkerChoice]:
                    steps = len(walker)
                    plt.plot(walker[:,par], '-', color='k', alpha = walkerOpacity)
            
            plt.axhline(y=startVal, color='r', linestyle='-')
            plt.title('BurnIn walker trace')
            plt.xlabel('step number')
            plt.ylabel(parameter)
            plt.show()
        
        print('Plotting all walkers completed.')
        
    
    print("Multiprocessing took {0:.1f} seconds".format(multi_time))
    print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
    print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))    
    
    samples     = sampler.flatchain
    best        = samples[np.argmax(sampler.flatlnprobability)]
    print("run time:",time.time()-strt)
    print("First Burn in log-likelihood: {0}".format(-np.max(sampler.flatlnprobability)))
    print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(sampler.flatlnprobability)]))
    print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(sampler.flatlnprobability)]))
    print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
    print("parameters{0}".format(best))
    f = open(output_dir+savename+'output.txt','a')
    f.write("run time: {0}\n".format(time.time()-strt))
    f.write("Final log-likelihood: {0}\n".format(-np.max(sampler.flatlnprobability)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(samples[np.argmax(sampler.flatlnprobability)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(sampler.flatlnprobability)])))
    f.write("Mean acceptance fraction:{0:.3f}\n".format(np.mean(sampler.acceptance_fraction)))
    f.close()
    ################################################################################
    # save results 
    DATAList = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
    if fitRstar:
        Rstar = best[mod.Soff]
        #print(fitRstar,Rstar)
    else:
        Rstar = STAR.Rstar
        #print(Rstar)
    #print("M,R burnin",Mstar,Rstar)
    for a in PlanetList: print(a.Rstar)
    print('*******************************************')
    STARResult  = Star(Mstar=Mstar,Rstar=Rstar,MstarE=MstarE,RstarE=RstarE)
    PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),Rstar)
    print('*******************************************')
    print(f'Rstar for STARResult: {Rstar}')
    print(f'RstarE for STARResult: {RstarE}')
    print(f'Mstar for STARResult: {Mstar}')
    print(f'MstarE for STARResult: {MstarE}')
    print(f'Rstar for PLANETResult:')
    for a in PlanetListResult: print(a.Rstar)
    print(f'Mstar for PLANETResult:')
    for a in PlanetListResult: print(a.Mstar)
    print('*******************************************')

    #print("M,R burnin",STARResult.Mstar,STARResult.Rstar)
    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    pkl_file = open(output_dir+savename+'System.pkl', 'wb')
    pickle.dump(STARResult,pkl_file)
    pickle.dump(PlanetListResult,pkl_file,-1)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'RV.pkl', 'wb')
    pickle.dump(DATAList[RVstart:RVstart+len(RVdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'AC.pkl', 'wb')
    pickle.dump(DATAList[ACstart:ACstart+len(ACdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'TR.pkl', 'wb')
    pickle.dump(DATAList[TRstart:TRstart+len(TRdata)],pkl_file)
    pkl_file.close()
        
                  
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' BurnIn',minPer)
    print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(best))
    
    if start_from_sample:
        shutil.copy(output_dir+savename+'burnin.h5',output_dir+savename+'.h5')

    backend = emcee.backends.HDFBackend(output_dir+savename+'.h5')
    
    if not start_from_sample: backend.reset(numWalkers, ndim)  # reset sampler?
    
    
    startPars = mod.gpAll.get_parameter_dict().items()
    
    print("Running MCMC...")
    samplers = []
    if numThread == 1:
        for samp in range(2):
            sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                                            backend=backend)
            start = time.time()
            p0,lp,state = sampler.run_mcmc(p0, numMCMC/2, progress=True)
            end = time.time()
            multi_time = end - start
            print("Multiprocessing took {0:.1f} seconds".format(multi_time))
            print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            try:
                reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
                tau = reader.get_autocorr_time()
            except:
                tau = samp*numMCMC/2
            #print(tau)
            if np.mean(tau)*50<i*samp*numMCMC/2: break
    else:
        for samp in range(2):
            with Pool() as pool:
                sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                          pool=mp.Pool(processes=numThread),
                          backend=backend)
                start = time.time()
                p0,lp,state = sampler.run_mcmc(p0, numMCMC/2, progress=True)
                end = time.time()
                multi_time = end - start
                print("Multiprocessing took {0:.1f} seconds".format(multi_time))
                print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                try:
                    reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
                    tau = reader.get_autocorr_time()
                except:
                    tau = samp*numMCMC/2
                #print(tau)
                if np.mean(tau)*50<i*samp*numMCMC/2: break  
                                
                if plotMCMC or showAll:
                    
                    s = copy.copy(sampler)
                    samplers.append(s)
                    
    if plotMCMC or showAll:
        for par, (key, startVal) in enumerate(startPars):
            parameter = key.split(':')[1]
                        
            print(f'Plotting {nPlotWalker} walkers of: {parameter}  ({par+1} of {len(startPars)})')
            
            for s in samplers:
                numWalker = len(s.chain)
                walkerChoice = np.arange(0, numWalker, int(np.ceil(numWalker/nPlotWalker)), dtype = int)
                for walker in s.chain[walkerChoice]:
                    steps = len(walker)
                    plt.plot(walker[:,par], '-', color='k', alpha=walkerOpacity)
            
            plt.axhline(y=startVal, color='r', linestyle='-')
            plt.title('MCMC walker trace')
            plt.xlabel('step number')
            plt.ylabel(parameter)
            plt.show()
        print('Plotting all walkers completed.')
         
    
    try:
        reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
        tau    = reader.get_autocorr_time()
        burnin = int(2 * np.max(tau))
        thin   = int(0.5 * np.min(tau))
        
        samples = reader.get_chain(flat=True)   #, discard=burnin, thin=thin)
        #print(flat_samples.shape)
        flatlnprob = reader.get_log_prob(flat=True) #, discard=burnin, thin=thin)
    except:
        samples    = sampler.flatchain
        flatlnprob = sampler.flatlnprobability
    
    print("Final log-likelihood: {0}".format(-np.max(flatlnprob)))
    print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(flatlnprob)]))
    print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(flatlnprob)]))
    print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
    f = open(output_dir+savename+'output.txt','a')
    f.write("run time: {0}\n".format(time.time()-strt))
    f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(samples[np.argmax(flatlnprob)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(flatlnprob)])))
    f.write("Mean acceptance fraction:{0:.3f}\n".format(np.mean(sampler.acceptance_fraction)))

    
    strt = time.time()
    #print("Running final LM fit...")
    #soln = minimize(mod.neg_log_like, samples[np.argmax(sampler.flatlnprobability)], 
    #                method="L-BFGS-B", bounds=bounds, args=(y,mod.gpAll))
    best = samples[np.argmax(sampler.flatlnprobability)]
    print('######################################################################')
    print("Result:",best)
    print("Final log-likelihood optimization: {0}".format(mod.log_probability(best)))
    print("ln L RV only:",mod.log_probability_RV(best))
    print("ln L TR only:",mod.log_probability_TR(best))
    print('######################################################################')
    print("Best fit parameter_vector:\n{0}\n".format(best))
    print('######################################################################')
    f.write('######################################################################\n')
    f.write("Result: {0}\n".format(best))
    f.write("Final log-likelihood optimization: {0}\n".format(mod.log_probability(best)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(best)))
    f.write("ln L TR only: {0}\n".format(mod.log_probability_TR(best)))
    f.write('######################################################################\n')
    f.write("Best fit parameter_vector:\n{0}\n".format(best))
    f.write('######################################################################\n')
    
    #######################################################################
    # save and show results
    
    if fitRstar: 
        Rstar = best[mod.Soff]
        #print(fitRstar,Rstar)
    else:
        Rstar = STAR.Rstar
        #print(Rstar)

    STARResult  = Star(Mstar=Mstar,Rstar=Rstar,MstarE=MstarE,RstarE=RstarE)
    PlanetListResult = mod.SaveThePlanet(best,Rstar)
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,'',minPer)
    print('*******************************************')
    print(f'Rstar for STARResult: {Rstar}')
    print(f'RstarE for STARResult: {RstarE}')
    print(f'Mstar for STARResult: {Mstar}')
    print(f'MstarE for STARResult: {MstarE}')
    print(f'Rstar for PLANETResult:')
    for a in PlanetListResult: print(a.Rstar)
    print(f'Mstar for PLANETResult:')
    for a in PlanetListResult: print(a.Mstar)
    print('*******************************************')

    print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(best))
    f.write("BIC RV only: {0}\n".format(BIC_1-2.*mod.log_probability_RV(best))) 
    
    ######################################################################
    # calculate derived quantities
    
    truth,xx = mod.derived_quantities(samples,best,STAR,mod,RVstart,ACstart,TRstart,ndim)
        
    ################################################################################
    # save results 
    
    DATAList = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
    
    #print("M,R final",STARResult.Mstar,STARResult.Rstar)
    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    pkl_file = open(output_dir+savename+'System.pkl', 'wb')
    pickle.dump(STARResult,pkl_file)
    pickle.dump(PlanetListResult,pkl_file,-1)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'RV.pkl', 'wb')
    pickle.dump(DATAList[RVstart:RVstart+len(RVdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'AC.pkl', 'wb')
    pickle.dump(DATAList[ACstart:ACstart+len(ACdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'TR.pkl', 'wb')
    pickle.dump(DATAList[TRstart:TRstart+len(TRdata)],pkl_file)
    pkl_file.close()
                  
    ################################################################################
    # make tick font size smaller for corner plots    
    plt.rc('xtick',labelsize=13)
    plt.rc('ytick',labelsize=13)
    
    print("best fit parameter_vector:\n{0}\n".format(truth))
    print('######################################################################')
    
    npTR = 0
    nADD = 0
    add_name = []
    for Plan in PlanetList:
        nADD += np.sum(Plan.varadd)
        add_name += list(add_names[np.where(np.array(Plan.varadd) == 1)[0]])
        #print(np.where(Plan.varadd == 1)[0])
        #print(add_name)

        ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL)) ).reshape(-1)
    for jj in range(ndim):
        ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        v=np.percentile(xx[ii[ifinite]][:,jj],[16,50,84])
        tmp_name = mod.gpAll.get_parameter_names()[jj].split(':')[1]                 #Todo
        print(tmp_name, v[1],'^',v[2]-v[1],'_',v[1]-v[0])
        f.write("{0} {1} {2} {3} {4} {5} {6}\n".format(tmp_name, v[1],'& +',v[2]-v[1],'& -',v[1]-v[0], truth[jj]))
        
    for jj in range(ndim,ndim+nADD):
        #print(jj,jj-ndim)
        ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        v=np.percentile(xx[ii[ifinite]][:,jj],[16,50,84])
        print(add_name[jj-ndim], v[1],'^',v[2]-v[1],'_',v[1]-v[0])
        f.write("{0} {1} {2} {3} {4} {5} {6}\n".format(add_name[jj-ndim], v[1],'& +',v[2]-v[1],'& -',v[1]-v[0], truth[jj]))

    f.write("Final log-likelihood: {0}\n".format(-np.max(sampler.flatlnprobability)))
    f.write("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    f.write("ln L RV only:{0}\n".format(mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    f.close()



    offA = ndim
    POff = 0
    for k,Plan in enumerate(PlanetList):
    # still to be modeified
    #    if k==0: 
    #        xx[:][:,Poff+k*Ppar+4] = (xx[:][:,Poff+k*Ppar+4] - 5) % xx[:][:,Poff+k*Ppar+1] + 5 #- np.median(xx[:][:,Poff+k*Ppar+1])
    #        truth[Poff+k*Ppar+4] = (truth[Poff+k*Ppar+4] - 5) % truth[Poff+k*Ppar+1] + 5 #- truth[Poff+k*Ppar+1]
    #    if k==0: 
    #        xx[:][:,Poff+k*Ppar+3] = (xx[:][:,Poff+k*Ppar+3] - 220.) % 360. + 220. 
    #        truth[Poff+k*Ppar+3] = (truth[Poff+k*Ppar+3] - 220.) % 360. + 220.
    
        
        #ind = np.arange(0)
        ind = np.arange(mod.Poff+POff,mod.Poff+POff+Plan.Ppar)
        #print(Plan.varadd)
        #print(Plan.varind)
        nAdd = np.sum(Plan.varadd)
        #if Plan.orbit == 'Circular':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: nAdd += 0
        #        else:                nAdd += 6
        #    else:                    nAdd += 3
        #if Plan.orbit == 'Kepler':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: nAdd += 0
        #        else:                nAdd += 6
        #    else:                    nAdd += 3
        #if Plan.orbit == 'Dynamic':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: nAdd += 0
        #        else:                nAdd += 6
        #    else:                    nAdd += 3

        #if Plan.Transit: 
        #    if Plan.TransitOnly: nAdd += 0
        #    else:                nAdd += 6
        #else:                    nAdd += 3
        #if (not Plan.TransitOnly) ^ (Plan.orbit == 'Dynamic'): nAdd += 3
        #if (not Plan.TransitOnly) & Plan.Transit:              nAdd += 3
        #ind = np.append(ind,np.arange(offA,offA+nAdd))
        
        #print(ind,offA,nAdd,mod.Poff,Plan.Ppar,Plan.orbit,Plan.Transit,Plan.TransitOnly)    
        
        #if Plan.orbit == 'Circular':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: indx = [2,5,6,7,9]
        #        else:                indx = [0,2,5,6,7,11,12,13,14,15,16]
        #        if Plan.fixInc or Plan.Coplanar: indx.remove(7)
        #    else:                    indx = [0,2,5,11,12,13]
        #if Plan.orbit == 'Kepler':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: indx = [2,3,4,5,6,7,9]
        #        else:                indx = [0,2,3,4,5,6,7,11,12,13,14,15,16]
        #        if Plan.fixInc or Plan.Coplanar: indx.remove(7)
        #    else:                    indx = [0,2,3,4,5,11,12,13]
        #if Plan.orbit == 'Dynamic':
        #   if Plan.Transit:
        #       if Plan.TransitOnly: indx = [2,3,4,5,6,7,8,9]
        #       else:                indx = [1,2,3,4,5,6,7,8,10,12,13,14,15,16]
        #   else:                    indx = [1,2,3,4,5,7,8,10,12,13]
        #   if Plan.fixInc or Plan.Coplanar: indx.remove(7)
        #   if Plan.fixOm:  indx.remove(8)
        ind = np.append(ind,np.arange(offA,offA+nAdd))
        #print(Plan.varind)
        #print(np.where(np.array(Plan.varadd==1)))
        #print(type(Plan.varind),type(Plan.varadd))

        itest = np.where(np.array(Plan.varadd==1))
        #print(itest[0]+len(Plan.varfix))

        indx= list(Plan.varind)+list(itest[0]+len(Plan.varfix))

        newnames = CN_P
        newnames = np.append(newnames,CN_P_A)
        newnames = newnames[indx]
        
        #print(ind,indx,newnames)
            
        if 4 in indx:
            ii = indx.index(4)
            nn,bb, = np.histogram(xx[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                xx[:,ind[ii]]  = (xx[:,ind[ii]]  + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
        if 8 in indx:
            ii = indx.index(8)
            nn,bb, = np.histogram(xx[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                xx[:,ind[ii]]  = (xx[:,ind[ii]]  + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
        if 5 in indx:
            ii   = indx.index(5)
            iPer = indx.index(2)
            nn,bb, = np.histogram(xx[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                Pbest = np.median(xx[:,ind[iPer]])
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                xx[:,ind[ii]]  = (xx[:,ind[ii]]  + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
        if 13 in indx:
            # for time of conjugation instead of mean longtiude
            ii = indx.index(13)
            nn, bb, = np.histogram(xx[:, ind[ii]], bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                Pbest = np.median(xx[:,ind[iPer]])
                mid = 0.5 * (bb[np.argmin(nn) + 1] - bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                xx[:,ind[ii]]  = (xx[:,ind[ii]]  + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                #xx[:, ind[ii]] = (xx[:, ind[ii]] + mid * np.sign(mid - 180)) % 360 - mid * np.sign(mid - 180)
                #truth[ind[ii]] = (truth[ind[ii]] + mid * np.sign(mid - 180)) % 360 - mid * np.sign(mid - 180)

        plotrange= [1.]*len(newnames)
    
        #for iind in ind:
        #    print(truth[iind],np.min(xx[ii][:,iind]),np.max(xx[ii][:,iind]))
    
        # reject bad samples
        ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL) )).reshape(-1) 
        #ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        #print(np.shape(ifinite),np.shape(ii[ifinite]))
        pp = PdfPages(output_dir+savename+"corner_planet"+str(k)+".pdf")
        corner.corner(xx[ii[ifinite]][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                  labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
                  show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
        if noShow:
            plt.ioff()
            pp.savefig(plt.gcf()) # This generates pdf page
            pp.close()
            plt.close()
        else:
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
        
        
        npTR = 0
        if Plan.Transit & (k==0) & (mod.Spar != 0): 
            newnames = []
            ind      = np.arange(0)
            ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
            if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
            for DATA in DATAList[TRstart:]:
                #print(DATA)
                if npTR == DATA.ParFileNr:
                    if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
                    if not DATA.fix3: newnames = np.append(newnames,CN_3L)
                    npTR += 1
            plotrange= [1.]*len(newnames)
        
            # reject bad samples
            ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL) )).reshape(-1) 
            #ifinite = np.where(np.isfinite(xx[ii[ifinite]][:,jj]))
            pp = PdfPages(output_dir+savename+"corner_STAR.pdf")
            corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),quantiles=[0.16,0.5,0.84],
                          show_titles=True,title_fmt='.3f',label_kwargs={"fontsize":12},range=(np.asarray(plotrange)))
            if noShow:
                plt.ioff()
                pp.savefig(plt.gcf()) # This generates pdf page
                pp.close()
                plt.close()
            else:
                pp.savefig(plt.gcf()) # This generates pdf page
                plt.show()
                pp.close()
        
        #if Plan.Transit: 
        #    if Plan.TransitOnly: nAdd += 0
        #    else:                nAdd += 6
        #else:                    nAdd += 3
        offA += nAdd
        POff += Plan.Ppar
    
    offA = ndim
    POff = 0
    for k,Plan in enumerate(PlanetList):
    # still to be modeified
    #    if k==0: 
    #        xx[:][:,Poff+k*Ppar+4] = (xx[:][:,Poff+k*Ppar+4] - 5) % xx[:][:,Poff+k*Ppar+1] + 5 #- np.median(xx[:][:,Poff+k*Ppar+1])
    #        truth[Poff+k*Ppar+4] = (truth[Poff+k*Ppar+4] - 5) % truth[Poff+k*Ppar+1] + 5 #- truth[Poff+k*Ppar+1]
    #    if k==0: 
    #        xx[:][:,Poff+k*Ppar+3] = (xx[:][:,Poff+k*Ppar+3] - 220.) % 360. + 220. 
    #        truth[Poff+k*Ppar+3] = (truth[Poff+k*Ppar+3] - 220.) % 360. + 220.
    
        
        #ind = np.arange(0)
        #ind = np.append(ind,np.arange(mod.Poff+POff,mod.Poff+POff+Plan.Ppar))
        #nAdd = 0
        ind = np.arange(mod.Poff + POff, mod.Poff + POff + Plan.Ppar)
        indx = list(Plan.varind)

        #if Plan.orbit == 'Circular':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: index = [2,5,6,7,9]
        #        else:                index = [0,2,5,6,7]
        #        if Plan.fixInc: index.remove(7)
        #    else:                    index = [0,2,5]
        #if Plan.orbit == 'Kepler':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: index = [2,3,4,5,6,7,9]
        #        else:                index = [0,2,3,4,5,6,7]
        #        if Plan.fixInc: index.remove(7)
        #    else:                    index = [0,2,3,4,5]
        #if Plan.orbit == 'Dynamic':
        #    if Plan.Transit:
        #        if Plan.TransitOnly: index = [2,3,4,5,6,7,8,9]
        #        else:                index = [1,2,3,4,5,6,7,8]
        #    else:                    index = [1,2,3,4,5,7,8]
        #    if Plan.fixInc or Plan.Coplanar: index.remove(7)
        #    if Plan.fixOm:  index.remove(8)
                
        newnames = []
        newnames = np.append(newnames,CN_P)
        newnames = newnames[indx]
        
        if Plan.Transit & (k==0): 
            npTR = 0
            ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
            if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
            for DATA in DATAList[TRstart:]:
                if npTR == DATA.ParFileNr:
                    if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
                    if not DATA.fix3: newnames = np.append(newnames,CN_3L)
                    npTR += 1
            plotrange= [1.]*len(newnames)
        
        plotrange= [1.]*len(newnames)
    
        # reject bad samples
        ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL) )).reshape(-1) 
        pp = PdfPages(output_dir+savename+"corner_raw_planet"+str(k)+".pdf")
        corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                  labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
                  show_titles=True,title_fmt='.3f')
        if noShow:
            plt.ioff()
            pp.savefig(plt.gcf()) # This generates pdf page
            pp.close()
            plt.close()
        else:
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
            
        offA += nAdd
        POff += Plan.Ppar
    
    newnames = []
    ind      = np.arange(0)
    for i,DATA in enumerate(DATAList):
        if isinstance(DATA,RVTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffRV+i,mod.JoffRV+i+1))
        if isinstance(DATA,ACTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffAC[i-ACstart],mod.JoffAC[i-ACstart]+1))
        if isinstance(DATA,TRTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffTR[i-TRstart],mod.JoffTR[i-TRstart]+1))
    ind = np.append(ind,np.arange(mod.OoffRV,mod.OoffRV+mod.nfRV))
    newnames = np.append(newnames,CN_Off*mod.nfRV)
    
    ind = np.append(ind,np.arange(mod.OoffAC,mod.OoffAC+mod.nfAC))
    newnames = np.append(newnames,CN_Off*mod.nfAC)
    
    ind = np.append(ind,np.arange(mod.OoffTR,mod.OoffTR+npTR))
    newnames = np.append(newnames,CN_Off*npTR)
    
    if mod.ndeg >= 1:
        ind = np.append(ind,[mod.linpos])
        newnames = np.append(newnames,CN_pol)
    if mod.ndeg == 2:
        ind = np.append(ind,[mod.quadpos])
        newnames = np.append(newnames,CN_pol)
    
    plotrange= [1.]*len(newnames)
    
    # reject bad samples
    ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL) )).reshape(-1)
    pp = PdfPages(output_dir+savename+"corner_Jitter.pdf")
    corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                  labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
                  show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
    if noShow:
        plt.ioff()
        pp.savefig(plt.gcf()) # This generates pdf page
        pp.close()
        plt.close()
    else:
        pp.savefig(plt.gcf()) # This generates pdf page
        plt.show()
        pp.close()
    
    
    newnames = []
    ind      = np.arange(0)
    for i,DATA in enumerate(DATAList):
        if i == RVstart:
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'SHO'): 
                newnames = np.append(newnames,CN_RV_GPm_SHO)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'REAL'):
                newnames = np.append(newnames,CN_RV_GPm_REAL)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
        if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'SHO'):
            newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfAC)
            ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
        if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'RV Driver'):
            newnames = np.append(newnames,CN_AC_GPm_RVD*mod.nfAC)
            ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
        if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'SHO'):
            newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfTR)
            ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
        if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'REAL'):
            newnames = np.append(newnames,CN_RV_GPm_REAL*mod.nfTR)
            ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
      
    plotrange= [1.]*len(newnames)
    
    if newnames != []:
        # reject bad samples
        ii = np.array(np.where(sampler.flatlnprobability > np.percentile(sampler.flatlnprobability, percLnL) )).reshape(-1)
        pp = PdfPages(output_dir+savename+"corner_GP.pdf")
        corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                      labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
                      show_titles=True,title_fmt='.3f')#,range=(np.asarray(plotrange)))
        if noShow:
            plt.ioff()
            pp.savefig(plt.gcf()) # This generates pdf page
            pp.close()
            plt.close()
        else:
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
        
    print('######################################################################')
    
    print("Final log-likelihood: {0}".format(-np.max(sampler.flatlnprobability)))
    print("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    print("Best fit parameter_dict:\n{0}\n".format(mod.gpAll.get_parameter_dict().items()))
    if autoT0 and t0corr!=0:
        tperis = dict(filter(lambda item: "tperi" in item[0], mod.gpAll.get_parameter_dict().items()))
        for k,v in tperis.items():
            k = k.split(':')[1]
            print(f"\t{k} (BJD): {v+t0}")
    print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
    print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
    #f.write("Final log-likelihood: {0}\n".format(-np.max(sampler.flatlnprobability)))
    #f.write("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    #f.write("ln L RV only:{0}\n".format(mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    #f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    #f.close()
    
    print('######################################################################')
    
