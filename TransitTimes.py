#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 18:12:38 2020

@author: dreizler, schwarz
"""

from Fit_TransitTimes import *
from multiprocessing import Pool

t0_TESS = 2457000.
t0_Kepler = 2454833.


   
def fit_AUMic():
    fit_TransitTimes(numBurn = 50, numMCMC = 1500, numWalkers = 400, numThread = 3, 
             inputData = 'AUMic_TT.yaml',
             savename = 'AUMic_TT', 
             output_dir = 'Outputs/',
             input_dir = 'Inputs/',
             t0 = 2458300,
             )
if __name__ == '__main__':
    fit_AUMic()  
   