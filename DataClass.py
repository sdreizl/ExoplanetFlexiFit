#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 13:31:54 2020

@author: dreizler

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import astropy
import gls
import sys

from PlanetClass import *
#from config import normalize

global count1,count2
count1 = 0
count2 = 0

class TimeSeries:
    def __init__(self, **kwargs):

        global count1,count2
        
        plot          = kwargs.get("plot", False)
        plotGLS       = kwargs.get("plotGLS", False)
        
        self.time     = None
        self.value    = None
        self.error    = None
        self.time_in  = None
        self.value_in = None
        self.error_in = None
        self.filename = kwargs.get("fileName")
        self.flareName = kwargs.get("flareName")
        self.name     = kwargs.get("dataName")
        self.GP       = kwargs.get("GP", 'NONE')
        self.GPpar    = kwargs.get("GPpar",[])
        self.boundsGP = kwargs.get('boundsGP',{})
        self.Jitter   = kwargs.get("Jitter",True)
        self.Jitterpar= kwargs.get("Jitterpar",-15.)
        self.boundsJ  = kwargs.get('boundsJ',{})
        self.t0       = kwargs.get("t0",0.)
        self.t0add    = kwargs.get("t0add",0)
        self.t0corr   = kwargs.get("t0corr",0)              # offset of t0Input to median of times (or first datapoint if Dynamic Fit)
        self.data     = kwargs.get("data", "RV")            # RV or AC (activity), TR (transit light curve), TTV, ETV
        self.off      = kwargs.get("off",0.)                # y-offset for data
        self.filetype = kwargs.get("filetype", "PlainCols") # PlainCols or Fits, or whatever format needed
        self.delimiter= kwargs.get("delimiter", " ")        # " " or ";" or "\t" or whatever
        self.binwidth = kwargs.get("binwidth", 0)
        self.color    = kwargs.get("color", "cornflowerblue")
        self.weight   = kwargs.get("weight", 1.)
        self.detrend  = kwargs.get("detrend", None)         # or Linear for ground based, Pixel for satellite data   
        self.vector   = kwargs.get("vector", np.array([]))          # detrending vector(s)
        self.ttras    = kwargs.get("ttras", [])             # transit time of planets (used to cut out light curves)
        self.ReadTheData = kwargs.get("ReadTheData",True) 
        self.FileNo     = kwargs.get("FileNo",None)
        self.All_TT_time= kwargs.get("All_TT_time",[])
        self.All_TT_planet= kwargs.get("All_TT_planet",[])
        self.All_TT_file = kwargs.get("All_TT_file",[])
        self.FitFlare = kwargs.get("FitFlare",False)
        self.FixFlareTime= kwargs.get("FixFlareTime",True)
        self.FixFlareAmp = kwargs.get("FixFlareAmp",True)
        self.FixFlareFWHM= kwargs.get("FixFlareFWHM",True)
        self.filter   = kwargs.get("filter",False)          # sigma clipping of data and errors
        self.sigma    = kwargs.get("sigma",5)               # sigma value for sigma clipping
        self.normalizeTR = kwargs.get("normalize",False)      # normalize Transit data sets
       
        self.label    = ""
        self.fbeg     = 0.001
        self.fend     = 0.67
        
        if (self.GP == 'ROTFLARE_RV') and (count1 == 0):
            self.GP = 'ROTFLARE_RV_lead'
            count1 += 1
        if (self.GP == 'ROT_RV') and (count2 == 0):
            self.GP = 'ROT_RV_lead'
            count2 += 1

        if (self.GP == 'CUST') & (len(self.GPpar) != 4):
            print("inconsistent GP kernel CUST with kernel parametres provided")
        if (self.GP == 'ROT3') & (len(self.GPpar) != 7):
            print("inconsistent GP kernel ROT with kernel parametres provided")
        if (self.GP == 'ROT') & (len(self.GPpar) != 5):
            print("inconsistent GP kernel ROT with kernel parametres provided")
        if (self.GP == 'ROTFLARE') & (len(self.GPpar) != 7):
            print("inconsistent GP kernel ROTFLARE with kernel parametres provided")
        if (self.GP == 'ROTFLARE_RV_lead') & (len(self.GPpar) != 4):
            print("inconsistent GP kernel ROTFLARE_RV_lead with kernel parametres provided")
        if (self.GP == 'ROT_RV_lead') & (len(self.GPpar) != 2):
            print("inconsistent GP kernel ROT_RV_lead with kernel parametres provided")
        if (self.GP == 'ROTFLARE_RV') & (len(self.GPpar) != 0):
            print("inconsistent GP kernel ROTFLARE_RV with kernel parametres provided")
        if (self.GP == 'ROT_RV') & (len(self.GPpar) != 0):
            print("inconsistent GP kernel ROT_RV with kernel parametres provided")
        if (self.GP == 'SHO') & (len(self.GPpar) != 3):
            print("inconsistent GP kernel SHO with kernel parametres provided")
        if (self.GP == 'SHO2') & (len(self.GPpar) != 6):
            print("inconsistent GP kernel SHO2 with kernel parametres provided")
        if (self.GP == 'SHOREAL') & (len(self.GPpar) != 5):
            print("inconsistent GP kernel SHOR with kernel parametres provided")
        if (self.GP == 'REAL') & (len(self.GPpar) != 2):
            print("inconsistent GP kernel REAL with kernel parametres provided")
        if (self.GP == 'RVdriver') & (len(self.GPpar) != 1):
            print("inconsistent GP kernel RVdriver with kernel parametres provided")
        if (self.GP == 'ROTdriver') & (len(self.GPpar) != 2):
            print("inconsistent GP kernel ROTdriver with kernel parametres provided")
        
        if (self.filetype != 'NONE') & self.ReadTheData:
            self.readin(self.filename,self.filetype,self.delimiter,self.t0,self.t0add)
            if plot:
                self.label = " input"
                ylabel='RV (m/s)'
                if isinstance(self,ACTimeSeries): ylabel='Activity Index'
                if isinstance(self,TRTimeSeries): ylabel='Rel. Flux'
                self.plot(ylabel=ylabel)
            if self.binwidth != 0:
                self.binning()
                if plot:
                    self.label = " binning"
                    self.plot(ylabel=ylabel)
            if self.filter:
                self.filtering()
                if plot:
                    self.label = " filtering"
                    self.plot(ylabel=ylabel)
            print("Flare Parameters:",self.filename, self.FitFlare,self.FixFlareTime,self.FixFlareAmp,self.FixFlareFWHM)
      
        self.nGPpar = 0
        if self.GP == 'CUST': self.nGPpar            = 4
        if self.GP == 'REAL': self.nGPpar            = 2
        if self.GP == 'ROT3': self.nGPpar            = 7
        if self.GP == 'ROT': self.nGPpar             = 5
        if self.GP == 'ROTFLARE': self.nGPpar        = 7
        if self.GP == 'ROTFLARE_RV': self.nGPpar     = 0
        if self.GP == 'ROTFLARE_RV_lead': self.nGPpar= 4
        if self.GP == 'ROT_RV': self.nGPpar          = 0
        if self.GP == 'ROT_RV_lead': self.nGPpar     = 2
        if self.GP == 'SHO': self.nGPpar             = 3
        if self.GP == 'SHO2': self.nGPpar            = 6
        if self.GP == 'SHOREAL': self.nGPpar         = 5
        if self.GP == 'RVdriver': self.nGPpar        = 1
        if self.GP == 'ROTdriver': self.nGPpar       = 2
        
        


    def set_TransitTimes(self,TransitTimes,TransitPlanet):
        self.TransitTimes = TransitTimes
        self.TransitPlanet = TransitPlanet
        
    def set_Times(self,time,value,error):
        self.time = time
        self.value= value
        self.error= error
    def set_Vecs(self,vecs):
        self.vector = vecs
        
    def set_Alltimes(self, All_time, All_value=None, All_error=None, All_index=None):
        ind_s= np.argsort(All_time)
        try: self.All_index = All_index[ind_s]
        except: self.All_index = All_index
        try: self.All_time  = All_time[ind_s] 
        except: self.All_time  = All_time 
        try: self.All_value = All_value[ind_s]
        except: self.All_value = All_value
        try: self.All_error = All_error[ind_s]
        except: self.All_error = All_error
        
    def set_error_c(self,error_c):
        self.error_c = error_c
        
    def set_value_c(self,value_c):
        self.value_c = value_c

    def get_phase(self,Period):
        self.phase  = self.time % Period
        self.ind_ph = np.argsort(self.phase)
    
    ##########################################################################
    # Read in file Types:
    #   PlainCols
    #   PlainColsTransit
    #   PlainColsTTV
    #   TRAPPIST_CSV
    #   AIJ_CSV
    #   TESS
    #   FITS
    #   PlainCols_ASAS_SD
    
    
    def readin(self,filename,filetype,delimiter,t0,t0add):
        print("")
        print("using "+filetype+" as file format")
        print(self.data+" data file: "+filename)
        
        if filetype == "PlainCols":
            cont = np.genfromtxt(filename,names=['x','y','ye'],
                                 usecols=(0,1,2),
                                 delimiter=delimiter,
                                 comments='#')    
            self.time_in  = cont['x'] + t0add
            self.time     = cont['x'] - t0 + t0add
            self.value_in = cont['y']
            self.value    = cont['y']# - np.mean(cont['y'])
            # value_in_median = np.median(self.value_in)
            
            self.error    = cont['ye']
            self.error_in = cont['ye']
            # if normalize:
            if isinstance(self,RVTimeSeries):
                self.value = self.value - np.mean(self.value)
            if isinstance(self,ACTimeSeries):
                self.value = self.value - np.mean(self.value)
            
        elif filetype == "PlainColsTransit":
            cont = np.genfromtxt(filename,names=['x','y','ye'],delimiter=delimiter,comments='#')
            self.time_in  = cont['x'] + t0add
            self.time     = cont['x'] - t0 + t0add
            self.value_in = cont['y']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            

            
            self.value    = cont['y'] /value_in_median
            self.error    = cont['ye']/value_in_median
            self.error_in = cont['ye']
            
            
        elif filetype == "TRAPPIST_CSV":
            delimiter = ';'
            cont = np.genfromtxt(filename,names=True,delimiter=delimiter)    
            self.time_in  = cont['HJDd']
            self.time     = cont['HJDd'] - t0
            self.value_in = cont['RFlux']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.value    = cont['RFlux']/value_in_median
            self.error    = cont['Err_RFlux']/value_in_median
            self.error_in = cont['Err_RFlux']
            if self.detrend == 'Linear':
                airmass = cont["airmass"]
                airmass = airmass-np.median(airmass)
                xshift  = cont["dXpix"]
                xshift  = airmass-np.median(xshift)
                yshift  = cont["dYpix"]
                yshift  = airmass-np.median(yshift)
                sky     = cont["backg"]
                sky     = airmass-np.median(sky)
                vecs=[airmass]
                #vecs=np.append(vecs,xshift)
                #vecs=np.append(vecs,yshift)
                #vecs=np.append(vecs,sky)
                vecs=np.append(vecs,np.zeros(len(self.time))+1.)
                vecs=np.append(vecs,self.time)
                
                vecs=np.asarray(vecs).reshape(3,len(self.time))
                self.vector = vecs
        elif filetype == "ASTEP-ANTARCTICA":
            delimiter = ','
            cont = np.genfromtxt(filename,names=True,delimiter=delimiter)    
            self.time_in  = cont['BJD']
            self.time     = cont['BJD'] - t0 + t0add
            self.value_in = cont['FLUX']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.value    = cont['FLUX']/value_in_median
            self.error    = cont['ERRFLUX']/value_in_median
            self.error_in = cont['ERRFLUX']
            if self.detrend == 'Linear':
                airmass = cont["AIRMASS"]
                airmass = airmass#-np.median(airmass)
                vecs=[airmass]
                #vecs=np.append(vecs,xshift)
                #vecs=np.append(vecs,yshift)
                #vecs=np.append(vecs,sky)
                vecs=np.append(vecs,np.zeros(len(self.time))+1.)
                vecs=np.append(vecs,self.time)
                vecs=np.append(vecs,self.time*self.time)
                
                vecs=np.asarray(vecs).reshape(4,len(self.time))
                self.vector = vecs
                
        elif filetype == "AIJ_TDB":
            delimiter = ','
            cont = np.genfromtxt(filename,names=True,delimiter=delimiter)
            #print(cont.dtype)
            #print(cont['BJD_TDB'] - t0)
            self.time_in  = cont['BJD_TDB']
            self.time     = cont['BJD_TDB'] - t0
            self.value_in = cont['rel_flux_T1']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.value    = cont['rel_flux_T1']/value_in_median
            self.error    = cont['rel_flux_err_T1']/value_in_median
            self.error_in = cont['rel_flux_err_T1']
            if self.detrend == 'Linear':
                #print(cont.dtype)
                airmass = cont["AIRMASS"]
                airmass = airmass#-np.median(airmass)
                #xshift  = cont["XFITS_T1"]
                #xshift  = airmass-np.median(xshift)
                #yshift  = cont["YFITS_T1"]
                #yshift  = airmass-np.median(yshift)
                #sky     = cont["SourceSky_T1"]
                #sky     = airmass-np.median(sky)
                vecs=[]
                vecs=np.append(vecs,airmass)
                #vecs=np.append(vecs,xshift)
                #vecs=np.append(vecs,yshift)
                #vecs=np.append(vecs,sky)
                vecs=np.append(vecs,self.time)   # linear term in time
                vecs=np.append(vecs,np.zeros(len(self.time))+1.)  # quadratic term in time
                
                vecs=np.asarray(vecs).reshape(3,len(self.time))
                self.vector = vecs

        elif filetype == "AIJ_UCT":
            delimiter = ','
            cont = np.genfromtxt(filename,names=True,delimiter=delimiter)    
            self.time_in  = cont['BJD_UCT']
            self.time     = cont['BJD_UCT'] - t0
            self.value_in = cont['rel_flux_T1']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.value    = cont['rel_flux_T1']/value_in_median
            self.error    = cont['rel_flux_err_T1']/value_in_median
            self.error_in = cont['rel_flux_err_T1']
            if self.detrend == 'Linear':
                #print(cont.dtype)
                airmass = cont["AIRMASS"]
                airmass = airmass-np.median(airmass)
                #xshift  = cont["XFITS_T1"]
                #xshift  = airmass-np.median(xshift)
                #yshift  = cont["YFITS_T1"]
                #yshift  = airmass-np.median(yshift)
                #sky     = cont["SourceSky_T1"]
                #sky     = airmass-np.median(sky)
                vecs=[]
                vecs=np.append(vecs,airmass)
                #vecs=np.append(vecs,xshift)
                #vecs=np.append(vecs,yshift)
                #vecs=np.append(vecs,sky)
                vecs=np.append(vecs,self.time)   # linear term in time
                vecs=np.append(vecs,np.zeros(len(self.time))+1.)  # quadratic term in time
                
                vecs=np.asarray(vecs).reshape(3,len(self.time))
                self.vector = vecs


        elif filetype == "FITS_TESS":
            hdu = astropy.io.fits.open(filename) # reading from original FITS data
            cont = hdu[1].data
            #print(hdu[1].header)
            
            BJDREFI = hdu[1].header['BJDREFI']  # Integer part of the BJD reference date
            BJDREFF = hdu[1].header['BJDREFF']  # fraction of the day in BTJD reference date  
            BJDcorr = BJDREFI + BJDREFF         # full BJD reference
            
            self.time_in  = cont['TIME']
            self.value_in = cont['PDCSAP_FLUX']
            self.error_in = cont['PDCSAP_FLUX_ERR']
            xb            = cont['TIME']
            yb            = cont['PDCSAP_FLUX']
            yeb           = cont['PDCSAP_FLUX_ERR']
            ind = np.where(np.isfinite(xb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yeb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            
            self.time_in = xb + BJDcorr 
            self.value_in = yb
            value_in_median = 1.
            if self.normalizeTR: 
                value_in_median = np.median(self.value_in)
                print('normalizing ',filename)
            self.error_in = yeb
            
            self.time     = xb + BJDcorr - t0
            self.value    = yb/np.median(yb)
            self.error    = yeb/np.median(yb)
            hdu.close()
            

        elif filetype == "FITS_Lightkurve":
            hdu = astropy.io.fits.open(filename) # reading from original FITS data
            cont = hdu[1].data
            #print(hdu[1].header)
            
            self.time_in  = cont['TIME']
            self.value_in = cont['FLUX']
            self.error_in = cont['FLUX_ERR']
            xb            = cont['TIME']
            yb            = cont['FLUX']
            yeb           = cont['FLUX_ERR']
            ind = np.where(np.isfinite(xb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yeb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            
            self.time_in = xb + t0add 
            self.value_in = yb
            value_in_median = 1.
            if self.normalizeTR: 
                value_in_median = np.median(self.value_in)
                print('normalizing ',filename)
            self.error_in = yeb
            
            self.time     = xb + t0add - t0
            self.value    = yb/np.median(yb)
            self.error    = yeb/np.median(yb)
            hdu.close()

            
        elif filetype == "EVEREST":
            hdu = astropy.io.fits.open(filename) # reading from EVEREST cleaned data
            cont = hdu[1].data
            print(hdu[1].header)
            self.time_in  = cont['TIME']
            self.value_in = cont['FLUX']
            self.error_in = cont['FRAW_ERR']
            xb            = cont['TIME']
            yb            = cont['FLUX']
            yeb           = cont['FRAW_ERR']
            ind = np.where(np.isfinite(xb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            ind = np.where(np.isfinite(yeb))
            xb  = xb[ind]
            yb  = yb[ind]
            yeb = yeb[ind]
            
            self.time_in = xb
            self.value_in = yb
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.error_in = yeb
            
            self.time     = xb + 2454833. - t0
            self.value    = yb/np.median(yb)
            self.error    = yeb/np.median(yb)
            
            hdu.close()

        elif filetype == "PlainCols_ASAS_SD":
            delimiter = ','
            cont = np.genfromtxt(filename,names=True,delimiter=delimiter)    
            self.time_in  = cont['HJD']
            self.time     = cont['HJD'] - t0
            self.value_in = cont['Mag']
            value_in_median = 1.
            if self.normalizeTR: value_in_median = np.median(self.value_in)
            self.value    = cont['Mag']-np.median(cont['Mag'])
            self.error    = cont['Mag_Error']-np.median(cont['Mag'])
            self.error_in = cont['Mag_Error']
                
        else:
            print(f'filetype: {filetype} not supported.')
            print(f'''Please make sure, the filetype is one of the following:
            #   PlainCols
            #   PlainColsTransit
            #   TRAPPIST_CSV
            #   AIJ_CSV
            #   FITS_TESS
            #   EVEREST
            #   PlainCols_ASAS_SD
            ''')
            sys.exit()
            
        if self.FitFlare:
            data = np.genfromtxt(self.flareName,names=['n','t','amp','fwhm'],delimiter=' ',comments='#')
            FlareT = data['t']
            FlareA = data['amp']#/value_in_median
            FlareF = data['fwhm']#/60./24.
            self.FlareList = []
            for fi,ft in enumerate(list(FlareT)):
                if (ft > self.time[0]) & (ft < self.time[-1]):
                    self.FlareList.append([FlareT[fi],FlareA[fi],FlareF[fi]])
            print("\n FlareList",self.FlareList,"\n")
            
        if self.PlanetList!= []:
            if isinstance(self,TRTimeSeries) and self.PlanetList.Transit and self.ReadTheData:
                if self.All_TT_time != []:
                    Per = self.ttras[0][1]
                    Toff= self.ttras[0][2]
                    for Ttra in self.All_TT_time:
                        ind = np.where(((self.time_in-t0) > Ttra - Toff*Per) & \
                                       ((self.time_in-t0) < Ttra + Toff*Per))
                    #print(f'Time_in: {self.time_in} ({type(self.time_in)})')
                    #print(f't0: {t0} ({type(t0)})')
                    #print(Per,Ttra,Toff,(self.time_in-t0)%Per )
                    #self.time  = self.time_in[ind] - t0
                    #self.value = self.value_in[ind]/value_in_median
                    #self.error = self.error_in[ind]/value_in_median
                    #self.value_c = np.copy(self.value)
                    #self.error_c = np.copy(self.error)
                                
                    self.Planet = self.ttras[0][3]
                    
                else:
                    if self.ttras != []:
                        for i in range(len(self.ttras)):
                            Per = self.ttras[i][1]
                            Ttra= self.ttras[i][0]
                            Toff= self.ttras[i][2]
                            #print(f'Time_in: {self.time_in} ({type(self.time_in)})')
                            #print(f't0: {t0} ({type(t0)})')
                            #print(Per,Ttra,Toff,(self.time_in-t0)%Per )
                            ind = np.where(((self.time_in-t0)%Per > Ttra - Toff*Per) & \
                                           ((self.time_in-t0)%Per < Ttra + Toff*Per))
                            if i == 0:
                                self.time  = self.time_in[ind] - t0
                                self.value = self.value_in[ind]/value_in_median
                                self.error = self.error_in[ind]/value_in_median
                                self.value_c = np.copy(self.value)
                                self.error_c = np.copy(self.error)
                            else:
                                self_time = np.concatenate((self.time,self.time_in[ind]-t0),axis=None)
                                self_value = np.concatenate((self.value,self.value_in[ind]/value_in_median),axis=None)
                                self_value_c = self_value
                                self_error = np.concatenate((self.error,self.error_in[ind]/value_in_median),axis=None)
                                self_error_c = self_error
                                
                            self.Planet = self.ttras[i][3]
                        if i> 1:
                            inds = np.argsort(self_time)
                            self.time = self_time[inds]
                            self.value = self_value[inds]
                            self.error = self_error[inds]
                            self.value_c = np.copy(self.value)
                            self.error_c = np.copy(self.error)
                    
                        
                if len(self.time) == 0: 
                    self.binwidth = 0
                    self.TransitTimes = []
                Plan = self.PlanetList
                if  (self.ReadTheData) and ("Dynamic" in self.PlanetList.orbit or 'TTV' in self.PlanetList.orbit) and (len(self.time) != 0):
                    if self.All_TT_time != []:
                        indP = np.where(self.All_TT_planet == self.ttras[0][3])
                        self.TransitTimes = self.All_TT_time[indP]
                        self.TransitFiles = self.All_TT_file[indP]
                        #print(self.TransitTimes)
                    else:
                        ntra = -1
                        nextTransit = self.ttras[0][0] -2.*self.ttras[0][1]  # SD start a bit earlier to not miss the first transit
                        while np.min(self.time) > nextTransit:
                            ntra += 1
                            nextTransit = self.ttras[0][0] + ntra*self.ttras[0][1]
                        
                        TransitFiles = []
                        TransitTimes = []
                        PeriodGuess  = np.copy(self.ttras[0][1])
                        count        = 0
                        
                        #print("Nexttransit",nextTransit,PeriodGuess,count,np.max(self.time))
                        
                        while nextTransit < np.max(self.time)+self.ttras[0][1]:
                            ind = np.where((self.time > nextTransit-self.ttras[0][2]*self.ttras[0][1]) & \
                                           (self.time < nextTransit+self.ttras[0][2]*self.ttras[0][1]) )
                            plt.plot(self.time[ind],self.value[ind])
                            plt.show()
                            plt.close()
                            if len(ind[0]) > 15:
                                xhi2 = []
                                for tt in self.time[ind]:
                                    PP = Planet(Mstar=Plan.Mstar,Rstar=Plan.Rstar,K=Plan.K,m=Plan.m,P=Plan.P,\
                                                e=Plan.e,om=0.5*np.pi,tperi=tt,inc=Plan.inc,\
                                                Rp2Rs=Plan.Rp2Rs,Om=Plan.Om,a2Rs=Plan.a2Rs,
                                                orbit=Plan.orbit,Transit=Plan.Transit)
                                    if Plan.TransitOnly:
                                        mod = PP.MandelAgol(self.time[ind],self.u1,self.u2)
                                    else:
                                        mod = PP.MandelAgolRV(self.time[ind],self.u1,self.u2)                            
                                    mod = mod*(1+ self.thirdL) - self.thirdL
                                    xhi2.append(np.sum((self.value[ind]-mod)**2))
                                xhi2 = np.array(xhi2)
                                ii = np.arange(np.argmin(xhi2)-7,np.argmin(xhi2)+7)
                                #print("1",self.time[ind][ii])
                                #print("2",xhi2[ii])
                                #plt.plot(self.time[ind],xhi2)
                                #plt.show()
                                try:
                                    pf = np.polyfit(self.time[ind][ii],xhi2[ii],2)
                                    tperi_tt = -0.5*pf[1]/pf[0]
                                    print("Transit time found:",tperi_tt)
                                except:
                                    tperi_tt = np.max(self.time[ind]-1.)
                                    print("determination of transit time failed, use dummy time")
                                #plt.plot(self.time[ind],xhi2)
                                #plt.plot(self.time[ind][ii],np.poly1d(pf)(self.time[ind][ii]))
                                #plt.show()
                            
                                PP = Planet(Mstar=Plan.Mstar,Rstar=Plan.Rstar,K=Plan.K,m=Plan.m,P=Plan.P,\
                                            e=Plan.e,om=0.5*np.pi,tperi=tperi_tt,inc=Plan.inc,\
                                            Rp2Rs=Plan.Rp2Rs,Om=Plan.Om,a2Rs=Plan.a2Rs,
                                            orbit=Plan.orbit,Transit=Plan.Transit)
                                if Plan.TransitOnly:
                                    mod = PP.MandelAgol(self.time[ind],self.u1,self.u2)
                                else:
                                    mod = PP.MandelAgolRV(self.time[ind],self.u1,self.u2) 
                                mod = mod*(1+ self.thirdL) - self.thirdL
                                plt.plot(self.time[ind],self.value[ind])
                                plt.plot(self.time[ind],mod)    
                                plt.show()
                                plt.close()
                                #stop
                                TransitTimes.append(tperi_tt)
                                TransitFiles.append(self.FileNo)
                                count += 1
                            else:
                                count = 0
                                
                            nextTransit += PeriodGuess
                            if count > 1:
                                PeriodGuess = TransitTimes[-1] - TransitTimes[-2] 
                            print(nextTransit,PeriodGuess,count)
                            self.TransitTimes = TransitTimes
                            self.TransitFiles = TransitFiles
                        
                        print(f"count of transits: {count}")
         
        ind_s = np.argsort(self.time)
        self.time    = self.time[ind_s]
        self.time_in = self.time_in[ind_s]
        self.value   = self.value[ind_s]
        self.value_in= self.value_in[ind_s]
        self.error   = self.error[ind_s]
        self.error_in= self.error_in[ind_s]
        
        
        print("number of data points: "+str(len(self.time)))
        return self.time, self.value, self.error, self.time_in, self.value_in, self.error_in

    def plot(self,xlabel='',ylabel='RV (m/s)',xsiz=7,ysiz=3,plotGLS=False):
        
        if xlabel == '': xlabel='Time (BJD - '+str(self.t0-self.t0corr)+')'

        fig = plt.figure()
        fig.set_size_inches(xsiz,ysiz)

        TimeSeries.plot_single(self,label=self.label,t0corr=self.t0corr)

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend(fontsize='xx-small')
        plt.show()
        plt.close()

    # binning
    def binning(self):
        x = self.time
        y = self.value
        ye= self.error
        # print(x)
        binrange=(np.int(np.min(x)-1),np.int(np.max(x)+1))
        numbins = np.int((np.int(np.max(x)+1) - np.int(np.min(x)))//(self.binwidth*1.))
        bin_meansx = np.histogram(x,range=binrange,bins=numbins,weights=x)[0] / \
                     np.histogram(x,range=binrange,bins=numbins)[0]
        bin_meansy = np.histogram(x,range=binrange,bins=numbins,weights=y)[0] / \
                     np.histogram(x,range=binrange,bins=numbins)[0]
        bin_weights= np.histogram(x,range=binrange,bins=numbins,weights=ye)[0] / \
                     np.histogram(x,range=binrange,bins=numbins)[0]
        bin_nums   = np.sqrt(np.histogram(x,range=binrange,bins=numbins)[0])
        ind = np.where(np.isfinite(bin_meansx))
        self.time  = bin_meansx[ind]
        self.value = bin_meansy[ind]
        self.error = bin_weights[ind]/bin_nums[ind]
        
        print("number of data points after binning: "+str(len(self.time)))
        return self.time, self.value, self.error

    # filter with sigma clipping
    def filtering(self):
        for kk in range(5):
            ind = np.where(np.abs(self.value-np.median(self.value)) < self.sigma*np.std(self.value))
            if len(self.time) > len(ind[0]): print("filtering data set ",self.filename)
            self.time = self.time[ind]
            self.value= self.value[ind]
            self.error= self.error[ind]
            ind = np.where(np.abs(self.error-np.median(self.error)) < self.sigma*np.std(self.error))
            self.time = self.time[ind]
            self.value= self.value[ind]
            self.error= self.error[ind]
            print("number of data points after filtering: "+str(len(self.time)))

    def save_data(self,DATAList,savename):
        for DATA in DATAList:
            np.savetxt(savename+"_"+self.filename+".dat",zip(DATA.time,DATA.value_c,DATA.error_c),fmt='%15.6f')


    @staticmethod
    def plot_single(DATA,modell=dict(),gpmod=dict(),modcol='k',Phase=False,minPer=30,\
                  label="",xlabel='',ylabel='RV (m/s)',xsiz=7,ysiz=3,correct=[0],\
                  Residuals=False,ax=None,ax2=None,xlims=None,ModLab=True,t0corr=0):
    
        if len(correct) == 1:corr = np.zeros(len(DATA.value))
        else: corr = correct
        
        if Residuals:
            
            if Phase:
                ind_mod = np.argsort(modell['time'])
                ax.errorbar(DATA.phase[DATA.ind_ph],DATA.value_c[DATA.ind_ph]-corr[DATA.ind_ph]\
                                                    -np.interp(DATA.phase[DATA.ind_ph],modell['time'][ind_mod],modell['value'][ind_mod]),\
                             DATA.error_c[DATA.ind_ph],
                             fmt='o',mec=DATA.color,ecolor=DATA.color, \
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name, zorder=1)
                ax2.errorbar(DATA.phase[DATA.ind_ph],DATA.value_c[DATA.ind_ph]\
                             -corr[DATA.ind_ph]-np.interp(DATA.phase[DATA.ind_ph],modell['time'][ind_mod],modell['value'][ind_mod])\
                             -np.interp(DATA.phase[DATA.ind_ph],gpmod['time'][ind_mod],gpmod['value'][ind_mod]),\
                             DATA.error_c[DATA.ind_ph],
                             fmt='o',mec=DATA.color,ecolor=DATA.color, \
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             zorder=1)
            else:
                ax.errorbar(DATA.time+t0corr,DATA.value_c,DATA.error_c,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, label=DATA.name+label, zorder=1)
                ax2.errorbar(DATA.time+t0corr,DATA.value_c-np.interp(DATA.time,modell['time'],modell['value']),DATA.error_c,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, zorder=1)
                plt.xlim(xlims+t0corr)
                #plt.xlim(np.min(DATA.time)-minPer*0.05,np.max(DATA.time)+minPer*0.05)
            if gpmod != dict():
                ind_mod = np.argsort(gpmod['time'])
                if ModLab:
                    ax.plot(gpmod['time'][ind_mod]+t0corr,gpmod['value'][ind_mod],color=modcol,label='model',zorder=10)
                    #ax.plot(DATA.phase[DATA.ind_ph],np.interp(DATA.phase[DATA.ind_ph],gpmod['time'][ind_mod],gpmod['value'][ind_mod]),'r')
                else:
                    ax.plot(gpmod['time'][ind_mod]+t0corr,gpmod['value'][ind_mod],color=modcol,zorder=10)
                    
            #if gpmod != dict():
            #    ax.fill_between(gpmod['time'],gpmod['value']-gpmod['std'],gpmod['value']+gpmod['std'],color='k',\
            #                     alpha=0.3, edgecolor="none")
            #    ax.plot(gpmod['time'],gpmod['value'],alpha=0.4,color='k')
        else:
        
            if Phase:
                plt.errorbar(DATA.phase[DATA.ind_ph],DATA.value[DATA.ind_ph]-corr[DATA.ind_ph],DATA.error[DATA.ind_ph],
                             fmt='o',mec=DATA.color,ecolor=DATA.color, \
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
            else:
                plt.errorbar(DATA.time+t0corr,DATA.value-corr,DATA.error,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, label=DATA.name+label, zorder=1)
                plt.xlim(np.min(DATA.time+t0corr)-minPer*0.05,np.max(DATA.time+t0corr)+minPer*0.05)
            if modell != dict():
                ind_mod = np.argsort(modell['time'])
                if ModLab:
                    plt.plot(modell['time'][ind_mod]+t0corr,modell['value'][ind_mod],color=modcol,label='model',zorder=10)
                else:
                    plt.plot(modell['time'][ind_mod]+t0corr,modell['value'][ind_mod],color=modcol,zorder=10)
                   
            if gpmod != dict():
                plt.fill_between(gpmod['time']+t0corr,gpmod['value']-gpmod['std'],gpmod['value']+gpmod['std'],color='k',\
                                 alpha=0.3, edgecolor="none")
                plt.plot(gpmod['time']+t0corr,gpmod['value'],alpha=0.4,color='k')




class RVTimeSeries(TimeSeries):
    def __init__(self,**kwargs):
        kwargs.update(data = kwargs.get("data", "RV"))
        
        self.PlanetList = kwargs.get("PlanetList",[])
        TimeSeries.__init__(self, **kwargs)
    
class ACTimeSeries(TimeSeries):
    def __init__(self,**kwargs):
        kwargs.update(data = kwargs.get("data", "AC"))
        
        self.PlanetList = kwargs.get("PlanetList",[])
        TimeSeries.__init__(self, **kwargs)
           
class TRTimeSeries(TimeSeries):
    def __init__(self,**kwargs):
        kwargs.update(data = kwargs.get("data", "TR"))
        
        self.Filter   = kwargs.get("Filter", "TESS")
        self.fixLD    = kwargs.get("fixLD", True)
        self.fix3     = kwargs.get("fix3", True)
        self.thirdL   = kwargs.get("thirdL", 0.)
        self.u1       = kwargs.get("u1", 0.4)
        self.u2       = kwargs.get("u2", 0.)
        self.ttras    = kwargs.get("ttras", [])
        self.PlanetList = kwargs.get("PlanetList",[])
        self.ParFileNr  = kwargs.get("ParFileNr",None)
        self.t0shift    = kwargs.get("t0shift",0.)
        self.do_t0shift = kwargs.get("do_t0shift",False)
        self.FileNo     = kwargs.get("FileNo",None)
        self.All_TT_time= kwargs.get("All_TT_time",[])
        self.All_TT_planet= kwargs.get("All_TT_planet",[])
        self.All_TT_file = kwargs.get("All_TT_file",[])
        self.FitFlare = kwargs.get("FitFlare",False)
        self.FixFlareTime= kwargs.get("FixFlareTime",True)
        self.FixFlareAmp = kwargs.get("FixFlareAmp",True)
        self.FixFlareFWHM= kwargs.get("FixFlareFWHM",True)
        
       
        # print("#################################PlanetList TRTimeseries")
        # print(self.PlanetList)
        if self.fix3:
            self.thirdL = 0.
            
            
            
        TimeSeries.__init__(self, **kwargs)
