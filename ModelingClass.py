# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 17:42:48 2020

@author: Stefan
"""

from astropy.io import ascii
from astropy import constants as const
import numpy as np
import celerite
from celerite import terms
from celerite.modeling import Model
import copy
import gc
from ttvfaster import run_ttvfaster

from PlanetClass import *
from DataClass import *
from config import *

import os.path as path
import os
import sys
import time
import platform

if platform.system() !='Windows':
    import rebound
    import reboundx
    from reboundx import constants
    #import planetplanet as pp
    from spock import FeatureClassifier
    #import manhattan.periodograms as cs 

import matplotlib.pyplot as plt
import aflare

#######################################################################

global PlanetListe
global RVListe
global ACListe
global TRListe
global TT_times
global TT_Stimes
global TT_Etimes
global TT_planet
global calc_rebound,calc_reboundTR
global iRV,iTR
global All_times, All_timesTR
global All_indexs, All_indexsTR
global Ps,a2Rss,es,oms,incs,tperis,Pnum,TTVs
global TTVs
global tTTV
global StabProb
global ttvs

#global datatype    

StabProb = 0.
calc_rebound   = False
calc_reboundTR = False

###############################################################################
class CustomTerm(terms.Term):
    parameter_names = ("log_a", "log_b", "log_c", "log_P")

    def get_real_coefficients(self, params):
        log_a, log_b, log_c, log_P = params
        b = np.exp(log_b)
        return (
            np.exp(log_a) * (1.0 + b) / (2.0 + b), np.exp(log_c),
        )

    def get_complex_coefficients(self, params):
        log_a, log_b, log_c, log_P = params
        b = np.exp(log_b)
        return (
            np.exp(log_a) / (2.0 + b), 0.0,
            np.exp(log_c), 2*np.pi*np.exp(-log_P),
        )
###############################################################################
class Paras():
    def __init__(self,PlanetList=[],RVList=[],ACList=[],TRList=[],TT_index=[],
                 lin=0.,quad=0.,
                 numPlanmax=config_numPlanMax,nfRVmax=7,nfACmax=2,nfTRmax=config_nfTRmax,flmax=30):
        # TODO: nfTRmax from config file
        
        self.PlanetList = PlanetList
        self.RVList     = RVList
        self.ACList     = ACList
        self.TRList     = TRList

        self.numPlanmax = numPlanmax  # max number Planets
        self.nfRVmax    = nfRVmax     # max number RV files
        self.nfACmax    = nfACmax     # max number AC files
        self.nfTRmax    = nfTRmax     # max number TR files
        self.flmax      = flmax       # max number of flares per TR data set
        #self.ndeg       = ndeg        # polynomial degree for RV background
        self.lin        = lin         # linear polynomial coeff for RV background
        self.quad       = quad        # quadratic polynomial coeff for RV background

        self.nfRV       = len(self.RVList)
        self.nfAC       = len(self.ACList)
        self.nfTR       = len(self.TRList)
        try:
            self.npTR       = self.TRList[-1].ParFileNr + 1
        except:
            self.npTR       = 0 
        self.numPlanets = len(self.PlanetList)
        
        
        # print(f"nfTRmax: {self.nfTRmax} ({type(self.nfTRmax)})")
        
        

###############################################################################
class ModelingClass():
    #def __init__(self,PlanetList,RVList,ACList,TRList,IndDict,bound,\
    def __init__(self,stellarParameter,PlanetList,DATAList,All_TT_time,All_TT_Stime,All_TT_Etime,All_TT_indx,All_TT_planet,\
                 All_time,All_index,\
                 All_timeTR,All_indexTR,TTV,bound,\
                 boundsgpRV=dict(),boundsgpAC=dict(),boundsgpTR=dict(),\
                 boundsJRV=dict(),boundsJAC=dict(),boundsJTR=dict(),\
                 lin=0.,quad=0.):
        global PlanetListe
        global RVListe
        global ACListe
        global TRListe
        global TT_times
        global TT_Stimes
        global TT_Etimes
        global TT_index
        global TT_planet
        global All_times
        global All_indexs
        global All_indexsTR
        global TTVs

        TT_times    = All_TT_time
        TT_Stimes   = All_TT_Stime
        TT_Etimes   = All_TT_Etime
        TT_index    = All_TT_indx
        TT_planet   = All_TT_planet
        All_times   = All_time
        All_indexs  = All_index
        All_timesTR = All_timeTR
        All_indexsTR= All_indexTR
        TTVs        = TTV

        RVList = []
        ACList = []
        TRList = []
        for L in DATAList:
            #print("RVList",L.GP,L.GPpar)
            if isinstance(L,RVTimeSeries): RVList.append(L)
            if isinstance(L,ACTimeSeries): ACList.append(L)
            if isinstance(L,TRTimeSeries): TRList.append(L)
            #print("RVList",RVList[-1].GP,RVList[-1].GPpar)
            #if L.data == 'AC': ACList.append(L)
            #if L.data == 'TR': TRList.append(L)

        PlanetListe = PlanetList
        RVListe     = RVList
        ACListe     = ACList
        TRListe     = TRList
        #Paras.__init__(self)#,ndeg=0,lin=0.,quad=0.,numPlanmax=4,nfRVmax=7,nfACmax=2,nfTRmax=10)
        #super().__init__(self)
        
        paras = Paras(PlanetList,RVList,ACList,TRList,TT_index,lin=lin,quad=quad)
        
        self.RVList     = RVList
        self.ACList     = ACList
        self.TRList     = TRList
        self.PlanetList = PlanetList
        self.DATAList   = DATAList
        #self.IndDict    = IndDict
        self.bounds     = bound
        self.boundsgpRV = boundsgpRV
        self.boundsgpAC = boundsgpAC
        self.boundsgpTR = boundsgpTR
        self.boundsJRV = boundsJRV
        self.boundsJAC = boundsJAC
        self.boundsJTR = boundsJTR
        self.lin        = lin         # linear polynomial coeff for RV background
        self.quad       = quad        # quadratic polynomial coeff for RV background
        
        ndeg = 2                      # determine polynomial degree
        if quad == 0.: ndeg = 1
        if lin  == 0.: ndeg = 0
        self.ndeg = ndeg
        
        self.nfRV = paras.nfRV
        self.nfAC = paras.nfAC
        self.nfTR = paras.nfTR
        self.numPlanets = paras.numPlanets
        
        # set parameter positions   GPRV,...,RVJitter,...,GPAC,...,ACJitter,GPTR,...,TRJitter,PlanetParams,StarParames,Offsets,Polynomial 
        GPoff   = 0
        GPoffRV = 0
        nRVpar  = 0
        #print("nGPpar",len(RVList))
        if len(RVList) ==0: nGPpar=0
        else: nGPpar  = RVList[0].nGPpar
        
        nRVpar  = nGPpar
        nGPRV   = nGPpar
        JoffRV  = GPoffRV + nGPpar
        GPoff   = JoffRV
        for i,RV in enumerate(self.RVList):
            #print('RV list',i,GPoff,RV.Jitter)
            if RV.Jitter: 
                GPoff += 1
                nRVpar += 1        

        #nGPpar = 0
        #if ACList != []: nGPpar = ACList[0].nGPpar
                
        GPoffAC= GPoff
        nGPAC  = []
        JoffAC = []
        #JoffT  = GPoff
                
        for i,AC in enumerate(ACList):
            #print('1',i,GPoff,AC.nGPpar)
            nGPAC.append(AC.nGPpar)
            GPoff = GPoff + AC.nGPpar
            JoffAC.append(GPoff)
            if AC.Jitter: GPoff = JoffAC[i] + 1
            #print('2',i,GPoff)
        #for i,AC in enumerate(ACList):
        #    print('AC list',i,GPoff)
        #    if AC.Jitter: GPoff += 1
        #print("AC",JoffAC,GPoff)
        nfAC = len(ACList)   

        GPoffTR= GPoff
        nGPTR  = []
        JoffTR = []
        #JoffT  = GPoff
        
        #print(GPoff)
        count = 0
        for i,TR in enumerate(TRList):
            if (TR.GP == 'ROTFLARE_TR') and (count == 0):
                GPoff = GPoff + TR.nGPpar
                count += 1
                print("using kernel ROTFLARE_TR: kernel parameters of first TR-data set will be used for all with ROTFLARE\n")
            elif (TR.GP == 'ROTFLARE_RV_lead') and (count == 0):
                GPoff = GPoff + TR.nGPpar
                #count += 1
                print("using kernel ROTFLARE_RV: kernel parameters for ROT kernel of first RV-data set\n\
                      and kernel parameters for REAL kernel of first TR-data set will be used for all with ROTFLARE,\n")
            elif (TR.GP == 'ROT_RV_lead') and (count == 0):
                GPoff = GPoff + TR.nGPpar
                #count += 1
                print("using kernel ROT_RV: kernel parameters for ROT kernel of first RV-data set\n\
                      will be used for all with ROTFLARE,\n")
            elif (count == 0):
                GPoff = GPoff + TR.nGPpar
            nGPTR.append(TR.nGPpar)
            JoffTR.append(GPoff)
            if TR.Jitter: GPoff = JoffTR[i] + 1
            #print('GPTRoff',i,GPoff,JoffTR[i])
        nfTR = len(TRList)   
        #print("TR",JoffTR,GPoff)
        
        Poff = GPoff
        Ppar = 0
        Pind = 0
        for i,PL in enumerate(PlanetList):
            Ppar += PL.Ppar
            PL.ind = Pind
            #print(PL.Ppar,len(PL.varfix[0:6])-np.sum(PL.varfix[0:6])+len(PL.varfix[7:9])-np.sum(PL.varfix[7:9]),Poff+Ppar)
            if PL.orbit == 'Circular': 
                nRVpar += len(PL.varfix[0:6])-np.sum(PL.varfix[0:6])  #3
            if PL.orbit == 'Kepler':   
                nRVpar += len(PL.varfix[0:6])-np.sum(PL.varfix[0:6])  #5
            if PL.orbit == 'Dynamic' or PL.orbit == 'TTV':
                nRVpar += len(PL.varfix[0:6])-np.sum(PL.varfix[0:6])+len(PL.varfix[7:9])-np.sum(PL.varfix[7:9])  #7
                #if PL.fixInc or PL.Coplanar : nRVpar -= 1  # inclination fixed
                #if PL.fixOm:                  nRVpar -= 1
            #print("Ppar",Ppar)
            Pind += len(PL.varfix)

        Soff = Poff + Ppar

        Spar     = 0
        nTransit = 0
        for i,PL in enumerate(PlanetList):
            if (PL.Transit & (not PL.TransitOnly)) & (not PL.fixRstar): nTransit += 1
        if nTransit > 0: Spar=1
        
        npTR = 0
        for i,TR in enumerate(TRList):
            kk = TR.ParFileNr
            if kk == npTR:
                if not TR.fixLD: Spar += 2
                if not TR.fix3:  Spar += 1
                npTR += 1
            else:
                pass
        if ((len(RVList) == 0) & (not TTV)) & nTransit>0: Spar = Spar-1
            
        OoffRV = Soff + Spar
        OoffAC = OoffRV + paras.nfRV
        OoffTR = OoffAC + nfAC
        Oofft0TR = OoffTR + npTR 
        #linpos = Oofft0TR + npTR -1 #
        linpos = OoffTR + npTR #
        if lin == 0: quadpos= linpos 
        else: quadpos= linpos + 1
        if lin == 0: flarepos = quadpos 
        else: flarepos = quadpos + 1
        
        nRVpar  +=  paras.nfRV + self.ndeg
        self.nRVpar = nRVpar
        print("Param Positions",Poff,Soff,OoffRV,OoffAC,OoffTR,JoffTR,flarepos)
        #print(Ppar,Spar)
        #stop

        self.Poff   = Poff
        self.Ppar   = Ppar
        self.Soff   = Soff
        self.Spar   = Spar

        self.GoffRV = GPoffRV
        self.nGPRV  = nGPRV
        self.JoffRV = JoffRV
        self.OoffRV = OoffRV

        self.GoffAC = GPoffAC
        self.nGPAC  = nGPAC
        self.JoffAC = JoffAC
        self.OoffAC = OoffAC

        self.GoffTR   = GPoffTR
        self.nGPTR    = nGPTR
        self.JoffTR   = JoffTR
        self.OoffTR   = OoffTR
        self.Oofft0TR = Oofft0TR
        
        self.linpos   = linpos
        self.quadpos  = quadpos
        self.flarepos = flarepos

        # define parameter set

        K     = []
        m     = []
        P     = []
        e     = []
        om    = []
        tperi = []
        Rp2Rs = []
        inc   = []
        Om    = []
        a2Rs  = []
        nTransit = 0
        
        argList = []
        
        for i,Plan in enumerate(PlanetList):
            if Plan.Transit: nTransit += 1
            K.append(Plan.K)
            m.append(Plan.m)
            P.append(Plan.P)
            e.append(Plan.e)
            om.append(Plan.om)
            tperi.append(Plan.tperi)
            Rp2Rs.append(Plan.Rp2Rs)
            inc.append(Plan.inc)
            Om.append(Plan.Om)
            a2Rs.append(Plan.a2Rs)
            
            argList.append(Plan.K)
            argList.append(Plan.m)
            argList.append(Plan.P)
            argList.append(Plan.e)
            argList.append(Plan.om)
            argList.append(Plan.tperi)
            argList.append(Plan.Rp2Rs)
            argList.append(Plan.inc)
            argList.append(Plan.Om)
            argList.append(Plan.a2Rs)
        #print("argList",len(argList))
        if PlanetList == []: i=-1
            
        for j in range(i+1,paras.numPlanmax):  #dummy planets up to numPlanmax
            K.append(0.)
            m.append(0.)
            P.append(0.)
            e.append(0.)
            om.append(0.)
            tperi.append(0.)
            Rp2Rs.append(0.)
            inc.append(0.)
            Om.append(0.)
            a2Rs.append(0.)
            for jj in range(10): argList.append(0.)
        #print("argList",len(argList))
            
        if PlanetList != []: 
            Rstar = Plan.Rstar
        else:
            Rstar = stellarParameter.get('Rstar',1)
        argList.append(Rstar)
        
        #print("argList",len(argList))
        
        u1     = []
        u2     = []
        thirdL = []
        fixLD  = []
        fix3   = []
        npTR   = 0

        for i,TR in enumerate(self.TRList):
            kk = TR.ParFileNr
            if kk == npTR:
                u1.append(TR.u1)
                u2.append(TR.u2)
                thirdL.append(TR.thirdL)
                fixLD.append(TR.fixLD)
                fix3.append(TR.fix3)
                argList.append(TR.u1)
                argList.append(TR.u2)
                argList.append(TR.thirdL)
                npTR += 1
            else:
                pass

        for j in range(npTR,paras.nfTRmax):  #dummies up to nfmax
            u1.append(0.)
            u2.append(0.)
            thirdL.append(0.)
            for jj in range(3): argList.append(0.)
        #print("argList",len(argList))


        offRV  = []

        for i,RV in enumerate(self.RVList):
            offRV.append(RV.off)
            argList.append(RV.off)
        #if self.RVList == []: paras.nfRV = 0
        #else: paras.nfRV = i+1
        for j in range(paras.nfRV,paras.nfRVmax):  #dummies up to nfmax
            offRV.append(0.)
            argList.append(0.)
        #print("argList",len(argList))
            
        offAC  = []

        for i,AC in enumerate(self.ACList):
            offAC.append(AC.off)
            argList.append(AC.off)
        #if self.ACList == []: nfAC = 0
        #else: nfAC = i+1
        for j in range(paras.nfAC,paras.nfACmax):  #dummies up to nfmax
            offAC.append(0.)
            argList.append(0.)
        #print("argList",len(argList))
            
        offTR  = []
        t0TR   = []

        npTR = 0
        for i,TR in enumerate(self.TRList):
            kk = TR.ParFileNr
            if kk == npTR:
                offTR.append(TR.off)
                argList.append(TR.off)
                npTR += 1
        for j in range(npTR,paras.nfTRmax):  #dummies up to nfmax
        #for j in range(paras.nfTR,paras.nfTRmax):  #dummies up to nfmax
            offTR.append(0.)
            argList.append(0.)
        #print("argList",len(argList))

        npTR = 0
        for i,TR in enumerate(self.TRList):
            kk = TR.ParFileNr
            if kk == npTR:
                if kk != 0:
                    t0TR.append(TR.t0shift)
                    #argList.append(TR.t0shift)
                npTR += 1
        for j in range(npTR,paras.nfTRmax):  #dummies up to nfmax
            t0TR.append(0.)
            #argList.append(0.)
        print("npTR",npTR)
            
        argList.append(paras.lin)
        argList.append(paras.quad)
        #print("argList",len(argList))
       
        for i,TR in enumerate(self.TRList):
            if TR.FitFlare:
                fl = len(TR.FlareList)
                for fi in range(fl):
                    argList.append(TR.FlareList[fi][0])
                    argList.append(TR.FlareList[fi][1])
                    argList.append(TR.FlareList[fi][2])
                for fi in range(fl,paras.flmax):
                    argList.append(0)
                    argList.append(0)
                    argList.append(0)
            else:
                for fi in range(paras.flmax):
                    argList.append(0)
                    argList.append(0)
                    argList.append(0)
        for i in range(len(self.TRList),paras.nfTRmax):
            for fi in range(paras.flmax):
                argList.append(0)
                argList.append(0)
                argList.append(0)

        print("argList",argList,len(argList),len(argList))
        print("K",K)
        print("m",m)
        print("P",P)
        print("e",e)
        print("om",om)
        print("tperi",tperi)
        print("Rp2Rs",Rp2Rs)
        print("inc",inc)
        print("Om",Om)
        print("a2Rs",a2Rs)
        print("Rstar",Rstar)
        print('u1',u1)
        print('u2',u2)

        print(self.bounds, sep='\t')
        mean_modelRV = MeanModOrbitRV(*argList, bounds=self.bounds)
        self.meanmod = mean_modelRV
        

        print()
        # print("Parameter set:",mean_modelRV.get_parameter_names())
        unimportant_params = (paras.nfTRmax*paras.flmax*3+paras.nfTRmax*3+paras.nfTRmax+paras.nfACmax+paras.nfRVmax)
        print("Parameter set: ",mean_modelRV.get_parameter_names()[:-unimportant_params],f'{len(mean_modelRV.get_parameter_names())}-{unimportant_params}')

        #self.nflares = []
        nflares = 0
        for i,TR in enumerate(self.TRList):
            if TR.FitFlare:
                fl = len(TR.FlareList)
                for fi in range(fl):
                    if TR.FixFlareTime: 
                        mean_modelRV.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                    else:
                        nflares += 1
                    if TR.FixFlareAmp:  
                        mean_modelRV.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                    else:
                        nflares += 1
                    if TR.FixFlareFWHM: 
                        mean_modelRV.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
                    else:
                        nflares += 1

                for fi in range(fl,paras.flmax):
                    mean_modelRV.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                    mean_modelRV.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                    mean_modelRV.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
            else:
                for fi in range(paras.flmax):
                    mean_modelRV.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                    mean_modelRV.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                    mean_modelRV.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
            #self.nflares.append(nflares)
        self.nflares = nflares
        for i in range(len(self.TRList),paras.nfTRmax):
            for fi in range(paras.flmax):
                mean_modelRV.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                mean_modelRV.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                mean_modelRV.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
        for i,Plan in enumerate(self.PlanetList):
            #print(Plan.varfix)
            for j,vf in enumerate(Plan.varfix):
                if vf == 1:
                    print("For planet {0} freeze parameter {1}".format(i,mean_modelRV.parameter_names[j+Plan.ind]))
                    mean_modelRV.freeze_parameter(mean_modelRV.parameter_names[j+Plan.ind])

        if nTransit == 0 or self.PlanetList[0].fixRstar:
            #print(nTransit,self.PlanetList[0].fixRstar)
            #stop
            print("freeze parameter Rstar")
            mean_modelRV.freeze_parameter("Rstar")

        for i in range(paras.numPlanets,paras.numPlanmax):
            print("skip planet",i)
            mean_modelRV.freeze_parameter("K"+str(i))
            mean_modelRV.freeze_parameter("m"+str(i))
            mean_modelRV.freeze_parameter("P"+str(i))
            mean_modelRV.freeze_parameter("e"+str(i))
            mean_modelRV.freeze_parameter("om"+str(i))
            mean_modelRV.freeze_parameter("tperi"+str(i))
            mean_modelRV.freeze_parameter("Rp2Rs"+str(i))
            mean_modelRV.freeze_parameter("inc"+str(i))
            mean_modelRV.freeze_parameter("Om"+str(i))                
            mean_modelRV.freeze_parameter("a2Rs"+str(i))

        for i in range(npTR,paras.nfTRmax):
                print("skip LD and 3rd light for TR data",i)
                mean_modelRV.freeze_parameter("u1"+str(i))
                mean_modelRV.freeze_parameter("u2"+str(i))
                mean_modelRV.freeze_parameter("thirdL"+str(i))
            
        for i,TR in enumerate(self.TRList):
            if TR.fixLD:
                kk = TR.ParFileNr
                print("freeze u1+u2",TR.name)
                mean_modelRV.freeze_parameter("u1"+str(kk))
                mean_modelRV.freeze_parameter("u2"+str(kk))
            if TR.fix3:
                kk = TR.ParFileNr
                print("freeze third light",TR.name)
                mean_modelRV.freeze_parameter("thirdL"+str(kk))
            #if not TR.do_t0shift or (i == 0):
            #    print("!!!!freez t0 shifts",i)
            #    mean_modelRV.freeze_parameter("t0TR"+str(i))

        for i in range(paras.nfRV,paras.nfRVmax):
            print("skip RV offset",i)
            mean_modelRV.freeze_parameter("offRV"+str(i))
        for i in range(paras.nfAC,paras.nfACmax):
            print("skip AC offset",i)
            mean_modelRV.freeze_parameter("offAC"+str(i))
        for i in range(npTR,paras.nfTRmax):
        #for i in range(paras.nfTR,paras.nfTRmax):
            print("skip TR offset",i)
            mean_modelRV.freeze_parameter("offTR"+str(i))
        #for i in range(npTR,paras.nfTRmax-1):
        #    print("skip t0 shifts",i)
        #    mean_modelRV.freeze_parameter("t0TR"+str(i))
        
        if ndeg < 2: 
            print("skip quadratic coeff",i)            
            mean_modelRV.freeze_parameter("quad")
        if ndeg == 0: 
            print("skip linear coeff",i)            
            mean_modelRV.freeze_parameter("lin")
            
        print()
        print("Parameter set RV:",mean_modelRV.get_parameter_names())

        if (nTransit >0) & (paras.nfTR == 0):
            #for Plan in self.PlanetList:
            #    Plan.Transit = False
            #    Tpar = 2
            #    if Plan.orbit == 'Dynamic':  
            #                     Tpar = 1  # inclination is alread a Dynamic parameter 
            #    else: 
            #        if Plan.Coplanar: Tpar = 1  # inclination of first planet
            #    Plan.Ppar -= Tpar

            print()
            print("#################################################################")
            print("No transit light curve provide, but transit fitting requested ?!")
            print("#################################################################")
            print()

                
        mean_modelTR = MeanModOrbitTR(*argList,bounds=self.bounds)

        for i,TR in enumerate(self.TRList):
            if TR.FitFlare:
                fl = len(TR.FlareList)
                for fi in range(fl):
                    if TR.FixFlareTime: 
                        mean_modelTR.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                        print("freeze FlareTime"+str(i+100)+str(fi))
                    if TR.FixFlareAmp:  
                        mean_modelTR.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                        print("freeze FlareAmp"+str(i+100)+str(fi))
                    if TR.FixFlareFWHM: 
                        mean_modelTR.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
                        print("freeze FlareFWHM"+str(i+100)+str(fi))
                for fi in range(fl,paras.flmax):
                    mean_modelTR.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                    mean_modelTR.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                    mean_modelTR.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
            else:
                for fi in range(paras.flmax):
                    mean_modelTR.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                    mean_modelTR.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                    mean_modelTR.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))
        for i in range(len(self.TRList),paras.nfTRmax):
            for fi in range(paras.flmax):
                mean_modelTR.freeze_parameter("FlareTime"+str(i+100)+str(fi))
                mean_modelTR.freeze_parameter("FlareAmp"+str(i+100)+str(fi))
                mean_modelTR.freeze_parameter("FlareFWHM"+str(i+100)+str(fi))

        for i,Plan in enumerate(self.PlanetList):
            #print(Plan.varfix)
            for j,vf in enumerate(Plan.varfix):
                if vf == 1:
                    print("For planet {0} freeze parameter {1}".format(i,mean_modelTR.parameter_names[j+Plan.ind]))
                    mean_modelTR.freeze_parameter(mean_modelTR.parameter_names[j+Plan.ind])

        if nTransit == 0 or self.PlanetList[0].fixRstar:
            mean_modelTR.freeze_parameter("Rstar")

        for i in range(paras.numPlanets,paras.numPlanmax):
            #print("skip planet",i)
            mean_modelTR.freeze_parameter("K"+str(i))
            mean_modelTR.freeze_parameter("m"+str(i))
            mean_modelTR.freeze_parameter("P"+str(i))
            mean_modelTR.freeze_parameter("e"+str(i))
            mean_modelTR.freeze_parameter("om"+str(i))
            mean_modelTR.freeze_parameter("tperi"+str(i))
            mean_modelTR.freeze_parameter("Rp2Rs"+str(i))
            mean_modelTR.freeze_parameter("inc"+str(i))
            mean_modelTR.freeze_parameter("Om"+str(i))                
            mean_modelTR.freeze_parameter("a2Rs"+str(i))

        for i in range(npTR,paras.nfTRmax):
                #print("skip LD and 3rd light for TR data",i)
                mean_modelTR.freeze_parameter("u1"+str(i))
                mean_modelTR.freeze_parameter("u2"+str(i))
                mean_modelTR.freeze_parameter("thirdL"+str(i))
            
        for i,TR in enumerate(self.TRList):
            if TR.fixLD:
                kk = TR.ParFileNr
                #print("freeze u1+u2",TR.name)
                mean_modelTR.freeze_parameter("u1"+str(kk))
                mean_modelTR.freeze_parameter("u2"+str(kk))
            if TR.fix3:
                kk = TR.ParFileNr
                #print("freeze third light",TR.name)
                mean_modelTR.freeze_parameter("thirdL"+str(kk))
            #if not TR.do_t0shift or (i == 0):
            #    #print("freez t0 shifts",i)
            #    mean_modelTR.freeze_parameter("t0TR"+str(i))

        for i in range(paras.nfRV,paras.nfRVmax):
            #print("skip RV offset",i)
            mean_modelTR.freeze_parameter("offRV"+str(i))
        for i in range(paras.nfAC,paras.nfACmax):
            #print("skip AC offset",i)
            mean_modelTR.freeze_parameter("offAC"+str(i))
        for i in range(npTR,paras.nfTRmax):
        #for i in range(paras.nfTR,paras.nfTRmax):
            #print("skip TR offset",i)
            mean_modelTR.freeze_parameter("offTR"+str(i))
        #for i in range(npTR,paras.nfTRmax-1):
        #    print("skip t0 shifts",i)
        #    mean_modelTR.freeze_parameter("t0TR"+str(i))
        
        if ndeg < 2: 
            #print("skip quadratic coeff",i)            
            mean_modelTR.freeze_parameter("quad")
        if ndeg == 0: 
            #print("skip linear coeff",i)            
            mean_modelTR.freeze_parameter("lin")
            
        print()
        print("Parameter set TR:",mean_modelTR.get_parameter_names())
        #for i,k in enumerate(mean_modelTR.get_parameter_names()):
        #    print("k",k)
        #    if ('e' in k) and ('t' not in k): print(k)
        
        if (nTransit >0) & (paras.nfTR == 0):
            print()
            print("#################################################################")
            print("No transit light curve provide, but transit fitting requested ?!")
            print("#################################################################")
            print()

        #######################################################################

        mean_modelC = MeanModelConstAC(offAC0=offAC[0],offAC1=offAC[1])#, bounds=boundAC)
        for i in range(paras.nfAC,paras.nfACmax):
            print("freezeC",i)
            mean_modelC.freeze_parameter("offAC"+str(i))
            
        #######################################################################
        # prepare model: GP kernels
                
        KernelList = []
        KernelListJ= []
        
        # RV files, read parameters from lead-RV file
        if len(RVList) > 0:
            if RVList[0].GP == 'ROT3':
                #print(RVList[0].GPpar)
                S0  = RVList[0].GPpar[0]
                w0  = 2.*np.pi/RVList[0].GPpar[2]
                tau = RVList[0].GPpar[1]
                Q   = 0.5*tau*w0
                # print('Kernel1')
                # print('w0: ',w0, np.exp(boundsgpRV["log_omega0"]))
                # print('S0: ',S0, np.exp(boundsgpRV["log_S0"]))
                # print('Q: ',Q, np.exp(boundsgpRV["log_Q"]))
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)    
                S0  = RVList[0].GPpar[3]
                w0  = 2.*np.pi/RVList[0].GPpar[2]*2. # force to half period
                tau = RVList[0].GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpRV["log_omega0"] = (boundsgpRV["log_omega0"][0],boundsgpRV["log_omega0"][1]+np.log(2))
                # print('Kernel2')
                # print('w0: ',w0, np.exp(boundsgpRV["log_omega0"]))
                # print('S0: ',S0, np.exp(boundsgpRV["log_S0"]))
                # print('Q: ',Q, np.exp(boundsgpRV["log_Q"]))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)
                kernel2.freeze_parameter("log_omega0")
                S0  = RVList[0].GPpar[5]
                w0  = 2.*np.pi/RVList[0].GPpar[2]*3. # force to half period
                tau = RVList[0].GPpar[6]
                Q   = 0.5*tau*w0
                boundsgpRV["log_omega0"] = (boundsgpRV["log_omega0"][0],boundsgpRV["log_omega0"][1]-np.log(2)+np.log(3))
                # print('Kernel3')
                # print('w0: ',w0, np.exp(boundsgpRV["log_omega0"]))
                # print('S0: ',S0, np.exp(boundsgpRV["log_S0"]))
                # print('Q: ',Q, np.exp(boundsgpRV["log_Q"]))
                kernel3    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)
                kernel3.freeze_parameter("log_omega0")
                kernelRV = kernel1+kernel2+kernel3
            if RVList[0].GP == 'ROT':
                #print(RVList[0].GPpar)
                S0  = RVList[0].GPpar[0]
                w0  = 2.*np.pi/RVList[0].GPpar[2]
                tau = RVList[0].GPpar[1]
                Q   = 0.5*tau*w0
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)    
                S0  = RVList[0].GPpar[3]
                w0  = 2.*np.pi/RVList[0].GPpar[2]*2. # force to half period
                tau = RVList[0].GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpRV["log_omega0"] = (boundsgpRV["log_omega0"][0],boundsgpRV["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)
                kernel2.freeze_parameter("log_omega0")
                kernelRV = kernel1+kernel2
            if RVList[0].GP == 'ROTFLARE':
                S0  = RVList[0].GPpar[0]
                w0  = 2.*np.pi/RVList[0].GPpar[2]
                tau = RVList[0].GPpar[1]
                Q   = 0.5*tau*w0
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)
                S0  = RVList[0].GPpar[3]
                w0  = 2.*np.pi/RVList[0].GPpar[2]*2. # force to half period
                tau = RVList[0].GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpRV["log_omega0"] = (boundsgpRV["log_omega0"][0],boundsgpRV["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)
                kernel2.freeze_parameter("log_omega0")
                a   = RVList[0].GPpar[5]
                c   = 1./RVList[0].GPpar[6]
                kernel3    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),\
                                            bounds=boundsgpRV)
                kernelRV = kernel1+kernel2+kernel3
            if RVList[0].GP == 'SHO':
                S0  = RVList[0].GPpar[0]
                w0  = 2.*np.pi/RVList[0].GPpar[2]
                tau = RVList[0].GPpar[1]
                Q   = 0.5*tau*w0
                kernelRV    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpRV)    
            if RVList[0].GP == 'REAL':
                a   = RVList[0].GPpar[0]
                c   = 1./RVList[0].GPpar[1]
                kernelRV    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),\
                                            bounds=boundsgpRV)
            if RVList[0].GP == 'CUST':
                a   = RVList[0].GPpar[0]
                b   = RVList[0].GPpar[1]
                c   = 1./RVList[0].GPpar[2]
                P   = RVList[0].GPpar[3]
                kernelRV = CustomTerm(log_a=np.log(a), log_b=np.log(b), \
                                      log_c=np.log(c), log_P=np.log(P),bounds=boundsgpRV)
        for i,RV in enumerate(RVList):                                
            if RV.Jitter:
                sigma = RV.Jitterpar
                print("bounds",boundsJRV)    
                kernelJ= celerite.terms.JitterTerm(log_sigma=np.log(sigma),bounds=boundsJRV)
                if RV.GP != 'NONE':
                    if i == 0: 
                        kernel   = kernelRV + kernelJ
                        kernelRV = kernel
                        kernelRVJ= kernelJ
                    else:
                        kernel   += kernelJ
                        kernelRVJ+= kernelJ
                else:
                    if i == 0: 
                        kernel   = kernelJ
                        kernelRV = kernel
                        kernelRVJ= kernelJ
                    else:
                        kernel+= kernelJ
                        kernelRVJ+= kernelJ
                    
            if not RV.Jitter:     # dummy jitter kernel with very small sigma
                sigma  = np.exp(-15.)
                kernelJ= celerite.terms.JitterTerm(log_sigma=np.log(sigma),bounds=dict(log_sigma=(-15.01,-15.)))
                kernelJ.freeze_parameter("log_sigma")
                if RV.GP != 'NONE':
                    if i == 0: 
                        kernel   = kernelRV + kernelJ
                        kernelRV = kernel
                        kernelRVJ= kernelJ
                    else:
                        kernel+= kernelJ
                        kernelRVJ+= kernelJ
                else:
                    if i == 0: 
                        kernel   = kernelJ
                        kernelRV = kernel
                        kernelRVJ= kernelJ
                    else:
                        kernel+= kernelJ
                        kernelRVJ+= kernelJ
        if RVList != []:
            KernelList.append(kernel) 
            KernelListJ.append(kernelRVJ) 

        # AC files
        KernelACList = []
        KernelACListJ= []
        for i,AC in enumerate(ACList):                                
            if AC.GP == 'SHO':
                S0  = AC.GPpar[0]
                w0  = 2.*np.pi/AC.GPpar[2]
                tau = AC.GPpar[1]
                Q   = 0.5*tau*w0
                kernelAC    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),
                                       log_omega0=np.log(w0),bounds=boundsgpAC[i])    
            if AC.GP == 'RVdriver':
                S0  = AC.GPpar[0]
                w0  = 2.*np.pi/AC.GPpar[2]
                tau = AC.GPpar[1]
                Q   = 0.5*tau*w0
                kernelAC    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),
                                   log_omega0=np.log(w0),bounds=boundsgpAC[i]) 
            if AC.GP == 'ROTdriver':
                S0  = AC.GPpar[0]
                w0  = 2.*np.pi/RV.GPpar[2]
                tau = RV.GPpar[1]
                Q   = 0.5*tau*w0
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),
                                       log_omega0=np.log(w0),bounds=boundsgpAC[i])
                kernel1.freeze_parameter("log_Q")
                kernel1.freeze_parameter("log_omega0")
                S0 = AC.GPpar[1]
                w0 = 2. * np.pi / RVList[0].GPpar[2] * 2.  # force to half period
                tau = RVList[0].GPpar[4]
                Q = 0.5 * tau * w0
                boundsgpAC[i]["log_omega0"] = (boundsgpAC[i]["log_omega0"][0], boundsgpAC[i]["log_omega0"][1] + np.log(2))
                kernel2  = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),
                                          log_omega0=np.log(w0), bounds=boundsgpAC[i])
                kernel2.freeze_parameter("log_Q")
                kernel2.freeze_parameter("log_omega0")

                kernelAC = kernel1 + kernel2
                #print("kernelAC",kernelAC.get_parameter_dict())
            if AC.GP == 'REAL':
                a   = AC.GPpar[0]
                c   = 1./AC.GPpar[1]
                kernelAC    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),
                                            bounds=self.boundsgpAC[i])
            if AC.Jitter:
                sigma = AC.Jitterpar
                kernel = celerite.terms.JitterTerm(log_sigma=np.log(sigma))#,bounds=boundJ)
                if AC.GP != 'NONE':
                    kernel = kernelAC + kernel
                KernelList.append(kernel)  
                KernelACList.append(kernel)
                KernelListJ.append(kernel)  
                KernelACListJ.append(kernel)  
            if not AC.Jitter:     # dummy jitter kernel with very small sigma
                sigma  = np.exp(-15.)
                kernel = celerite.terms.JitterTerm(log_sigma=np.log(sigma))#,bounds=boundJ)
                kernel.freeze_parameter("log_sigma")
                if AC.GP != 'NONE':
                    kernel = kernelAC + kernel
                KernelList.append(kernel) 
                KernelACList.append(kernel)
                KernelListJ.append(kernel)  
                KernelACListJ.append(kernel)  
        if ACList != []:
            kernelAC = kernel
                    
        # TR files
        KernelTRList = []
        KernelTRListJ= []
        count = 0
        for i,TR in enumerate(TRList):
            if TR.GP == 'ROT':
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                #print(TR.GPpar)
                S0  = TR.GPpar[0]
                w0  = 2.*np.pi/TR.GPpar[2]
                tau = TR.GPpar[1]
                Q   = 0.5*tau*w0
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                S0  = TR.GPpar[3]
                w0  = 2.*np.pi/TR.GPpar[2]*2. # force to half period
                tau = TR.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_omega0")
                S0  = TR.GPpar[5]
                w0  = 2.*np.pi/TR.GPpar[2]*3. # force to half period
                tau = TR.GPpar[6]
                Q   = 0.5*tau*w0
                kernelTR = kernel1+kernel2
            if TR.GP == 'ROT3':
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                #print(TR.GPpar)
                S0  = TR.GPpar[0]
                w0  = 2.*np.pi/TR.GPpar[2]
                tau = TR.GPpar[1]
                Q   = 0.5*tau*w0
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                S0  = TR.GPpar[3]
                w0  = 2.*np.pi/TR.GPpar[2]*2. # force to half period
                tau = TR.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_omega0")
                S0  = TR.GPpar[5]
                w0  = 2.*np.pi/TR.GPpar[2]*3. # force to half period
                tau = TR.GPpar[6]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(3),
                                               boundsgpTR[i]["log_omega0"][1]+np.log(3))
                kernel3    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel3.freeze_parameter("log_omega0")
                kernelTR = kernel1+kernel2+kernel3
            if TR.GP == 'ROTFLARE_RV_lead':
                count += 1
                self.RFl_ind = i
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                #print(TR.GPpar)
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TR.GPpar[0]
                w0  = 2.*np.pi/RV.GPpar[2]
                tau = RV.GPpar[1]
                Q   = 0.5*tau*w0
                #print(np.log(S0),np.log(Q),np.log(w0),i)
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                kernel1.freeze_parameter("log_Q")
                kernel1.freeze_parameter("log_omega0")
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TR.GPpar[1]
                w0  = 2.*np.pi/RV.GPpar[2]*2. # force to half period
                tau = RV.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                #print(np.log(S0),np.log(Q),np.log(w0),boundsgpTR_c[i])
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_Q")
                kernel2.freeze_parameter("log_omega0")
                a   = TR.GPpar[2]
                c   = 1./TR.GPpar[3]
                #print(i,a,c,np.log(a),np.log(c),boundsgpTR[i])
                kernel3    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),\
                                            bounds=boundsgpTR[i])
                #kernel3.freeze_parameter("log_a")
                #kernel3.freeze_parameter("log_c")
                kernelTR = kernel1+kernel2+kernel3
            if TR.GP == 'ROTFLARE_RV':
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TRList[self.RFl_ind].GPpar[0]
                w0  = 2.*np.pi/RV.GPpar[2]
                tau = RV.GPpar[1]
                Q   = 0.5*tau*w0
                #print(np.log(S0),np.log(Q),np.log(w0),i)
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                kernel1.freeze_parameter("log_S0")
                kernel1.freeze_parameter("log_Q")
                kernel1.freeze_parameter("log_omega0")
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TRList[self.RFl_ind].GPpar[1]
                w0  = 2.*np.pi/RV.GPpar[2]*2. # force to half period
                tau = RV.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_S0")
                kernel2.freeze_parameter("log_Q")
                kernel2.freeze_parameter("log_omega0")
                # use RV.GPar to synchronize RV and TR kernels
                a   = TRList[self.RFl_ind].GPpar[2]  #RV.GPpar[5]#
                c   = 1./TRList[self.RFl_ind].GPpar[3] #RV.GPpar[6]   #
                #print(i,a,c,np.log(a),np.log(c),boundsgpTR[i])
                kernel3    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),\
                                            bounds=boundsgpTR[i])
                kernel3.freeze_parameter("log_a")
                kernel3.freeze_parameter("log_c")
                kernelTR = kernel1+kernel2+kernel3
            if TR.GP == 'ROT_RV_lead':
                count += 1
                self.Rl_ind = i
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                #print(TR.GPpar)
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TR.GPpar[0]
                w0  = 2.*np.pi/RV.GPpar[2]
                tau = RV.GPpar[1]
                Q   = 0.5*tau*w0
                #print(np.log(S0),np.log(Q),np.log(w0),i)
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                kernel1.freeze_parameter("log_Q")
                kernel1.freeze_parameter("log_omega0")
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TR.GPpar[1]
                w0  = 2.*np.pi/RV.GPpar[2]*2. # force to half period
                tau = RV.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                #print(np.log(S0),np.log(Q),np.log(w0),boundsgpTR_c[i])
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_Q")
                kernel2.freeze_parameter("log_omega0")
                kernelTR = kernel1+kernel2
            if TR.GP == 'ROT_RV':
                boundsgpTR_c = copy.deepcopy(boundsgpTR)
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TRList[self.Rl_ind].GPpar[0]
                w0  = 2.*np.pi/RV.GPpar[2]
                tau = RV.GPpar[1]
                Q   = 0.5*tau*w0
                #print(np.log(S0),np.log(Q),np.log(w0),i)
                kernel1    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR[i])
                kernel1.freeze_parameter("log_S0")
                kernel1.freeze_parameter("log_Q")
                kernel1.freeze_parameter("log_omega0")
                # use RV.GPar to synchronize RV and TR kernels
                S0  = TRList[self.Rl_ind].GPpar[1]
                w0  = 2.*np.pi/RV.GPpar[2]*2. # force to half period
                tau = RV.GPpar[4]
                Q   = 0.5*tau*w0
                boundsgpTR_c[i]["log_omega0"] = (boundsgpTR[i]["log_omega0"][0]+np.log(2),boundsgpTR[i]["log_omega0"][1]+np.log(2))
                kernel2    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),\
                                       log_omega0=np.log(w0),bounds=boundsgpTR_c[i])
                kernel2.freeze_parameter("log_S0")
                kernel2.freeze_parameter("log_Q")
                kernel2.freeze_parameter("log_omega0")
                kernelTR = kernel1+kernel2
            if TR.GP == 'SHO':
                S0  = TR.GPpar[0]
                w0  = 2.*np.pi/TR.GPpar[2]
                tau = TR.GPpar[1]
                Q   = 0.5*tau*w0
                # print("boundsGP TR",TR.boundsGP)
                kernelTR    = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q),
                                       log_omega0=np.log(w0),bounds=TR.boundsGP)    
            if TR.GP == 'REAL':
                a   = TR.GPpar[0]
                c   = 1./TR.GPpar[1]
                kernelTR    = terms.RealTerm(log_a=np.log(a), log_c=np.log(c),\
                                            bounds=TR.boundsGP)
            if TR.Jitter:
                sigma = TR.Jitterpar
                kernelJ = celerite.terms.JitterTerm(log_sigma=np.log(sigma),bounds=TR.boundsJ)
                if TR.GP == 'NONE':
                    kernel = kernelJ
                else:
                    if count > 1:
                        kernel = kernelJ
                    else:
                        kernel = kernelTR + kernelJ
                        
                KernelList.append(kernel)  
                KernelTRList.append(kernel)
                KernelTRListJ.append(kernelJ)  
            if not TR.Jitter:     # dummy jitter kernel with very small sigma
                sigma  = np.exp(-20.)
                #print("boundsJ TR",self.boundsJTR[i])
                kernelJ = celerite.terms.JitterTerm(log_sigma=np.log(sigma),bounds=dict(log_sigma=(-20.01,-20.)))
                kernelJ.freeze_parameter("log_sigma")
                if TR.GP == 'NONE':
                    kernel = kernelJ
                else:
                    if count > 1:
                        kernel = kernelJ
                    else:
                        kernel = kernelTR + kernelJ
                KernelList.append(kernel) 
                KernelTRList.append(kernel)
                KernelTRListJ.append(kernelJ)  
        if TRList != []:
            kernelTR = kernel
                
        kernelAll = KernelList[0]
        for i,Kernel in enumerate(KernelList[1:]):
            kernelAll += Kernel
        KernelList.append(kernelAll)
        self.KernelList = KernelList
        
        #print("")
        #print(KernelList)
        #print("")
        #print(KernelTRList)
        #stop

        # define GP
        self.gpAll = celerite.GP(kernelAll, mean=mean_modelRV, fit_mean=True)
            
        if RVList != []: 
            self.gpRV  = celerite.GP(kernelRV,  mean=mean_modelRV, fit_mean=True) 
            self.gpRVJ = celerite.GP(kernelRVJ, mean=mean_modelRV, fit_mean=True) 

        if ACList != []: 
            self.gpAC  = celerite.GP(kernelAC,  mean=mean_modelC, fit_mean=True) 

            self.gpACList = []
            self.gpACListJ= []
            for i,AC in enumerate(ACList):
                self.gpACList.append(celerite.GP(KernelACList[i],  mean=mean_modelC, fit_mean=True))
                self.gpACListJ.append(celerite.GP(KernelACListJ[i],  mean=mean_modelC, fit_mean=True))
                #print(KernelACList[i])

        if TRList != []: 
            #self.gpTR  = celerite.GP(kernelTR,  mean=mean_modelTR, fit_mean=True) 
    
            self.gpTRList = []
            self.gpTRListJ= []
            for i,TR in enumerate(TRList):
                self.gpTRList.append(celerite.GP(KernelTRList[i],  mean=mean_modelTR, fit_mean=True))
                self.gpTRListJ.append(celerite.GP(KernelTRListJ[i],  mean=mean_modelTR, fit_mean=True))
    ######################################################################
    def neg_log_like(self,params, y, gp):

        paras = Paras(self.PlanetList,self.RVList,self.ACList,self.TRList)


        global calc_rebound,calc_reboundTR
        global iRV,iTR,StabProb
        global ttvs
               
        nloglike = 0.
        

        Poff   = self.Poff
        Ppar   = self.Ppar
        Soff   = self.Soff
        Spar   = self.Spar

        GoffRV = self.GoffRV
        nGPRV  = self.nGPRV
        JoffRV = self.JoffRV
        OoffRV = self.OoffRV

        GoffAC = self.GoffAC
        nGPAC  = self.nGPAC
        JoffAC = self.JoffAC
        OoffAC = self.OoffAC

        GoffTR = self.GoffTR
        nGPTR  = self.nGPTR
        JoffTR = self.JoffTR
        OoffTR = self.OoffTR
        Oofft0TR = self.Oofft0TR
        
        linpos = self.linpos
        quadpos= self.quadpos
        ndeg   = self.ndeg
        
        nfRV       = paras.nfRV
        nfAC       = paras.nfAC
        nfTR       = paras.nfTR
        npTR       = paras.npTR

        calc_rebound = True
        for k,RV in enumerate(self.RVList):
            x = RV.time
            y = RV.value
            e = RV.error
    
            ind = np.arange(GoffRV,GoffRV+nGPRV)
            if RV.Jitter: ind = np.append(ind,JoffRV + k)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares))#[k]))

            #print(params,len(params))
            #print(ind,len(ind))
            #print(self.nflares)
            #print(params[ind])
            #print("RV",ind)
            #print(linpos)

            polcoeff = []
            if ndeg  == 2: polcoeff.append(params[quadpos])
            if ndeg  == 1: polcoeff.append(params[linpos])
            polcoeff.append(params[OoffRV+k])
            polynom  = np.poly1d(polcoeff)  
            
            #print("neg_log: ind",ind)
            #print("neg_log: ind",params[ind])           
            if self.RVList[0].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.RVList[0].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                #print("RV",self.gpRV.get_parameter_dict())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.RVList[0].GP == 'ROTFLARE':
                vector = np.zeros(len(params[ind]) + 1)
                vector[:5] = params[ind[:5]]
                vector[5] = params[ind[2]] + np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                # print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpRV.set_parameter_vector(params[ind])
            self.gpRV.compute(x, e)
            
            #print(self.gpRV.get_parameter_dict())
            #print(self.gpAll.get_parameter_vector())
            
            #plt.plot(x,y)
            #modell,_ = self.gpRV.predict(y,x)
            #plt.plot(x,modell)
            #plt.show()
            
            iRV=k
            nloglike += -self.gpRV.log_likelihood(y-polynom(x))
            #print(nloglike)
            # oder -gpRVJ
            calc_rebound = False
        
        Goff = GoffAC
        for k,AC in enumerate(self.ACList):
            x = AC.time
            y = AC.value
            e = AC.error
            if AC.GP == 'NONE':
                ind = np.arange(0)
            #print(AC.GP,ind)
            else:
                ind = np.arange(Goff,Goff+nGPAC[k])

            if AC.GP != 'NONE': Goff = Goff+nGPAC[k]

            #print(ind)
            if AC.Jitter: 
                ind = np.append(ind,JoffAC[k])
                Goff = Goff+1
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            #print("AC",ind)
            #print(params[ind])
            #stop

            polynom = np.poly1d(params[OoffAC+k])  
            
            if self.ACList[k].GP == 'RVdriver':
                # use kernel parameter from RV data sets to synchronize with AC kernel parameter
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[4:] = params[ind[1:]]
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpACList[k].set_parameter_vector(vector)
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.ACList[k].GP == 'ROTdriver':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                vector[6:] = params[ind[2:]]
                self.gpACList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                #print("AC",self.gpACList[k].get_parameter_dict())
                #print("ALL",gp.get_parameter_dict())
                self.gpACList[k].set_parameter_vector(vector)
                self.gpACList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpACList[k].set_parameter_vector(params[ind])

            self.gpACList[k].compute(x, e)
            #print("AC",self.gpACList[k].get_parameter_names())
            #print("AC",self.gpACList[k].get_parameter_dict(include_frozen=True))
            #stop
            nloglike += -AC.weight*self.gpACList[k].log_likelihood(y-polynom(x))
    
        #datatype ="Transit"
        Goff = GoffTR
        calc_reboundTR = True
        for k,TR in enumerate(self.TRList):
            x = TR.time
            y = TR.value
            e = TR.error
            
            if TR.GP == 'NONE':
                ind = np.arange(0)
            else:
                ind = np.arange(Goff,Goff+nGPTR[k])
                Goff = Goff+nGPTR[k]
            #ind = np.arange(GoffTR,GoffTR+nGPTR)
            if TR.Jitter: 
                ind = np.append(ind,JoffTR[k])
                Goff = Goff+1
            #print(ind)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            #print(ind)
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            #print(ind)
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            #print(ind)
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            #print(ind)
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            #print(ind)
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares)) ##[0]))
            
            #print(params,len(params))
            #print(ind)
            #print(k,self.flarepos,self.flarepos+self.nflares[k])
            #print("TR",ind,len(ind),Poff,Soff,Spar,OoffRV,OoffTR,OoffTR+npTR)
            #print("TR",self.gpTRList[k].get_parameter_dict())
            #print("All",self.gpAll.get_parameter_dict()[ind])
            #print(linpos)
            
            kk = TR.ParFileNr
            polynom = np.poly1d(params[OoffTR+kk]) 
            #polynom = np.poly1d(params[OoffTR+k]) 
                  
            if self.TRList[k].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV_lead':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV':
                vector = np.zeros(len(params[ind])+8)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6] = params[GoffTR + 2]
                vector[7] = params[GoffTR + 3]
                vector[8:]= params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_c')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_c')
            elif self.TRList[k].GP == 'ROT_RV_lead':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT_RV':
                vector = np.zeros(len(params[ind])+6)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:] = params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpTRList[k].set_parameter_vector(params[ind])

            iTR=k
            #self.gpTRList[k].set_parameter_vector(params[ind])
            self.gpTRList[k].compute(x, e)
            
            
            nloglike += -TR.weight*self.gpTRList[k].log_likelihood(y-polynom(x))
            #print(TR.weight,nloglike)
            calc_reboundTR = False
        #print(nloglike,StabProb,nloglike  + StabProb*10)
        stabPen = 0
        diff = 0
        if nfRV > 0:
            stabPen += len(ind)*np.log(nfRV)*StabProb
        if nfTR > 0:
            stabPen += len(ind)*np.log(nfTR)*StabProb
            if (ttvs != []) and usediff:
                diff = 1./(2.*1./60./24.)/(2.*1./60./24.)*np.sum(np.array(ttvs)**2) 
                if not np.isfinite(diff): diff = np.inf
            #print(diff)
        else:
            diff = 0.
            
        return nloglike - stabPen + diff
        #return diff
 
    def log_prior(self,params):
        #gaussian prior on a
        mu = 0
        sigma = 0.05
        a = params[self.Poff+2]#np.array([params[self.Poff+2],params[self.Poff+2+5],params[self.Poff+2+10]])
        lp = np.log(1.0/(np.sqrt(2*np.pi)*sigma))-0.5*(a-mu)**2/sigma**2
        a = params[self.Poff+2+5]#np.array([params[self.Poff+2],params[self.Poff+2+5],params[self.Poff+2+10]])
        lp += np.log(1.0/(np.sqrt(2*np.pi)*sigma))-0.5*(a-mu)**2/sigma**2
        #print(params[self.Poff+2],self.Poff+2)
        #print(np.log(1.0/(np.sqrt(2*np.pi)*sigma))-0.5*(a-mu)**2/sigma**2)
        return lp
    
    def log_probability(self,params):
        global calc_rebound, calc_reboundTR
        global iRV,iTR,StabProb
       
        paras = Paras(self.PlanetList,self.RVList,self.ACList,self.TRList)

        #global datatype    
           
        self.gpAll.set_parameter_vector(params)
        lp = self.gpAll.log_prior()
        #lp += self.log_prior(params)
        
        if not np.isfinite(lp):
            #print("Huston we have a problem")
            #print(params)
            return -np.inf

        loglike = lp
        
        Poff   = self.Poff
        Ppar   = self.Ppar
        Soff   = self.Soff
        Spar   = self.Spar

        GoffRV = self.GoffRV
        nGPRV  = self.nGPRV
        JoffRV = self.JoffRV
        OoffRV = self.OoffRV

        GoffAC = self.GoffAC
        nGPAC  = self.nGPAC
        JoffAC = self.JoffAC
        OoffAC = self.OoffAC

        GoffTR = self.GoffTR
        nGPTR  = self.nGPTR
        JoffTR = self.JoffTR
        OoffTR = self.OoffTR
        Oofft0TR = self.Oofft0TR
        
        linpos = self.linpos
        quadpos= self.quadpos
        ndeg   = self.ndeg
        
        nfRV       = paras.nfRV
        nfAC       = paras.nfAC
        nfTR       = paras.nfTR
        npTR       = paras.npTR
       
        #datatype ="Radial Velocity"
        calc_rebound = True
        for k,RV in enumerate(self.RVList):
            x = RV.time
            y = RV.value
            e = RV.error
    
            ind = np.arange(GoffRV,GoffRV+nGPRV)
            if RV.Jitter: ind = np.append(ind,JoffRV + k)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares)) ##[0]))

            #print(params[ind][3])
            #print("RV",ind)
            #print(linpos)

            polcoeff = []
            if ndeg  == 2: polcoeff.append(params[quadpos])
            if ndeg  == 1: polcoeff.append(params[linpos])
            polcoeff.append(params[OoffRV+k])
            polynom  = np.poly1d(polcoeff)  
            

            if self.RVList[0].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.RVList[0].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.RVList[0].GP == 'ROTFLARE':
                vector = np.zeros(len(params[ind]) + 1)
                vector[:5] = params[ind[:5]]
                vector[5] = params[ind[2]] + np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                # print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpRV.set_parameter_vector(params[ind])
            self.gpRV.compute(x, e)
            
            #print(self.gpRV.get_parameter_dict())
            #print(self.gpAll.get_parameter_vector())
            
            #plt.plot(x,y)
            #modell,_ = self.gpRV.predict(y,x)
            #plt.plot(x,modell)
            #plt.show()
            
            iRV=k
            loglike += self.gpRV.log_likelihood(y-polynom(x))
            #print(nloglike)
            # oder -gpRVJ
            calc_rebound = False
        
        Goff = GoffAC
        for k, AC in enumerate(self.ACList):
            x = AC.time
            y = AC.value
            e = AC.error
            if AC.GP == 'NONE':
                ind = np.arange(0)
            # print(AC.GP,ind)
            else:
                ind = np.arange(Goff, Goff + nGPAC[k])

            if AC.GP != 'NONE': Goff = Goff + nGPAC[k]

            # print(ind)
            if AC.Jitter:
                ind = np.append(ind, JoffAC[k])
                Goff = Goff + 1
            ind = np.append(ind, np.arange(OoffAC, OoffAC + nfAC))
            # print("AC",ind)
            # print(params[ind])
            # stop

            polynom = np.poly1d(params[OoffAC + k])

            if self.ACList[k].GP == 'RVdriver':
                # use kernel parameter fromRV data sets to synchronize with AC kernel parameter
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[4:] = params[ind[1:]]
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpACList[k].set_parameter_vector(vector)
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.ACList[k].GP == 'ROTdriver':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                vector[6:] = params[ind[2:]]
                self.gpACList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                #print("AC",self.gpACList[k].get_parameter_dict())
                #print("ALL",gp.get_parameter_dict())
                self.gpACList[k].set_parameter_vector(vector)
                self.gpACList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpACList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpACList[k].set_parameter_vector(params[ind])

            self.gpACList[k].compute(x, e)
            loglike += AC.weight*self.gpACList[k].log_likelihood(y-polynom(x))
    
        #datatype ="Transit"
        Goff = GoffTR
        calc_reboundTR = True
        for k,TR in enumerate(self.TRList):
            x = TR.time
            y = TR.value
            e = TR.error
            
            if TR.GP == 'NONE':
                ind = np.arange(0)
            else:
                ind = np.arange(Goff,Goff+nGPTR[k])
                Goff = Goff+nGPTR[k]
            #ind = np.arange(GoffTR,GoffTR+nGPTR)
            #print(ind)
            if TR.Jitter: 
                ind = np.append(ind,JoffTR[k])
                Goff = Goff+1
            #print(ind)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            #print(ind)
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            #print(ind)
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            #print(ind)
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            #print(ind)
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            #print(ind)
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares)) ##[0]))
            
            #print(params)
            #print("TR",ind)
            #print(linpos)

            kk = TR.ParFileNr
            polynom = np.poly1d(params[OoffTR+kk])
            #polynom = np.poly1d(params[OoffTR+k]) 
                  
            if self.TRList[k].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV_lead':
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV':
                vector = np.zeros(len(params[ind])+8)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6] = params[GoffTR + 2]
                vector[7] = params[GoffTR + 3]
                vector[8:]= params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_c')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_c')
            elif self.TRList[k].GP == 'ROT_RV_lead':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT_RV':
                vector = np.zeros(len(params[ind])+6)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:] = params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpTRList[k].set_parameter_vector(params[ind])

            iTR=k
            #self.gpTRList[k].set_parameter_vector(params[ind])
            self.gpTRList[k].compute(x, e)
            loglike += TR.weight*self.gpTRList[k].log_likelihood(y-polynom(x))
            #print(TR.weight,nloglike)
            calc_reboundTR = False
        #print(loglike,np.exp(StabProb),loglike  + len(ind)*np.log(nfRV)*StabProb,len(ind))
        stabPen = 0
        diff = 0
        if nfRV > 0:
            stabPen += len(ind)*np.log(nfRV)*StabProb
        if nfTR > 0:
            stabPen += len(ind)*np.log(nfTR)*StabProb
            if (ttvs != []) and usediff:
                diff = 1./(2.*1./60./24.)/(2.*1./60./24.)*np.sum(np.array(ttvs)**2) 
                if not np.isfinite(diff): diff = np.inf
        else:
            diff = 0.
            
        return loglike + stabPen -diff
        #return -diff
 
    def log_probability_RV(self,params):
        global calc_rebound
        global iRV,StabProb
    
        paras = Paras(self.PlanetList,self.RVList,self.ACList,self.TRList)

        #global datatype    

        #print("log_prob_RV",self.gpAll.get_parameter_names())
        #print("log_prob_RV",params)
        self.gpAll.set_parameter_vector(params)
        lp = self.gpAll.log_prior()
        
        if not np.isfinite(lp):
            #print("Huston we have a problem")
            #print(params)
            return -np.inf

        loglike = 0.
        
        Poff   = self.Poff
        Ppar   = self.Ppar
        Soff   = self.Soff
        Spar   = self.Spar

        GoffRV = self.GoffRV
        nGPRV  = self.nGPRV
        JoffRV = self.JoffRV
        OoffRV = self.OoffRV

        OoffAC = self.OoffAC

        OoffTR = self.OoffTR
        Oofft0TR = self.Oofft0TR
        
        linpos = self.linpos
        quadpos= self.quadpos
        ndeg   = self.ndeg
        
        nfRV       = paras.nfRV
        nfAC       = paras.nfAC
        nfTR       = paras.nfTR
        npTR       = paras.npTR

        
        #datatype ="Radial Velocity"
        calc_rebound = True
        for k,RV in enumerate(self.RVList):
            x = RV.time
            y = RV.value
            e = RV.error
    
            ind = np.arange(GoffRV,GoffRV+nGPRV)
            if RV.Jitter: ind = np.append(ind,JoffRV + k)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares)) ##[0]))

            #print(params)
            #print(ind)
            #print(linpos)

            polcoeff = []
            if ndeg  == 2: polcoeff.append(params[quadpos])
            if ndeg  == 1: polcoeff.append(params[linpos])
            polcoeff.append(params[OoffRV+k])
            polynom  = np.poly1d(polcoeff)  
            

            if self.RVList[0].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpRV.freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.RVList[0].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.RVList[0].GP == 'ROTFLARE':
                vector = np.zeros(len(params[ind]) + 1)
                vector[:5] = params[ind[:5]]
                vector[5] = params[ind[2]] + np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpRV.thaw_parameter('kernel:terms[1]:log_omega0')
                # print(gp.get_parameter_names())
                self.gpRV.set_parameter_vector(vector)
                self.gpRV.freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpRV.set_parameter_vector(params[ind])
            self.gpRV.compute(x, e)
            
            #print(self.gpRV.get_parameter_dict())
            #print(self.gpAll.get_parameter_vector())
            
            #plt.plot(x,y)
            #modell,_ = self.gpRV.predict(y,x)
            #plt.plot(x,modell)
            #plt.show()
            
            iRV=k
            loglike += self.gpRV.log_likelihood(y-polynom(x))
            #print(loglike)
            # oder -gpRVJ
            calc_rebound = False

        return loglike
    def log_probability_TR(self,params):
    
        global calc_reboundTR
        global iTR

        paras = Paras(self.PlanetList,self.RVList,self.ACList,self.TRList)

        #global datatype    
           
        self.gpAll.set_parameter_vector(params)
        lp = self.gpAll.log_prior()
        
        if not np.isfinite(lp):
            #print("Huston we have a problem")
            #print(params)
            return -np.inf

        loglike = 0.
        
        Poff   = self.Poff
        Ppar   = self.Ppar
        Soff   = self.Soff
        Spar   = self.Spar

        GoffRV = self.GoffRV
        nGPRV  = self.nGPRV
        JoffRV = self.JoffRV
        OoffRV = self.OoffRV

        GoffTR = self.GoffTR
        nGPTR  = self.nGPTR
        JoffTR = self.JoffTR
        OoffTR = self.OoffTR
        Oofft0TR = self.Oofft0TR

        OoffAC = self.OoffAC

        OoffTR = self.OoffTR
        
        linpos = self.linpos
        quadpos= self.quadpos
        ndeg   = self.ndeg
        
        nfRV       = paras.nfRV
        nfAC       = paras.nfAC
        nfTR       = paras.nfTR
        npTR       = paras.npTR

        Goff = GoffTR
        calc_reboundTR = True
        for k,TR in enumerate(self.TRList):
            x = TR.time
            y = TR.value
            e = TR.error
            
            if TR.GP == 'NONE':
                ind = np.arange(0)
            else:
                ind = np.arange(Goff,Goff+nGPTR[k])
                Goff = Goff+nGPTR[k]
            #ind = np.arange(GoffTR,GoffTR+nGPTR)
            #print(ind)
            if TR.Jitter: 
                ind = np.append(ind,JoffTR[k])
                Goff = Goff+1
            #print(ind)
            ind = np.append(ind,np.arange(Poff,Poff+Ppar))
            #print(ind)
            ind = np.append(ind,np.arange(Soff,Soff+Spar))
            #print(ind)
            ind = np.append(ind,np.arange(OoffRV,OoffRV+nfRV))
            #print(ind)
            ind = np.append(ind,np.arange(OoffAC,OoffAC+nfAC))
            #print(ind)
            ind = np.append(ind,np.arange(OoffTR,OoffTR+npTR))
            #ind = np.append(ind,np.arange(Oofft0TR,Oofft0TR+npTR-1))
            #ind = np.append(ind,np.arange(OoffTR,OoffTR+nfTR))
            #print(ind)
            if ndeg > 0: ind = np.append(ind,np.arange(linpos,linpos+ndeg))
            ind = np.append(ind,np.arange(self.flarepos,self.flarepos+self.nflares)) ##[0]))
            
            #print(params)
            #print("TR",ind)
            #print(linpos)

            kk = TR.ParFileNr
            polynom = np.poly1d(params[OoffTR+kk]) 
            #polynom = np.poly1d(params[OoffTR+k]) 
                  
            if self.TRList[k].GP == 'ROT':
                vector = np.zeros(len(params[ind])+1)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:] = params[ind[5:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT3':
                vector = np.zeros(len(params[ind])+2)
                vector[:5] = params[ind[:5]]
                vector[5]  = params[ind[2]]+np.log(2)
                vector[6:8] = params[ind[5:7]]
                vector[8] = params[ind[2]] + np.log(3)
                vector[9:] = params[ind[7:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_omega0')
                #print(gp.get_parameter_names())
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV_lead':
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROTFLARE_RV':
                vector = np.zeros(len(params[ind])+8)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6] = params[GoffTR + 2]
                vector[7] = params[GoffTR + 3]
                vector[8:]= params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].thaw_parameter('kernel:terms[2]:log_c')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_a')
                self.gpTRList[k].freeze_parameter('kernel:terms[2]:log_c')
            elif self.TRList[k].GP == 'ROT_RV_lead':
                vector = np.zeros(len(params[ind])+4)
                vector[0] = params[ind[0]]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3]  = params[GoffRV+1:GoffRV+3]
                vector[3] = params[ind[1]]
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:]= params[ind[2:]]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            elif self.TRList[k].GP == 'ROT_RV':
                vector = np.zeros(len(params[ind])+6)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[0] = params[GoffTR + 0]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[1:3] = params[GoffRV+1:GoffRV+3]
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[3] = params[GoffTR + 1]
                # use kernel parameter from RV data sets to synchronize with TR kernel parameter
                vector[4] = params[GoffRV + 4]
                vector[5] = params[GoffRV + 2] + np.log(2)
                # use kernel parameter from first TR data sets to synchronize with TR kernel parameter
                vector[6:] = params[ind]
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].thaw_parameter('kernel:terms[1]:log_omega0')
                self.gpTRList[k].set_parameter_vector(vector)
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[0]:log_omega0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_S0')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_Q')
                self.gpTRList[k].freeze_parameter('kernel:terms[1]:log_omega0')
            else:
                self.gpTRList[k].set_parameter_vector(params[ind])

            iTR=k
            ##params[self.gpTRList[k].set_parameter_vector(params[ind])
            self.gpTRList[k].compute(x, e)
            loglike += TR.weight*self.gpTRList[k].log_likelihood(y-polynom(x))
            #print(TR.weight,nloglike)
            calc_reboundTR = False
        if nfTR > 0:
            if (ttvs != []) and usediff:
                diff = 1./(2.*1./60./24.)/(2.*1./60./24.)*np.sum(np.array(ttvs)**2)
                if not np.isfinite(diff): diff = np.inf
                #print(diff)
        else:
            diff = 0.
        return loglike #- diff
        #return -diff
    
############################################################################################################
    def plot_input(self,savename,output_dir,RVstart,ACstart,TRstart,xsiz=8,ysiz=5,ysizGLS=6,t0corr=0):
        
        print("")
        print("")
        print("showing input data without start model")
        print("")
        
            
        for DATA in self.DATAList:
            label  = " input"
            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+".pdf")
            xlabel = 'Time (BJD -'+str(self.DATAList[0].t0)+')'
            if isinstance(DATA, RVTimeSeries): 
                ylabel = 'RV (m/s)'
            if isinstance(DATA, ACTimeSeries): 
                ylabel = 'Activity Index'
            if isinstance(DATA, TRTimeSeries): 
                ylabel = 'Rel. Flux'
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysiz)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.errorbar(DATA.time,DATA.value,DATA.error,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
            
            glsout = gls.Gls((DATA.time,DATA.value,DATA.error), verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
            FAP01   = glsout.powerLevel(0.01)
            FAP001  = glsout.powerLevel(0.001)
            FAP0001 = glsout.powerLevel(0.0001)

            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_gls_f.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.plot(glsout.freq,glsout.power,color='cornflowerblue',label=DATA.name+label)
            plt.plot([DATA.fbeg,DATA.fend],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.plot([DATA.fbeg,DATA.fend],[FAP001,FAP001],':',color='cornflowerblue')
            plt.plot(np.linspace(DATA.fbeg,DATA.fend,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Frequency (1/d)')
            plt.ylabel('Power')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf())
            plt.show()
            pp.close()

            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_gls_p.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.semilogx(1./glsout.freq,glsout.power,color='cornflowerblue',label=DATA.name+label)
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP001,FAP001],':',color='cornflowerblue')
            plt.semilogx(np.linspace(1./DATA.fend,1./DATA.fbeg,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Period (d)')
            plt.ylabel('Power')
            plt.grid(which='both',axis='x')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
            
            '''output = cs.basis_pursuit(np.copy(DATA.time),np.copy(DATA.value),nfreqs=5000,fmin=DATA.fbeg,fmax=DATA.fend,polyorder=2)
            power = (output['sines']**2 + output['cosines']**2)
            freqs = output['freqs']
            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_L1_p.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.semilogx(1./freqs,np.sqrt(power))
            plt.xlabel('Period (d)')
            plt.ylabel('Power')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()
            '''
            
        if self.nfRV > 1:
            pp = PdfPages(output_dir+savename+"_All_"+label+".pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysiz)
            for i,DATA in enumerate(self.DATAList):
                if isinstance(DATA, RVTimeSeries):
                    DATARV = DATA
                    ylabel = 'RV (m/s)'
                    inds = np.where(DATA.All_index == i-RVstart)
                    plt.errorbar(DATA.All_time[inds],DATA.All_value[inds],DATA.All_error[inds],fmt='o',
                                 mec=DATA.color,ecolor=DATA.color, \
                                 elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                                 label=DATA.name+label, zorder=1)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend(fontsize='xx-small')
            plt.show()
            pp.savefig(plt.gcf()) # This generates pdf page
            pp.close()
            
            glsout = gls.Gls((DATARV.All_time,DATARV.All_value,DATARV.All_error), verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
            FAP01   = glsout.powerLevel(0.01)
            FAP001  = glsout.powerLevel(0.001)
            FAP0001 = glsout.powerLevel(0.0001)

            pp = PdfPages(output_dir+savename+"_All_"+label+"_gls_f.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.plot(glsout.freq,glsout.power,color='cornflowerblue',label="All RV"+label)
            plt.plot([DATA.fbeg,DATA.fend],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.plot([DATA.fbeg,DATA.fend],[FAP001,FAP001],':',color='cornflowerblue')
            plt.plot(np.linspace(DATA.fbeg,DATA.fend,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Frequency (1/d)')
            plt.ylabel('Power')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf())
            plt.show()
            pp.close()
            stop

            pp = PdfPages(output_dir+savename+"_All_"+label+"_gls_p.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.semilogx(1./glsout.freq,glsout.power,color='cornflowerblue',label="All RV"+label)
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP001,FAP001],':',color='cornflowerblue')
            plt.semilogx(np.linspace(1./DATA.fend,1./DATA.fbeg,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Period (d)')
            plt.ylabel('Power')
            plt.grid(which='both',axis='x')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()
            pp.close()

        print("")
        print("showing input data without start model phase folded")
        print("")
        
        for Plan in self.PlanetList:
            for DATA in self.DATAList:
                label  = ' input'
                pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+".pdf")
                xlabel = 'Phase (d)'
                if isinstance(DATA, RVTimeSeries): 
                    ylabel = 'RV (m/s)'
                if isinstance(DATA, ACTimeSeries): 
                    continue
                if isinstance(DATA, TRTimeSeries): 
                    ylabel = 'Rel. Flux'
                fig = plt.figure()
                fig.set_size_inches(xsiz,ysiz+self.nfRV*0.5)
                plt.xlabel(xlabel)
                plt.ylabel(ylabel)
                plt.errorbar(DATA.time%Plan.P,DATA.value,DATA.error,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                                 elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                                 label=DATA.name+label, zorder=1)
                plt.legend(fontsize='xx-small')
                plt.show()
                pp.savefig(plt.gcf()) # This generates pdf page
                pp.close()

        for DATA in self.DATAList:
            label  = ' input'
            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+".pdf")
            xlabel = 'Time (BJD -'+str(self.DATAList[0].t0-t0corr)+')'
            if isinstance(DATA, RVTimeSeries): 
                ylabel = 'RV (m/s)'
            if isinstance(DATA, ACTimeSeries): 
                ylabel = 'Activity Index'
            if isinstance(DATA, TRTimeSeries): 
                ylabel = 'Rel. Flux'
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysiz)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.errorbar(DATA.time+t0corr,DATA.value,DATA.error,fmt='o',mec=DATA.color,ecolor=DATA.color, \
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
            plt.legend(fontsize='xx-small')
            plt.show()
            pp.savefig(plt.gcf()) # This generates pdf page
            pp.close()
            
            gc.collect()

    ###########################################################################
    def plot_result(self, timesRV, timesTR, t0corr,
                    savename, output_dir,
                    RVstart, ACstart, TRstart, mod, label, minPer, showAll,
                    xsiz=8, ysiz=5, ysizGLS=6):
        global iRV,iTR
        global calc_rebound,calc_reboundTR
        global RVListe
        global vzp

        print("")
        print("")
        print("showing input data with model")
        print("")
        
        try:
            os.stat(output_dir)
        except:
            os.mkdir(output_dir)

        # correct errors by jitter, subtract polynomial   
        All_errorRV = []        
        All_valueRV = []        
        All_timeRV  = []        
        All_errorTR = []        
        All_valueTR = []        
        All_timeTR  = [] 
        # correct for polynomial background and jitter
        for i,DATA in enumerate(self.DATAList):
            if isinstance(DATA, RVTimeSeries): 
                if DATA.Jitter:
                    error = np.sqrt(DATA.error**2 + np.exp(mod.gpAll.get_parameter_vector()[mod.JoffRV+i])**2)
                else:
                    error = DATA.error
                DATA.set_error_c(error)
                All_errorRV = np.append(All_errorRV,error)
                polcoeff = []
                params   = mod.gpAll.get_parameter_vector()
                if self.ndeg  == 2: polcoeff.append(params[self.quadpos])
                if self.ndeg  == 1: polcoeff.append(params[self.linpos])
                polcoeff.append(params[self.OoffRV+i-RVstart])
                #print(polcoeff,self.OoffRV+i-RVstart)
                polynom  = np.poly1d(polcoeff)  
                
                DATA.set_value_c(DATA.value-polynom(DATA.time))
                #print(polynom(DATA.time))
                All_valueRV = np.append(All_valueRV,DATA.value-polynom(DATA.time))
                All_timeRV  = np.append(All_timeRV,DATA.time)
                
            if isinstance(DATA, ACTimeSeries): 
                if DATA.Jitter:
                    error = np.sqrt(DATA.error**2 + np.exp(mod.gpAll.get_parameter_vector()[mod.JoffAC[i-ACstart]])**2)
                else:
                    error = DATA.error
                DATA.set_error_c(error)
                polcoeff = []
                params   = mod.gpAll.get_parameter_vector()
                polcoeff.append(params[self.OoffAC+i-ACstart])
                polynom  = np.poly1d(polcoeff)  
                DATA.set_value_c(DATA.value-polynom(DATA.time))
            if isinstance(DATA,TRTimeSeries):
                if DATA.Jitter:
                    error = np.sqrt(DATA.error**2 + np.exp(mod.gpAll.get_parameter_vector()[mod.JoffTR[i-TRstart]])**2)
                else:
                    error = DATA.error
                DATA.set_error_c(error)
                All_errorTR = np.append(All_errorTR,error)
                
                polcoeff = []
                params   = mod.gpAll.get_parameter_vector()
                kk = DATA.ParFileNr
                polcoeff.append(params[self.OoffTR+kk])  # instead of i
                #print("plotcoeff",polcoeff,self.OoffTR+kk,kk,i)
                
                polynom  = np.poly1d(polcoeff)  
                
                
                DATA.set_value_c(DATA.value-polynom(DATA.time))
                
                All_valueTR = np.append(All_valueTR,DATA.value-polynom(DATA.time))
                All_timeTR  = np.append(All_timeTR,DATA.time)
                ascii.write([],output_dir+savename+"_"+DATA.name+"_"+label+".dat", fast_writer=False, overwrite = True)
                np.savetxt(output_dir+savename+"_"+DATA.name+"_"+label+".dat",list(zip(DATA.time,DATA.value_c,DATA.error_c)),fmt='%15.6f')

        if All_timeRV != []:
            inds = np.argsort(All_timeRV)
            All_timeRV  = All_timeRV[inds]
            All_valueRV = All_valueRV[inds]
            All_errorRV = All_errorRV[inds]
        if All_timeTR != []:
            inds = np.argsort(All_timeTR)
            All_timeTR  = All_timeTR[inds]
            All_valueTR = All_valueTR[inds]
            All_errorTR = All_errorTR[inds]

        # plot as function of time 
        count1 = 0
        for i,DATA in enumerate(self.DATAList):
            pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_model.pdf")
            xlabel = 'Time (BJD -'+str(self.DATAList[0].t0-t0corr)+')'
            fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz))
            ax2.set_xlabel(xlabel)
            ax2.set_ylabel('Residuals')
            if isinstance(DATA, RVTimeSeries): 
                ylabel = 'RV (m/s)'
                ax.set_ylabel(ylabel)
                if i-RVstart == 0:
                    iRV = -1
                    calc_rebound = True
                    mod.gpRVJ.get_parameter_vector()
                    mod.gpRVJ.compute(All_timeRV,All_errorRV)
                    RVJmodD,_ = mod.gpRVJ.predict(All_valueRV,All_timeRV,return_var=True)
                    iRV = -1
                    calc_rebound = True
                    mod.gpRVJ.get_parameter_vector()
                    RVJmodT,_ = mod.gpRVJ.predict(All_valueRV,timesRV,return_var=True)
                    iRV = -1
                    calc_rebound = True
                    mod.gpRV.get_parameter_vector()
                    mod.gpRV.compute(All_timeRV,All_errorRV)
                    RVmodD,_ = mod.gpRV.predict(All_valueRV,All_timeRV,return_var=True)
                    iRV = -1
                    calc_rebound = True
                    mod.gpRV.get_parameter_vector()
                    RVmodT,varT = mod.gpRV.predict(All_valueRV,timesRV,return_var=True)
                    
                inds = np.where(DATA.All_index == i-RVstart)
                ax.errorbar(All_timeRV[inds]+t0corr,All_valueRV[inds],All_errorRV[inds],
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label, zorder=3)
                # in case of GP corrected data
                #ax.errorbar(All_timeRV[inds]+t0corr,All_valueRV[inds]-RVmodD[inds]+RVJmodD[inds],All_errorRV[inds],
                #         fmt='o',mec=DATA.color,ecolor=DATA.color,
                #         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                #         label=DATA.name+label, zorder=3)
                #ax.errorbar(DATA.All_time[inds]+t0corr,DATA.All_value[inds],fmt='o',mec='slategrey',
                #         elinewidth=2, mfc = 'white', ms = 7, alpha=0.3, 
                #         label=DATA.name+label, zorder=1)
                ax.fill_between(timesRV+t0corr,RVmodT-varT,RVmodT+varT,color='k',\
                                 alpha=0.3, edgecolor="none",zorder=1)
                ax.plot(timesRV+t0corr,RVmodT,color='k',alpha=0.8,zorder=2)
                ax.plot(timesRV+t0corr,RVmodT,color='k',alpha=0.2,zorder=4)
                ax2.errorbar(All_timeRV[inds]+t0corr,All_valueRV[inds]-RVmodD[inds],All_errorRV[inds],
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label, zorder=1)
                np.savetxt(output_dir+savename+"_"+DATA.name+"_"+label+".res",list(zip(All_timeRV[inds],All_valueRV[inds]-RVmodD[inds],All_errorRV[inds])),fmt='%15.6f')
                ax.legend(fontsize='xx-small')
                plt.xlim(np.min(All_timeRV[inds]+t0corr)-0.01*(np.max(All_timeRV[inds])-np.min(All_timeRV[inds])),np.max(All_timeRV[inds]+t0corr)+0.01*(np.max(All_timeRV[inds])-np.min(All_timeRV[inds])))
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()
                
                glsout = gls.Gls((All_timeRV[inds],All_valueRV[inds],All_errorRV[inds]), 
                                 verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
                glsoutR = gls.Gls((All_timeRV[inds],All_valueRV[inds]-RVmodD[inds],All_errorRV[inds]), 
                                  verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
                FAP01   = glsout.powerLevel(0.01)
                FAP001  = glsout.powerLevel(0.001)
                FAP0001 = glsout.powerLevel(0.0001)
    
                pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_gls_f.pdf")
                plt.figure()
                # plt.set_size_inches(xsiz,ysizGLS)
                plt.plot(glsout.freq,glsout.power,color='cornflowerblue',label=DATA.name+label)
                plt.plot(glsoutR.freq,glsoutR.power,color='darkorange',label=DATA.name+label+' res')
                plt.plot([DATA.fbeg,DATA.fend],[FAP01,FAP01],'--',color='cornflowerblue')
                plt.plot([DATA.fbeg,DATA.fend],[FAP001,FAP001],':',color='cornflowerblue')
                plt.plot(np.linspace(DATA.fbeg,DATA.fend,20),[FAP0001]*20,'-.',color='cornflowerblue')
                plt.xlabel('Frequency (1/d)')
                plt.ylabel('Power')
                plt.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                plt.close()
                pp.close()
                plt.clf()
    
                pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_gls_p.pdf")
                fig = plt.figure()
                fig.set_size_inches(xsiz,ysizGLS)
                plt.semilogx(1./glsout.freq,glsout.power,color='cornflowerblue',label=DATA.name+label)
                plt.semilogx(1./glsoutR.freq,glsoutR.power,color='darkorange',label=DATA.name+label+' res')
                plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP01,FAP01],'--',color='cornflowerblue')
                plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP001,FAP001],':',color='cornflowerblue')
                plt.semilogx(np.linspace(1./DATA.fend,1./DATA.fbeg,20),[FAP0001]*20,'-.',color='cornflowerblue')
                plt.xlabel('Period (d)')
                plt.ylabel('Power')
                plt.grid(which='both',axis='x')
                plt.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()
                            
                
                '''
                # L1 Periodogram, seems to impact the modeling....
                output = cs.basis_pursuit(np.copy(DATA.time),np.copy(DATA.value),nfreqs=5000,fmin=DATA.fbeg,fmax=DATA.fend,polyorder=2)
                power = (output['sines']**2 + output['cosines']**2)
                freqs = output['freqs']
                pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_L1_p.pdf")
                fig = plt.figure()
                fig.set_size_inches(xsiz,ysizGLS)
                plt.semilogx(1./freqs,np.sqrt(power))
                plt.xlabel('Period (d)')
                plt.ylabel('Power')
                plt.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                plt.show()
                pp.close()
                '''
                
                # phase folded
                ylabel = 'RV (m/s)'
                xlabel = 'Phase (d)'
                dynPlan = 0
                for k,Plan in enumerate(self.PlanetListResult):
                    ind = list(range(len(self.PlanetListResult)))
                    ind.remove(k)
                    KEmodR  = np.zeros(len(timesRV))
                    if len(self.PlanetListResult) > 1:
                        for j in ind:
                            if Plan.orbit == 'Dynamic' or Plan.orbit == 'TTV':
                               KEmodR  += vzp[:,j]*timeconv       
                               dynPlan += 1
                            else:
                               KEmodR  += self.PlanetListResult[j].Kepler(t=timesRV)
                    isort = np.argsort(timesRV%Plan.P)
                    
                    pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_model_phase"+str(np.round(Plan.P,2))+".pdf")
                    fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                        gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz))
                    ax.set_ylabel(ylabel)
                    ax2.set_xlabel(xlabel)
                    ax2.set_ylabel('Residuals')
                    if DATA.GP != 'NONE': addLab = ' GP corr.' 
                    else: addLab = ''
                    ax.errorbar(All_timeRV[inds]%Plan.P,All_valueRV[inds]-RVmodD[inds]+RVJmodD[inds]\
                                -np.interp(All_timeRV,timesRV,KEmodR)[inds],All_errorRV[inds],
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label+addLab, zorder=3)
                    ax.errorbar(All_timeRV[inds]%Plan.P,All_valueRV[inds]\
                                -np.interp(All_timeRV,timesRV,KEmodR)[inds],All_errorRV[inds],
                                fmt='o',mec='dimgrey',ecolor='dimgrey',
                                elinewidth=2, mfc = 'white', ms = 3, alpha=0.5, 
                                label=DATA.name+label, zorder=1)
                    ax.plot((timesRV%Plan.P)[isort],(RVJmodT-KEmodR)[isort],'k')
                    ax2.errorbar(All_timeRV[inds]%Plan.P,
                                 All_valueRV[inds]-RVmodD[inds],
                                 All_errorRV[inds],
                                 fmt='o',mec=DATA.color,ecolor=DATA.color,
                                 elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                                 label=DATA.name+label, zorder=1)
                    ax.legend(fontsize='xx-small')
                    #plt.xlim(0,90)
                    pp.savefig(plt.gcf()) # This generates pdf page
                    if noShow:
                        plt.ioff()
                    else:
                        # pp.savefig(plt.gcf()) # This generates pdf page
                        plt.show()
                    pp.close()
                    plt.close()
                    plt.clf()
            
            if isinstance(DATA, ACTimeSeries): 
                ylabel = 'Activity Index'
                ylabel = 'Activity Index'
                iAC = i-ACstart
                mod.gpACListJ[i-ACstart].get_parameter_vector()
                mod.gpACListJ[i-ACstart].compute(DATA.time,DATA.error_c)
                ACJmodD,_ = mod.gpACListJ[i-ACstart].predict(DATA.value_c,DATA.time,return_var=True)
                iAC = i-ACstart
                mod.gpACList[i-ACstart].get_parameter_vector()
                mod.gpACList[i-ACstart].compute(DATA.time,DATA.error_c)
                ACmodD,_ = mod.gpACList[i-ACstart].predict(DATA.value_c,DATA.time,return_var=True)
                iAC = i-ACstart
                mod.gpACList[i-ACstart].get_parameter_vector()
                ACmodT,varTAC = mod.gpACList[i-ACstart].predict(DATA.value_c,DATA.time,return_var=True)
                    
                ax.set_ylabel(ylabel)
                ax2.set_xlabel(xlabel)
                ax2.set_ylabel('Residuals')
                ax.errorbar(DATA.time+t0corr,DATA.value_c-ACmodD+ACJmodD,DATA.error_c,
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label+" GP corr.", zorder=3)
                #ax.errorbar(DATA.time,DATA.value_c,fmt='.',mec='red',
                #         elinewidth=2, mfc = 'white', ms = 7, alpha=0.3, 
                #         label=DATA.name+label, zorder=1)
                ax.fill_between(DATA.time+t0corr,ACmodT-ACmodD+ACJmodD-varTAC,ACmodT-ACmodD+ACJmodD+varTAC,color='k',\
                                 alpha=0.3, edgecolor="none",zorder=1)
                #ax.plot(DATA.time,ACJmodD,'.r',zorder=9)
                ax.plot(DATA.time+t0corr,ACmodT-ACmodD+ACJmodD,color='k',alpha=0.8,zorder=2)
                ax.plot(DATA.time+t0corr,ACmodT-ACmodD+ACJmodD,color='k',alpha=0.2,zorder=5)
                ax2.errorbar(DATA.time+t0corr,DATA.value_c-ACmodD,DATA.error_c,
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label, zorder=1)

                ax.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()
                    
                glsout = gls.Gls((DATA.time+t0corr, DATA.value_c-ACmodD+ACJmodD,DATA.error_c),
                                 verbose=True, fbeg=DATA.fbeg, fend=DATA.fend)
                glsoutR = gls.Gls((DATA.time+t0corr, DATA.value_c-ACmodD,DATA.error_c),
                                  verbose=True, fbeg=DATA.fbeg, fend=DATA.fend)
                FAP01 = glsout.powerLevel(0.01)
                FAP001 = glsout.powerLevel(0.001)
                FAP0001 = glsout.powerLevel(0.0001)

                pp = PdfPages(output_dir + savename + "_" + DATA.name + "_" + label + "_gls_f.pdf")
                fig = plt.figure()
                fig.set_size_inches(xsiz, ysizGLS)
                plt.plot(glsout.freq, glsout.power, color='cornflowerblue', label=DATA.name + label)
                plt.plot(glsoutR.freq, glsoutR.power, color='darkorange', label=DATA.name + label+' res')
                plt.plot([DATA.fbeg, DATA.fend], [FAP01, FAP01], '--', color='cornflowerblue')
                plt.plot([DATA.fbeg, DATA.fend], [FAP001, FAP001], ':', color='cornflowerblue')
                plt.plot(np.linspace(DATA.fbeg, DATA.fend, 20), [FAP0001] * 20, '-.', color='cornflowerblue')
                plt.xlabel('Frequency (1/d)')
                plt.ylabel('Power')
                plt.legend(fontsize='xx-small')
                pp.savefig(plt.gcf())  # This generates pdf page
                if noShow:
                    plt.ioff()
                    plt.close()
                else:
                    plt.show()

                pp.close()

                pp = PdfPages(output_dir + savename + "_" + DATA.name + "_" + label + "_gls_p.pdf")
                fig = plt.figure()
                fig.set_size_inches(xsiz, ysizGLS)
                plt.semilogx(1. / glsout.freq, glsout.power, color='cornflowerblue', label=DATA.name + label)
                plt.semilogx(1. / glsoutR.freq, glsoutR.power, color='darkorange', label=DATA.name + label+' res')
                plt.semilogx([1. / DATA.fend, 1. / DATA.fbeg], [FAP01, FAP01], '--', color='cornflowerblue')
                plt.semilogx([1. / DATA.fend, 1. / DATA.fbeg], [FAP001, FAP001], ':', color='cornflowerblue')
                plt.semilogx(np.linspace(1. / DATA.fend, 1. / DATA.fbeg, 20), [FAP0001] * 20, '-.', color='cornflowerblue')
                plt.xlabel('Period (d)')
                plt.ylabel('Power')
                plt.grid(which='both',axis='x')
                plt.legend(fontsize='xx-small')
                pp.savefig(plt.gcf())  # This generates pdf page
                if noShow:
                    plt.ioff()
                    plt.close()
                else:
                    plt.show()
                pp.close()
                
            if isinstance(DATA, TRTimeSeries):
                ylabel = 'Rel. Flux'
                timesTR = np.linspace(np.min(DATA.time),np.max(DATA.time),numtTR)
                #print("file",DATA.name,i-TRstart,DATA.time[0])
                iTR = i-TRstart
                calc_reboundTR = True
                mod.gpTRListJ[iTR].get_parameter_vector()
                mod.gpTRListJ[iTR].compute(DATA.time,DATA.error_c)
                TRJmodD,_ = mod.gpTRListJ[iTR].predict(DATA.value_c,DATA.time,return_var=True)
                iTR = iTR
                calc_reboundTR = True
                TRJmodT,_ = mod.gpTRListJ[iTR].predict(DATA.value_c,timesTR,return_var=True)

                #if DATA.GP == "ROTFLARE_RV_lead": iTR = 0 
                calc_reboundTR = True

                mod.gpTRList[iTR].get_parameter_vector()
                mod.gpTRList[iTR].compute(DATA.time,DATA.error_c)
                TRmodD,_ = mod.gpTRList[iTR].predict(DATA.value_c,DATA.time,return_var=True)
                iTR = iTR
                calc_reboundTR = True
                #mod.gpTRList[iTR].get_parameter_vector()
                TRmodT,varTTR = mod.gpTRList[iTR].predict(DATA.value_c,timesTR,return_var=True)
                
                ax.set_ylabel(ylabel)
                ax2.set_xlabel(xlabel)
                ax2.set_ylabel('Residuals')
                ax.errorbar(DATA.time+t0corr,DATA.value_c,DATA.error_c,
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label, zorder=3)
                #ax.errorbar(DATA.time,DATA.value_c,fmt='.',mec='red',
                #         elinewidth=2, mfc = 'white', ms = 7, alpha=0.3, 
                #         label=DATA.name+label, zorder=1)
                ax.fill_between(timesTR+t0corr,TRmodT-varTTR,TRmodT+varTTR,color='k',\
                                 alpha=0.3, edgecolor="none",zorder=1)
                #ax.plot(DATA.time,TRJmodD,'.r',zorder=9)
                ax.plot(timesTR+t0corr,TRmodT,color='k',alpha=0.8,zorder=2)
                ax.plot(timesTR+t0corr,TRmodT,color='k',alpha=0.2,zorder=5)
                ax2.errorbar(DATA.time+t0corr,DATA.value_c-TRmodD,DATA.error_c,
                         fmt='o',mec=DATA.color,ecolor=DATA.color,
                         elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                         label=DATA.name+label, zorder=1)

                ax.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()

                #######################################################################
                # phase folded
                
                ylabel = 'Rel. Flux'
                xlabel = 'Phase (d)'
                for k,Plan in enumerate(self.PlanetListResult):
                    if Plan.Transit and DATA.Planet==k:
                        ind = list(range(len(self.PlanetListResult)))
                        ind.remove(k)
                        MAmodR  = np.zeros(len(timesTR))
                        
                        if len(self.PlanetListResult) > 1:
                            MAmodR -= (len(self.PlanetListResult)-1)*(1+DATA.thirdL) 
                            for j in ind:
                                if Plan.TransitOnly:
                                    MAmodR  += self.PlanetListResult[j].MandelAgol(timesTR,DATA.u1,DATA.u2)
                                else:
                                    MAmodR  += self.PlanetListResult[j].MandelAgolRV(timesTR,DATA.u1,DATA.u2)                                    
                            MAmodR  += DATA.thirdL * (len(self.PlanetListResult)-1)      
                            MAmodR  = MAmodR/(1+DATA.thirdL)
                        
                        isort = np.argsort(timesTR%Plan.P)
    
                        FlareModD = np.zeros(len(DATA.time))
                        FlareModT = np.zeros(len(timesTR))
                        if DATA.FitFlare:
                            for fl in DATA.FlareList:
                                Ftime = fl[0]
                                if not DATA.FixFlareTime: 
                                    Ftime = mod.gpAll.get_parameter_vector()[self.flarepos+count1]
                                    count1 += 1
                                Famp = fl[1]
                                if not DATA.FixFlareAmp: 
                                    Famp = np.abs(mod.gpAll.get_parameter_vector()[self.flarepos+count1])
                                    count1 += 1
                                Ffwhm = fl[2]
                                if not DATA.FixFlareFWHM: 
                                    Ffwhm = np.abs(mod.gpAll.get_parameter_vector()[self.flarepos+count1])
                                    count1 += 1
                                FlareModD += aflare.aflare1(DATA.time,Ftime,Ffwhm,Famp)
                                FlareModT += aflare.aflare1(timesTR,Ftime,Ffwhm,Famp)
                                print(count1,np.round(Ftime,6),np.round(Famp,6),np.round(Ffwhm,6))

                        pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_model_phase_GP"+str(np.round(Plan.P,2))+".pdf")
                        fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                            gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz))
                        ax2.set_xlabel(xlabel)
                        ax.set_ylabel(ylabel)
                        ax2.set_ylabel('Residuals')
                        if DATA.GP != 'NONE': addLab = ' GP corr.' 
                        else: addLab = ''
                        ax.errorbar(DATA.time%Plan.P,DATA.value_c-TRmodD+TRJmodD-np.interp(DATA.time,timesTR,MAmodR)-FlareModD,DATA.error_c,
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label+addLab, zorder=1)
                        ax.plot((timesTR%Plan.P)[isort],(TRJmodT-MAmodR-FlareModT)[isort],'k',zorder=2)
                        #ax.plot((timesTR%Plan.P)[isort],(1+FlareModT)[isort],'r',zorder=3)
                        #ax.plot((timesTR%Plan.P)[isort],(TRmodT-FlareModT)[isort],'b',zorder=3)
                        #ax.plot((timesTR%Plan.P)[isort],(TRJmodT-FlareModT)[isort],'g',zorder=3)
                        #ax.plot((timesTR%Plan.P)[isort],(TRmodT-TRJmodT+1)[isort],'y',zorder=3)
                        #ax.plot((timesTR%Plan.P)[isort],(TRmodT-FlareModT)[isort],'r',zorder=3)
                        ax2.errorbar(DATA.time%Plan.P,DATA.value_c-TRmodD,DATA.error_c,
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
                        ax.legend(fontsize='xx-small')
                        
                        pp.savefig(plt.gcf()) # This generates pdf page
                        if noShow:
                            plt.ioff()
                        else:
                            plt.show()
                        pp.close()
                        plt.close()
                        plt.clf()

                        pp = PdfPages(output_dir+savename+"_"+DATA.name+"_"+label+"_model_phase"+str(np.round(Plan.P,2))+".pdf")
                        fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                            gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz))
                        ax2.set_xlabel(xlabel)
                        ax.set_ylabel(ylabel)
                        ax2.set_ylabel('Residuals')
                        ax.errorbar(DATA.time%Plan.P,DATA.value_c-np.interp(DATA.time,timesTR,MAmodR),DATA.error_c,
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
                        ax.plot((timesTR%Plan.P)[isort],(TRmodT-MAmodR)[isort],'k',zorder=2)
                        ax2.errorbar(DATA.time%Plan.P,DATA.value_c-TRmodD,DATA.error_c,
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
                        ax.legend(fontsize='xx-small')
                        pp.savefig(plt.gcf()) # This generates pdf page
                        if noShow:
                            plt.ioff()
                        else:
                            # pp.savefig(plt.gcf()) # This generates pdf page
                            plt.show()
                        pp.close()
                        plt.close()
                        plt.clf()
                
        #######################################################################
        # plot all RV data
        if self.nfRV > 1:
            pp = PdfPages(output_dir+savename+"_All_"+label+"_model.pdf")
            xlabel = 'Time (BJD -'+str(self.DATAList[0].t0-t0corr)+')'
            fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz))
            ax2.set_xlabel(xlabel)
            ax2.set_ylabel('Residuals')
            ylabel = 'RV (m/s)'
            ax.set_ylabel(ylabel)
            
            for i,DATA in enumerate(self.DATAList):        
                if isinstance(DATA, RVTimeSeries): 
                        
                    inds = np.where(DATA.All_index == i-RVstart)
                    ax.errorbar(All_timeRV[inds]+t0corr,All_valueRV[inds]-RVmodD[inds]+RVJmodD[inds],All_errorRV[inds],
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label+" GP corr.", zorder=3)
                    ax.fill_between(timesRV+t0corr,RVmodT-varT,RVmodT+varT,color='k',\
                                     alpha=0.3, edgecolor="none",zorder=1)
                    ax.plot(timesRV+t0corr,RVmodT,color='k',alpha=0.8,zorder=2)
                    ax.plot(timesRV+t0corr,RVmodT,color='k',alpha=0.2,zorder=4)
                    ax2.errorbar(All_timeRV[inds]+t0corr,All_valueRV[inds]-RVmodD[inds],All_errorRV[inds],
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
            
            ax.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            if noShow:
                plt.ioff()
            else:
                # pp.savefig(plt.gcf()) # This generates pdf page
                plt.show()
            pp.close()
            plt.close()
            plt.clf()
               
            # phase folded
            ylabel = 'RV (m/s)'
            xlabel = 'Phase (d)'
            for k,Plan in enumerate(self.PlanetListResult):
                ind = list(range(len(self.PlanetListResult)))
                ind.remove(k)
                KEmodR  = np.zeros(len(timesRV))
                if len(self.PlanetListResult) > 1:
                    for j in ind:
                        if Plan.orbit == 'Dynamic' or Plan.orbit == 'TTV':
                           KEmodR  += vzp[:,j]*timeconv       
                           dynPlan += 1
                        else:
                           KEmodR  += self.PlanetListResult[j].Kepler(t=timesRV)
                isort = np.argsort(timesRV%Plan.P)
                
                pp = PdfPages(output_dir+savename+"_All_"+label+"_model_phase"+str(np.round(Plan.P,2))+".pdf")
                fig,(ax,ax2) = plt.subplots(2,1,sharex=True, \
                    gridspec_kw = {'height_ratios':[5,2]}, facecolor='w',figsize=(xsiz,ysiz+self.nfRV*0.5))
                ax.set_ylabel(ylabel)
                ax2.set_xlabel(xlabel)
                ax2.set_ylabel('Residuals')
                for i,DATA in enumerate(self.DATAList):        
                    if isinstance(DATA, RVTimeSeries):
                        inds = np.where(DATA.All_index == i-RVstart)
                        ax.errorbar(All_timeRV[inds]%Plan.P,All_valueRV[inds]-RVmodD[inds]+RVJmodD[inds]\
                                    -np.interp(All_timeRV,timesRV,KEmodR)[inds],All_errorRV[inds],
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label+" GP corr.", zorder=3)
                        ax.plot((timesRV%Plan.P)[isort],(RVJmodT-KEmodR)[isort],'k')
                        ax2.errorbar(All_timeRV[inds]%Plan.P,All_valueRV[inds]-RVmodD[inds],All_errorRV[inds],
                             fmt='o',mec=DATA.color,ecolor=DATA.color,
                             elinewidth=2, mfc = 'white', ms = 7, alpha=0.8, 
                             label=DATA.name+label, zorder=1)
                        ax.legend(fontsize='xx-small')
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()


            glsout = gls.Gls((All_timeRV,All_valueRV,All_errorRV), 
                             verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
            glsoutR = gls.Gls((All_timeRV,All_valueRV-RVmodD,All_errorRV), 
                              verbose=True,fbeg=DATA.fbeg,fend=DATA.fend)
            FAP01   = glsout.powerLevel(0.01)
            FAP001  = glsout.powerLevel(0.001)
            FAP0001 = glsout.powerLevel(0.0001)
    
            pp = PdfPages(output_dir+savename+"_All_"+label+"_gls_f.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.plot(glsout.freq,glsout.power,color='cornflowerblue',label="All "+label)
            plt.plot(glsoutR.freq,glsoutR.power,color='darkorange',label="All "+label+' res')
            plt.plot([DATA.fbeg,DATA.fend],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.plot([DATA.fbeg,DATA.fend],[FAP001,FAP001],':',color='cornflowerblue')
            plt.plot(np.linspace(DATA.fbeg,DATA.fend,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Frequency (1/d)')
            plt.ylabel('Power')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            if noShow:
                plt.ioff()
            else:
                # pp.savefig(plt.gcf()) # This generates pdf page
                plt.show()
            pp.close()
            plt.close()
            plt.clf()
    
            pp = PdfPages(output_dir+savename+"_All_"+label+"_gls_p.pdf")
            fig = plt.figure()
            fig.set_size_inches(xsiz,ysizGLS)
            plt.semilogx(1./glsout.freq,glsout.power,color='cornflowerblue',label="All "+label)
            plt.semilogx(1./glsoutR.freq,glsoutR.power,color='darkorange',label="All "+label+' res')
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP01,FAP01],'--',color='cornflowerblue')
            plt.semilogx([1./DATA.fend,1./DATA.fbeg],[FAP001,FAP001],':',color='cornflowerblue')
            plt.semilogx(np.linspace(1./DATA.fend,1./DATA.fbeg,20),[FAP0001]*20,'-.',color='cornflowerblue')
            plt.xlabel('Period (d)')
            plt.ylabel('Power')
            plt.grid(which='both',axis='x')
            plt.legend(fontsize='xx-small')
            pp.savefig(plt.gcf()) # This generates pdf page
            if noShow:
                plt.ioff()
            else:
                # pp.savefig(plt.gcf()) # This generates pdf page
                plt.show()
            pp.close()
            plt.close()
            plt.clf()
    
            #######################################################################
            # in smaller chunks
            if showAll:
                tstart = np.min(All_timeRV)
                tend   = np.max(All_timeRV)
                nint   = int((tend-tstart)/minPer + 1)
                pcount = 0
                for k in range(nint):
                    cc    = 0
                    for DATA in RVListe:
                        xx = DATA.time
                        ind = np.where((xx > tstart+k*minPer-minPer*0.05) & (xx < tstart+(k+1)*minPer+minPer*0.05))
                        #print(kk,len(ind[0]))
                        if len(ind[0]) != 0: cc=1
                    if cc != 0: pcount += 1
    
                pp = PdfPages(output_dir+savename+"All_time_RV.pdf")
                fig = plt.figure()
                fig.set_size_inches(17, pcount*3)
        
                count = 0
                for k in range(nint):
                    cc    = 0
                    for DATA in RVListe:
                        xx = DATA.time
                        ind = np.where((xx > tstart+k*minPer-minPer*0.05) & (xx < tstart+(k+1)*minPer+minPer*0.05))
                        #print("len(ind)",len(ind[0]))
                        if len(ind[0]) != 0: cc=1
                    if cc != 0: 
                        count += 1
                        #print("count",count,k,nint)
                        plt.subplot(pcount,1,count)
                        for i,DATA in enumerate(RVListe):
                            #ind = np.where((x > tstart+k*minper-minPer*0.05) & (x < tstart+(k+1)*mnper+minPer*0.05))
                            inds = np.where(DATA.All_index == i)
                            ind  = np.where((All_timeRV[inds] > tstart+k*minPer-minPer*0.05) & (All_timeRV[inds] < tstart+(k+1)*minPer+minPer*0.05))
                            plt.errorbar(All_timeRV[inds][ind]+t0corr,All_valueRV[inds][ind]-RVmodD[inds][ind]+RVJmodD[inds][ind],All_errorRV[inds][ind],
                                 fmt='o',mec=DATA.color,ecolor=DATA.color,
                                 elinewidth=2, mfc = 'white', ms = 7, alpha=0.8,\
                                 zorder=10,label=DATA.name+label)
                            ind  = np.where((timesRV > tstart+k*minPer-minPer*0.05) & (timesRV < tstart+(k+1)*minPer+minPer*0.05))
                            plt.fill_between(timesRV[ind]+t0corr,RVmodT[ind]-varT[ind],RVmodT[ind]+varT[ind],color='k',\
                                             alpha=0.3, edgecolor="none",zorder=1)
                        #plt.xlim(tstart+k*minPer-minPer*0.05,tstart+(k+1)*minPer+minPer*0.05)
                        if k == nint-1: plt.legend(fontsize='xx-small')
                        plt.ylabel(ylabel)
                plt.xlabel(xlabel)
                pp.savefig(plt.gcf()) # This generates pdf page
                if noShow:
                    plt.ioff()
                else:
                    # pp.savefig(plt.gcf()) # This generates pdf page
                    plt.show()
                pp.close()
                plt.close()
                plt.clf()

            #######################################################################
            wMean = np.sum((All_valueRV-RVmodD)/All_errorRV**2)/np.sum(1./All_errorRV**2)
            wRMS  = np.sqrt(np.sum((All_valueRV-RVmodD)**2/All_errorRV**2)/np.sum(1./All_errorRV**2))
            CHI2  = np.sum((All_valueRV-RVmodD)**2/All_errorRV**2)
            print("weighted mean: {0}".format(wMean))
            print("weighted RMS: {0}".format(wRMS))
            print("chi²: {0}".format(CHI2))
            print("number of RV data points: {0}".format(len(All_valueRV)))
            print("number of RV parameters: {0}".format(self.nRVpar))
            print("reduced chi²: {0}".format(CHI2/(len(All_valueRV)-self.nRVpar)))
            print("first term of BIC: {0}".format(self.nRVpar*np.log(len(All_valueRV))))
            if path.exists(output_dir+savename+'output.txt'):
                f = open(output_dir+savename+'output.txt','a')
            else:
                f = open(output_dir+savename+'output.txt','w')
            f.write("weighted mean: {0}\n".format(wMean))
            f.write("weighted RMS: {0}\n".format(wRMS))
            f.write("chi²: {0}\n".format(CHI2))
            f.write("number of RV data points: {0}\n".format(len(All_valueRV)))
            f.write("number of RV parameters: {0}\n".format(self.nRVpar))
            f.write("reduced chi²: {0}\n".format(CHI2/(len(All_valueRV)-self.nRVpar)))
            f.write("first term of BIC: {0}\n".format(self.nRVpar*np.log(len(All_valueRV))))
            f.close()
            BIC_1 = self.nRVpar*np.log(len(All_valueRV))
        else:
            BIC_1 = 0.
        
        gc.collect()
        return BIC_1

################################################################################
# store planet results in object 
    def SaveThePlanet(self,best,Rstar):
        #inc = 0
        #print(f"########################################### {self.PlanetList[0].inc} ####################")
        PlanetListResult = []
        ioff = self.Poff
        for i,Plan in enumerate(self.PlanetList):
            if Plan.varfix[0] == 0:
                K     = best[ioff]
                ioff += 1
            else:
                K     = self.PlanetList[i].K

            if Plan.varfix[1] == 0:
                m     = best[ioff]
                ioff += 1
            else:
                m     = self.PlanetList[i].m
            if Plan.varfix[2] == 0:
                P     = best[ioff]
                ioff += 1
            else:
                P     = self.PlanetList[i].P
            if Plan.varfix[3] == 0:
                e     = best[ioff]
                ioff += 1
            else:
                e     = self.PlanetList[i].e
            if Plan.varfix[4] == 0:
                om    = best[ioff]
                ioff += 1
            else:
                om    = self.PlanetList[i].om
            if Plan.varfix[5] == 0:
                tperi = best[ioff]
                ioff += 1
            else:
                tperi = self.PlanetList[i].tperi
            if Plan.varfix[6] == 0:
                Rp2Rs = best[ioff]
                ioff += 1
            else:
                Rp2Rs = self.PlanetList[i].Rp2Rs
            if Plan.varfix[7] == 0:
                inc   = best[ioff]
                ioff += 1
            else:
                inc   = self.PlanetList[i].inc
            if Plan.varfix[8] == 0:
                Om    = best[ioff]
                ioff += 1
            else:
                Om    = self.PlanetList[i].Om
            if Plan.varfix[9] == 0:
                a2Rs  = best[ioff]
                ioff += 1
            else:
                a2Rs  = self.PlanetList[i].a2Rs
            PlanetListResult.append(Planet(Mstar=self.PlanetList[i].Mstar,Rstar=Rstar,
                                    Teff=self.PlanetList[i].Teff,t0=self.PlanetList[i].t0,
                                    firstcall=True, 
                                    m=m, K=K, P=P,
                                    e=e, om=om, tperi=tperi, Rp2Rs=Rp2Rs,
                                    inc=inc,Om=Om,a2Rs=a2Rs, 
                                    albedo=self.PlanetList[i].albedo,
                                    orbit=self.PlanetList[i].orbit,
                                    Transit=self.PlanetList[i].Transit,
                                    Coplanar=self.PlanetList[i].Coplanar,
                                    fixOm=self.PlanetList[i].fixOm,
                                    fixInc=self.PlanetList[i].fixInc,
                                    TransitOnly=self.PlanetList[i].TransitOnly,
                                    fixK=self.PlanetList[i].fixK,
                                    fixm=self.PlanetList[i].fixm,
                                    fixP=self.PlanetList[i].fixP,
                                    fixe=self.PlanetList[i].fixe,
                                    fixom=self.PlanetList[i].fixom,
                                    fixtperi=self.PlanetList[i].fixtperi,
                                    fixRp2Rs=self.PlanetList[i].fixRp2Rs,
                                    fixa2Rs=self.PlanetList[i].fixa2Rs,
                                    fixRstar=self.PlanetList[i].fixRstar,
                                    predict_a2Rs=self.PlanetList[i].predict_a2Rs,
                                    Ppar=self.PlanetList[i].Ppar))
            #print(self.PlanetList[i].varind)
            #print(PlanetListResult[i].varind)

        '''
        poff             = self.Poff
        for k,Plan in enumerate(self.PlanetList):
            if Plan.orbit == 'Circular':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            TOff= 1
                        else:
                            inc = best[poff+3]
                            TOff= 0
                        P     = best[poff+0]
                        tperi = best[poff+1]
                        Rp2Rs = best[poff+2]
                        a2Rs  = best[poff+4-TOff]
                        m     = self.PlanetList[k].m
                        K     = self.PlanetList[k].K
                        e     = self.PlanetList[k].e
                        om    = self.PlanetList[k].om
                        Om    = self.PlanetList[k].Om
                    else:                
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            inc = self.PlanetList[k].inc
                        else:
                            inc   = best[poff+4]
                        K     = best[poff+0]
                        P     = best[poff+1]
                        tperi = best[poff+2]
                        Rp2Rs = best[poff+3]
                        m     = self.PlanetList[k].m
                        e     = self.PlanetList[k].e
                        om    = self.PlanetList[k].om
                        Om    = self.PlanetList[k].Om
                        a2Rs  = self.PlanetList[k].a2Rs
                else:
                        K     = best[poff+0]
                        P     = best[poff+1]
                        tperi = best[poff+2]
                        m     = self.PlanetList[k].m
                        e     = self.PlanetList[k].e
                        om    = self.PlanetList[k].om
                        Rp2Rs = self.PlanetList[k].Rp2Rs
                        inc   = self.PlanetList[k].inc
                        Om    = self.PlanetList[k].Om
                        a2Rs  = self.PlanetList[k].a2Rs
                        
            if Plan.orbit == 'Kepler':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            TOff= 1
                        else:
                            inc = best[poff+5]
                            TOff= 0
                        P     = best[poff+0]
                        e     = best[poff+1]
                        om    = best[poff+2]
                        tperi = best[poff+3]
                        Rp2Rs = best[poff+4]
                        a2Rs  = best[poff+6-TOff]
                        m     = self.PlanetList[k].m
                        K     = self.PlanetList[k].K
                        Om    = self.PlanetList[k].Om
                    else:                
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                        else:
                            inc = best[poff+6]
                        K     = best[poff+0]
                        P     = best[poff+1]
                        e     = best[poff+2]
                        om    = best[poff+3]
                        tperi = best[poff+4]
                        Rp2Rs = best[poff+5]
                        m     = self.PlanetList[k].m
                        Om    = self.PlanetList[k].Om
                        a2Rs  = self.PlanetList[k].a2Rs
                else:
                        K     = best[poff+0]
                        P     = best[poff+1]
                        e     = best[poff+2]
                        om    = best[poff+3]
                        tperi = best[poff+4]
                        m     = self.PlanetList[k].m
                        Rp2Rs = self.PlanetList[k].Rp2Rs
                        inc   = self.PlanetList[k].inc
                        Om    = self.PlanetList[k].Om
                        a2Rs  = self.PlanetList[k].a2Rs
                        
            if Plan.orbit == 'Dynamic':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            TOff= 1
                        else:
                            inc = best[poff+5]
                            TOff= 0
                        if Plan.fixOm:
                            Om = self.PlanetList[k].Om
                            TOff+= 1
                        else:
                            Om = best[poff+6-TOff]
                            TOff= 0
                        P     = best[poff+0]
                        e     = best[poff+1]
                        om    = best[poff+2]
                        tperi = best[poff+3]
                        Rp2Rs = best[poff+4]
                        a2Rs  = best[poff+7-TOff]
                        m     = self.PlanetList[k].m
                        K     = self.PlanetList[k].K
                    else:
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            TOff= 1
                        else:
                            inc = best[poff+6]
                            TOff= 0
                        if Plan.fixOm:
                            Om = self.PlanetList[k].Om
                        else:
                            Om = best[poff+7-TOff]
                        m     = best[poff+0]
                        P     = best[poff+1]
                        e     = best[poff+2]
                        om    = best[poff+3]
                        tperi = best[poff+4]
                        Rp2Rs = best[poff+5]
                        K     = self.PlanetList[k].K
                        a2Rs  = self.PlanetList[k].a2Rs
                else:
                        if Plan.fixInc or Plan.Coplanar:
                            if Plan.Coplanar:
                                inc = self.PlanetList[0].inc
                            else:
                                inc = self.PlanetList[k].inc
                            TOff= 1
                        else:
                            inc = best[poff+5]
                            TOff= 0
                        if Plan.fixOm:
                            Om = self.PlanetList[k].Om
                        else:
                            Om = best[poff+6-TOff]
                        m     = best[poff+0]
                        P     = best[poff+1]
                        e     = best[poff+2]
                        om    = best[poff+3]
                        tperi = best[poff+4]
                        K     = self.PlanetList[k].K
                        Rp2Rs = self.PlanetList[k].Rp2Rs
                        a2Rs  = self.PlanetList[k].a2Rs


            PlanetListResult.append(Planet(Mstar=self.PlanetList[k].Mstar/M_sun,Rstar=Rstar,
                                               Teff=self.PlanetList[k].Teff,t0=self.PlanetList[k].t0,
                                               firstcall=True, m=m, K=K, P=P, 
                                               e=e, om=om, tperi=tperi, Rp2Rs=Rp2Rs, 
                                               inc=inc,Om=Om,a2Rs=a2Rs, albedo=self.PlanetList[k].albedo,
                    orbit=self.PlanetList[k].orbit,Transit=self.PlanetList[k].Transit,\
                    Coplanar=self.PlanetList[k].Coplanar,fixOm=self.PlanetList[k].fixOm,\
                    fixInc=self.PlanetList[k].fixInc,TransitOnly=self.PlanetList[k].TransitOnly,\
                    fixK=self.PlanetList
                    Ppar=self.PlanetList[k].Ppar))
            poff = poff + self.PlanetList[k].Ppar
            '''

        self.PlanetListResult = PlanetListResult
        return PlanetListResult

################################################################################
# save Data files
    def SaveTheDatafiles(self,best,RVstart,ACstart,TRstart):
        global input_dir
        global TTVs
        DATAListResult = []
        OffJ = 0
        for i,RV in enumerate(self.DATAList[RVstart:RVstart+self.nfRV]):
            GPpar     = best[self.GoffRV:self.GoffRV+self.nGPRV]
            if RV.GP == 'ROT3':
                #print(self.GoffRV,self.GoffRV+self.nGPRV)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                tau1 = np.exp(GPpar[4])*P0*0.5/np.pi
                S01  = np.exp(GPpar[3])
                tau2 = np.exp(GPpar[6])*P0/3/np.pi
                S02  = np.exp(GPpar[5])
                GPpar = [S0,tau,P0,S01,tau1,S02,tau2]
            if RV.GP == 'ROT':
                #print(self.GoffRV,self.GoffRV+self.nGPRV)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                tau1 = np.exp(GPpar[4])*P0*0.5/np.pi
                S01  = np.exp(GPpar[3])
                GPpar = [S0,tau,P0,S01,tau1]
            if RV.GP == 'ROTFLARE':
                #print(self.GoffRV,self.GoffRV+self.nGPRV)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                tau1 = np.exp(GPpar[4])*P0*0.5/np.pi
                S01  = np.exp(GPpar[3])
                a    = np.exp(GPpar[5])
                c    = 1./np.exp(GPpar[6])
                GPpar = [S0,tau,P0,S01,tau1,a,c]
            if RV.GP == 'SHO':
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                GPpar = [S0,tau,P0]
            if RV.GP == 'SHOREAL':
                #print(self.GoffRV,self.GoffRV+self.nGPRV)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                a    = np.exp(GPpar[3])
                c    = 1./np.exp(GPpar[4])
                GPpar = [S0,tau,P0,a,c]
            if RV.GP == 'REAL':
                a   = np.exp(GPpar[0])
                c   = 1./np.exp(GPpar[1])
                GPpar = [a,c]
            if RV.GP == 'CUST':
                a   = np.exp(GPpar[0])
                b   = np.exp(GPpar[1])
                c   = 1./np.exp(GPpar[2])
                P   = np.exp(GPpar[3])
                GPpar = [a,b,c,P]
            Jitterpar = 1.
            if RV.Jitter: Jitterpar = np.exp(best[self.JoffRV+OffJ])
            off       = best[self.OoffRV+i]
            DATAListResult.append(RVTimeSeries(fileName=self.DATAList[RVstart+i].filename,
                                  dataName=self.DATAList[RVstart+i].name,
                                  GP=self.DATAList[RVstart].GP,
                                  Jitter=self.DATAList[RVstart+i].Jitter,
                                  GPpar=GPpar,
                                  Jitterpar=Jitterpar, off=off, 
                                  t0=self.DATAList[RVstart+i].t0,
                                  color=self.DATAList[RVstart+i].color,
                                  weight=self.DATAList[RVstart+i].weight,
                                  delimiter=self.DATAList[RVstart+i].delimiter,
                                  ReadTheData = False))
            DATAListResult[RVstart+i].time  = RV.time
            DATAListResult[RVstart+i].value = RV.value
            DATAListResult[RVstart+i].error = RV.error
            DATAListResult[RVstart+i].value_c = RV.value_c
            DATAListResult[RVstart+i].error_c = RV.error_c
            OffJ += 1
        
        OffG = 0
        for i,AC in enumerate(self.DATAList[ACstart:ACstart+self.nfAC]):
            GPpar     = best[self.GoffAC+OffG:self.GoffAC+OffG+self.nGPAC[i]]
            if AC.GP == 'SHO':
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                GPpar = [S0,tau,P0]
            if AC.GP == 'RVdriver':
                S0  = np.exp(GPpar[0])
                GPpar = [S0]
            if AC.GP == 'ROTdriver':
                GPpar = [np.exp(GPpar[0]),np.exp(GPpar[1])]
            if AC.GP == 'REAL':
                a   = np.exp(GPpar[0])
                c   = 1./np.exp(GPpar[1])
                GPpar = [a,c]
            Jitterpar = 1.
            if AC.Jitter: 
                Jitterpar = np.exp(best[self.JoffAC[i]])
                OffG += 1
            off       = best[self.OoffAC+i]
            DATAListResult.append(ACTimeSeries(fileName = self.DATAList[ACstart+i].filename,
                                  dataName = self.DATAList[ACstart+i].name,
                                  GP = self.DATAList[ACstart+i].GP,
                                  Jitter = self.DATAList[ACstart+i].Jitter,
                                  GPpar = GPpar,
                                  Jitterpar = Jitterpar, off = off, 
                                  t0 = self.DATAList[ACstart+i].t0,
                                  color = self.DATAList[ACstart+i].color,
                                  binwidth = self.DATAList[ACstart+i].binwidth,
                                  filetype = self.DATAList[ACstart+i].filetype,
                                  weight = self.DATAList[ACstart+i].weight,
                                  delimiter = self.DATAList[ACstart+i].delimiter,
                                  ReadTheData = False))
            DATAListResult[ACstart+i].time  = AC.time
            DATAListResult[ACstart+i].value = AC.value
            DATAListResult[ACstart+i].error = AC.error
            OffG += self.nGPAC[i]
        
        
        ttras = []
        tmpPlanetListResult = copy.deepcopy(self.PlanetListResult)
        for i,Plan in enumerate(tmpPlanetListResult):
            trad = Plan.get_trad()
            ttras.append((Plan.ttra,Plan.P,trad/Plan.P,i))

        OffG = 0
        SOFF = 1
        npTR = 0
        if (len(RVListe) == 0) & (not TTVs): SOFF = 0
        for i,TR in enumerate(self.DATAList[TRstart:TRstart+self.nfTR]):
            GPpar     = best[self.GoffTR+OffG:self.GoffTR+OffG+self.nGPTR[i]]
            if TR.GP == 'SHO':
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                GPpar = [S0,tau,P0]
            if TR.GP == 'ROT':
                #print(self.GoffTR,self.GoffTR+self.nGPTR)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                tau1 = np.exp(GPpar[4])*P0*0.5/np.pi
                S01  = np.exp(GPpar[3])
                GPpar = [S0,tau,P0,S01,tau1]
            if TR.GP == 'ROT3':
                #print(self.GoffTR,self.GoffTR+self.nGPTR)
                #print(GPpar,len(GPpar))
                P0  = 2.*np.pi/np.exp(GPpar[2])
                tau = np.exp(GPpar[1])*P0/np.pi
                S0  = np.exp(GPpar[0])
                tau1 = np.exp(GPpar[4])*P0*0.5/np.pi
                S01  = np.exp(GPpar[3])
                tau2 = np.exp(GPpar[6])*P0/3/np.pi
                S02  = np.exp(GPpar[5])
                GPpar = [S0,tau,P0,S01,tau1,S02,tau2]
            if TR.GP == 'ROTFLARE_RV_lead':
                #print(self.GoffTR,self.GoffTR+self.nGPTR)
                #print(GPpar,len(GPpar))
                S0  = np.exp(GPpar[0])
                S1  = np.exp(GPpar[1])
                a    = np.exp(GPpar[2])
                c    = 1./np.exp(GPpar[3])
                GPpar = [S0,S01,a,c]
            if TR.GP == 'ROTFLARE_RV':
                #print(self.GoffTR,self.GoffTR+self.nGPTR)
                #print(GPpar,len(GPpar))
                S0  = np.exp(GPpar[0])
                S1  = np.exp(GPpar[1])
                GPpar = [S0,S01]
            if TR.GP == 'REAL':
                a   = np.exp(GPpar[0])
                c   = 1./np.exp(GPpar[1])
                GPpar = [a,c]
            Jitterpar = 1.e-4
            if TR.Jitter: 
                Jitterpar = np.exp(best[self.JoffTR[i]])
                OffG += 1
            kk  = TR.ParFileNr
            off = best[self.OoffTR+kk]  # instead of i
            #print(f"light curve parameter {self.Soff}, {SOFF}")
            if kk == npTR:
                if TR.fixLD:
                    u1 = TR.u1
                    u2 = TR.u2
                else:
                    u1        = best[self.Soff+SOFF]
                    u2        = best[self.Soff+SOFF+1]
                    SOFF     += 2
                if TR.fix3:
                    thirdL = TR.thirdL
                else:
                    thirdL    = best[self.Soff+SOFF]
                    SOFF += 1
                npTR += 1
            #print(i,u1,u2,thirdL)
            #print(f"light curve parameter {self.Soff}, {SOFF}, {u1}, {u2}, {thirdL}")
            
            DATAListResult.append(TRTimeSeries(fileName = self.DATAList[TRstart+i].filename,
                                  dataName = self.DATAList[TRstart+i].name,
                                  GP = self.DATAList[TRstart+i].GP,
                                  Jitter=self.DATAList[TRstart+i].Jitter,
                                  GPpar=GPpar,
                                  Jitterpar=Jitterpar, off=off,
                                  fixLD=self.DATAList[TRstart+i].fixLD,
                                  u1=u1, 
                                  u2=u2,
                                  fix3=self.DATAList[TRstart+i].fix3,
                                  thirdL=thirdL,
                                  t0=self.DATAList[TRstart+i].t0,
                                  filetype=self.DATAList[TRstart+i].filetype,
                                  color=self.DATAList[TRstart+i].color,
                                  detrend=self.DATAList[TRstart+i].detrend,
                                  binwidth=self.DATAList[TRstart+i].binwidth,
                                  ttras=ttras,
                                  weight=self.DATAList[TRstart+i].weight,
                                  delimiter=self.DATAList[TRstart+i].delimiter,
                                  PlanetList=self.DATAList[TRstart+i].PlanetList,
                                  ParFileNr=self.DATAList[TRstart+i].ParFileNr,
                                  ReadTheData = False))
            DATAListResult[TRstart+i].time  = TR.time
            DATAListResult[TRstart+i].value = TR.value
            DATAListResult[TRstart+i].error = TR.error
            DATAListResult[TRstart+i].value_c = TR.value_c
            DATAListResult[TRstart+i].error_c = TR.error_c
            OffG += self.nGPTR[i]
        return DATAListResult
        
################################################################################
# calculate derived quantities (and prepare for corner plots)
    def derived_quantities(self,samples,best,star,mod,RVstart,ACstart,TRstart,ndim):
        nADD = 0
        for i,Plan in enumerate(self.PlanetListResult):
            #print(Plan.K,Plan.m,Plan.P,Plan.e,Plan.om,Plan.tperi,Plan.Rp2Rs,Plan.inc,Plan.Om,Plan.a2Rs)
            #print(PlanetListe[i].K,PlanetListe[i].m,PlanetListe[i].P,PlanetListe[i].e,PlanetListe[i].om,PlanetListe[i].tperi,PlanetListe[i].Rp2Rs,PlanetListe[i].inc,PlanetListe[i].Om,PlanetListe[i].a2Rs)
            Plan.varadd[0] = Plan.varfix[0]
            Plan.varadd[1] = Plan.varfix[1]
            Plan.varadd[2] = 1
            Plan.varadd[3] = 1
            Plan.varadd[8] = 1
            if Plan.Transit:
                Plan.varadd[4] = Plan.varfix[9]
                Plan.varadd[5] = 1
                Plan.varadd[6] = 1
                Plan.varadd[7] = 1
            nADD += np.sum(Plan.varadd)
            #print("nADD",nADD,Plan.varadd)
            #print("varfix",Plan.varfix,Plan.varind)
        '''   
        nADD = 0
        for Plan in self.PlanetList:
            if Plan.orbit == 'Circular':
                if Plan.Transit:
                    if Plan.TransitOnly: nADD += 0
                    else:                nADD += 6
                else:                    nADD += 3
            if Plan.orbit == 'Kepler':
                if Plan.Transit:
                    if Plan.TransitOnly: nADD += 0
                    else:                nADD += 6
                else:                    nADD += 3
            if Plan.orbit == 'Dynamic':
                if Plan.Transit:
                    if Plan.TransitOnly: nADD += 0
                    else:                nADD += 6
                else:                    nADD += 3
            #if (not Plan.TransitOnly) or (Plan.orbit == 'Dynamic'): nADD += 3
            #if (not Plan.TransitOnly) and Plan.Transit:             nADD += 3
        '''
            
        xx = np.zeros((ndim+nADD)*len(samples)).reshape(-1,ndim+nADD)
        for k in range(ndim):
            xx[:,k] = samples[:,k] * 1.
        
        truth = np.zeros(len(best)+nADD)
        truth[:ndim] = best * 1.
        
        for i,DATA in enumerate(self.DATAList):
            if isinstance(DATA,RVTimeSeries) & DATA.Jitter:
                xx[:,mod.JoffRV+i-RVstart] = np.exp(xx[:,mod.JoffRV+i-RVstart]) # mod.Jitter RV
                truth[mod.JoffRV+i-RVstart]= np.exp(truth[mod.JoffRV+i-RVstart]) # mod.Jitter RV
            if isinstance(DATA,ACTimeSeries) & DATA.Jitter:
                xx[:,mod.JoffAC[i-ACstart]] = np.exp(xx[:,mod.JoffAC[i-ACstart]]) # mod.Jitter AC
                truth[mod.JoffAC[i-ACstart]]= np.exp(truth[mod.JoffAC[i-ACstart]]) # mod.Jitter AC
            if isinstance(DATA,TRTimeSeries) & DATA.Jitter:
                xx[:,mod.JoffTR[i-TRstart]] = np.exp(xx[:,mod.JoffTR[i-TRstart]]) # mod.Jitter TR
                truth[mod.JoffTR[i-TRstart]]= np.exp(truth[mod.JoffTR[i-TRstart]]) # mod.Jitter TR
        
        if mod.nfRV > 0:
            if self.DATAList[RVstart].GP == 'ROT3':        
                w0 = np.exp(xx[:, mod.GoffRV+2])                         # w0
                Q  = np.exp(xx[:, mod.GoffRV+1])                         # Q
                xx[:,mod.GoffRV+0] = np.exp(xx[:,mod.GoffRV+0])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+1] = 2.*Q/w0                             # damping time scale of oscillator
                xx[:,mod.GoffRV+2] = 2.*np.pi/w0                         # oscillator period
                w0t= np.exp(truth[ mod.GoffRV+2])                        # w0
                Qt = np.exp(truth[ mod.GoffRV+1])                        # Q
                truth[mod.GoffRV+0] = np.exp(truth[mod.GoffRV+0])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                truth[mod.GoffRV+2] = 2.*np.pi/w0t                       # oscillator period
                w0 = w0*2.                     # w0
                Q  = np.exp(xx[:, mod.GoffRV+4])                         # Q
                xx[:,mod.GoffRV+3] = np.exp(xx[:,mod.GoffRV+3])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+4] = 2.*Q/w0                             # damping time scale of oscillator
                w0t= w0t*2.                    # w0
                Qt = np.exp(truth[ mod.GoffRV+4])                        # Q
                truth[mod.GoffRV+3] = np.exp(truth[mod.GoffRV+3])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+4] = 2.*Qt/w0t                          # damping time scale of oscillator
                w0 = w0*3./2.                     # w0
                Q  = np.exp(xx[:, mod.GoffRV+6])                         # Q
                xx[:,mod.GoffRV+5] = np.exp(xx[:,mod.GoffRV+5])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+6] = 2.*Q/w0                             # damping time scale of oscillator
                w0t= w0t*3./2.                    # w0
                Qt = np.exp(truth[ mod.GoffRV+6])                        # Q
                truth[mod.GoffRV+5] = np.exp(truth[mod.GoffRV+5])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+6] = 2.*Qt/w0t                          # damping time scale of oscillator
    
            if self.DATAList[RVstart].GP == 'ROT':        
                w0 = np.exp(xx[:, mod.GoffRV+2])                         # w0
                Q  = np.exp(xx[:, mod.GoffRV+1])                         # Q
                xx[:,mod.GoffRV+0] = np.exp(xx[:,mod.GoffRV+0])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+1] = 2.*Q/w0                             # damping time scale of oscillator
                xx[:,mod.GoffRV+2] = 2.*np.pi/w0                         # oscillator period
                w0t= np.exp(truth[ mod.GoffRV+2])                        # w0
                Qt = np.exp(truth[ mod.GoffRV+1])                        # Q
                truth[mod.GoffRV+0] = np.exp(truth[mod.GoffRV+0])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                truth[mod.GoffRV+2] = 2.*np.pi/w0t                       # oscillator period
                w0 = w0*2.                     # w0
                Q  = np.exp(xx[:, mod.GoffRV+4])                         # Q
                xx[:,mod.GoffRV+3] = np.exp(xx[:,mod.GoffRV+3])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+4] = 2.*Q/w0                             # damping time scale of oscillator
                w0t= w0t*2.                    # w0
                Qt = np.exp(truth[ mod.GoffRV+4])                        # Q
                truth[mod.GoffRV+3] = np.exp(truth[mod.GoffRV+3])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+4] = 2.*Qt/w0t                          # damping time scale of oscillator
    
            if self.DATAList[RVstart].GP == 'ROTFLARE':
                w0 = np.exp(xx[:, mod.GoffRV+2])                         # w0
                Q  = np.exp(xx[:, mod.GoffRV+1])                         # Q
                xx[:,mod.GoffRV+0] = np.exp(xx[:,mod.GoffRV+0])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+1] = 2.*Q/w0                             # damping time scale of oscillator
                xx[:,mod.GoffRV+2] = 2.*np.pi/w0                         # oscillator period
                w0t= np.exp(truth[ mod.GoffRV+2])                        # w0
                Qt = np.exp(truth[ mod.GoffRV+1])                        # Q
                truth[mod.GoffRV+0] = np.exp(truth[mod.GoffRV+0])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                truth[mod.GoffRV+2] = 2.*np.pi/w0t                       # oscillator period
                w0 = w0*2.                     # w0
                Q  = np.exp(xx[:, mod.GoffRV+4])                         # Q
                xx[:,mod.GoffRV+3] = np.exp(xx[:,mod.GoffRV+3])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+4] = 2.*Q/w0                             # damping time scale of oscillator
                w0t= w0t*2.                    # w0
                Qt = np.exp(truth[ mod.GoffRV+4])                        # Q
                truth[mod.GoffRV+3] = np.exp(truth[mod.GoffRV+3])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+4] = 2.*Qt/w0t                          # damping time scale of oscillator
                xx[:,mod.GoffRV+5] = np.exp(xx[:,mod.GoffRV+5])   # a (variance)
                xx[:,mod.GoffRV+6] = np.exp(-xx[:,mod.GoffRV+6])  # damping time scale
                truth[mod.GoffRV+5]= np.exp(truth[mod.GoffRV+5])  # a (variance)
                truth[mod.GoffRV+6]= np.exp(-truth[mod.GoffRV+6]) # damping time scale
    
            if self.DATAList[RVstart].GP == 'SHO':
                w0 = np.exp(xx[:, mod.GoffRV+2])                         # w0
                Q  = np.exp(xx[:, mod.GoffRV+1])                         # Q
                xx[:,mod.GoffRV+0] = np.exp(xx[:,mod.GoffRV+0])*w0*Q     # a=S0*w0*Q (variance)
                xx[:,mod.GoffRV+1] = 2.*Q/w0                             # damping time scale of oscillator
                xx[:,mod.GoffRV+2] = 2.*np.pi/w0                         # oscillator period
                w0t= np.exp(truth[ mod.GoffRV+2])                        # w0
                Qt = np.exp(truth[ mod.GoffRV+1])                        # Q
                truth[mod.GoffRV+0] = np.exp(truth[mod.GoffRV+0])*w0t*Qt # a=S0*w0*Q
                truth[mod.GoffRV+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                truth[mod.GoffRV+2] = 2.*np.pi/w0t                       # oscillator period
                
            if self.DATAList[RVstart].GP == 'REAL':
                xx[:,mod.GoffRV+0] = np.exp(xx[:,mod.GoffRV+0])   # a (variance)
                xx[:,mod.GoffRV+1] = np.exp(-xx[:,mod.GoffRV+1])  # damping time scale
                truth[mod.GoffRV+0]= np.exp(truth[mod.GoffRV+0])  # a (variance)
                truth[mod.GoffRV+1]= np.exp(-truth[mod.GoffRV+1]) # damping time scale
            
        if mod.nfAC > 0:
            off = mod.GoffAC
            for i,DATA in enumerate(self.DATAList[ACstart:TRstart]):
                if DATA.GP == 'SHO':  
                    w0 = np.exp(xx[:, off+2])                  # w0
                    Q  = np.exp(xx[:, off+1])                  # Q
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+1] = 2.*Q/w0                      # damping time scale of oscillator
                    xx[:,off+2] = 2.*np.pi/w0                  # oscillator period
                    w0t= np.exp(truth[ off+2])                 # w0
                    Qt = np.exp(truth[ off+1])                 # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    truth[off+1] = 2.*Qt/w0t                   # damping time scale of oscillator
                    truth[off+2] = 2.*np.pi/w0t                # oscillator period
                
                if DATA.GP == 'RVdriver':        
                    w0 = 2.*np.pi/np.exp(xx[:, mod.GoffRV+2])  # w0 from RVGP
                    Q  = np.exp(xx[:, mod.GoffRV+1])           # Q  from RVGP
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q
                    w0t= 2.*np.pi/np.exp(truth[ mod.GoffRV+2]) # w0
                    Qt = np.exp(truth[ mod.GoffRV+1])          # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                if DATA.GP == 'ROTdriver':
                    w0 = 2.*np.pi/np.exp(xx[:, mod.GoffRV+2])  # w0 from RVGP
                    Q  = np.exp(xx[:, mod.GoffRV+1])           # Q  from RVGP
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q
                    w0t= 2.*np.pi/np.exp(truth[mod.GoffRV+2]) # w0
                    Qt = np.exp(truth[mod.GoffRV+1])          # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    w0 = w0*2.  # harmonic
                    Q = np.exp(xx[:, mod.GoffRV + 4])  # Q  from RVGP
                    xx[:,off+1] = np.exp(xx[:,off+1]) * w0 * Q  # a=S0*w0*Q
                    w0t = w0t*2. # harmonic
                    Qt = np.exp(truth[mod.GoffRV + 4])  # Q
                    truth[off+1] = np.exp(truth[off+1]) * w0t * Qt  # a=S0*w0*Q
                off += DATA.nGPpar
        
        if mod.nfTR > 0:
            off = mod.GoffTR
            for i,DATA in enumerate(self.DATAList[TRstart:]):
                if DATA.GP == 'REAL':
                    xx[:,off+0] = np.exp(xx[:,off+0])  # a
                    xx[:,off+1] = np.exp(-xx[:,off+1]) # damping time scale
                    truth[off+0]= np.exp(truth[off+0]) # a
                    truth[off+1]= np.exp(truth[off+1]) # damping time scale
                if DATA.GP == 'SHO':
                    w0 = np.exp(xx[:, off+2])                  # w0
                    Q  = np.exp(xx[:, off+1])                  # Q
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+1] = 2.*Q/w0                      # damping time scale of oscillator
                    xx[:,off+2] = 2.*np.pi/w0                  # oscillator period
                    w0t= np.exp(truth[ off+2])                 # w0
                    Qt = np.exp(truth[ off+1])                 # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    truth[off+1] = 2.*Qt/w0t                   # damping time scale of oscillator
                    truth[off+2] = 2.*np.pi/w0t                # oscillator period
                if DATA.GP == 'ROT':
                    w0 = np.exp(xx[:, off+2])                         # w0
                    Q  = np.exp(xx[:, off+1])                         # Q
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+1] = 2.*Q/w0                             # damping time scale of oscillator
                    xx[:,off+2] = 2.*np.pi/w0                         # oscillator period
                    w0t= np.exp(truth[ off+2])                        # w0
                    Qt = np.exp(truth[ off+1])                        # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    truth[off+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                    truth[off+2] = 2.*np.pi/w0t                       # oscillator period
                    w0 = w0*2.                     # w0
                    Q  = np.exp(xx[:, off+4])                         # Q
                    xx[:,off+3] = np.exp(xx[:,off+3])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+4] = 2.*Q/w0                             # damping time scale of oscillator
                    w0t= w0t*2.                    # w0
                    Qt = np.exp(truth[ off+4])                        # Q
                    truth[off+3] = np.exp(truth[off+3])*w0t*Qt # a=S0*w0*Q
                    truth[off+4] = 2.*Qt/w0t                          # damping time scale of oscillator
                if DATA.GP == 'ROT3':
                    w0 = np.exp(xx[:, off+2])                         # w0
                    Q  = np.exp(xx[:, off+1])                         # Q
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+1] = 2.*Q/w0                             # damping time scale of oscillator
                    xx[:,off+2] = 2.*np.pi/w0                         # oscillator period
                    w0t= np.exp(truth[ off+2])                        # w0
                    Qt = np.exp(truth[ off+1])                        # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    truth[off+1] = 2.*Qt/w0t                          # damping time scale of oscillator
                    truth[off+2] = 2.*np.pi/w0t                       # oscillator period
                    w0 = w0*2.                     # w0
                    Q  = np.exp(xx[:, off+4])                         # Q
                    xx[:,off+3] = np.exp(xx[:,off+3])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+4] = 2.*Q/w0                             # damping time scale of oscillator
                    w0t= w0t*2.                    # w0
                    Qt = np.exp(truth[ off+4])                        # Q
                    truth[off+3] = np.exp(truth[off+3])*w0t*Qt # a=S0*w0*Q
                    truth[off+4] = 2.*Qt/w0t                          # damping time scale of oscillator
                    w0 = w0*3./2.                     # w0
                    Q  = np.exp(xx[:, off+6])                         # Q
                    xx[:,off+5] = np.exp(xx[:,off+5])*w0*Q     # a=S0*w0*Q (variance)
                    xx[:,off+6] = 2.*Q/w0                             # damping time scale of oscillator
                    w0t= w0t*3./2.                    # w0
                    Qt = np.exp(truth[ off+6])                        # Q
                    truth[off+5] = np.exp(truth[off+5])*w0t*Qt # a=S0*w0*Q
                    truth[off+6] = 2.*Qt/w0t                          # damping time scale of oscillator
                if DATA.GP == 'ROTFLARE_RV_lead':
                    w0 = 2.*np.pi/np.exp(xx[:, mod.GoffRV+2])  # w0 from RVGP
                    Q  = np.exp(xx[:, mod.GoffRV+1])           # Q  from RVGP
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q
                    w0t= 2.*np.pi/np.exp(truth[mod.GoffRV+2]) # w0
                    Qt = np.exp(truth[mod.GoffRV+1])          # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    w0 = w0*2.  # harmonic
                    Q = np.exp(xx[:, mod.GoffRV + 4])  # Q  from RVGP
                    xx[:,off+1] = np.exp(xx[:,off+1]) * w0 * Q  # a=S0*w0*Q
                    w0t = w0t*2. # harmonic
                    Qt = np.exp(truth[mod.GoffRV + 4])  # Q
                    truth[off+1] = np.exp(truth[off+1]) * w0t * Qt  # a=S0*w0*Q
                    xx[:,off+2] = np.exp(xx[:,off+2])   # a (variance)
                    xx[:,off+3] = np.exp(-xx[:,off+3])  # damping time scale
                    truth[off+2]= np.exp(truth[off+2])  # a (variance)
                    truth[off+3]= np.exp(-truth[off+3]) # damping time scale
                if DATA.GP == 'ROTFLARE_RV':
                    w0 = 2.*np.pi/np.exp(xx[:, mod.GoffRV+2])  # w0 from RVGP
                    Q  = np.exp(xx[:, mod.GoffRV+1])           # Q  from RVGP
                    xx[:,off+0] = np.exp(xx[:,off+0])*w0*Q     # a=S0*w0*Q
                    w0t= 2.*np.pi/np.exp(truth[mod.GoffRV+2]) # w0
                    Qt = np.exp(truth[mod.GoffRV+1])          # Q
                    truth[off+0] = np.exp(truth[off+0])*w0t*Qt # a=S0*w0*Q
                    w0 = w0*2.  # harmonic
                    Q = np.exp(xx[:, mod.GoffRV + 4])  # Q  from RVGP
                    xx[:,off+1] = np.exp(xx[:,off+1]) * w0 * Q  # a=S0*w0*Q
                    w0t = w0t*2. # harmonic
                    Qt = np.exp(truth[mod.GoffRV + 4])  # Q
                    truth[off+1] = np.exp(truth[off+1]) * w0t * Qt  # a=S0*w0*Q
                off += DATA.nGPpar
        
        ioff  = self.Poff
        ioffA = ndim
        for i,Plan in enumerate(self.PlanetListResult):
            #print(Plan.om, Plan.inc, Plan.Om)
            if Plan.varfix[0] == 0:
                K     = truth[ioff]
                Ks    = xx[:,ioff]
                ioff += 1
            if Plan.varfix[1] == 0:
                m     = truth[ioff]
                ms    = xx[:,ioff]
                ioff += 1

            if Plan.varfix[2] == 0:
                P     = truth[ioff]
                Ps    = xx[:,ioff]
                ioff += 1
            else:
                P     = Plan.P
                Ps    = Plan.P

            if Plan.varfix[3] == 0:
                e     = truth[ioff]
                es    = xx[:,ioff]
                ioff += 1
            else:
                e     = Plan.e
                es    = Plan.e

            if Plan.varfix[4] == 0:
                om    = np.copy(truth[ioff])
                oms   = np.copy(xx[:,ioff])
                truth[ioff] = ((20000.*np.pi + truth[ioff]) % (2.*np.pi))* 180./np.pi
                xx[:,ioff]  = ((20000.*np.pi + xx[:,ioff])  % (2.*np.pi))* 180./np.pi
                ioff += 1
            else:
                om    = Plan.om
                oms   = Plan.om

            if Plan.varfix[5] == 0:
                tperi = (200.*P + truth[ioff]) % P
                tperis= (200.*Ps + xx[:,ioff])  % Ps
                truth[ioff] = tperi
                xx[:,ioff]  = tperis
                ioff += 1
            else:
                tperi = (200.*P + Plan.tperi)  % P
                tperis= (200.*Ps+ Plan.tperi)  % Ps

            if Plan.varfix[6] == 0:
                Rp2Rs = truth[ioff]
                Rp2Rss= xx[:,ioff]
                ioff += 1
            else:
                Rp2Rs = Plan.Rp2Rs
                Rp2Rss= Plan.Rp2Rs

            if Plan.varfix[7] == 0:
                inc   = np.copy(truth[ioff])
                incs  = np.copy(xx[:,ioff])
                #print("inc", inc,Rp2Rs,tperi,P,K,ioff)
                truth[ioff] = (20000.*np.pi + truth[ioff]) % (2.*np.pi)* 180./np.pi
                xx[:,ioff]  = (20000.*np.pi + xx[:,ioff]) %  (2.*np.pi)* 180./np.pi
                ioff += 1
                #print("inc", inc)
            else:
                inc   = Plan.inc
                incs  = Plan.inc
            #print("inc",inc)
            if Plan.varfix[8] == 0:
                Om    = np.copy(truth[ioff])
                Oms   = np.copy(xx[:,ioff])
                truth[ioff] = (20000.*np.pi + truth[ioff]) % (2.*np.pi)* 180./np.pi
                xx[:,ioff]  = (20000.*np.pi + xx[:,ioff])  % (2.*np.pi)* 180./np.pi
                ioff += 1
            else:
                Om    = Plan.Om
                Oms   = Plan.Om

            if Plan.varfix[9] == 0:
                a2Rs = truth[ioff]
                a2Rss= xx[:,ioff]
                ioff += 1
            else:
                a2Rs  = Plan.a*au/star.Rstar/R_sun
                a2Rss = Plan.a*au/np.random.normal(star.Rstar,star.RstarE,len(samples))/R_sun
            #print(Plan.varfix[9],Plan.a2Rs,Plan.Rstar)

            if Plan.varfix[1] == 1:
                m     = Plan.m
                ms    = Plan.m
            if Plan.varfix[0] == 1:
                #print("inc", inc)
                K     = G13/tau13/np.sqrt(1.-e*e)   * m  *m_earth*np.sin(inc)\
                        / ((Plan.Mstar*M_sun+m*m_earth)**(2./3.)) / ((P*d2s)**(1./3.))
                Ks    = np.abs(G13/tau13/np.sqrt(1.-es*es) * ms *m_earth*np.sin(incs)\
                        / ((np.random.normal(star.Mstar,star.MstarE,len(samples))*M_sun+ms*m_earth)**(2./3.))/((Ps*d2s)**(1./3.)))
                #ind = np.where(not np.isfinite(Ks))
                #print(ind,ind[0],type(ind),type(ind[0]))
                #if ind[0] != []:
                #    print(es[ind])
                #    print(incs[ind])
                #    print(ms)
                #    print(Ps[ind])
                #    print(star.Mstar,star.MstarE,)
                #print(e,m,inc,Plan.Mstar,P)
                #print(es,ms,incs,np.random.normal(star.Mstar,star.MstarE,len(samples)),Ps)
            if Plan.varfix[1] == 1:
                mplanet = m * m_earth
                diff = 1.
                count = 0
                #print("inc", inc)
                while (diff > 1.e-3) & (count < 10):
                    try:
                        mnew = (P*d2s)**(1./3.) * tau13/G13*K*(Plan.Mstar*M_sun + mplanet)**(2./3.)*np.sqrt(1.-e*e)/np.sin(inc)
                    except ValueError:
                        mnew = mplanet
                        print('PlanetClass: problematic parameters:', P * d2s, K, Mstar)
                    diff = np.abs(mnew - mplanet) / mplanet
                    # print(diff,mnew,mplanet)
                    mplanet = mnew
                    count += 1
                m = mplanet/m_earth
                mplanet = ms * m_earth
                diff = 1.
                count = 0
                while (np.max(diff) > 1.e-3) & (count < 10):
                    try:
                        #print(np.sum(np.where(Ps < 0.)))
                        #print(np.sum(np.where(Ks < 0.)))
                        #print(np.sum(np.where(1-es*es < 0.)))
                        #print(np.sum(np.where(incs == 0.)))

                        mnew = (Ps*d2s)**(1./3.)*tau13/G13*Ks*\
                               (np.random.normal(Plan.Mstar,star.MstarE,len(samples))*M_sun+mplanet)**(2./3.) \
                                   * np.sqrt(1.-es*es)/np.sin(incs)
                        #if mnew <= 0:
                        #    print("mnew",mnew)
                        #if mnew <= 0:
                        #    print(1./0.)
                    except ValueError:
                        mnew = mplanet
                        print('PlanetClass: problematic parameters:', Ps * d2s, Ks, Plan.Mstar,star.MstarE,np.min(Ps), np.min(Ks))
                    diff = np.abs(mnew - mplanet) / mplanet
                    # print(diff,mnew,mplanet)
                    mplanet = mnew
                    count += 1
                ms = mplanet / m_earth
            if Plan.varfix[1] == 0:
                mplanet = ms * m_earth
                diff = 1.
                count = 0
                while (np.max(diff) > 1.e-5) & (count < 10):
                    try:
                        #print(np.sum(np.where(Ps < 0.)))
                        #print(np.sum(np.where(Ks < 0.)))
                        #print(np.sum(np.where(1-es*es < 0.)))
                        #print(np.sum(np.where(incs == 0.)))

                        mnew = (P*d2s)**(1./3.)*tau13/G13*Ks*\
                               (Plan.Mstar*M_sun+mplanet)**(2./3.) \
                                   * np.sqrt(1.-e*e)/np.sin(inc)
                        #if mnew <= 0:
                        #    print("mnew",mnew)
                        #if mnew <= 0:
                        #    print(1./0.)
                    except ValueError:
                        mnew = mplanet
                        print('PlanetClass: problematic parameters:', Ps * d2s, Ks, Plan.Mstar,star.MstarE,np.min(Ps), np.min(Ks))
                    diff = np.abs(mnew - mplanet) / mplanet
                    # print(diff,mnew,mplanet)
                    mplanet = mnew
                    count += 1
                ms = mplanet / m_earth

            if Plan.varadd[0] == 1:
                truth[ioffA] = K
                xx[:,ioffA] = Ks
                ioffA += 1
            if Plan.varadd[1] == 1:
                truth[ioffA] = m
                xx[:,ioffA] = ms
                ioffA += 1
            if Plan.varadd[2] == 1:
                #print(star.Mstar,mplanet)
                truth[ioffA] = (P*d2s)**(2./3.)*G13*tau23*(star.Mstar*M_sun+m* m_earth)**(1./3.)/au
                xx[:,ioffA]  = (Ps*d2s)**(2./3.)*G13*tau23*(np.random.normal(star.Mstar,star.MstarE,len(samples))*M_sun+ms*m_earth)**(1./3.)/au
                ioffA += 1
            if Plan.varadd[3] == 1:
                #print(om,e,P,tperi)
                f_tra = (0.5 * np.pi - om) % pi2
                tanE2 = np.tan(0.5 * f_tra) * np.sqrt((1. - e) / (1. + e))
                E = 2. * np.arctan(tanE2)
                Ma = E - e * np.sin(E)
                truth[ioffA] = Ma * P / pi2 + tperi
                f_tra = (0.5 * np.pi - oms) % pi2
                tanE2 = np.tan(0.5 * f_tra) * np.sqrt((1. - es) / (1. + es))
                E = 2. * np.arctan(tanE2)
                Ma = E - e * np.sin(E)
                xx[:,ioffA] = Ma * Ps / pi2 + tperis
                #print(om,e,P,tperi)
                #print(oms,es,Ps,tperis)

                ioffA += 1
            if Plan.varadd[4] == 1:
                truth[ioffA] = a2Rs
                xx[:,ioffA]  = a2Rss
                ioffA += 1
            if Plan.varadd[5] == 1:
                #print(Plan.Rstar,star.Rstar,star.RstarE)
                truth[ioffA] = Rp2Rs*Plan.Rstar*R_sun/r_earth
                if Plan.fixRstar:
                    xx[:,ioffA]  = Rp2Rss*np.random.normal(star.Rstar,star.RstarE,len(samples))*R_sun/r_earth
                else:
                    xx[:,ioffA]  = Rp2Rss*xx[:,mod.Soff]*R_sun/r_earth
                ioffA += 1
            if Plan.varadd[6] == 1:
                truth[ioffA] = m/truth[ioffA-1]**3*5.51
                xx[:,ioffA] = ms/xx[:,ioffA-1]**3*5.51
                ioffA += 1
            if Plan.varadd[7] == 1:
                #print("inc und om",inc,om)
                truth[ioffA] = P/a2Rs/np.pi * np.sqrt(1.-(a2Rs*np.cos(inc)*(1-e*e)/(1+np.abs(e)*np.sin(om)))**2)
                xx[:,ioffA]  = Ps/a2Rss/np.pi * np.sqrt(1.-(a2Rss*np.cos(incs)*(1-es*es)/(1+np.abs(es)*np.sin(oms)))**2)
                ind = np.where((a2Rss*np.cos(incs)*(1-es*es)/(1+np.abs(es)*np.sin(oms)))**2>=1)
                xx[ind,ioffA] = 0.
                ioffA += 1
            if Plan.varadd[8] == 1:
                xx[:,ioffA]  = ((2.*np.pi/Ps*(0.-tperis) + 
                                          oms + 200.*np.pi) % (2.*np.pi)) * 180./np.pi  # lambda
                truth[ioffA] = ((2.*np.pi/P*(0.-tperi) + 
                                          om  + 200.*np.pi) % (2.*np.pi)) * 180./np.pi # lambda
                #print('lambda',truth[ioffA])
                ioffA += 1
            self.PlanetList[i].varadd = np.copy(Plan.varadd)

        print("truth",truth)

        '''
            if Plan.orbit == 'Circular':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if not Plan.fixInc:
                            xx[:,offP+3] = ((200.*np.pi + xx[:,offP+3]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+3]= ((200.*np.pi + truth[offP+3]) % (2.*np.pi))* 180./np.pi  # inclination
                        xx[:,offP+1]  = (200.*xx[:,offP+0]  + xx[:,offP+1])  % xx[:,offP+0]  # tperi
                        truth[offP+1] = (200.*truth[offP+0] + truth[offP+1]) % truth[offP+0] # tperi
                    else:
                        if not Plan.fixInc:
                            xx[:,offP+4] = ((200.*np.pi + xx[:,offP+4]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+4]= ((200.*np.pi + truth[offP+4]) % (2.*np.pi))* 180./np.pi  # inclination                       
                        xx[:,offP+2]  = (200.*xx[:,offP+1]  + xx[:,offP+2])  % xx[:,offP+1]  # tperi
                        truth[offP+2] = (200.*truth[offP+1] + truth[offP+2]) % truth[offP+1] # tperi
                else:
                    xx[:,offP+2]  = (200.*xx[:,offP+1]  + xx[:,offP+2])  % xx[:,offP+1]  # tperi
                    truth[offP+2] = (200.*truth[offP+1] + truth[offP+2]) % truth[offP+1] # tperi
 
                     
                if not Plan.TransitOnly:
                    xx[:,offA+2]  = ((2.*np.pi/xx[:,offP+1]*(0.-xx[:,offP+2])  + 0.5*np.pi)% (2.*np.pi)) * 180./np.pi  # lambda
                    truth[offA+2] = ((2.*np.pi/truth[offP+1]*(0.-truth[offP+2])+ 0.5*np.pi)% (2.*np.pi)) * 180./np.pi  # lambda
                    mplanet = 0.
                    for i in range(10):
                        mplanet = (xx[:,offP+1]*d2s)**(1./3.)*tau13/G13*\
                                  xx[:,offP+0]*(np.random.normal(star.Mstar,star.MstarE,len(samples))+mplanet)**(2./3.)
                        if Plan.Transit:
                            if not Plan.fixInc:
                                mplanet = mplanet*np.sin(xx[:,offP+4]*np.pi/180.)
                            else:
                                mplanet = mplanet*np.sin(Plan.inc)

                    xx[:,offA+0] = mplanet/m_earth # planet mass
                    mplanet = 0.
                    for i in range(10):
                        mplanet = (truth[offP+1]*d2s)**(1./3.)*tau13/G13*\
                                  truth[offP+0]*(star.Mstar+mplanet)**(2./3.)
                        if Plan.Transit: 
                            if not Plan.fixInc:
                                mplanet = mplanet*np.sin(truth[offP+4]*np.pi/180.)
                            else:
                                mplanet = mplanet*np.sin(Plan.inc)
                    truth[offA+0] = mplanet/m_earth  # planet mass
                    xx[:,offA+1]  = (xx[:,offP+1]*d2s)**(2./3.)*G13*tau23*\
                                         (np.random.normal(star.Mstar,star.MstarE,len(samples))+xx[:,offA+0]*m_earth)**(1./3.)/au  # semi major axis
                    truth[offA+1] = (truth[offP+1]*d2s)**(2./3.)*G13*tau23*\
                                     (star.Mstar+truth[offA+0]*m_earth)**(1./3.)/au # semi major axis
                    if Plan.Transit:
                        xx[:,offA+3]  = xx[:,offA+1]/np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples))*au/R_sun # a to R star
                        truth[offA+3] = truth[offA+1]/truth[mod.Soff+0]*au/R_sun # a to R star
                        xx[:,offA+4]  = xx[:,offP+3]*R_sun/r_earth*\
                                            np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples)) # planet radius
                        truth[offA+4] = truth[offP+3]*R_sun/r_earth*truth[mod.Soff+0]  # planet radius
                        xx[:,offA+5]  = xx[:,offA+0]/xx[:,offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
                        truth[offA+5] = truth[offA+0]/truth[offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
            if Plan.orbit == 'Kepler':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if not Plan.fixInc:
                            xx[:,offP+5] = ((200.*np.pi + xx[:,offP+5]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+5]= ((200.*np.pi + truth[offP+5]) % (2.*np.pi))* 180./np.pi  # inclination
                        xx[:,offP+3]  = (200.*xx[:,offP+0]  + xx[:,offP+3])  % xx[:,offP+0]  # tperi
                        truth[offP+3] = (200.*truth[offP+0] + truth[offP+3]) % truth[offP+0] # tperi
                        xx[:,offP+1] = np.abs(xx[:,offP+1])                                            # eccentricity
                        truth[offP+1]= np.abs(truth[offP+1])                                           # eccentricity
                        xx[:,offP+2] = ((20000.*np.pi + xx[:,offP+2]) % (2.*np.pi)) * 180./np.pi       # omega
                        truth[offP+2]= ((20000.*np.pi + truth[offP+2]) % (2.*np.pi))* 180./np.pi       # omega
                    else:
                        if not Plan.fixInc:
                            xx[:,offP+6] = ((200.*np.pi + xx[:,offP+6]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+6]= ((200.*np.pi + truth[offP+6]) % (2.*np.pi))* 180./np.pi  # inclination                       
                        xx[:,offP+4]  = (200.*xx[:,offP+1]  + xx[:,offP+4])  % xx[:,offP+1]  # tperi
                        truth[offP+4] = (200.*truth[offP+1] + truth[offP+4]) % truth[offP+1] # tperi
                        xx[:,offP+2] = np.abs(xx[:,offP+2])                                            # eccentricity
                        truth[offP+2]= np.abs(truth[offP+2])                                           # eccentricity
                        xx[:,offP+3] = ((20000.*np.pi + xx[:,offP+3]) % (2.*np.pi)) * 180./np.pi       # omega
                        truth[offP+3]= ((20000.*np.pi + truth[offP+3]) % (2.*np.pi))* 180./np.pi       # omega
                else:
                    xx[:,offP+4]  = (200.*xx[:,offP+1]  + xx[:,offP+4])  % xx[:,offP+1]  # tperi
                    truth[offP+4] = (200.*truth[offP+1] + truth[offP+4]) % truth[offP+1] # tperi
                    xx[:,offP+2] = np.abs(xx[:,offP+2])                                            # eccentricity
                    truth[offP+2]= np.abs(truth[offP+2])                                           # eccentricity
                    xx[:,offP+3] = ((20000.*np.pi + xx[:,offP+3]) % (2.*np.pi)) * 180./np.pi       # omega
                    truth[offP+3]= ((20000.*np.pi + truth[offP+3]) % (2.*np.pi))* 180./np.pi       # omega
                      
                if not Plan.TransitOnly:
                    xx[:,offA+2] = ((2.*np.pi/xx[:,offP+1]*(0.-xx[:,offP+4]) + 
                                          xx[:,offP+3]*np.pi/180. + 20000.*np.pi) % (2.*np.pi)) * 180./np.pi  # lambda
                    truth[offA+2] = ((2.*np.pi/truth[offP+1]*(0.-truth[offP+4]) + 
                                          truth[offP+3]*np.pi/180. + 20000.*np.pi) % (2.*np.pi)) * 180./np.pi # lambda
                    mplanet = 0.
                    for i in range(10):
                        #print(np.min(xx[:,offP+1]),np.max(xx[:,offP+1]))
                        #print(np.min(xx[:,offP+2]),np.max(xx[:,offP+2]))
                        #print(np.min(xx[:,offP+0]),np.max(xx[:,offP+0]))
                        #if not Plan.fixInc:print(np.min(xx[:,offP+6]),np.max(xx[:,offP+6]))
                        #print(star.Mstar,star.MstarE)
                        #print(np.random.normal(star.Mstar,star.MstarE,len(samples))+mplanet)
                        #print(mplanet/m_earth)
                        mplanet = (xx[:,offP+1]*d2s)**(1./3.)*np.sqrt(1.-xx[:,offP+2]**2)*tau13/G13*\
                                  xx[:,offP+0]*(np.random.normal(star.Mstar,star.MstarE,len(samples))+mplanet)**(2./3.)
                        if Plan.Transit: 
                            if not Plan.fixInc:
                                mplanet = mplanet*np.sin(xx[:,offP+6]*np.pi/180.)
                            else:
                                mplanet = mplanet*np.sin(Plan.inc)
                    xx[:,offA+0] = mplanet/m_earth # planet mass
                    mplanet = 0.
                    for i in range(10):
                        mplanet = (truth[offP+1]*d2s)**(1./3.)*np.sqrt(1.-truth[offP+2]**2)*tau13/G13*\
                                  truth[offP+0]*(star.Mstar+mplanet)**(2./3.)
                        if Plan.Transit: 
                            if not Plan.fixInc:
                                mplanet = mplanet*np.sin(truth[offP+6]*np.pi/180.)
                            else:
                                mplanet = mplanet*np.sin(Plan.inc)
                    truth[offA+0] = mplanet/m_earth  # planet mass
                    xx[:,offA+1]  = (xx[:,offP+1]*d2s)**(2./3.)*G13*tau23*\
                                         (np.random.normal(star.Mstar,star.MstarE,len(samples))+xx[:,offA+0]*m_earth)**(1./3.)/au  # semi major axis
                    truth[offA+1] = (truth[offP+1]*d2s)**(2./3.)*G13*tau23*\
                                     (star.Mstar+truth[offA+0]*m_earth)**(1./3.)/au # semi major axis
                    if Plan.Transit:
                        xx[:,offA+3]  = xx[:,offA+1]/np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples))*au/R_sun # a to R star
                        truth[offA+3] = truth[offA+1]/truth[mod.Soff+0]*au/R_sun # a to R star
                        xx[:,offA+4]  = xx[:,offP+5]*R_sun/r_earth*\
                                            np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples)) # planet radius
                        truth[offA+4] = truth[offP+5]*R_sun/r_earth*truth[mod.Soff+0]  # planet radius
                        xx[:,offA+5]  = xx[:,offA+0]/xx[:,offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
                        truth[offA+5] = truth[offA+0]/truth[offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
            if Plan.orbit == 'Dynamic':
                if Plan.Transit:
                    if Plan.TransitOnly:
                        if not Plan.fixInc and not Plan.Coplanar:
                            xx[:,offP+5] = ((200.*np.pi + xx[:,offP+5]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+5]= ((200.*np.pi + truth[offP+5]) % (2.*np.pi))* 180./np.pi  # inclination
                        xx[:,offP+3]  = (200.*xx[:,offP+0]  + xx[:,offP+3])  % xx[:,offP+0]  # tperi
                        truth[offP+3] = (200.*truth[offP+0] + truth[offP+3]) % truth[offP+0] # tperi
                        xx[:,offP+1] = np.abs(xx[:,offP+1])                                            # eccentricity
                        truth[offP+1]= np.abs(truth[offP+1])                                           # eccentricity
                        xx[:,offP+2] = ((20000.*np.pi + xx[:,offP+2]) % (2.*np.pi)) * 180./np.pi       # omega
                        truth[offP+2]= ((20000.*np.pi + truth[offP+2]) % (2.*np.pi))* 180./np.pi       # omega
                    else:
                        if not Plan.fixInc and not Plan.Coplanar:
                            xx[:,offP+6] = ((200.*np.pi + xx[:,offP+6]) % (2.*np.pi)) * 180./np.pi # inclination
                            truth[offP+6]= ((200.*np.pi + truth[offP+6]) % (2.*np.pi))* 180./np.pi  # inclination                       
                        xx[:,offP+4]  = (200.*xx[:,offP+1]  + xx[:,offP+4])  % xx[:,offP+1]  # tperi
                        truth[offP+4] = (200.*truth[offP+1] + truth[offP+4]) % truth[offP+1] # tperi
                        xx[:,offP+2] = np.abs(xx[:,offP+2])                                            # eccentricity
                        truth[offP+2]= np.abs(truth[offP+2])                                           # eccentricity
                        xx[:,offP+3] = ((20000.*np.pi + xx[:,offP+3]) % (2.*np.pi)) * 180./np.pi       # omega
                        truth[offP+3]= ((20000.*np.pi + truth[offP+3]) % (2.*np.pi))* 180./np.pi       # omega
                else:
                    if not Plan.fixInc and not Plan.Coplanar:
                        xx[:,offP+5] = ((200.*np.pi + xx[:,offP+5]) % (2.*np.pi)) * 180./np.pi # inclination
                        truth[offP+5]= ((200.*np.pi + truth[offP+5]) % (2.*np.pi))* 180./np.pi  # inclination
                    xx[:,offP+4]  = (200.*xx[:,offP+1]  + xx[:,offP+4])  % xx[:,offP+1]  # tperi
                    truth[offP+4] = (200.*truth[offP+1] + truth[offP+4]) % truth[offP+1] # tperi
                    xx[:,offP+2] = np.abs(xx[:,offP+2])                                            # eccentricity
                    truth[offP+2]= np.abs(truth[offP+2])                                           # eccentricity
                    xx[:,offP+3] = ((20000.*np.pi + xx[:,offP+3]) % (2.*np.pi)) * 180./np.pi       # omega
                    truth[offP+3]= ((20000.*np.pi + truth[offP+3]) % (2.*np.pi))* 180./np.pi       # omega
                      
                if not Plan.TransitOnly:
                    xx[:,offA+2] = ((2.*np.pi/xx[:,offP+1]*(0.-xx[:,offP+4]) + 
                                          xx[:,offP+3]*np.pi/180. + 20000.*np.pi) % (2.*np.pi)) * 180./np.pi  # lambda
                    truth[offA+2] = ((2.*np.pi/truth[offP+1]*(0.-truth[offP+4]) + 
                                          truth[offP+3]*np.pi/180. + 20000.*np.pi) % (2.*np.pi)) * 180./np.pi # lambda
                    if not Plan.fixInc and not Plan.Coplanar:
                        xx[:,offA+0] = G13/tau13/np.sqrt(1.-xx[:,offP+2]**2) * xx[:,offP+0]*m_earth*np.sin(xx[:,offP+6]*np.pi/180.) \
                                       / ((np.random.normal(star.Mstar,star.MstarE,len(samples))+xx[:,offP+0]*m_earth)**(2./3.)) \
                                       / ((xx[:,offP+1]*d2s)**(1./3.)) # RV amplitude
                        truth[offA+0] = G13/tau13/np.sqrt(1.-truth[offP+2]**2) * truth[offP+0]*m_earth*np.sin(truth[offP+6]*np.pi/180.) \
                                       / ((star.Mstar+truth[offP+0]*m_earth)**(2./3.)) \
                                       / ((truth[offP+1]*d2s)**(1./3.)) # RV amplitude
                    else:
                        xx[:,offA+0] = G13/tau13/np.sqrt(1.-xx[:,offP+2]**2) * xx[:,offP+0]*m_earth*np.sin(Plan.inc) \
                                       / ((np.random.normal(star.Mstar,star.MstarE,len(samples))+xx[:,offP+0]*m_earth)**(2./3.)) \
                                       / ((xx[:,offP+1]*d2s)**(1./3.)) # RV amplitude
                        truth[offA+0] = G13/tau13/np.sqrt(1.-truth[offP+2]**2) * truth[offP+0]*m_earth*np.sin(Plan.inc) \
                                       / ((star.Mstar+truth[offP+0]*m_earth)**(2./3.)) \
                                       / ((truth[offP+1]*d2s)**(1./3.)) # RV amplitude
                    xx[:,offA+1]  = (xx[:,offP+1]*d2s)**(2./3.)*G13*tau23*\
                                         (np.random.normal(star.Mstar,star.MstarE,len(samples))+xx[:,offP+0]*m_earth)**(1./3.)/au  # semi major axis
                    truth[offA+1] = (truth[offP+1]*d2s)**(2./3.)*G13*tau23*\
                                     (star.Mstar+truth[offP+0]*m_earth)**(1./3.)/au # semi major axis
                    if Plan.Transit:
                        xx[:,offA+3]  = xx[:,offA+1]/np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples))*au/R_sun # a to R star
                        truth[offA+3] = truth[offA+1]/truth[mod.Soff+0]*au/R_sun # a to R star
                        xx[:,offA+4]  = xx[:,offP+5]*R_sun/r_earth*\
                                            np.random.normal(xx[:,mod.Soff+0],star.RstarE/R_sun,len(samples)) # planet radius
                        truth[offA+4] = truth[offP+5]*R_sun/r_earth*truth[mod.Soff+0]  # planet radius
                        xx[:,offA+5]  = xx[:,offP+0]/xx[:,offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
                        truth[offA+5] = truth[offP+0]/truth[offA+4]**3*5.51 # planet density; earth density=5.51 g/cm^3
            offP += Plan.Ppar
            if Plan.Transit:
                if Plan.TransitOnly: nAdd = 0
                else:                nAdd = 6
            else:                    nAdd = 3
            offA += nAdd
        '''
        return (truth,xx)
                        
################################################################################
class MeanModOrbitRV(Model):
    paras = Paras()
    paranames = []
    for k in range(paras.numPlanmax):
        paranames.append("K"+str(k))
        paranames.append("m"+str(k))
        paranames.append("P"+str(k))
        paranames.append("e"+str(k))
        paranames.append("om"+str(k))
        paranames.append("tperi"+str(k))
        paranames.append("Rp2Rs"+str(k))
        paranames.append("inc"+str(k))
        paranames.append("Om"+str(k))
        paranames.append("a2Rs"+str(k))
    paranames.append("Rstar")
    for k in range(paras.nfTRmax):
        paranames.append("u1"+str(k))
        paranames.append("u2"+str(k))
        paranames.append("thirdL"+str(k))
 
    for k in range(paras.nfRVmax): paranames.append("offRV"+str(k))    
    for k in range(paras.nfACmax): paranames.append("offAC"+str(k))    
    for k in range(paras.nfTRmax): paranames.append("offTR"+str(k))    
    #for k in range(paras.nfTRmax-1): paranames.append("t0TR"+str(k))    
    
    paranames.append("lin")    
    paranames.append("quad")    
    
    for k in range(paras.nfTRmax):
        for kk in range(paras.flmax):
            paranames.append("FlareTime"+str(k+100)+str(kk))
            paranames.append("FlareAmp"+str(k+100)+str(kk))
            paranames.append("FlareFWHM"+str(k+100)+str(kk))

    parameter_names = (tuple(paranames))
    unimportant_params = (paras.nfTRmax*paras.flmax*3+paras.nfTRmax*3+paras.nfTRmax+paras.nfACmax+paras.nfRVmax+2)
    print("Model Orbit",parameter_names[:-unimportant_params],len(parameter_names))
    print(unimportant_params)
   
    def get_value(self, t):
        #global datatype
        import time

        global PlanetListe
        global RVListe
        global TRListe
        
        global calc_rebound
        global iRV
        global All_times
        global All_indexs
        global vz,vzp,StabProb
    
        
        numPlanets = len(PlanetListe)
        #nfRV       = len(RVListe)
        #nfTR       = len(TRListe)
        try:
            #minPer = np.min([l.P for l in PlanetListe if (l.P > 0.) & (l.orbit == 'Dynamic')])
            minPer = np.min([l.P for l in PlanetListe if (l.orbit == 'Dynamic' or l.orbit == 'TTV')])
        except:
            minPer = minPER
        
        if PlanetListe != []:
            Mstar   = PlanetListe[0].Mstar
        else:
            Mstar = 1.
        Planets  = []
        for i,Plan in enumerate(PlanetListe):
            
            if Plan.Coplanar:   inc = self.inc0
            else:               inc = eval(f"self.inc{i}")

            Planets.append(Planet(Mstar = Mstar, Rstar = self.Rstar,
                                 K  = eval(f"self.K{i}"),
                                 m  = eval(f"self.m{i}"),
                                 P  = eval(f"self.P{i}"),
                                 e  = eval(f"self.e{i}"),
                                 om = eval(f"self.om{i}"),
                                 tperi = eval(f"self.tperi{i}"),
                                 Rp2Rs = eval(f"self.Rp2Rs{i}"),
                                 inc = inc,
                                 Om= eval(f"self.Om{i}"),
                                 a2Rs= eval(f"self.a2Rs{i}"),

                                 orbit = PlanetListe[i].orbit,
                                 Transit = PlanetListe[i].Transit,
                                 TransitOnly = PlanetListe[i].TransitOnly
                                 ))

        mod = np.zeros(len(t))
        Kmod = np.zeros(len(t))
        calcDynamic = False
        if platform.system() != 'Windows':
            sim = rebound.Simulation()
            sim.add(m=Mstar) # central star in solar units
        dynPlan = 0
        mratios = []
        for j,Plan in enumerate(Planets):
            #print(Plan.K)
            if (Plan.orbit == 'Dynamic' or Plan.orbit == 'TTV'):
                calcDynamic = True
                dynPlan += 1
                #star = pp.Star('star', m = Mstar/M_sun, r = self.Rstar/R_sun)
                #print(Plan.m, Plan.K, Plan.P, Plan.e, Plan.om,
                #        Plan.tperi , Plan.inc, Plan.Om)
                #print(Plan.m,Plan.P,Plan.e,Plan.om,Plan.tperi,Plan.inc,Plan.Om)
                sim.add(m=Plan.m*m_earth/M_sun, P=Plan.P/yr2pi, e=Plan.e, omega=Plan.om,
                        T=Plan.tperi/yr2pi , inc=Plan.inc, Omega=Plan.Om)
                mratios.append(Plan.m*m_earth/Mstar/M_sun)
                #planet = pp.Planet('planet', m = Plan.m, per = Plan.P, \
                #                   inc = Plan.inc*180/np.pi, t0 = Plan.tperi,\
                #                   w=Plan.om*180/np.pi,ecc=Plan.e)
                Kmod += Plan.Kepler(t=t)   #Plan.Dynamic(t=t)
            else:
                mod += Plan.Kepler(t=t)
                Kmod += Plan.Kepler(t=t)   #Plan.Dynamic(t=t)
        #print("calc_dynamic",calcDynamic,calc_rebound)
        if calcDynamic:
            if calc_rebound:
                strt = time.time()
                sim.move_to_com()
                #print(SPOCK,dynPlan)
                if SPOCK & (dynPlan > 2):
                    #print("test dynamical stability")
                    #print(sim.status())
                    model = FeatureClassifier()
                    #print("FeatureClassifier")
                    StabProb = np.log(model.predict_stable(sim)+0.0001)#1-model.predict_stable(sim)
                    #print(StabProb,model.predict_stable(sim))             
                sim.dt = minPer*reboundPrec/yr2pi # only relevant for WHFAST
                sim.integrator = integrator
                if iRV >= 0:
                    RV_times = All_times/yr2pi
                else: 
                    RV_times = t/yr2pi
                vz     = []
                vzp    = np.zeros((len(RV_times),numPlanets))#,dynPlan))
                for j,times in enumerate(list(RV_times)):
                    sim.integrate(times, exact_finish_time=1)
                    sim.move_to_com()
                    vz.append(sim.particles[0].vz)
                    for jj in range(numPlanets): #dynPlan):
                        if Planets[jj].orbit == "dynamic":
                            vzp[j,jj] = np.array(sim.particles[1+jj].vz)*np.array(mratios[jj])
                    #print(sim.particles[0].vx)
                    #print(sim.particles[0].vy)
                    #print(sim.particles[0].vz)
                if iRV >= 0: calc_rebound = False
            if iRV >= 0:
                ind = np.where(All_indexs == iRV)
                #print("len(ind)",len(ind[0]))
                mod += -np.array(vz)[ind]*timeconv
            else:
                mod += -np.array(vz)*timeconv
            
            #print(Plan.inc)
            #print(np.sqrt(G*M_sun/au),pi2*au/365.25/d2s)
            #print(np.max(mod),np.max(Plan.Kepler(t=t)))
            #if iRV == 2:
            #    fig = plt.figure(figsize=(20,10))
            #    plt.plot(t,Kmod)
            #    plt.plot(All_times[ind],mod)
            #    plt.plot(All_times,-np.array(vz)*timeconv)
            #    #plt.plot(t,Kmod-mod)
            #    plt.show()
            #    print(All_times)
            #    stop
            #fig = plt.figure()plt.plot(t,Kmod)
            #fig.set_size_inches(xsiz,ysiz)
            #plt.plot(t,mod)
            #plt.plot(t,Kmod)
            #plt.show()
            #print(len(t),minPer)
            #print("run time:",time.time()-strt)
            #print(np.std(mod-Plan.Kepler(t=t)))
            #sim.status()
            #
            #strt = time.time()
            #system=pp.System(star, planet)#,integrator='WHFAST')
            #system.compute(t)
            #plt.plot(t,star.flux)
            #plt.show()
            #plt.plot(t,star.vz)
            #print(star.vz)
            #plt.show()
            #print("run time:",time.time()-strt)
            #stop
        #print()
        #plt.plot(t,mod)
        #plt.show()
 
        return mod

######################################################################
class MeanModOrbitTR(Model):
    #def __init__(self,RVList,TRList):
    #    Paras.__init__(self)#,ndeg=0,lin=0.,quad=0.,numPlanmax=4,nfRVmax=7,nfACmax=2,nfTRmax=10)
    #    #super()
    #    #self.numPlanmax = numPlanmax

    #global datatype
    paras = Paras()
    paranames = []
    for k in range(paras.numPlanmax):
        paranames.append("K"+str(k))
        paranames.append("m"+str(k))
        paranames.append("P"+str(k))
        paranames.append("e"+str(k))
        paranames.append("om"+str(k))
        paranames.append("tperi"+str(k))
        paranames.append("Rp2Rs"+str(k))
        paranames.append("inc"+str(k))
        paranames.append("Om"+str(k))
        paranames.append("a2Rs"+str(k))
    paranames.append("Rstar")
    for k in range(paras.nfTRmax):
        paranames.append("u1"+str(k))
        paranames.append("u2"+str(k))
        paranames.append("thirdL"+str(k))
 
    for k in range(paras.nfRVmax): paranames.append("offRV"+str(k))    
    for k in range(paras.nfACmax): paranames.append("offAC"+str(k))    
    for k in range(paras.nfTRmax): paranames.append("offTR"+str(k))    
    #for k in range(paras.nfTRmax-1): paranames.append("t0TR"+str(k))    
    
    paranames.append("lin")    
    paranames.append("quad")    
    
    for k in range(paras.nfTRmax):
        for kk in range(paras.flmax):
            paranames.append("FlareTime"+str(k+100)+str(kk))
            paranames.append("FlareAmp"+str(k+100)+str(kk))
            paranames.append("FlareFWHM"+str(k+100)+str(kk))
    
    parameter_names = (tuple(paranames))
    # print("Model Orbit",parameter_names)
    unimportant_params = (paras.nfTRmax*paras.flmax*3+paras.nfTRmax*3+paras.nfTRmax+paras.nfACmax+paras.nfRVmax+2)
    print("Model Orbit",parameter_names[:-unimportant_params],len(parameter_names))
    print(unimportant_params)

    def get_value(self, t):
        #global datatype
        global PlanetListe
        global RVListe
        global TRListe
        global TT_times
        global TT_Stimes
        global TT_Etimes
        global TT_index
        global TT_planet

        global calc_reboundTR
        global iTR
        global All_timesTR
        global All_indexsTR
        global tTTV
        global Ps,a2Rss,es,oms,incs,tperis,tis,Pnum,Tas
        global TTVs,StabProb
        global ttvs
        
        ttvs = []
             
        numPlanets = len(PlanetListe)
        nfTR       = len(TRListe)
        nfRV       = len(RVListe)
        
        numPlanets = len(PlanetListe)
        #nfRV       = len(RVListe)
        #nfTR       = len(TRListe)
        try:
            #minPer = np.min([l.P for l in PlanetListe if (l.P > 0.) & (l.orbit == 'Dynamic')])
            minPer = np.min([l.P for l in PlanetListe if (l.orbit == 'Dynamic')])
        except:
            minPer = 30
        
        Mstar   = PlanetListe[0].Mstar
        Planets  = []
        for i,Plan in enumerate(PlanetListe):
            #print("TransitOnly",PlanetListe[i].TransitOnly)
            if Plan.Coplanar:   inc = self.inc0
            else:               inc = eval(f"self.inc{i}")

            Planets.append(Planet(Mstar = Mstar, Rstar = self.Rstar,
                                 K  = eval(f"self.K{i}"),
                                 m  = eval(f"self.m{i}"),
                                 P  = eval(f"self.P{i}"),
                                 e  = eval(f"self.e{i}"),
                                 om = eval(f"self.om{i}"),
                                 tperi = eval(f"self.tperi{i}"),
                                 Rp2Rs = eval(f"self.Rp2Rs{i}"),
                                 inc = inc,
                                 Om= eval(f"self.Om{i}"),
                                 a2Rs= eval(f"self.a2Rs{i}"),

                                 orbit = PlanetListe[i].orbit,
                                 Transit = PlanetListe[i].Transit,
                                 TransitOnly = PlanetListe[i].TransitOnly
                                 ))
        TRansits = []
        for i in range(nfTR):
            kk = TRListe[i].ParFileNr
            if TRListe[i].do_t0shift:
                t0shift=eval(f"self.t0TR{kk}")
            else:
                t0shift = 0.
            TRansits.append(TRTimeSeries(fileName = TRListe[i], dataName= "dummy",
                                         GP=TRListe[i].GP, filetype= 'NONE',
                                         GPpar=TRListe[i].GPpar, 
                                         u1= eval(f"self.u1{kk}"), 
                                         u2= eval(f"self.u2{kk}"),
                                         thirdL= eval(f"self.thirdL{kk}"),
                                         fix3=TRListe[i].fix3,
                                         fixLD=TRListe[i].fixLD,
                                         t0shift=t0shift,
                                         do_t0shift=TRListe[i].do_t0shift))

        mod = np.zeros(len(t))+1.-numPlanets
        calcDynamic = False
        if platform.system() != 'Windows':
            sim = rebound.Simulation()
            sim.add(m=Mstar) # central star in solar units
        dynPlan = 0
        TTVplanets = np.zeros(len(Planets))
        for j,Plan in enumerate(Planets):
            #print('j')
            leastsq_model = np.zeros(len(t))     #len(TRListe[iTR].time))
            if (TRListe[iTR].detrend == 'Linear') and (Plan.orbit != 'Dynamic'):
                MAmod        = Plan.MandelAgol(TRListe[iTR].time,TRansits[iTR].u1,TRansits[iTR].u2)
                leastsq_coef  = np.dot(MAmod-TRListe[iTR].value,np.linalg.pinv(TRListe[iTR].vector))
                leastsq_model = np.dot(leastsq_coef,TRListe[iTR].vector)
            if Plan.orbit == 'Dynamic':
                calcDynamic = True
                dynPlan += 1
                #star = pp.Star('star', m = Mstar/M_sun, r = self.Rstar/R_sun) # a possible call to planetplanet
                #
                # in heliocentric coordinates, for Moons: use index of parent body as primary
                #sim.move_to_hel()
                #sim.add(primary=sim.particles[0],m=Plan.m*m_earth/M_sun, P=Plan.P/yr2pi, e=Plan.e, omega=Plan.om,
                #        T=Plan.tperi/yr2pi , inc=Plan.inc, Omega=Plan.Om)
                sim.add(m=Plan.m*m_earth/M_sun, P=Plan.P/yr2pi, e=Plan.e, omega=Plan.om,
                        T=Plan.tperi/yr2pi , inc=Plan.inc, Omega=Plan.Om)
                if SPOCK & (dynPlan > 2):
                    model = FeatureClassifier()
                    StabProb = np.log(model.predict_stable(sim)+0.0001)#1-model.predict_stable(sim)
            elif Plan.orbit == 'TTV':
                TTVplanets[j] = 1
            else:
                if Plan.TransitOnly:
                    mod += Plan.MandelAgol(t,TRansits[iTR].u1,TRansits[iTR].u2,TRansits[iTR].t0shift) - leastsq_model
                else:
                    mod += Plan.MandelAgolRV(t,TRansits[iTR].u1,TRansits[iTR].u2) - leastsq_model                        

        if (sum(TTVplanets) == 1 or sum(TTVplanets >=9)):
            print(f'For TTV calculation at least 2 and maximal 9 planets necessary. {int(sum(TTVplanets))} planet(s) given.')
            for Plan in [p for p,TTV in zip(Planets,TTVplanets) if TTV]:
                if Plan.TransitOnly:
                    mod += Plan.MandelAgol(t,TRansits[iTR].u1,TRansits[iTR].u2,TRansits[iTR].t0shift) - leastsq_model
                else:
                    mod += Plan.MandelAgolRV(t,TRansits[iTR].u1,TRansits[iTR].u2) - leastsq_model

        elif sum(TTVplanets) >= 2:
            # print('TTVplanets: ',TTVplanets)
            # print('iTR: ', iTR)
            ######################################
            # calculate TTVfaster transit time
            # and corresponding MA-lightcurve
            
            # TODO: x t adaptation for TTV
            #       x tmin and tmax FROM WHERE???
            #       x matching transit from simulation with current cutout
            #         fixing mix of dynamic/TTV and others (not working since Pnum out of bounds)
            
            if iTR == 0:
                tTTVfull = []
                Ps     = []
                es     = []
                oms    = []
                tTTV = [] 
                incs   = []
                a2Rss  = []
                Pnum   = []
                tis    = []
                ttvs = []
                
                # [tTTV.append([]) for _ in range(numPlanets)]
                # [Pnum.append([]) for _ in range(numPlanets)]
                # [tis.append([]) for _ in range(numPlanets)]
            
                tmin =   0     # initial time
                tmax =   np.amax(TT_Etimes)     # final time
                j_max = 6   # maximum j to evaluate
                
                # print(f'TTV:\n start: {tmin}\n end: {tmax}')
                # print(f'TTV:\n start: {np.amin(t)}\n end: {np.amax(t)}')
                
                params = []
                params.append(Planets[0].Mstar)
                
                for P in [p for p,TTV in zip(Planets,TTVplanets) if TTV]:   
                    # creating parameter vector for TTV faster sim
                    #                m                 P   e*cos(arg peri)     i    Omega  e*sin(arg peri)    TT
                    params.extend([P.m*m_earth/M_sun, P.P, P.e*np.cos(P.om), P.inc, P.Om-np.pi/2, P.e*np.sin(P.om), P.get_ttra()-tmin])
                # print(f"TTVfaster input Parameter: {params}")
                
                tTTVfull = run_ttvfaster(sum(TTVplanets), params, tmin, tmax, j_max)
                # print('tTTVfull: ',tTTVfull)
                
                for ti,times in enumerate(TT_times):
                    planTTV = 0
                    for j, TTV in enumerate(TTVplanets):
                        if TTV:
                            Pnum.append(TT_index[ti])
                            tis.append(ti)
                                                       
                            trad = Planets[j].get_trad()

                            ttra = Planets[j].get_ttra()
                            
                            for TTVtime in tTTVfull[planTTV]:
                                if TT_Stimes[ti]-trad/2 <= TTVtime and TTVtime <= TT_Etimes[ti]+trad/2:
                                    tTTV.append(TTVtime)
                                    ttvs.append(TTVtime-ttra)
                                    
                            if len(tis) != len(tTTV):
                                tTTV.append(0)
                                ttvs.append(0)

                                
                            planTTV += 1
                            
            tTTV   = np.array(tTTV)
            incs   = np.array(incs)                     
                    
            # print('Pnum: ',Pnum)
            # print('tTTV: ',tTTV)
            if iTR >= 0:
                for j,TTV in enumerate(TTVplanets):
                    # print('Planet',j)
                    # print('iTR:',iTR)
                    if TTV:
                        # print('index, where Pnum=iTR: ',np.where(np.array(Pnum) == iTR))
                        inds = (np.where(np.array(Pnum) == iTR))[0][j]
                        # print('#####################################')
                        # print('inds: ',inds)
                        # print('iTR:', iTR)
                        # print(np.amin(t), tTTV[inds], np.amax(t))
                        # print('#####################################')
                        PlanetMA= Planet(Mstar=Mstar,Rstar=self.Rstar,
                                         K=Planets[j].K,
                                         m=Planets[j].m,
                                         P=Planets[j].P,
                                         e=Planets[j].e,
                                         om=Planets[j].om,
                                         tperi=Planets[j].get_tperi(tTTV[inds]),
                                         inc=Planets[j].inc,
                                         Rp2Rs=Planets[j].Rp2Rs,
                                         Om=Planets[j].Om,
                                         a2Rs=Planets[j].a2Rs,
                                         orbit=Planets[j].orbit,
                                         Transit=Planets[j].Transit,
                                         TransitOnly=Planets[j].TransitOnly)
                        
                        ti = tis[inds]
                        trad = PlanetMA.get_trad()
                        ttra = PlanetMA.get_ttra()
                        # if TT_Stimes[ti]-trad*5 <= ttra and ttra <= TT_Etimes[ti]+trad*5:
                        mod += PlanetMA.MandelAgolRV(t,TRansits[iTR].u1,TRansits[iTR].u2) - leastsq_model
                        # else:
                        #     mod += 1.
            # stop
            
            
        if calcDynamic:
            if calc_reboundTR:
                # calculate a rebound model for all data sets, 
                # output Keplerian orbital elements for Transit mid points and 
                # calculate a Mandel-Agol model with parameter set from rebound for each transit.
                # Important: That approach assuems that the change of orbital elements over the transit is negligible
                strt = time.time()
                sim.move_to_com()
                sim.dt = minPer*reboundPrec/yr2pi # only relevant for WHFAST
                sim.integrator = integrator
                gr = False #True
                if gr:
                    rebx = reboundx.Extras(sim)
                    gr = rebx.load_force("gr")
                    rebx.add_force(gr)
                    gr.params["c"] = constants.C
                Ps     = []
                es     = []
                oms    = []
                tperis = []
                incs   = []
                a2Rss  = []
                Pnum   = []
                tis    = []
                ttras  = []
                ttvs   = []  # difference between observed and calculated transit time, effective in case of usediff==True
                if iTR >= 0:
                    TR_times = np.array(TT_times)/yr2pi
                else: 
                    TR_times = t/yr2pi
                for ti,times in enumerate(list(TR_times)):
                    sim.integrate(times, exact_finish_time=1)
                    #sim.status()
                    for j in range(numPlanets):
                        if Planets[j].orbit == 'Dynamic':
                            # if projected distance of planet relative to star shall be used
                            #z = np.sqrt((sim.particles[0].x-sim.particles[j+1].x)**2+(sim.particles[0].y-sim.particles[j+1].y)**2)*au/self.Rstar/R_sun
                            # use Keplerian orbital elements
                            OrbitElements = sim.particles[j+1].calculate_orbit(primary=sim.particles[0])
                            Ps.append(OrbitElements.P*yr2pi)
                            es.append(OrbitElements.e)
                            oms.append(OrbitElements.omega)
                            tperis.append(OrbitElements.T*yr2pi)
                            incs.append(OrbitElements.inc)
                            a2Rss.append(OrbitElements.a*au/self.Rstar/R_sun)
                            Pnum.append(TT_index[ti])
                            tis.append(ti)

                            f_tra = (0.5 * np.pi - OrbitElements.omega) % pi2
                    
                            tanE2 = np.tan(0.5 * f_tra) * np.sqrt((1. - np.abs(OrbitElements.e)) / (1. + np.abs(OrbitElements.e)))
                            E = 2. * np.arctan(tanE2)
                            Ma = E - np.abs(OrbitElements.e) * np.sin(E)
                            ttras.append(Ma * OrbitElements.P*yr2pi / pi2 + OrbitElements.T*yr2pi)
                            if iTR >= 0: 
                                if TT_planet[ti] == j: 
                                    ttvs.append(((times*yr2pi)) -(ttras[-1]))
                                    #print(j,times*yr2pi,((times*yr2pi)),(ttras[-1]))                                

                Ps     = np.array(Ps)
                es     = np.array(es)
                oms    = np.array(oms)
                tperis = np.array(tperis)
                incs   = np.array(incs)
                a2Rss  = np.array(a2Rss)
                if iTR >= 0: calc_reboundTR = False
            # print('Pnum: ',Pnum)
            if iTR >= 0:
                for j in range(numPlanets):
                    if Planets[j].orbit == 'Dynamic':
                        inds = (np.where(np.array(Pnum) == iTR))[0][j]
                        #print('inds: ',inds)
                        PlanetMA= Planet(Mstar=Mstar,Rstar=self.Rstar,
                                         K=Planets[j].K,m=Planets[j].m,
                                         P=Ps[inds],e=es[inds],om=oms[inds],
                                         tperi=tperis[inds],
                                         inc=incs[inds],
                                         Rp2Rs=Planets[j].Rp2Rs,
                                         Om=Planets[j].Om,
                                         a2Rs=a2Rss[inds],
                                         orbit=Planets[j].orbit,
                                         Transit=Planets[j].Transit,
                                         TransitOnly=Planets[j].TransitOnly)
                        trad = PlanetMA.get_trad()
                        ttra = PlanetMA.get_ttra()
                        
                        # if TT_Stimes[tis[inds]]-trad <= ttra and ttra <= TT_Etimes[tis[inds]]+trad:
                        mod += PlanetMA.MandelAgolRV(t,TRansits[iTR].u1,TRansits[iTR].u2) - leastsq_model  # leastsq needs to be provided
                        if TRListe[iTR].detrend == 'Linear':
                            leastsq_coef  = np.dot(MAmod-TRListe[iTR].value,np.linalg.pinv(TRListe[iTR].vector))
                            leastsq_model = np.dot(leastsq_coef,TRListe[iTR].vector)
                        # else:
                        #     mod += 1.
        mod = mod + TRansits[iTR].thirdL
        mod = mod/(1.+TRansits[iTR].thirdL)

        
        # flares
        #for it,TR in enumerate(TRListe):
        if TRListe[iTR].FitFlare:
            for fi,fl in enumerate(TRListe[iTR].FlareList):
                Ftime = fl[0]
                if not TRListe[iTR].FixFlareTime: 
                    Ftime = eval(f"self.FlareTime{iTR+100}{fi}")
                Famp = fl[1]
                if not TRListe[iTR].FixFlareAmp: 
                    Famp = np.abs(eval(f"self.FlareAmp{iTR+100}{fi}"))
                Ffwhm = fl[2]
                if not TRListe[iTR].FixFlareFWHM: 
                    Ffwhm = np.abs(eval(f"self.FlareFWHM{iTR+100}{fi}"))
                mod += aflare.aflare1(t,Ftime,Ffwhm,Famp)
                #print(it,fi,fl[0],f[1],eval(f"self.FlareAmp{it}{fi}"))
            
        #plt.plot(TR.time,TR.value)
        #plt.plot(t,mod)
        #plt.show()
        #stop    
        return mod

######################################################################
class MeanModelConstAC(Model):
    
    paranames = []
    paranames.append("offAC0")    
    paranames.append("offAC1")    

    parameter_names = (tuple(paranames))
    print("ModellConst",parameter_names)
    def get_value(self, t):
        const = np.zeros(len(t))
        return const

######################################################################

