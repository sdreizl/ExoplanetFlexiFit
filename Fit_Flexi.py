#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 13:31:54 2020

@author: dreizler
"""

#import astropy
from astropy.io import fits
import corner
#import celerite
#from celerite import terms
#from celerite.modeling import Model
import emcee
import gc
#import gls
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
#import matplotlib.cm as cm
import numpy as np
import pprint,pickle
from scipy.optimize import minimize
from optimparallel import minimize_parallel
import time
#import transit
import copy

import os
import sys
import shutil

from multiprocessing import Pool, cpu_count
import multiprocessing as mp
from schwimmbad import MPIPool

from optimparallel import minimize_parallel

from DataClass import *
from PlanetClass import *
from ModelingClass import ModelingClass
from InputFunction import *

from config import *

global input_dir


def fitFlexi(**kwargs):
    from config import noTT
    output_dir = kwargs.get('output_dir', "Outputs/")
    input_dir  = kwargs.get('input_dir',"InputTests/")
    savename   = kwargs.get('savename', 'test')
    savenamein = kwargs.get('savenamein', savename)
    inputData  = kwargs.get('inputData','InputData.yaml')
    numBurn    = kwargs.get('numBurn', 2000)
    numMCMC    = kwargs.get('numMCMC', 10000)
    numWalkers = kwargs.get('numWalkers', 200)
    numThread  = kwargs.get('numThread', 1)
    overwrite  = kwargs.get('overwrite', True)
    showInput  = kwargs.get('showInput', False)
    showCornerTruth = kwargs.get('showCornerTruth', True) # show the best fit in Corner Plots
    showAll    = kwargs.get('showAll', False)
    
    plotBurnIn = kwargs.get('plotBurnIn', False)        # plots the BurnIn walker traces
    plotMCMC   = kwargs.get('plotMCMC', False)          # plots the MCMC walker traces
    
    start_from_sample = kwargs.get('start_from_sample', False)
    append_sample     = kwargs.get('append_sample', False)
    t0                = kwargs.get('t0', 2450000)
    #numt              = kwargs.get('numt', 10000)  is in config
    #numtTR            = kwargs.get('numtTR', 1000)is inconfig

    if SPOCK: numThread = 1
    
    if noShow:
        plotBurnIn = False
        plotMCMC = False
    
    count = 0
    p0    = []
    strtTimeGlob = time.perf_counter()
    
    
    ###############################################################################     
    # some basic settings
       
    DATAList   = []        # list for all observational data
    PlanetList = []        # list for all planets
    
    #savename  += str(numProc)          # prefix of files to be saved
    #savenamein = savename              # prefix of files to be saved
    
    ReStart    = False                 # re-start from previous result?



    ###############################################################################
    # get t0corr
    print("#######################")
          
    if autoT0:
        t0corr = np.inf
        minTTR = np.inf
        minTRV = np.inf
        
        RVgeneral, RVdata, boundsgpRV, boundsJRV, ReStartDataRV = fileInput( "RV", inputData, input_dir)
        ACdata, boundsgpAC, boundsJAC, ReStartDataAC = fileInput("AC", inputData, input_dir)
        TRdata, boundsgpTR, boundsJTR, boundsTR, ReStartDataTR = fileInput("TR", inputData, input_dir)

        #TODO: rethink restart (read from pickle and set t0corr accordingly)
        if not ReStartDataRV:
            for i, RV in enumerate(RVdata):
                RV_input = RVTimeSeries(**RVgeneral, **RV, **boundsgpRV,
                                         t0=t0, t0corr= t0corr,
                                         plot = False,
                                         ReadTheData = True,
                                         color=INSTCOLORS[i])
                minTRV = min([item for s in [[minTRV],RV_input.time] for item in s])
        else:
            pkl_file = open(output_dir+savenamein+'RV.pkl', 'rb')
            #Stern    = pickle.load(pkl_file)
            #Planets  = pickle.load(pkl_file)
            Data     = pickle.load(pkl_file)
            pprint.pprint(Data)
            pkl_file.close()
            for i in range(len(RVdata)):
                minTRV = min([item for s in [[minTRV],Data[i].time] for item in s])
                print(f"i: {i}")
                print(f"min(Data[i].time: {min(Data[i].time)})")
        print(f"minTRV: {minTRV}")
            
        if not ReStartDataTR:
            for i, TR in enumerate(TRdata):
                TR_input = TRTimeSeries(**TR, boundsJ=boundsJTR[i],
                                            boundsgpTR=boundsgpTR[i], 
                                            t0= t0, ttras= [], plot= False,
                                            #PlanetList= PlanetList[j], 
                                            data = 'TR', 
                                            ReadTheData = True)
                minTTR = min([item for s in [[minTTR],TR_input.time] for item in s])
        else:
            pkl_file = open(output_dir+savenamein+'TR.pkl', 'rb')
            #Stern    = pickle.load(pkl_file)
            #Planets  = pickle.load(pkl_file)
            Data     = pickle.load(pkl_file)
            pprint.pprint(Data)
            pkl_file.close()
            TRstart = len(RVdata)+len(ACdata)
            for i in range(len(TRdata)):
                minTTR = min([item for s in [[minTTR],Data[TRstart+i].time] for item in s])
        print(f"minTTR: {minTTR}")
            
            
        if np.isfinite(minTRV) and not np.isfinite(minTTR):
            minT = minTRV
        elif np.isfinite(minTTR): # != np.inf or not np.isnan(minTTR):
            minT = minTTR
        else:
            raise Exception("no RV or TR input data found")
            
        if minT < t0corr:
            t0corr = minT
            
            
        t0 += t0corr
    else:
        t0corr=0
        
    print(f"# t0: {t0} \n#\tt0corr: {t0corr}")
    print("#######################\n")
      
    try:
        os.stat(output_dir)
    except:
        os.mkdir(output_dir)
    f = open(output_dir+savename+'output.txt','w+')
    f.write(f"# t0: {t0} \n#\tt0corr: {t0corr}\n\n")
    f.close()
          
    try:
        os.stat(output_dir)
    except:
        os.mkdir(output_dir)
    f = open(output_dir+savename+'output.txt','w+')
    f.write(f"# t0: {t0} \n#\tt0corr: {t0corr}\n\n")
    f.close()
          
    # sys.exit()

   
    ###############################################################################
    # define planets and star, parameters need to be adapted
    #
    # read the input file of the parameters of the stellar system
    #
    ###############################################################################
    

    stellarParameter = {}
    planetParameter  = []
    bounds = {}
    
    stellarParameter, planetParameter, bounds, ReStart = planetInput(inputData, input_dir, PrintAll = False) 
    print("stellarParameter",stellarParameter)
    print("planetParameter",planetParameter)
    print(f"ReStart: {ReStart}")
    print('bounds', bounds)
    # sys.exit()
    
    ###############################################################################
    if ReStart:
        pkl_file = open(output_dir+savenamein+'System.pkl', 'rb')
        Stern    = pickle.load(pkl_file)
        Planets  = pickle.load(pkl_file)
        #Data     = pickle.load(pkl_file)
        #pprint.pprint(Stern)
        #pprint.pprint(Planets)
        #print(Stern.Mstar)
        #print(Stern.MstarE)
        #print(Stern.Rstar)
        #print(Stern.RstarE)
        #print(Planets[0].Mstar)
        #print(Planets[0].Rstar)
        #print(Planets[0].P)
        #print(Planets[0].e)
        #print(Planets[0].om)
        #print(Planets[0].tperi)
        #print(Planets[0].Ma)
        #pprint.pprint(Data)
        pkl_file.close()
        # decide, which planet and star parameters to be taken from previous solution
        # planet index 0 from previous solution
        
    ###############################################################################    
    
    Rstar = stellarParameter.get('Rstar',1)
    RstarE = stellarParameter.get('RstarE',1)
    Mstar = stellarParameter.get('Mstar',1)
    MstarE = stellarParameter.get('MstarE',1)
    fixRstar= stellarParameter.get('fixRstar',1)

    #print("Rstar",Rstar)
    
    if stellarParameter.get('restart',False):
        STAR  = Star(Mstar=Stern.Mstar, Rstar=Stern.Rstar,
                     MstarE=Stern.MstarE,RstarE=Stern.RstarE)
    else:
        STAR  = Star(**stellarParameter)
        
    if ReStart:
        print('**********************************************')
        print(f'STAR.Rstar: {STAR.Rstar}')
        print(f'STAR.RstarE: {STAR.RstarE}')
        print(f'STAR.RMstar: {STAR.Mstar}')
        print(f'STAR.RMstarE: {STAR.MstarE}')
        print('**********************************************')
        # sys.exit()
        
    #Rstar = STAR.Rstar
    #RstarE = STAR.RstarE
    #Mstar = STAR.Mstar
    #MstarE = STAR.MstarE
    #stellarParameter['Rstar'] = STAR.Rstar
    #stellarParameter['RstarE'] = STAR.RstarE
    #stellarParameter['Mstar'] = STAR.Mstar
    #stellarParameter['MstarE'] = STAR.MstarE
    #print("M,R input",STAR.Mstar,STAR.Rstar)
    #print(Rstar,Mstar)
    
    
    for i, planet in enumerate(planetParameter):
        try:
            if ReStart and planet.get('restart',False):
                planet.pop('restart')
                for key, value in planet.items():
                    if type(value) == str:
                        exec(f'Planets[i].{key} = "{value}"')
                    if type(value) == float:
                        exec(f'Planets[i].{key} = {value}')
                    Planets[i].set_parameters(True)
                
                PlanetList.append(Planets[i])
            
            else:
                print('Planet',i,' parameter input: ', planet)
                PlanetList.append(Planet(t0 = t0, firstcall = True, 
                                     **stellarParameter, **planet))
        except IndexError:
            raise IndexError("More Planets set to Restart than present in save file")
        except:
            raise
            
        
    try:
        print("######################################################################")
        print(f"STAR.Mstar: {STAR.Mstar}")
        print(f"STAR.MstarE: {STAR.MstarE}")
        print(f"STAR.Rstar: {STAR.Rstar}")
        print(f"STAR.RstarE: {STAR.RstarE}")
        for i, planet in enumerate(PlanetList):
            #print(f"parameter vector: {len(dir(PlanetList[i]))}")
            #print(dir(PlanetList[i]))
            print("  ")
            print(f"orbit: {PlanetList[i].orbit}")
            print(f"P: {PlanetList[i].P}")
            print(f"K: {PlanetList[i].K}")
            print(f"m: {PlanetList[i].m}")
            print(f"e: {PlanetList[i].e}")
            print(f"om: {PlanetList[i].om}")
            print(f"tperi: {PlanetList[i].tperi}")
            print(f"inc: {PlanetList[i].inc}")
            print(f"Om: {PlanetList[i].Om}")
            print(f"ttra: {PlanetList[i].ttra}")
            print(f"Rp2Rs: {PlanetList[i].Rp2Rs}")
            print(f"a2Rs: {PlanetList[i].a2Rs}")
            print(f"Rs (Planet): {PlanetList[i].Rstar}")
            
    except:
        print("######################################################################")
        print("no Planet Input")
    try:
        minPer = np.min([l.P for l in PlanetList if (l.P > 0.) & (l.orbit == 'Dynamic')])*5
    except:
        minPer = minPER
    
    fitRstar = False
    for Plan in PlanetList:
        #print(Plan.fixRstar)
        #if Plan.Transit & (not Plan.TransitOnly): fitRstar = True
        if (not Plan.fixRstar) & Plan.Transit & (not Plan.TransitOnly): fitRstar = True
        else: break
    
    print("")
    print("finished defining host star and planet(s)")
    print("")

    ###############################################################################
    
    # first file is "lead" data set
    RVstart= 0
    tminmax  = []
    All_time = []
    All_value= []
    All_index= []
    All_error= []
    All_index= []
    ttras    = []
    
    
    RVgeneral, RVdata, boundsgpRV, boundsJRV, ReStartData = fileInput( "RV", inputData, input_dir)
    #print(RVgeneral.get('GPpar'))
    #stop
    if ReStartData:
        pkl_file = open(output_dir+savenamein+'RV.pkl', 'rb')
        #Stern    = pickle.load(pkl_file)
        #Planets  = pickle.load(pkl_file)
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
    
    for i,RV in enumerate(RVdata):
        if RVgeneral.get('restartGeneral', False) or RV.get('restart', False):
            DATAList.append(Data[i])
        else:
            DATAList.append(RVTimeSeries(**RVgeneral, **RV, **boundsgpRV,
                                         t0=t0, t0corr= t0corr,
                                         color=INSTCOLORS[i]))
        tminmax.append([np.min(DATAList[i].time),np.max(DATAList[i].time)])
        All_index= np.append(All_index,np.ones(len(DATAList[i].time))*i)
        All_time = np.append(All_time, DATAList[i].time)
        All_value= np.append(All_value,DATAList[i].value)
        All_error= np.append(All_error,DATAList[i].error)
        
    for i,RV in enumerate(RVdata): DATAList[i].set_Alltimes(All_time, All_value, All_error, All_index)

    ###############################################################################
    ACstart= len(RVdata)
    ACdata, boundsgpAC, boundsJAC, ReStartData = fileInput("AC", inputData, input_dir)
    if ReStartData:
        pkl_file = open(output_dir+savenamein+'AC.pkl', 'rb')
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
    
    for i,AC in enumerate(ACdata):
        if AC.get('restart', False):
            DATAList.append(Data[i])
        else:
            DATAList.append(ACTimeSeries(**AC, **boundsgpAC[i],  **boundsJAC[i], 
                                         t0=t0, t0corr= t0corr,
                                         color=INSTCOLORS[i]))
            
    ###############################################################################
    TRstart = ACstart + len(ACdata)
    TRdata, boundsgpTR_prel, boundsJTR_prel, boundsTR, ReStartData = fileInput("TR", inputData, input_dir)

    boundsgpTR = []
    boundsJTR = []
    for i in range(len(boundsgpTR_prel)):
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsgpTR.append(boundsgpTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])
        boundsJTR.append(boundsJTR_prel[i])

    if ReStartData:
        pkl_file = open(output_dir+savenamein+'TR.pkl', 'rb')
        Data     = pickle.load(pkl_file)
        pprint.pprint(Data)
        pkl_file.close()
        
    #print(TRdata)
    bounds.update(boundsTR)
    
    for i,Plan in enumerate(PlanetList):
        if Plan.Transit: 
            trad = Plan.get_trad()
            print(f"Transit Time and Duration: {Plan.ttra}, {trad}")
            Plan.ttra = Plan.ttra % Plan.P
            if i==0:
                ttras.append((Plan.ttra,Plan.P, 1.*trad/Plan.P,i ))
            else:
                ttras.append((Plan.ttra,Plan.P, 1.*trad/Plan.P,i ))

    print(f"transit time parameters {ttras}")

    All_timeTR  = []
    All_valueTR = []
    All_indexTR = []
    All_TT_time   = []
    All_TT_Stime  = []
    All_TT_Etime  = []
    All_TT_indx   = []
    All_TT_planet = []
    All_TT_time_in   = []
    All_TT_planet_in = []
    All_TT_file_in   = []
    TTV = False
    count = 0
    
    # read transit times from file
    if noTT:
        data = np.genfromtxt(output_dir+savename+"_TransitTimes",delimiter=',',names=['Time','Indx','Plan','File'])
        All_TT_time_in  = data['Time']
        All_TT_indx_in  = data['Indx'].astype(int)
        All_TT_planet_in= data['Plan'].astype(int)
        All_TT_file_in = data['File'].astype(int)
        #print(All_TT_time_in)
    
   
    for i, TR in enumerate(TRdata):
        if TR.get('restart', False):
            DATAList.append(Data[i])
        else:
            for j,Plan in enumerate(PlanetList):
                if Plan.Transit:
                    TR_input = TRTimeSeries(**TR, **boundsgpTR[i], **boundsJTR[i],
                                            t0= t0, t0corr= t0corr,
                                            ttras= [ttras[j]], plot= False,
                                            PlanetList= PlanetList[j], 
                                            data = 'TR', FileNo=i, 
                                            All_TT_time = All_TT_time_in, 
                                            All_TT_planet = All_TT_planet_in,
                                            All_TT_file=All_TT_file_in)

                    #print(f"Dynamic: {i, TR_input}")
                    if (Plan.orbit == 'Dynamic' or Plan.orbit == 'TTV'): # and (not noTT):
                        TTV = True

                        for k,nTr in enumerate(TR_input.TransitTimes):
                            TR_store = copy.copy(TR_input)
                            #print(k,TR_input.time[0],TR_input.time[-1],nTr - 2*ttras[j][2]*ttras[j][1],nTr + 2*ttras[j][2]*ttras[j][1])
                            ind = np.where((TR_input.time > nTr - 2*ttras[j][2]*ttras[j][1]) & \
                                           (TR_input.time < nTr + 2*ttras[j][2]*ttras[j][1]) )
                            if ind[0] != [] and i==TR_input.TransitFiles[k] :
                                print("cutting out transits",TR_input.name,j,count,nTr,2*ttras[j][2]*ttras[j][1])
                                #print("ind[0]",ind[0])
                                #errfac = 1.-0.99*np.abs((TR_input.time[ind] - nTr)/(ttras[j][2]*ttras[j][1]))
                                #plt.plot(TR_input.time[ind],TR_input.value[ind])
                                #plt.show()
                                #plt.close()
                                #plt.plot(TR_input.time[ind],TR_input.error[ind]*errfac)
                                #plt.show()
                                #plt.close()
                                #plt.plot(TR_input.time[ind],errfac)
                                #plt.show()
                                #plt.close()
                                if TR_input.vector != np.array([]): TR_store.set_Vecs(TR_input.vector[:,ind[0]])
                                TR_store.set_Times(TR_input.time[ind],TR_input.value[ind],TR_input.error[ind])#*errfac)
                                TR_store.set_TransitTimes(nTr,j)
                                TR_store.name = TR_input.name+"_"+str(k)
                                DATAList.append(TR_store)
                                #print(DATAList[-1].Planet)
                                DATAList[-1].ParFileNr = i
                                #All_index= np.append(All_index,np.ones(len(ind[0])*(TRstart+k+\
                                #                     j*len(TR_input.TransitTimes)+i*len(PlanetList))))                
                                All_TT_time.append(nTr)
                                All_TT_Stime.append(np.min(TR_input.time[ind]))
                                All_TT_Etime.append(np.max(TR_input.time[ind]))
                                All_TT_indx.append(count)
                                All_TT_planet.append(j)
                                count += 1
                                #plt.plot(DATAList[-1].time,DATAList[-1].value-0.01)
                                #plt.plot(TR_store.time,TR_store.value)
                                #plt.show()
                                All_indexTR += [i+TRstart+j*len(TR_input.TransitTimes)+k]*len(ind[0])
                                All_timeTR  += list(TR_store.time)
                                All_valueTR += list(TR_store.value)
                    else:
                        DATAList.append(TR_input)
                        #All_index= np.append(All_index,np.ones(len(DATAList[TRstart+i].time))*(i+TRstart))
                        DATAList[-1].ParFileNr = i
                        #DATAList.append(TR_input)
                        #DATAList[-1].ParFileNr = i
    # print('All_indxTR',All_indexTR)
    # stop
                        
    ###############################################################################
    # provide the information that only transit light curves are fitted
    TransitOnly = False
    if (RVdata == []) & (not TTV): TransitOnly = True
    for Plan in PlanetList:
        Plan.set_TransitOnly(TransitOnly)
    
    print("")
    print("")
    print("finished reading data files")
    print("")
    ###############################################################################
    # define model
    
    #print("All-TT",All_TT_time)
    inds = np.argsort(All_time)
    if inds != []: 
        All_time  = All_time[inds]
        All_index = All_index[inds]
    #if noTT:
    #    data = np.genfromtxt(output_dir+savename+"_TransitTimes",delimiter=',',names=['Time','Indx','Plan'])
    #    All_TT_time  = data['Time']
    #    All_TT_indx  = data['Indx'].astype(int)
    #    All_TT_planet= data['Plan'].astype(int)
    #    #print(All_TT_planet)
    inds = np.argsort(All_TT_time)
    if inds != []:
        All_TT_time  = np.array(All_TT_time)[inds]
        #All_TT_Stime = np.array(All_TT_Stime)[inds]
        #All_TT_Etime = np.array(All_TT_Etime)[inds]
        All_TT_indx  = np.array(All_TT_indx)[inds]
        All_TT_planet= np.array(All_TT_planet)[inds]
        if not noTT: np.savetxt(output_dir+savename+"_TransitTimes_new",list(zip(All_TT_time,All_TT_indx,All_TT_planet)),fmt='%15.6f,%4i,%4i')
        for j,Plan in enumerate(PlanetList):
            ind = np.where(All_TT_planet == j)
            TranNr = np.round((All_TT_time[ind]-(All_TT_time[ind[0][0]])) / Plan.P)
            polcoef = np.polyfit(TranNr,All_TT_time[ind],1)
            #polcoef[0] = Plan.P
            polynom  = np.poly1d(polcoef)
            print("ephemeris: {0}, {1}".format(polynom[0],polynom[1]))
            pp = PdfPages(output_dir+savename+"_palnet"+str(j)+"_TTV.pdf")
            fig = plt.figure()
            plt.plot(TranNr,All_TT_time[ind]-polynom(TranNr),'o')
            plt.xlabel("Transit Number")
            plt.ylabel("O-C [d]")
            pp.savefig(plt.gcf()) # This generates pdf page
            plt.show()                
            plt.close()
            pp.close()

       
    #stop
    try:
        mod  = ModelingClass(stellarParameter,PlanetList,DATAList,All_TT_time,All_TT_Stime,All_TT_Etime,All_TT_indx,All_TT_planet,\
                        All_time,All_index,\
                        All_timeTR,All_indexTR,TTV,\
                        bounds,boundsgpRV=boundsgpRV,boundsgpAC=boundsgpAC,\
                        boundsgpTR=boundsgpTR,boundsJRV=boundsJRV,\
                        boundsJAC=boundsJAC,boundsJTR=boundsJTR,\
                        lin=0.)
    except ValueError:
        print('Print all variables and boundaries here!')
        raise
    except:
        raise
    
    
    # set time arrays for model output and plot input model
    try: 
        timesRV = np.linspace(np.min(tminmax)- 0., np.max(tminmax)+10, numt)
    except:
        timesRV = np.linspace(0,1)
    try:
        timesTR = np.linspace(Plan.ttra-Plan.trad*Plan.P,Plan.ttra+Plan.trad*Plan.P, numtTR)
    except:
        timesTR = np.linspace(0,1)

    if showInput: mod.plot_input(savename,output_dir,RVstart,ACstart,TRstart,t0corr=t0corr)
    
    print("#######################")
    #print(mod.gpAll.get_parameter_vector())
    #print(mod.gpAll.get_parameter_names())
    #stop
    #sys.exit()
    
    #print("M,R plot_input",Mstar,Rstar,STAR.Rstar,STAR.RstarE)
    STARResult  = Star(Mstar=STAR.Mstar,Rstar=STAR.Rstar,MstarE=STAR.MstarE,RstarE=STAR.RstarE)
    #print("Rstar",STAR.Rstar)
    PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),STAR.Rstar)
    #print("M,R plot input",STARResult.Mstar,STARResult.Rstar,STARResult.RstarE,PlanetListResult[0].Rstar)

    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' Start',minPer,showAll)
    #stop
    ###############################################################################
    initial_params = mod.gpAll.get_parameter_vector()
    bounds         = mod.gpAll.get_parameter_bounds()
    
    # initialize all parameters
    x = DATAList[0].time
    y = DATAList[0].value
    e = DATAList[0].error
    for i in range(1,len(DATAList)):
        x = np.concatenate((x,DATAList[i].time),axis=None)
        y = np.concatenate((y,DATAList[i].value),axis=None)
        e = np.concatenate((e,DATAList[i].error),axis=None)
        
    inds = np.argsort(x)
    x    = x[inds]
    y    = y[inds]
    e    = e[inds]
    
    mod.gpAll.compute(x, e)
    
    print("##############################")
    print(mod.gpAll.get_parameter_dict())
    
    
    if start_from_sample:
        reader           = emcee.backends.HDFBackend(output_dir+savenamein+'.h5')
        p_sample         = reader.get_last_sample()
        samples          = reader.get_chain(flat=True)
        #print(samples[mod.Soff+0,:])
        #samples[:,mod.Soff+1] = np.abs(samples[:,mod.Soff+1])
        #samples[:,mod.Soff+2] = np.abs(samples[:,mod.Soff+2])
        #samples[:,mod.Soff+3] = np.abs(samples[:,mod.Soff+3])
        #samples[:,mod.Soff+4] = np.abs(samples[:,mod.Soff+4])
        log_prob_samples = reader.get_log_prob(flat=True)
        best             = samples[np.argmax(log_prob_samples)]
        ndim             = len(best)
        DATAList         = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
        print('######################################################################')
        print("Best fit from sample: {0}".format(best))
        print("ln L RV only:",mod.log_probability_RV(best))
        print("ln L TR only:",mod.log_probability_TR(best))
        print('######################################################################')
        ###############################################################################
        # save and show results

        if fitRstar: Rstar = best[mod.Soff]
        PlanetListResult = mod.SaveThePlanet(best,Rstar)
        #BIC_1 = mod.plot_result(timesRV,timesTR,savename,output_dir,RVstart,ACstart,TRstart,mod,'Start',minPer)

    else:
        print("")
        print("... start minimzation...")
        """
        startTime = time.clock()
        soln = minimize(mod.neg_log_like, initial_params, 
                        method="L-BFGS-B", bounds=bounds, args=(y,mod.gpAll),\
                        #method="Nelder-Mead", args=(y,mod.gpAll),\
                        options={'ftol': 2.220446049250313e-09, 'gtol': 1e-08, \
                                 'eps': 1e-08, 'maxfun': 25000, 'maxiter': 200})
        minimizeTime = time.clock() - startTime
        print(soln)
        """
        startTime = time.perf_counter()
        soln = minimize_parallel(mod.neg_log_like, initial_params, 
                        bounds=bounds, args=(y,mod.gpAll),\
                        options={'ftol': 2.220446049250313e-09, 'gtol': 1e-08, \
                                 'eps': 1e-08, 'maxfun': 25000, 'maxiter': 200})
        minimizeParallelTime = time.perf_counter() - startTime

        print(f"Time minimizeParallel: {minimizeParallelTime}")
        print(mod.gpAll.get_parameter_dict())
        
        ftol  = 2.220446049250313e-09
        tmp_i = np.zeros(len(soln.x))
        uncertainty_i = np.zeros(len(soln.x))
        for i in range(len(soln.x)):
            tmp_i[i] = 1.0
            uncertainty_i[i] = np.sqrt(max(1, abs(soln.fun))*ftol*soln.hess_inv(tmp_i)[i])
            tmp_i[i] = 0.0
            print('{0:12.4e} ± {1:.1e}'.format(soln.x[i], uncertainty_i[i]))
        """
        ftol  = 2.220446049250313e-09
        tmp_i = np.zeros(len(soln.x))
        uncertainty_i = np.zeros(len(soln.x))
        uncertainty2_i = np.zeros(len(soln.x))
        print("comparing minimize and optimparallel.minimize_parallel:")
        print(f"Time minimize: {minimizeTime}")
        print(f"Time minimizeParallel: {minimizeParallelTime}")
        print(f"faster by factor: {minimizeTime/minimizeParallelTime}")
        for i in range(len(soln.x)):
            tmp_i[i] = 1.0
            uncertainty_i[i] = np.sqrt(max(1, abs(soln.fun))*ftol*soln.hess_inv(tmp_i)[i])
            uncertainty2_i[i] = np.sqrt(max(1, abs(soln2.fun))*ftol*soln2.hess_inv(tmp_i)[i])
            tmp_i[i] = 0.0
            print('{0:12.4e} ± {1:.1e} ---- {0:12.4e} ± {1:.1e}'.format(soln.x[i], uncertainty_i[i], soln2.x[i], uncertainty2_i[i]))
        """
        
        
        DATAList = mod.SaveTheDatafiles(soln.x,RVstart,ACstart,TRstart)    
        print('######################################################################')
        print("Result:",soln)
        print("Final log-likelihood optimization: {0}".format(soln.fun))
        print("ln L RV only:",mod.log_probability_RV(soln.x))
        print("ln L TR only:",mod.log_probability_TR(soln.x))
        print('######################################################################')
        f = open(output_dir+savename+'output.txt','a')
        f.write("Final log-likelihood optimization: {0}\n".format(soln.fun))
        f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(soln.x)))
        f.write("ln L TR only: {0}\n".format(mod.log_probability_TR(soln.x)))
        f.close()
        ###############################################################################
        # save and show results
        #print("fitRstar",fitRstar,Rstar,STAR.Rstar,soln.x[mod.Soff])
        if fitRstar:
            Rstar = soln.x[mod.Soff]
            #print(fitRstar,Rstar)
        else:
            Rstar = STAR.Rstar
            #print(Rstar)
        if fitRstar: Rstar = soln.x[mod.Soff]
        PlanetListResult = mod.SaveThePlanet(soln.x,Rstar)
        BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' LM',minPer,showAll)
        print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(soln.x))
        #stop
        ###############################################################################
        
        initial = np.array(soln.x)
        #initial = np.array(initial_params)
        ndim    = len(initial)
    
        print("Initializing parameter sets")
        while (len(p0) != numWalkers):
            # tmp = float("%0.2f" % (count/initlim*100))
            # print(f"\r{tmp} % of max.Iterations ({count}/{initlim})  \r", end='')
            params = (initial + ufact * uncertainty_i * np.random.randn(1, ndim)).reshape(-1)
            #print(params.reshape(-1))
            mod.gpAll.set_parameter_vector(params)
            lnL = mod.gpAll.log_prior()
            if np.isfinite(lnL):
                p0.append(params)
            count += 1
            if count > initlim:
                raise Exception("WARNING: only {0} of {1} initial parameter sets found: reduce parameter ufact OR increase initlim in config.py and rerun".format(len(p0),numWalkers))
                sys.exit()
                
            tmp = float("%0.2f" % (count/initlim*100))
            tmp2 = float("%0.2f" % (len(p0)/numWalkers*100))
            print(f"\r{tmp2} % ({len(p0)}/{numWalkers}) \t {tmp}% of maxIter \r", end='')

        print("\n{0} initializations needed to create start distribution\n".format(count))
        p0 =  np.array(p0).reshape(numWalkers,ndim)
        
    print("")
    print('######################################################################')
    print("... start MCMC...")
    
    strt = time.time()
    if start_from_sample:
        shutil.copy(output_dir+savenamein+'.h5',output_dir+savename+'burnin.h5')
    
    backend = emcee.backends.HDFBackend(output_dir+savename+'burnin.h5')
    # creating attribute of the backend to save the parameter names
    parameter_names = [name.split(':')[1] for name in mod.gpAll.get_parameter_names()]
    
    backend.open('a').attrs.create(name="parameter_names", data=parameter_names)
    
    if not start_from_sample: 
        print("resetting sample")
        backend.reset(numWalkers, ndim)
    print(f"Running burn-in with {ndim} dimensions")
    
    startPars = mod.gpAll.get_parameter_dict().items()
    
    garbage = 0
    samplers = []
    if numThread == 1:
        for samp in range(10):
            sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                      backend=backend) ## or fraction of CPUs??(cpu_count()//4)),
            start = time.time()
            if start_from_sample: 
                p0,lp,state = sampler.run_mcmc(None, numBurn/10, progress=True)
            else:
                p0,lp,state = sampler.run_mcmc(p0, numBurn/10, progress=True)
            end = time.time()
            multi_time = end - start

            print("Multiprocessing took {0:.1f} seconds".format(multi_time))
            print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))

            if plotBurnIn:
                s = copy.copy(sampler)
                samplers.append(s)
                
            garbage += gc.collect()
            
            garbage += gc.collect()
            
    else:
        for samp in range(10):
            with Pool(processes=numThread) as pool:
                sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                          pool=pool,backend=backend) ## or fraction of CPUs??(cpu_count()//4)),
                start = time.time()
                if start_from_sample: 
                    p0,lp,state = sampler.run_mcmc(None, numBurn/10, progress=True)
                else:
                    p0,lp,state = sampler.run_mcmc(p0, numBurn/10, progress=True)
                end = time.time()
                multi_time = end - start
                                
                print(f'\n',"Multiprocessing took {0:.1f} seconds".format(multi_time))    
                print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))

                if plotBurnIn:
                    s = copy.copy(sampler)
                    samplers.append(s)
                    
            garbage += gc.collect()
            pool.close()
    
    
    if plotBurnIn:
        for par, (key, startVal) in enumerate(startPars):
            parameter = key.split(':')[1]
            print(f'Plotting {nPlotWalker} walkers of: {parameter}  ({par+1} of {len(startPars)})')
            
            for s in samplers:
                numWalker = len(s.chain)
                walkerChoice = np.arange(0, numWalker, int(np.ceil(numWalker/nPlotWalker)), dtype = int)
                for walker in s.chain[walkerChoice]:
                    steps = len(walker)
                    fig = plt.plot(walker[:,par], '-', color='k', alpha = walkerOpacity)
            
            plt.axhline(y=startVal, color='r', linestyle='-')
            plt.title('BurnIn walker trace')
            plt.xlabel('step number')
            plt.ylabel(f"{parameter[:-1]}$_{parameter[-1]}$")
            plt.show()
            plt.clf()
            plt.close()
        
        print('Plotting all walkers completed.')
        
    
    print("Multiprocessing took {0:.1f} seconds".format(multi_time))
    print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
    print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))    
    
    samples     = sampler.flatchain
    best        = samples[np.argmax(sampler.flatlnprobability)]
    print("run time:",time.time()-strt)
    print("First Burn in log-likelihood: {0}".format(-np.max(sampler.flatlnprobability)))
    print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(sampler.flatlnprobability)]))
    print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(sampler.flatlnprobability)]))
    print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
    print("parameters{0}".format(best))
    f = open(output_dir+savename+'output.txt','a')
    f.write("run time: {0}\n".format(time.time()-strt))
    f.write("Final log-likelihood: {0}\n".format(-np.max(sampler.flatlnprobability)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(samples[np.argmax(sampler.flatlnprobability)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(sampler.flatlnprobability)])))
    f.write("Mean acceptance fraction:{0:.3f}\n".format(np.mean(sampler.acceptance_fraction)))
    f.close()
    ################################################################################
    # save results 
    DATAList = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
    if fitRstar:
        Rstar = best[mod.Soff]
        #print(fitRstar,Rstar)
    else:
        Rstar = STAR.Rstar
        
    print('*******************************************')
    STARResult  = Star(Mstar=Mstar,Rstar=Rstar,MstarE=MstarE,RstarE=RstarE)
    PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),Rstar)
    print('*******************************************')
    # print(f'Rstar for STARResult: {Rstar}')
    # print(f'RstarE for STARResult: {RstarE}')
    # print(f'Mstar for STARResult: {Mstar}')
    # print(f'MstarE for STARResult: {MstarE}')
    # print(f'Rstar for PLANETResult:')
    # for a in PlanetListResult: print(a.Rstar)
    # print(f'Mstar for PLANETResult:')
    # for a in PlanetListResult: print(a.Mstar)
    # print('*******************************************')

    #print("M,R burnin",STARResult.Mstar,STARResult.Rstar)
    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    pkl_file = open(output_dir+savename+'System.pkl', 'wb')
    pickle.dump(STARResult,pkl_file)
    pickle.dump(PlanetListResult,pkl_file,-1)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'RV.pkl', 'wb')
    pickle.dump(DATAList[RVstart:RVstart+len(RVdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'AC.pkl', 'wb')
    pickle.dump(DATAList[ACstart:ACstart+len(ACdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'TR.pkl', 'wb')
    pickle.dump(DATAList[TRstart:TRstart+len(TRdata)],pkl_file)
    pkl_file.close()
        
                  
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,' BurnIn',minPer,showAll)
    print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(best))
    
    if start_from_sample:
        shutil.copy(output_dir+savename+'burnin.h5',output_dir+savename+'.h5')

    backend = emcee.backends.HDFBackend(output_dir+savename+'.h5')
    
    # creating attribute of the backend to save the parameter names
    parameter_names = [name.split(':')[1] for name in mod.gpAll.get_parameter_names()]
    backend.open('a').attrs.create(name="parameter_names", data=parameter_names)
    
    if not start_from_sample: backend.reset(numWalkers, ndim)  # reset sampler?
    
    
    startPars = mod.gpAll.get_parameter_dict().items()
    
    print("Running MCMC...")
    samplers = []
    garbage = 0
    if numThread == 1:
        for samp in range(2):
            sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                                            backend=backend)
            start = time.time()
            p0,lp,state = sampler.run_mcmc(p0, numMCMC/2, progress=True)
            end = time.time()
            multi_time = end - start
            print("Multiprocessing took {0:.1f} seconds".format(multi_time))
            print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
            try:
                reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
                tau = reader.get_autocorr_time()
            except:
                tau = samp*numMCMC/2
            #print(tau)
            if np.mean(tau)*50<i*samp*numMCMC/2: break
            garbage += gc.collect()
    else:
        for samp in range(2):
            with Pool(processes=numThread) as pool:
                sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                          pool=pool,
                          backend=backend)
                start = time.time()
                p0,lp,state = sampler.run_mcmc(p0, numMCMC/2, progress=True)
                end = time.time()
                multi_time = end - start
                print("Multiprocessing took {0:.1f} seconds".format(multi_time))
                print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
                try:
                    reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
                    tau = reader.get_autocorr_time()
                except:
                    tau = samp*numMCMC/2
                #print(tau)
                if np.mean(tau)*50<i*samp*numMCMC/2: break  
                                
                if plotMCMC:
                    
                    s = copy.copy(sampler)
                    samplers.append(s)
                    
            garbage += gc.collect()
            pool.close()
           
    if plotMCMC:
        for par, (key, startVal) in enumerate(startPars):
            parameter = key.split(':')[1]
                        
            print(f'Plotting {nPlotWalker} walkers of: {parameter}  ({par+1} of {len(startPars)})')
            
            for s in samplers:
                numWalker = len(s.chain)
                walkerChoice = np.arange(0, numWalker, int(np.ceil(numWalker/nPlotWalker)), dtype = int)
                for walker in s.chain[walkerChoice]:
                    steps = len(walker)
                    fig = plt.plot(walker[:,par], '-', color='k', alpha=walkerOpacity)
            
            plt.axhline(y=startVal, color='r', linestyle='-')
            plt.title('MCMC walker trace')
            plt.xlabel('step number')
            plt.ylabel(f"{parameter[:-1]}$_{parameter[-1]}$")
            plt.show()
            plt.clf()
            plt.close()
        print('Plotting all walkers completed.')
                 
    
    try:
        reader = emcee.backends.HDFBackend(output_dir+savename+'.h5')
        tau    = reader.get_autocorr_time()
        burnin = int(2 * np.max(tau))
        thin   = int(0.5 * np.min(tau))
        
        # TODO: discard and thin dynamic
        samples = reader.get_chain(flat=True)   #, discard=burnin, thin=thin)
        #print(flat_samples.shape)
        flatlnprob = reader.get_log_prob(flat=True) #, discard=burnin, thin=thin)
    except:
        samples = reader.get_chain(flat=True, discard=int(0.2*numMCMC), thin=int(0.1*numMCMC))
        flatlnprob = reader.get_log_prob(flat=True, discard=int(0.2*numMCMC), thin=int(0.1*numMCMC))
        #samples    = sampler.flatchain
        #flatlnprob = sampler.flatlnprobability
    
    print("Final log-likelihood: {0}".format(-np.max(flatlnprob)))
    print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(flatlnprob)]))
    print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(flatlnprob)]))
    print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
    f = open(output_dir+savename+'output.txt','a')
    f.write("run time: {0}\n".format(time.time()-strt))
    f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(samples[np.argmax(flatlnprob)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(flatlnprob)])))
    f.write("Mean acceptance fraction:{0:.3f}\n".format(np.mean(sampler.acceptance_fraction)))

    
    strt = time.time()
    #print("Running final LM fit...")
    #soln = minimize(mod.neg_log_like, samples[np.argmax(sampler.flatlnprobability)], 
    #                method="L-BFGS-B", bounds=bounds, args=(y,mod.gpAll))
    best = samples[np.argmax(flatlnprob)]
    print('######################################################################')
    print("Result:",best)
    print("Final log-likelihood optimization: {0}".format(mod.log_probability(best)))
    print("ln L RV only:",mod.log_probability_RV(best))
    print("ln L TR only:",mod.log_probability_TR(best))
    print('######################################################################')
    print("Best fit parameter_vector:\n{0}\n".format(best))
    print('######################################################################')
    f.write('######################################################################\n')
    f.write("Result: {0}\n".format(best))
    f.write("Final log-likelihood optimization: {0}\n".format(mod.log_probability(best)))
    f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(best)))
    f.write("ln L TR only: {0}\n".format(mod.log_probability_TR(best)))
    f.write('######################################################################\n')
    f.write("Best fit parameter_vector:\n{0}\n".format(best))
    f.write('######################################################################\n')
    
    #######################################################################
    # save and show results
    
    if fitRstar: 
        Rstar = best[mod.Soff]
        #print(fitRstar,Rstar)
    else:
        Rstar = STAR.Rstar
        #print(Rstar)
    STARResult  = Star(Mstar=Mstar,Rstar=Rstar,MstarE=MstarE,RstarE=RstarE)
    PlanetListResult = mod.SaveThePlanet(best,Rstar)
    BIC_1 = mod.plot_result(timesRV,timesTR,t0corr,savename,output_dir,RVstart,ACstart,TRstart,mod,'',minPer,showAll)
    print('*******************************************')
    # print(f'Rstar for STARResult: {Rstar}')
    # print(f'RstarE for STARResult: {RstarE}')
    # print(f'Mstar for STARResult: {Mstar}')
    # print(f'MstarE for STARResult: {MstarE}')
    # print(f'Rstar for PLANETResult:')
    # for a in PlanetListResult: print(a.Rstar)
    # print(f'Mstar for PLANETResult:')
    # for a in PlanetListResult: print(a.Mstar)
    # print('*******************************************')

    print("BIC RV only:",BIC_1-2.*mod.log_probability_RV(best))
    f.write("BIC RV only: {0}\n".format(BIC_1-2.*mod.log_probability_RV(best))) 
    
    ######################################################################
    # calculate derived quantities
    
    truth,xx = mod.derived_quantities(samples,best,STAR,mod,RVstart,ACstart,TRstart,ndim)
        
    ################################################################################
    # save results 
    
    DATAList = mod.SaveTheDatafiles(best,RVstart,ACstart,TRstart)    
    
    #print("M,R final",STARResult.Mstar,STARResult.Rstar)
    #for i in range(len(PlanetListResult)): print("M,R plot input",PlanetListResult[i].Mstar,PlanetListResult[i].Rstar)
    pkl_file = open(output_dir+savename+'System.pkl', 'wb')
    pickle.dump(STARResult,pkl_file)
    pickle.dump(PlanetListResult,pkl_file,-1)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'RV.pkl', 'wb')
    pickle.dump(DATAList[RVstart:RVstart+len(RVdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'AC.pkl', 'wb')
    pickle.dump(DATAList[ACstart:ACstart+len(ACdata)],pkl_file)
    pkl_file.close()
    pkl_file = open(output_dir+savename+'TR.pkl', 'wb')
    pickle.dump(DATAList[TRstart:TRstart+len(TRdata)],pkl_file)
    pkl_file.close()
                  
    ################################################################################
    # make tick font size smaller for corner plots    
    plt.rc('xtick',labelsize=13)
    plt.rc('ytick',labelsize=13)
    
    print("best fit parameter_vector:\n{0}\n".format(truth))
    print('######################################################################')
    
    npTR = 0
    nADD = 0
    add_name = []
    for Plan in PlanetList:
        nADD += np.sum(Plan.varadd)
        print(f"add_names: {add_names}, len: {len(add_names)}")
        print(f"Plan.varadd: {Plan.varadd}, len: {len(Plan.varadd)}")
        tmp = np.where(np.array(Plan.varadd) == 1)
        print(f"Plan.varadd_tmp: {tmp}, len: {len(tmp)}")
        add_name += list(add_names[np.where(np.array(Plan.varadd) == 1)[0]])
        #print(np.where(Plan.varadd == 1)[0])
        #print(add_name)

    ii = np.array(np.where(flatlnprob > np.percentile(flatlnprob, percLnL)) ).reshape(-1)
    zz = np.copy(xx[ii])
    for jj in range(ndim):
        #ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        ifinite = np.where(np.isfinite(zz[:,jj]))
        zz = zz[ifinite]
        #v=np.percentile(xx[ii[ifinite]][:,jj],[16,50,84])
        v=np.percentile(zz[:,jj],[16,50,84])
        tmp_name = mod.gpAll.get_parameter_names()[jj].split(':')[1]                 #Todo
        print(tmp_name, v[1],'^',v[2]-v[1],'_',v[1]-v[0])
        f.write("{0} {1} {2} {3} {4} {5} {6}\n".format(tmp_name, v[1],'& +',v[2]-v[1],'& -',v[1]-v[0], truth[jj]))
        
    for jj in range(ndim,ndim+nADD):
        #print(jj,jj-ndim)
        #ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        #v=np.percentile(xx[ii[ifinite]][:,jj],[16,50,84])
        ifinite = np.where(np.isfinite(zz[:,jj]))
        zz = zz[ifinite]
        v=np.percentile(zz[:,jj],[16,50,84])
        print(add_name[jj-ndim], v[1],'^',v[2]-v[1],'_',v[1]-v[0])
        f.write("{0} {1} {2} {3} {4} {5} {6}\n".format(add_name[jj-ndim], v[1],'& +',v[2]-v[1],'& -',v[1]-v[0], truth[jj]))

    f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
    f.write("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    f.write("ln L RV only:{0}\n".format(mod.log_probability_RV(samples[np.argmax(flatlnprob)])))
    f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(flatlnprob)])))
    f.close()



    offA = ndim
    POff = 0
    for k,Plan in enumerate(PlanetList):
        ind = np.arange(mod.Poff+POff,mod.Poff+POff+Plan.Ppar)
        nAdd = np.sum(Plan.varadd)
        ind = np.append(ind,np.arange(offA,offA+nAdd))

        itest = np.where(np.array(Plan.varadd==1))
        #print(itest[0]+len(Plan.varfix))

        indx= list(Plan.varind)+list(itest[0]+len(Plan.varfix))

        newnames = CN_P
        newnames = np.append(newnames,CN_P_A)
        newnames = newnames[indx]
        
        #print(ind,indx,newnames)
            
        if 4 in indx:
            ii = indx.index(4)
            nn,bb, = np.histogram(zz[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                zz[:,ind[ii]]  = (zz[:,ind[ii]]  + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
        if 8 in indx:
            ii = indx.index(8)
            nn,bb, = np.histogram(zz[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                zz[:,ind[ii]]  = (zz[:,ind[ii]]  + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-180)) % 360 - mid*np.sign(mid-180)
        if 5 in indx and not Plan.fixP:
            ii   = indx.index(5)
            iPer = indx.index(2)
            nn,bb, = np.histogram(zz[:,ind[ii]],bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                Pbest = np.median(zz[:,ind[iPer]])
                mid = 0.5*(bb[np.argmin(nn)+1]-bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                zz[:,ind[ii]]  = (zz[:,ind[ii]]  + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
        if 13 in indx and not Plan.fixP:
            # for time of conjugation instead of mean longtiude
            ii = indx.index(13)
            nn, bb, = np.histogram(zz[:, ind[ii]], bins=20)
            jj = np.where(nn > np.median(nn))
            if (np.min(jj) == 0) & (np.max(jj) == 19):
                Pbest = np.median(zz[:,ind[iPer]])
                mid = 0.5 * (bb[np.argmin(nn) + 1] - bb[np.argmin(nn)]) + bb[np.argmin(nn)]
                zz[:,ind[ii]]  = (zz[:,ind[ii]]  + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                truth[ind[ii]] = (truth[ind[ii]] + mid*np.sign(mid-Pbest*0.5)) % Pbest - mid*np.sign(mid-Pbest*0.5)
                #zz[:, ind[ii]] = (zz[:, ind[ii]] + mid * np.sign(mid - 180)) % 360 - mid * np.sign(mid - 180)
                #truth[ind[ii]] = (truth[ind[ii]] + mid * np.sign(mid - 180)) % 360 - mid * np.sign(mid - 180)

        plotrange= [1.]*len(newnames)
    
        #for iind in ind:
        #    print(truth[iind],np.min(xx[ii][:,iind]),np.max(xx[ii][:,iind]))
    
        # reject bad samples
        # ii = np.array(np.where(flatlnprob > np.percentile(flatlnprob, percLnL) )).reshape(-1) 
        #ifinite = np.where(np.isfinite(xx[ii][:,jj]))
        #print(np.shape(ifinite),np.shape(ii[ifinite]))
        pp = PdfPages(output_dir+savename+"corner_planet"+str(k)+".pdf")
        if showCornerTruth:
            corner.corner(zz[:,ind], truths=truth[ind],
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84], show_titles=True,
                          title_fmt='.3f',range=(np.asarray(plotrange)))
        else:
            corner.corner(zz[:,ind], 
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84], show_titles=True,
                          title_fmt='.3f',range=(np.asarray(plotrange)))
        
        pp.savefig(plt.gcf()) # This generates pdf page
        if noShow:
            plt.ioff()
        else:
            plt.show()
        pp.close()
        plt.close()
        plt.clf()
        
        npTR = 0
        if Plan.Transit & (k==0) & (mod.Spar != 0): 
            newnames = []
            ind      = np.arange(0)
            ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
            if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
            for DATA in DATAList[TRstart:]:
                #print(DATA)
                if npTR == DATA.ParFileNr:
                    if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
                    if not DATA.fix3: newnames = np.append(newnames,CN_3L)
                    npTR += 1
            plotrange= [1.]*len(newnames)
        
            # reject bad samples
            # ii = np.array(np.where(flatlnprob > np.percentile(flatlnprob, percLnL) )).reshape(-1) 
            #ifinite = np.where(np.isfinite(xx[ii[ifinite]][:,jj]))
            pp = PdfPages(output_dir+savename+"corner_STAR.pdf")
            if showCornerTruth:
                corner.corner(zz[:,ind], truths=truth[ind],
                              title_kwargs={"fontsize":12},
                              labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                              quantiles=[0.16,0.5,0.84],show_titles=True,
                              title_fmt='.3f',range=(np.asarray(plotrange)))
            else:
                corner.corner(zz[:,ind],
                              title_kwargs={"fontsize":12},
                              labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                              quantiles=[0.16,0.5,0.84],show_titles=True,
                              title_fmt='.3f',range=(np.asarray(plotrange)))
            
            pp.savefig(plt.gcf()) # This generates pdf page
            if noShow:
                plt.ioff()
            else:
                plt.show()
            pp.close()
            plt.close()
            plt.clf()
            
        plt.close('all')
        garbage_stats = gc.get_stats()
        garbage = gc.collect()
        
        offA += nAdd
        POff += Plan.Ppar
    
    offA = ndim
    POff = 0
    for k,Plan in enumerate(PlanetList):
        ind = np.arange(mod.Poff + POff, mod.Poff + POff + Plan.Ppar)
        indx = list(Plan.varind)
                
        newnames = []
        newnames = np.append(newnames,CN_P)
        newnames = newnames[indx]
        
        if Plan.Transit & (k==0): 
            npTR = 0
            ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
            if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
            for DATA in DATAList[TRstart:]:
                if npTR == DATA.ParFileNr:
                    if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
                    if not DATA.fix3: newnames = np.append(newnames,CN_3L)
                    npTR += 1
            plotrange= [1.]*len(newnames)
        
        plotrange= [1.]*len(newnames)
    
        # reject bad samples
        pp = PdfPages(output_dir+savename+"corner_raw_planet"+str(k)+".pdf")
        if showCornerTruth:
            corner.corner(zz[:,ind], truths=truth[ind],
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84],
                          show_titles=True,title_fmt='.3f')
        else:
            corner.corner(zz[:,ind],
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84],
                          show_titles=True,title_fmt='.3f')
        
        pp.savefig(plt.gcf()) # This generates pdf page
        if noShow:
            plt.ioff()
        else:
            plt.show()
        pp.close()
        plt.close('all')
        plt.clf()
            
        offA += nAdd
        POff += Plan.Ppar
        
        gc.collect()
    
    newnames = []
    ind      = np.arange(0)
    for i,DATA in enumerate(DATAList):
        if isinstance(DATA,RVTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffRV+i,mod.JoffRV+i+1))
        if isinstance(DATA,ACTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffAC[i-ACstart],mod.JoffAC[i-ACstart]+1))
        if isinstance(DATA,TRTimeSeries) & DATA.Jitter:
            newnames = np.append(newnames,CN_Jm)
            ind = np.append(ind,np.arange(mod.JoffTR[i-TRstart],mod.JoffTR[i-TRstart]+1))
    ind = np.append(ind,np.arange(mod.OoffRV,mod.OoffRV+mod.nfRV))
    newnames = np.append(newnames,CN_Off*mod.nfRV)
    
    ind = np.append(ind,np.arange(mod.OoffAC,mod.OoffAC+mod.nfAC))
    newnames = np.append(newnames,CN_Off*mod.nfAC)
    
    ind = np.append(ind,np.arange(mod.OoffTR,mod.OoffTR+npTR))
    newnames = np.append(newnames,CN_Off*npTR)
    
    if mod.ndeg >= 1:
        ind = np.append(ind,[mod.linpos])
        newnames = np.append(newnames,CN_pol)
    if mod.ndeg == 2:
        ind = np.append(ind,[mod.quadpos])
        newnames = np.append(newnames,CN_pol)
    
    plotrange= [1.]*len(newnames)
    
    # reject bad samples
    pp = PdfPages(output_dir+savename+"corner_Jitter.pdf")
    if showCornerTruth:
        corner.corner(zz[:,ind], truths=truth[ind],
                      title_kwargs={"fontsize":12},
                      labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                      quantiles=[0.16,0.5,0.84],show_titles=True,
                      title_fmt='.3f',range=(np.asarray(plotrange)))
    else:
        corner.corner(zz[:,ind],
                      title_kwargs={"fontsize":12},
                      labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                      quantiles=[0.16,0.5,0.84],show_titles=True,
                      title_fmt='.3f',range=(np.asarray(plotrange)))
    
    pp.savefig(plt.gcf()) # This generates pdf page
    if noShow:
        plt.ioff()
    else:
        plt.show()
    pp.close()
    plt.close('all')
    plt.clf()
    
    
    newnames = []
    ind      = np.arange(0)
    for i,DATA in enumerate(DATAList):
        if i == RVstart:
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'SHO'): 
                newnames = np.append(newnames,CN_RV_GPm_SHO)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'ROT3'): 
                newnames = np.append(newnames,CN_RV_GPm_ROT3)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'ROT'): 
                newnames = np.append(newnames,CN_RV_GPm_ROT)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'CUST'): 
                newnames = np.append(newnames,CN_RV_GPm_CUST)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
            if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'REAL'):
                newnames = np.append(newnames,CN_RV_GPm_REAL)
                ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
        if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'SHO'):
            newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfAC)
            ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
        if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'RV Driver'):
            newnames = np.append(newnames,CN_AC_GPm_RVD*mod.nfAC)
            ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
        if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'SHO'):
            newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfTR)
            ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
        if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'REAL'):
            newnames = np.append(newnames,CN_RV_GPm_REAL*mod.nfTR)
            ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
      
    plotrange= [1.]*len(newnames)
    
    if newnames != []:
        # reject bad samples
        
        pp = PdfPages(output_dir+savename+"corner_GP.pdf")
        if showCornerTruth:
            corner.corner(zz[:,ind], truths=truth[ind],
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84],
                          show_titles=True,title_fmt='.3f')
        else:
            corner.corner(zz[:,ind],
                          title_kwargs={"fontsize":12},
                          labels=np.asarray(newnames),label_kwargs={"fontsize":12},
                          quantiles=[0.16,0.5,0.84],
                          show_titles=True,title_fmt='.3f')
        
        pp.savefig(plt.gcf()) # This generates pdf page
        if noShow:
            plt.ioff()
        else:
            plt.show()
        pp.close()
        plt.close()
        plt.clf()
        
    
    garbage = gc.collect()
    
    print('######################################################################')
    
    print("Final log-likelihood: {0}".format(-np.max(flatlnprob)))
    print("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    print("Best fit parameter_dict:\n{0}\n".format(mod.gpAll.get_parameter_dict().items()))
    f = open(output_dir+savename+'output.txt','a')
    if autoT0 and t0corr!=0:
        tperis = dict(filter(lambda item: "tperi" in item[0], mod.gpAll.get_parameter_dict().items()))
        for k,v in tperis.items():
            k = k.split(':')[1]
            print(f"\t{k} (BJD): {v+t0}")
            f.write(f"\t{k} (BJD): {v+t0}\n")
    f.close()
    print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(flatlnprob)]))
    print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(flatlnprob)]))
    #f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
    #f.write("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
    #f.write("ln L RV only:{0}\n".format(mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    #f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)])))
    #f.close()
    
    print('######################################################################')
    
    print("DataList:")
    print(DATAList)
    print('duration of run: ',time.perf_counter() - strtTimeGlob)
    
    return True
