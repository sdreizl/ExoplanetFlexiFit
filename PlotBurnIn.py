#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 12:38:35 2020

@author: pal
"""

import pickle
import emcee
import matplotlib.pyplot as plt
import numpy as np

savename = 'TOI-175'
#savename = 'TOI-157'

inputData = savename + '.yaml'
output_dir = 'Outputs/TOI-175'
#output_dir = 'Outputs/test'

sampler = []
chains = []

for samp in range(10):
    pkl_file = open(output_dir+'/BurnIn/'+savename+f'_sampler{samp}.pkl', 'rb')
    sampler.append(pickle.load(pkl_file))
    pkl_file.close()
    


for par in range(len(sampler[0].chain[0][0])):
    for samp in sampler:
        numWalker = len(samp.chain)
        #print(f'Walker: {numWalker}')
        #for walker in samp.chain:
        i = np.arange(0, numWalker, int(np.ceil(numWalker/500)), dtype = int)
        for walker in samp.chain[i]:
            steps = len(walker)
            plt.plot(walker[:,par], '-', color='k', alpha=0.005)
        
    plt.title('walker trace')
    plt.xlabel('step number')
    plt.ylabel('parameter')
    plt.show()

