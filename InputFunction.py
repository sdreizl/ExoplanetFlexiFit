#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 12:09:45 2020

@author: Paul Schwarz
"""

import yaml
import numpy as np


###############################################################################
# 
#   evalDictOfDics is a recursive Function
#   for the evaluation of dictionaries containing parameter as strings
#
#   The parameter print enables Printing the dictionary as it is evaluated
#
###############################################################################
def evalDictOfDicts(dictOfDicts, depth = 0, Print = False):
    tabs = depth * '\t'
    for key, val in dictOfDicts.items():
        if not isinstance(val,dict):
            try:
                val = eval(val)
                dictOfDicts[key] = val
            except:
                pass
            if Print: print(tabs + f"{key}: {val} ({type(val)})")
            
            if isinstance(val,list):
                for i,x in enumerate(val):
                    try:
                        val[i] = float(x)
                    except:
                        pass
                # val = list(((try: float(x) except: x) for x in val))
                dictOfDicts[key] = val
                if Print: 
                    for x in val:
                        print(tabs+ '\t' + f"{x} ({type(x)})")
        else:
            if Print: print(tabs+ f"{key} ({type(val)})")
            evalDictOfDicts(val, depth+1, Print)
            
            
            
       
# print the values(types) of all entries in the dictOfDict

def printDictOfDicts(dictOfDicts, depth = 0):
    depth += 1
    tabs = depth*'\t'
    for key, val in dictOfDicts.items():
        if not isinstance(val,dict):
            print(tabs + f"{key}: {val} ({type(val)})")
        else:
            printDictOfDicts(val, depth)

###############################################################################
# planetInput(fileName, directory)
#
# Reads the input file for the details of the central Star and the planets
# modifies the global arrays planetData and bounds
#
# Returns:
#   stellarParameter (dict)
#   planetParameter  (list of dict)
#       starting with planet 0
#       all planet parameters
#   bounds           (dict)
#       dictionary containing ALL boundaries as e.g.:
#           K0: (,)
#           P0: (,)
#           e0: (,)
#           ...
#           K1: (,)
#           P1: (,)
#           e1: (,)
#           ...
#       NOTE: all bounds of all parameters from all objects are stored here!!
#   
###############################################################################


def planetInput(fileName, input_dir='', Print = False, PrintAll = False):
    
    stellarParams = {}
    planetParams = []
    bounds = {}
    ReStart = False
    
    if PrintAll: Print = True
    
    with open(input_dir + fileName) as file:
        data =  yaml.full_load(file)
    
    evalDictOfDicts(data, Print=PrintAll)
           
    for i in range(data["numPlanets"]):
        tmpData = data[f"planet{i}"]  
        tmpBounds = tmpData.pop("bounds",{})      
        
        restart = tmpData["restart"]
        if restart:
            ReStart = True
            tmpData.pop('parameter')
            #tmpBounds
            planetData = tmpData
        else:
            planetData = tmpData['parameter']
            
        e = planetData.get(f"e{i}")
        P = planetData.get(f"P{i}")
        tperi = planetData.get(f"tperi{i}")
        om = planetData.get(f"om{i}")
        a2Rs = planetData.get(f"a2Rs{i}")
            
        
            
        planetParams.append(planetData)
        
        
        
        if not tmpBounds.get('e'):
            tmpBounds.update(e = (0, 0.9))
            
            
        if not tmpBounds.get('inc') and not ReStart:
            if om and a2Rs:
                iamp = np.pi/2 - np.arccos((1+e*np.sin(om))/(1-e**2) * 1/a2Rs)
                print(f"max inclination calculated with a2Rs={a2Rs}, om={om} and e={e}")
                print(f"inclination bounds: {(np.pi/2 - iamp, np.pi/2 + iamp)}")
            elif not om and a2Rs:
                om = np.pi/2
                iamp = np.pi/2 - np.arccos(1/(1-e) * 1/a2Rs)
                print(f"max inclination calculated with a2Rs={a2Rs}, om={om} and e={e}")
                print(f"inclination bounds: {(np.pi/2 - iamp, np.pi/2 + iamp)}")
            else:
                iamp = np.pi/2
                print("No a2Rs given, inclination set to max range")
                print(f"inclination bounds: {(np.pi/2 - iamp, np.pi/2 + iamp)}")
            
            tmpBounds.update(inc = (np.pi/2 - iamp, np.pi/2 + iamp))
                
                
        if not ReStart:
            if not tmpBounds.get('Rp2Rs'):
                tmpBounds.update(Rp2Rs = (0, 1))
            if not tmpBounds.get('a2Rs'):
                tmpBounds.update(a2Rs = (1, 200000))
            if not tmpBounds.get('tperi'):
                tmpBounds.update(tperi = (0, P))
        
        tmpBounds = {k+str(i): v for k, v in tmpBounds.items()}
        
    
        if not restart:
            for key, bound in tmpBounds.items():
                key = key[:-1]
                #print(key)
                val = planetData.get(key)        
                #print(val)
                if val!=None:
                    if "tperi" not in key and (val < bound[0] or val > bound[1]):
                        raise ValueError(f"Planet{i}: {key}={val} not in bounds {bound}")
                else:
                    print(f"Planet{i}: {key} bounds given, but no start Value found")
                    # raise ValueError(f"Planet{i}: {key} bounds given, but no start Value found")
                
    
        bounds.update(tmpBounds)
    # sys.exit()
                
                
    if data['centralStar']['restart']:
        ReStart = True
        data['centralStar'].pop('parameter')
        data['centralStar'].pop('bounds')
        
        stellarParams = data['centralStar']
    else:
        stellarParams = data["centralStar"]['parameter']

    if not data["centralStar"].get("bounds",None) == None:
        tmpBounds = {k: v for k, v in data["centralStar"]["bounds"].items()}
        bounds.update(tmpBounds)
    
    '''
    #old
    if data['centralStar']['restart']:
        ReStart = True
        data['centralStar'].pop('parameter')
        data['centralStar'].pop('bounds')
        
        stellarParams = data['centralStar']
    else:
        stellarParams = data["centralStar"]['parameter']
        if data['centralStar'].get('bounds',{}) is not None:
            bounds.update(data['centralStar'].get('bounds',{}))
    '''    
    
    return stellarParams, planetParams, bounds, ReStart
    
###############################################################################
# planetInput(fileType, fileName, directory)
#
# Reads the input file for getting the data files, their type 
# and the necessary parameters needed for the TimeSeries function
#
# Returns for fileType RV:
#   RVgeneral (dict)
#       general parameter for RV fit
#   RVdata (list of dict)
#       dictionaries contain information for RVTimeSeries for each input file
#   boundsgpRV (dict)
#       boundaries for the gaussian process (GP)
#   boundsJRV (dict)
#       boundaries for the jitter
#
#
# Returns for fileType TR:
#   TRgeneral (dict)
#       general parameter for TR fit
#   TRdata (list of dict)
#       dictionaries contain information for TRTimeSeries for each input file
#   boundsgpTR (list of dict)
#        gpAC boundaries for each input file
#   boundsTR (dict)
#       contains updated bounds for the global bounds dict necessary 
#       for the TR fitting
#
#   
# Returns for fileType AC:
#   ACgeneral (dict)
#       general parameter for AC fit
#   ACdata (list of dict)
#       dictionaries contain information for ACTimeSeries for each input file
#   boundsgpAC (list of dict)
#        gpAC boundaries for each input file
#
#   
###############################################################################
def fileInput(setType, fileName, input_dir='', Print = False, PrintAll = False):    
    setType = setType.upper()
    general = {}
    boundsGP = []
    boundsJ = []
    bounds = {}
    
    returnLst = []
    numSets = 0
    
    ReStartData = False
    
    
    with open(input_dir + fileName) as file:
        try:
            data =  yaml.full_load(file)[setType]
        except:
            if Print: 
                print(f"ERROR: Unknown set Type for file input ({setType})")
            return 


    evalDictOfDicts(data, Print=PrintAll)
                        
                        
    general = data.get('general',{}) 
    numSets = data.get(f'numSets{setType}', 0)
    if numSets == None:
        numSets = 0    
    
    if 'TR' in setType:
        bounds.update(data.pop('bounds', {}))

    if 'boundsGP' in data:
        boundsGP = data.pop('boundsGP', [])
        
    if 'boundsJ' in data:
        boundsJ  = data.pop('boundsJ', [{}])
             
        
    for i in range(numSets):
        j=i+1
        tmp = data.get(f'{setType}Set{j}')
            
        boundsGPtmp = tmp.pop('boundsGP',None)
        if not type(boundsGP) == dict:
            if boundsGPtmp == None:
                boundsGP.append({})
            else:
                boundsGP.append(boundsGPtmp)
                
            
        boundsJtmp = tmp.pop('boundsJ',None)
        if not type(boundsJ) == dict:
            if boundsJtmp == None:
                boundsJ.append({})
            else:
                boundsJ.append(boundsJtmp)
            
        returnLst.append(tmp)
        
        if not ReStartData:
            tmpBounds = {}
            if not tmpBounds.get('thirdL'):
                tmpBounds.update(thirdL = (0.0, 1.0))
                tmpBounds = {k+str(i): v for k, v in tmpBounds.items()}
                if 'TR' in setType:
                    bounds.update(tmpBounds)
    
    if not numSets == 0:
        
        for i,tmpList in enumerate(returnLst):
            
            for key, val in tmpList.items():
                if 'fileName' in key:
                    returnLst[i][key] = input_dir + val
                
            if tmpList.pop('restart'):
                ReStartData = True
                returnLst[i] = {'restart': True}
        
        if general.get('restartGeneral', False):    
            ReStartData = True
        
        if Print:    
            try:
                print("boundsGP: ")
                print(*boundsGP, sep = "\n")
                print("boundsJ: ")
                print(*boundsJ, sep = "\n")
            except:
                print(boundsGP)
    
            if "TR" in setType:
                print("bounds: ")
                print(*bounds.items(), sep = "\n")
            print("\n\n")
        

        
        if "RV" in setType:
            return general, returnLst, boundsGP, boundsJ, ReStartData
        elif "AC" in setType:
            return returnLst, boundsGP, boundsJ, ReStartData
        elif "TR" in setType:
            return returnLst, boundsGP, boundsJ, bounds, ReStartData
    else:
        if "RV" in setType:
            return {}, [], {}, {}, ReStartData #general, returnLst, boundsGP, boundsJRV
        elif "AC" in setType:
            return [], [], [], ReStartData #general, returnLst, boundsGP
        elif "TR" in setType:
            return [], [], [], {}, ReStartData #returnLst, bounds, boundsGP

# test for the functionality of the program
if __name__ == '__main__':
    # testIn = 'InputTests/test.yaml'
    testIn = 'InputFiles/TOI-175/TOI-175_SERVAL.yaml'
    Print = True
    
    # print(planetInput(testIn, PrintAll = False))
    # print(fileInput('RV', testIn))
    print(fileInput('AC', testIn, PrintAll = True))
    # print(fileInput('TR', testIn, Print = False))
    
