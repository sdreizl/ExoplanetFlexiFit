import numpy as np
import matplotlib

###############################################################################
#   Constants

M_sun   = np.float64(1.9884754159665356e+30)#1.988416e30)       # mercury6 value SI
R_sun   = np.float64(6.957e8)       # SI value
G       = np.float64(6.67408e-11)#6.67428e-11)     # SI
m_earth = np.float64(5.972365261370795e+24)#5.9722e24)       # SI
r_earth = np.float64(6.3781e6)     # SI GRS80 mean value
d2s     = np.float64(86400.)
au      = np.float64(149597870700.)#1.49598e11)   # mercury6 value SI

yr2pi   = np.sqrt(149597870700.**3/1.3271244004193938e20)/d2s

pi2   = np.float64(2.*np.pi)
G13   = np.float64(G**(1./3.))
tau13 = np.float64((2.*np.pi)**(-1./3.))
tau23 = np.float64((2.*np.pi)**(-2./3.))

timeconv = au/yr2pi/d2s


###############################################################################
#   Plot design

matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['font.size'] = '20'
matplotlib.rcParams['xtick.labelsize'] = 18
matplotlib.rcParams['ytick.labelsize'] = 18
matplotlib.rcParams['axes.linewidth'] = 1.2
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['lines.markeredgewidth'] = 1

INSTCOLORS = ['cornflowerblue', 'darkorange', 'purple','crimson',\
                  'orangered','darkgreen','mediumslateblue']
    
###############################################################################
#   walker trace Plot parameters
nPlotWalker = 200
walkerOpacity = 0.01

#####################################################################################################
# corner plot labels
CN_RV_GP_SHO  = ['ln S$_0$','ln Q','ln $omega_0$']
CN_RV_GPm_SHO = ['S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]']

CN_RV_GP_SHOREAL  = ['ln S$_0$','ln Q','ln $omega_0$','ln a','ln c']
CN_RV_GPm_SHOREAL = ['S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]','a [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d}$  [d]']

CN_RV_GP_REAL = ['ln a','ln c']
CN_RV_GPm_REAL= ['a [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d}$  [d]']

CN_RV_GP_ROTREAL  = ['ln S$_0$','ln Q','ln $omega_0$','ln S$_0$','ln Q',\
                 'ln a','ln c']
CN_RV_GPm_ROTREAL = ['S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]',\
                 'S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','a [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$']

CN_RV_GP_ROT  = ['ln S$_0$','ln Q','ln $omega_0$','ln S$_0$','ln Q']
CN_RV_GPm_ROT = ['S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]',\
                 'S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$']
    
CN_RV_GP_ROT3  = ['ln S$_0$','ln Q','ln $omega_0$','ln S$_0$','ln Q',\
                 'ln S$_0$','ln Q']
CN_RV_GPm_ROT3 = ['S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]',\
                 'S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$','S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$']

CN_AC_GP_RVD  = ['ln S$_0$']
CN_AC_GPm_RVD = ['S$_0$ [m$^2$ s$^{-2}$]']

CN_AC_GP_ROTD  = ['ln S$_0$', 'ln S$_0$']
CN_AC_GPm_ROTD = ['S$_0$ [m$^2$ s$^{-2}$]', 'S$_0$ [m$^2$ s$^{-2}$]']

CN_TR_GP_SHO  = ['ln S$_0$','ln Q','ln $omega_0$']
CN_TR_GPm_SHO = ['S$_0$','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]']

CN_TR_GP_REAL = ['ln a','ln c']
CN_TR_GPm_REAL= ['a','$\\tau_\\mathrm{d}$  [d]']

CN_TR_GP_ROT  = ['ln S$_0$','ln Q','ln $omega_0$','ln S$_0$','ln Q',\
                 'ln S$_0$','ln Q']
CN_TR_GPm_ROT = ['S$_0$','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]',\
                 'S$_0$','$\\tau_\\mathrm{d} [d]$','S$_0$ [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$']

CN_TR_GP_ROTREAL  = ['ln S$_0$','ln Q','ln $omega_0$','ln S$_0$','ln Q',\
                 'ln a','ln c']
CN_TR_GPm_ROTREAL = ['S$_0$','$\\tau_\\mathrm{d} [d]$','P$_0$ [d]',\
                 'S$_0$','$\\tau_\\mathrm{d} [d]$','a [m$^2$ s$^{-2}$]','$\\tau_\\mathrm{d} [d]$']

CN_P          = ['K [m s$^{-1}$]','m [M$_\oplus$]','P [d]','e','$\omega$ [$^\circ$]',\
                 'T$_\mathrm{peri}$ [d]','R$_{p}$/R$_{*}$','i  [$^\circ$]',\
                 '$\Omega$ [$^\circ$]','a/R$_{*}$',]
CN_P_A        = ['K [m s$^{-1}$]','m [M$_\oplus$]','a [au]','t$_{conj}$  [d]',\
                 'a/R$_{*}$','r [R$_\oplus$]','$\\rho$ [g/cm$^3$]','t$_{dur}$  [d]','$\lambda$ [$^\circ$]']


CN_S          = ['R$_{*}$ [R$_\odot$]']
CN_LS         = ['u$_{1}$','u$_{2}$']
CN_3L         = ['D']
CN_J          = ['ln Jitter']
CN_Jm         = ['Jitter [m s$^{-1}$]']
CN_JmAC         = ['Jitter']
CN_JmTR         = ['Jitter']
CN_Off        = ['Offset [m s$^{-1}$]']
CN_pol        = ['Trend  [m s$^{-1}$ d$^{-1}$]']

add_names = np.array(['K','m','a','t_conj','a/R_s','R_p','rho_p','t_dur','lambda'])

percLnL       = 0.01 # cut off the lowes percLnL from the ln L distribution

###############################################################################
# Global variables

t0 = 0.
input_dir =''
mod = ''
bounds = {}
boundsgpRV = {}
boundsgpTR = {}
boundsgpAC = {}
STAR = {}
PlanetList = {}
DataList = {}

minPER = 150.

noShow = False          # prevent showing plots
showAll= False          # show small chuncks of data in large plot (memeory intensive)

autoT0 = True           # automatic correction of t0 to first transit (or RV) measured value
checkStart= False

reboundPrec = 0.001     # fraction of period of the shortest dynamically modelled planet 
SPOCK       = False     # perform SPOCK stabitiy test prior to model calculation

ufact = 1               # factor of uncertainty for initialization of parameter set
initlim = 1e8           # number of tries to initilize the distributions

minimizeMaxiter = 800   # maximum iteration steps for minimize
minimizeEps = 1e-08     # 
minimizeMaxfun  = 25000 


gr = False             # includ GR effect ?
integrator = 'ias15'   #ias15 or whfast

numtTR = 1000
numt   = 10000

config_numPlanMax = 4           # max number of planets in the system
config_nfTRmax = 30            # max number of TR datasets. Has to be adapted for dynamical fit


noTT    = False    # read in transit times from file in case of True, else try to get it from ephemeris
usediff = False    # use transit time deviation for fitting

