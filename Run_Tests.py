#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 18:12:38 2020

@author: dreizler, schwarz
"""

from Fit_Flexi import *
from multiprocessing import Pool

numThread = 3

def fitFunc1():
    # test for RV+TR with 3 planets, 2 transiting
    return fitFlexi(numBurn=50, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV-TR_noJitter_noGP_3Kepler_noRestart.yaml',
             savename   = 'TEST_RV-TR_noJitter_noGP_3Kepler_noRestart', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = False,
             t0=2457000
             #t0=2458438
             )
def fitFunc2():
    # testing the start from sample
    fitFlexi(numBurn=50, numMCMC=50, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV-TR_noJitter_noGP_3Kepler_noRestart.yaml',
             savename   = 'TEST_RV-TR_noJitter_noGP_3Kepler_noRestart', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = True,
             #t0=2458438
             t0=2457000
             )
def fitFunc2a():
    # testing the restart
    fitFlexi(numBurn=50, numMCMC=50, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV-TR_noJitter_noGP_3Kepler_Restart.yaml',
             savenamein = 'TEST_RV-TR_noJitter_noGP_3Kepler_noRestart', 
             savename   = 'TEST_RV-TR_noJitter_noGP_3Kepler_Restart', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = False,
             #showInput=True,
             t0=2457000)
def fitFunc2b():
    # testing the restart
    fitFlexi(numBurn=50, numMCMC=50, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV-TR_noJitter_noGP_3Kepler_Restart.yaml',
             savenamein = 'TEST_RV-TR_noJitter_noGP_3Kepler_Restart', 
             savename   = 'TEST_RV-TR_noJitter_noGP_3Kepler_Restart', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = False,
             # t0=2458438
             t0=2457000
             )
def fitFunc3():
    # test for RV with 3 planets
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_3Kepler_noRestart.yaml',
             savename   = 'TEST_RV_noJitter_noGP_3Kepler_noRestart', 
             output_dir = 'Outputs/TEST_RV_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = False,
             #t0=2458438,
             t0=2458000,
             showInput=False)
    
def fitFunc4():
    # test for TR with 2 planets transiting, no TTVs
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=100, numThread=numThread, 
             inputData  = 'Input_TR_noJitter_noGP_3Kepler_noRestart.yaml',
             savename   = 'TEST_TR_noJitter_noGP_3Kepler_noRestart', 
             output_dir = 'Outputs/TEST_TR_noJitter_noGP_3Kepler_noRestart/',
             start_from_sample = False,
             # t0=2458438)
             t0=2457000)
    
def fitFunc5():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting a base model without planets
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV-TR_noJitter_noGP_noPlanet_noRestart.yaml',
             savename   = 'TEST_RV-TR_noJitter_noGP_noPlanet_noRestart', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_noPlanet_noRestart/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc6():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting 1 circular planet to RVs
    fitFlexi(numBurn=500, numMCMC=5000, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_1Circular.yaml',
             savename   = 'TEST_RV-TR_noJitter_1Circular', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc7():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting 2 circular planets to RVs
    fitFlexi(numBurn=500, numMCMC=5000, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_2Circular.yaml',
             savenamein = 'TEST_RV-TR_noJitter_1Circular', 
             savename   = 'TEST_RV-TR_noJitter_2Circular', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc8():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting 3 circular planets to RVs
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_3Circular.yaml',
             savenamein = 'TEST_RV-TR_noJitter_2Circular', 
             savename   = 'TEST_RV-TR_noJitter_3Circular', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc9():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting 2 Kepler planets to RVs
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=400, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_3Kepler.yaml',
             savenamein = 'TEST_RV-TR_noJitter_3Circular', 
             savename   = 'TEST_RV-TR_noJitter_3Kepler', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc10():
    # test for RV+TR data set having 3 planets, 2 transiting, fitting 2 Kepler 1 dynamic planets to RVs, should be the same as Nr.9
    fitFlexi(numBurn=50, numMCMC=200, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_noJitter_noGP_2Kepler1Dynamic.yaml',
             savenamein = 'TEST_RV-TR_noJitter_3Kepler', 
             savename   = 'TEST_RV-TR_noJitter_2Kepler1Dynamic', 
             output_dir = 'Outputs/TEST_RV-TR_noJitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc11():
    # test for RV data set having 3 planets, 2 transiting, fitting 2 Kepler 1 dynamic planets to RVs, now with JITTER
    fitFlexi(numBurn=50, numMCMC=100, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_Jitter_noGP_2Kepler1Dynamic.yaml',
             #savenamein = 'TEST_RV-TR_Jitter_3Kepler', 
             savename   = 'TEST_RV-TR_Jitter_2Kepler1Dynamic', 
             output_dir = 'Outputs/TEST_RV-TR_Jitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc11a():
    # test for RV data set having 3 planets, 2 transiting, fitting 1 Kepler 2 dynamic planets to RVs, now with JITTER
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_Jitter_noGP_1Kepler2Dynamic.yaml',
             #savenamein = 'TEST_RV-TR_Jitter_3Kepler', 
             savename   = 'TEST_RV-TR_Jitter_1Kepler2Dynamic', 
             output_dir = 'Outputs/TEST_RV-TR_Jitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc12():
    # test for RV data set having 3 planets, 2 transiting, fitting 3 Kepler to RVs, now with JITTER
    fitFlexi(numBurn=50, numMCMC=100, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_Jitter_noGP_3Kepler.yaml',
             #savenamein = 'TEST_RV-TR_Jitter_3Kepler', 
             savename   = 'TEST_RV-TR_Jitter_3Kepler', 
             output_dir = 'Outputs/TEST_RV-TR_Jitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc13():
    # test for RV data set having 3 planets, 2 transiting, fitting 1 Dynamic, now with JITTER
    fitFlexi(numBurn=500, numMCMC=10000, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_Jitter_noGP_1Dynamic.yaml',
             #savenamein = 'TEST_RV-TR_Jitter_3Kepler', 
             savename   = 'TEST_RV-TR_Jitter_1Dynamic', 
             output_dir = 'Outputs/TEST_RV-TR_Jitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc14():
    # test for RV data set having 3 planets, 2 transiting, fitting 1 Dynamic, now with JITTER, should be the same as 13
    fitFlexi(numBurn=500, numMCMC=10000, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_RV_Jitter_noGP_1Kepler.yaml',
             #savenamein = 'TEST_RV-TR_Jitter_3Kepler', 
             savename   = 'TEST_RV-TR_Jitter_1Kepler', 
             output_dir = 'Outputs/TEST_RV-TR_Jitter_noGP_PlanetSequence/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc15():
    # test for TR data set having 3 planets, 2 transiting, fitting 2 Dynamic, compare to Nr.4
    fitFlexi(numBurn=500, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_TR_noJitter_noGP_2Dynamic_noRestart.yaml',
             savenamein = 'TEST_TR_noJitter_noGP_2Dynamic_noRestart', 
             savename   = 'TEST_TR_noJitter_noGP_2Dynamic_noRestart', 
             output_dir = 'Outputs/TEST_TR_noJitter_noGP_2Dynamic_noRestart/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc16():
    # test for TR data set having 3 planets, 2 transiting, fitting 1 Dynamic, compare to Nr.4
    fitFlexi(numBurn=500, numMCMC=500, numWalkers=200, numThread=numThread, 
             inputData  = 'Input_TR_noJitter_noGP_1Dynamic_noRestart.yaml',
             savename   = 'TEST_TR_noJitter_noGP_1Dynamic_noRestart', 
             output_dir = 'Outputs/TEST_TR_noJitter_noGP_1Dynamic_noRestart/',
             start_from_sample = False,
             #t0=2458438)
             t0=2457000)
    
def fitFunc17():
    # test for TTVs data set using a Kepler-9 simulation
    '''
    import planetplanet as pp
    import numpy as np
    import matplotlib.pyplot as plt
    
    #Filterdaten einlesen
    myData = np.loadtxt("InputFiles/Kepler_Kepler.K.dat", delimiter = " ")
    myData = np.transpose(myData)
    
    #Filterobjekt erstellen. Wellenlängen in um umrechnen.
    Ffilter = pp.detect.jwst.Filter(wl=myData[0]/10000, throughput=myData[1], dwl = np.ones(len(myData[0]))*0.001,eff_wl = 5982.31/10000., eff_dwl = 3745.13/10000.)
    
    #System definieren
    star = pp.Star('star', m = 1.022, r = 0.958, teff = 5774, limbdark = [0.3,0.2])
    b = pp.Planet('b', m = 44.71, r = 8.252, per = 19.23891, inc = 88.936, ecc = 0.06378, t0 = 2., phasecurve = False)
    c = pp.Planet('c', m = 30.79, r = 8.077, per = 38.9853, inc = 89.180, ecc = 0.067990, t0 = 32.26, phasecurve = False)
    d = pp.Planet('d', m = 0., r = 1.64, per = 1.592851, inc = 90., ecc = 0., t0 = 1, phasecurve = False)
    
    #system = pp.System(star, b, c, d, distance = 640)
    system = pp.System(star, b, c, distance = 640)
    
    #Zeitintervall festlegen und Simulation ausführen
    time = np.arange(0, 365, 0.001)
    system.compute(time, lambda1 = 0.347, lambda2 = 0.971, R = 100)
    
    #Plotten und txt speichern (save = 1 für txt speichern, 0 für nicht speichern)
    fig, ax = system.observe(save = 1, filter = Ffilter, stack = 1, instrument = 'jwst')
    
    #system.plot_lightcurve(wavelength = 0.89)
    plt.show()
    from shutil import copyfile
    copyfile(src, dst)

    '''
    
    fitFlexi(numBurn=500, numMCMC=1000, numWalkers=400, numThread=numThread, 
             inputData  = 'Input_PhotDyn_Kepler9.yaml',
             savename   = 'PhotDyn_Kepler9', 
             output_dir = 'Outputs/PhotDyn_Kepler9/',
             start_from_sample = False,
             t0=0)

def fitFunc18():
    # test for planet-planet interaction in RV data, using a subset of RV data available
     
    fitFlexi(numBurn=500, numMCMC=1000, numWalkers=400, numThread=numThread, 
             inputData  = 'Input_GJ876_test.yaml',
             savename   = 'GJ876_Test', 
             output_dir = 'Outputs/GJ876_Test/',
             start_from_sample = False,t0=2450000)

def fitFunc18a():
    # as 18 but restart 
       
    fitFlexi(numBurn=50, numMCMC=50, numWalkers=400, numThread=numThread, 
             inputData  = 'Input_GJ876_test_restart.yaml',
             savenamein = 'GJ876_Test',
             savename   = 'GJ876_TestRestart', 
             output_dir = 'Outputs/GJ876_Test/',
             start_from_sample = True,
             append_sample=False,
             t0=2450000)
    
def fitFunc19():
    # 2 planets TR with GP
    fitFlexi(numBurn=50, numMCMC=500, numWalkers=400, numThread=numThread, 
             inputData  = 'GPTest.yaml',
             savename   = 'GPTest',
             output_dir = 'Outputs/GPTest/',
             start_from_sample=False,
             # plotMCMC = True,
             # showInput = True,
             #t0=0
             t0=2457000
             )

if __name__ == '__main__':    
    # print('test1: ')
    # s2time = time.perf_counter()
    # fitFunc1()
    # print('test1 done in ', time.perf_counter()-s2time, 'seconds (perf_counter)')
    # print('test2: ')
    # stime = time.process_time()
    # s2time = time.perf_counter()
    # fitFunc2()
    # print('test2 done in ', time.process_time()-stime, 'seconds (process_time)')
    # print('test2 done in ', time.perf_counter()-s2time, 'seconds (perf_counter)')
    # print('test2a: ')
    # stime = time.process_time()
    # s2time = time.perf_counter()
    # fitFunc2a()
    # print('test2a done in ', time.process_time()-stime, 'seconds (process_time)')
    # print('test2a done in ', time.perf_counter()-s2time, 'seconds (perf_counter)')
    # print('test2b: ')
    # stime = time.process_time()
    # s2time = time.perf_counter()
    # fitFunc2b()
    # print('test2b done in ', time.process_time()-stime, 'seconds (process_time)')
    # print('test2b done in ', time.perf_counter()-s2time, 'seconds (perf_counter)')
    #fitFunc3()
    #fitFunc4()
    # fitFunc4b()
    #fitFunc5()
    # fitFunc6()
    # fitFunc7()
    # fitFunc8()
    # fitFunc9()
    
    # fitFunc10()
    # fitFunc11()
    fitFunc11a()
    #fitFunc12()
    #fitFunc13()
    #fitFunc14()
    # fitFunc15()
    # fitFunc16()
    
    #fitFunc17()
    #fitFunc18()
    #fitFunc18a()
    
    #fitFunc19()
