#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Sun Jul 12 19:45:40 2020

@author: dreizler
"""
import planetplanet as pp
import rebound
import numpy as np
import matplotlib.pyplot as plt
import transit

M_sun   = np.float64(1.9884754159665356e+30)#1.988416e30)       # mercury6 value SI
R_sun   = np.float64(6.957e8)       # SI value
G       = np.float64(6.67408e-11)#6.67428e-11)     # SI
m_earth = np.float64(5.972365261370795e+24)#5.9722e24)       # SI
r_earth = np.float64(6.3781e6)     # SI GRS80 mean value
d2s     = np.float64(86400.)
au      = np.float64(149597870700.)#1.49598e11)   # mercury6 value SI

yr2pi   = np.sqrt(149597870700.**3/1.3271244004193938e20)/d2s

pi2   = np.float64(2.*np.pi)
G13   = np.float64(G**(1./3.))
tau13 = np.float64((2.*np.pi)**(-1./3.))
tau23 = np.float64((2.*np.pi)**(-2./3.))

timeconv = au/yr2pi/d2s

sim = rebound.Simulation()
#sim.units = ('yr', 'AU', 'Msun')
sim.add(m=1.) # central star
#sim.move_to_hel()
sim.add(m=40.*m_earth/M_sun, a=0.01)#x= 1, y=0,  vx = 0, vy = 1.0000)
sim.add(m=40.*m_earth/M_sun, a=0.02)#,f=np.pi)#x= 1, y=0,  vx = 0, vy = 1.0000)
sim.status()

print(sim.particles[1].calculate_orbit(primary=sim.particles[0]))
print(sim.particles[2].calculate_orbit(primary=sim.particles[0]))



def MandelAgolRV(t,P,tperi,a2Rs,Rp2Rs,e,om,inc,u1=0.,u2=0.):
               
    Ma  = pi2/P*(t-tperi)

    Ea   = Ma
    diff = 1.
    count = 0
    while (diff > 1.e-6) and (count < 100):
        Enew = Ea - (Ea - e*np.sin(Ea) -Ma)/(1. - e*np.cos(Ea))
        #print(f'{Ea}')
        diff   = np.max(np.abs(Enew-Ea)/np.pi)
        #print(f'\r diff: {diff}', end='')
        Ea     = Enew
        count += 1
       
    Ta  = 2.*np.arctan(np.sqrt((1.+e)/(1.-e))*np.tan(0.5*Ea))
        
    z = a2Rs*(1.-e*e)/(1.+e*np.cos(Ta)) \
         *np.sqrt(1.-np.sin(om+Ta)**2*np.sin(inc)**2)
    sign = np.sign(np.sin(om+Ta)*np.sin(inc))
    Photo = transit.occultquad(z+1.-sign,Rp2Rs,[u1,u2]) 
    #plt.plot(t,z)
    #plt.title('z')
    #plt.show()
    return Photo



time_pp = np.arange(0, 1000, 0.001)

star = pp.Star('star', m = 1, r = 1, limbdark=[0.3,0.2])
planeta = pp.Planet('planeta', m = 40, r = 10., e = 0.01, per = 20., inc = 89.5, t0 = 4.)
planetb = pp.Planet('planetb', m = 30, r = 20., e = 0.01, per = 40.01, inc = 89.5, t0 = 5.)
system = pp.System(star, planeta,planetb)
#system = pp.System(star, planetb)
system.compute(time_pp, lambda1 = 0.5, lambda2 = 0.5, R = 1)
plt.plot(time_pp,system.flux[:,0]/system.flux[0][0])
plt.xlim(3.9,4.1)
plt.show()

#time_rb = np.array([863.7,983.6])/yr2pi
#time_rb = np.array([44.95,124.9,564.97,964.75])/yr2pi
time_rb = np.arange(5,1000,40)/yr2pi

a1 = (10*d2s)**(2./3.)*G13*tau23*(1.*M_sun+40*m_earth)**(1./3.)/au
a2 = (20*d2s)**(2./3.)*G13*tau23*(1.*M_sun+30*m_earth)**(1./3.)/au


XX = []
YY = []
ZZ = []
VX = []
VY = []
VZ = []
M  = []

sim = rebound.Simulation()
sim.add(m=1.) # central star
sim.move_to_hel()
sim.add(m=40*m_earth/M_sun, P=20./yr2pi, e=0.01, omega=90/180*np.pi,
                        T=4./yr2pi , inc=89.5/180*np.pi)
XX.append(sim.particles[1].x)
YY.append(sim.particles[1].y)
ZZ.append(sim.particles[1].z)
VX.append(sim.particles[1].vx)
VY.append(sim.particles[1].vy)
VZ.append(sim.particles[1].vz)
M.append(sim.particles[1].m)
print(sim.particles[1].x)
print("1")
sim.status()


sim = rebound.Simulation()
sim.add(m=1.) # central star
sim.move_to_hel()
sim.add(m=30*m_earth/M_sun, P=40.01/yr2pi, e=0.01, omega=90/180*np.pi,
                        T=5./yr2pi , inc=89.5/180*np.pi)
XX.append(sim.particles[1].x)
YY.append(sim.particles[1].y)
ZZ.append(sim.particles[1].z)
VX.append(sim.particles[1].vx)
VY.append(sim.particles[1].vy)
VZ.append(sim.particles[1].vz)
M.append(sim.particles[1].m)
print("2")
sim.status()

sim = rebound.Simulation()
sim.add(m=1.) # central star
sim.move_to_hel()
#sim.add(m=40*m_earth/M_sun, x=0.1371866676986225, y=0.00038646530471314454, z=0.04428453758889174,
#        vx=-0.8116756615787022, vy=0.021870375875873004, vz=2.506096849436916)
#sim.add(m=30*m_earth/M_sun, x=0.1619742448382569, y=0.001409478207088856, z=0.16151020513699227, 
#        vx=-1.4780079267693154, vy=0.012916161297736319, vz=1.4800454879600433)
sim.add(m=M[0],x=XX[0],y=YY[0],z=ZZ[0],vx=VX[0],vy=VY[0],vz=VZ[0])
sim.add(m=M[1],x=XX[1],y=YY[1],z=ZZ[1],vx=VX[1],vy=VY[1],vz=VZ[1])




'''
sim = rebound.Simulation()
sim.add(m=1.) # central star
sim.move_to_hel()
sim.add(m=0.04*m_earth/M_sun, P=20./yr2pi, e=0.001, omega=90/180*np.pi,
                        T=4./yr2pi , inc=89.5/180*np.pi)
sim.add(m=30*m_earth/M_sun, P=40./yr2pi, e=0.001, omega=90/180*np.pi,
                        T=5./yr2pi , inc=89.5/180*np.pi)
'''

sim.move_to_com()
sim.dt = 20*0.001/yr2pi
#sim.integrator = "whfast"

Ps     = []
es     = []
oms    = []
tperis = []
incs   = []
a2Rss  = []
Pnum   = []
Tas    = []
Xs     = []
Ys     = []
Zs     = []
Ts     = []

for times in list(time_rb):
    #for times in list(time_pp/yr2pi):
    sim.integrate(times, exact_finish_time=1)
    #sim.move_to_hel()
    for j in range(2):
        #sim.particles[j+1].calculate_orbit(primary=sim.particles[0])
        Ps.append(sim.particles[j+1].P*yr2pi)
        es.append(sim.particles[j+1].e)
        oms.append(sim.particles[j+1].omega)
        tperis.append(sim.particles[j+1].T*yr2pi)
        incs.append(sim.particles[j+1].inc)
        a2Rss.append(sim.particles[j+1].a*au/1/R_sun)
        Tas.append((sim.particles[j+1].f))
        Xs.append(sim.particles[j+1].x*au)
        Ys.append(sim.particles[j+1].y*au)
        Zs.append(np.sqrt(Xs[-1]**2+Ys[-1]**2)/R_sun)
        Ts.append(times*yr2pi)

        #if (times*yr2pi > 44.9) & (times*yr2pi< 45.1):
        timesMA = np.arange(times*yr2pi-0.35,times*yr2pi+0.35,0.001)
        mod = MandelAgolRV(timesMA,
                       Ps[-1],tperis[-1],a2Rss[-1],20*r_earth/1/R_sun,es[-1],oms[-1],incs[-1],u1=0.3,u2=0.2)
        plt.plot(timesMA,mod)
        plt.plot(time_pp,system.flux[:,0]/system.flux[0][0],'--')
        #plt.plot(Ts,transit.occultquad(Zs,20*r_earth/1/R_sun,[0.3,0.2]),'.-')
        plt.xlim(np.min(timesMA),np.max(timesMA))
        #plt.xlim(44.5,45.1)
        plt.show()
    #sim.move_to_com()