#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 13:31:54 2020

@author: dreizler
"""

import astropy
from astropy.io import fits
import corner
import celerite
from celerite import terms
from celerite.modeling import Model
import emcee
import gls
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.cm as cm
import numpy as np
import pprint,pickle
from scipy.optimize import minimize
import time
import transit
import copy
import sys
import os
from multiprocessing import Pool, cpu_count
import multiprocessing as mp

#import multiprocessing


from DataClass_new import *
from PlanetClass_new import *
from ModelingClass_new import *

#######################################################################
# astro constants
M_sun   = np.float64(1.9891e30)       # mercury6 value SI
R_sun   = np.float64(6.96342e8)       # SI value
G       = np.float64(6.67384e-11)     # SI
m_earth = np.float64(5.9736e24)       # SI
r_earth = np.float64(6371000.785)     # SI GRS80 mean value
d2s     = np.float64(86400.)
au      = np.float64(149597870000.)   # mercury6 value SI

pi2   = np.float64(2.*np.pi)
G13   = np.float64(G**(1./3.))
tau13 = np.float64((2.*np.pi)**(-1./3.))
tau23 = np.float64((2.*np.pi)**(-2./3.))
#######################################################################
# plot design
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['font.size'] = '20'
matplotlib.rcParams['xtick.labelsize'] = 18
matplotlib.rcParams['ytick.labelsize'] = 18
matplotlib.rcParams['axes.linewidth'] = 1.2
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['lines.markeredgewidth'] = 1
INSTCOLORS = ['cornflowerblue', 'darkorange', 'purple','crimson',\
                  'orangered','darkgreen','mediumslateblue']

ncpu = cpu_count()
print("{0} CPUs".format(ncpu))

#######################################################################
# some basic settings

t0         = 2458438.  # offset time
numt       = 10000     # number of points for model plot
numBurn    = 5000      # burn-in steps MCMC
numMCMC    = 5000     # steps for MCMC
numWalkers = 200       # walkers for MCMC, minimum is 2 times number of free parameters

DATAList   = []        # list for all observational data
PlanetList = []        # list for all planets
savenamein = 'Test_3Planets_2Transits_Dynamic' # prefix of files to be saved
savename   = 'Test_3Planets_2Transits_Dynamic'    # prefix of files to be saved
ReStart    = False      # re-start from previous result?

###############################################################################
if ReStart:
    pkl_file = open("Outputs/"+savenamein+'.pkl', 'rb')
    Stern    = pickle.load(pkl_file)
    Planets  = pickle.load(pkl_file)
    Data     = pickle.load(pkl_file)
    pprint.pprint(Stern)
    pprint.pprint(Planets)
    pprint.pprint(Data)
    pkl_file.close()
    # decide, which planet and star parameters to be taken from previous solution
    # planet index 0 from previous solution
    ReStartP = [True,True,True]
    ReStartS = True
    ReStartRV= [True,True,True]
    ReStartAC= [False]
    ReStartTR= [True,False]
else:
    ReStartP = [False,False,False,False]
    ReStartS = False
    ReStartRV= [False,False,False,False,False,False,False,False,False,False,False,False]
    ReStartAC= [False,False,False,False,False,False,False,False,False,False,False,False]
    ReStartTR= [False,False,False,False,False,False,False,False,False,False,False,False]
    
###############################################################################
# define planets and star, parameters need to be adapted
Mstar = np.float64(0.5) # stellar mass
MstarE= np.float64(0.02) # error
Rstar = np.float64(0.5) # stellar radius GUESSED only
RstarE= np.float64(0.015) # error  GUESSED only
if ReStartS:
    STAR  = Star(Mstar=Stern.Mstar,Rstar=Stern.Rstar,MstarE=Stern.MstarE,RstarE=Stern.RstarE)
else:
    STAR  = Star(Mstar=Mstar,Rstar=Rstar,MstarE=MstarE,RstarE=RstarE)

bounds = dict(Rstar=(0.45,0.99))

# for Dynamic orbits: for co-planat orbits, all planets need "Coplanar=True" (default); 
# for non-coplanar orbits: set the first to "Coplanar=True", the others to "False". Only Delta Omegas can be determined
# Planet index 0


K0_0            = np.float64(3.1)         # replace with planet mass for dynamical fits
m0_0            = np.float64(30.)
P0_0            = np.float64(2.1)
e0_0            = np.float64(0.005)
om0_0           = np.float64(60/180*np.pi)   # Standard for circular Orbit
tperi0_0        = np.float64(0.5)#1.28159)
Rp2Rs0_0        = np.float64(0.05)
inc0_0          = np.float64(87.5/180.*np.pi)
Om0_0            = np.float64(0.)          # Standard for non-Dynamic Orbit, allways fix for first planet 
bounds['K0']    =(0.,2000.) 
#bounds['m0']    =(0.,1000.) 
#bounds['P0']    =(4.05,4.06)
bounds['e0']    =(-0.,0.45) 
#bounds['om0']   =(-1.*np.pi,2.*np.pi)
#bounds['tperi0']=(-60,62)
#bounds['Rp2Rs0']=(0.0,0.5)
#bounds['inc0']  =(0.05*np.pi,np.pi*0.501)
if ReStartP[0]:
    PlanetList.append(Planets[0])
    #PlanetList[0].Transit=True
    #PlanetList[0].Rp2Rs = 0.039
    PlanetList[0].e = 0.05
    #PlanetList[0].Rstar=Rstar*R_sun
    #PlanetList[0].Mstar=Mstar*M_sun
    PlanetList[0].set_orbit('Kepler')
    #PlanetList[0].set_fixInc(fixInc=False)
    #PlanetList[0].set_Coplanar(Coplanar=False)
else:
    PlanetList.append(Planet(Mstar=Mstar,Rstar=Rstar,\
                         orbit="Kepler",Transit=True,Coplanar=False,fixOm=True,fixInc=True,\
                         K=K0_0,m=m0_0,P=P0_0,tperi=tperi0_0,e=e0_0,om=om0_0,\
                         Rp2Rs=Rp2Rs0_0,inc=inc0_0,Om=Om0_0,t0=t0,firstcall=True))
print('Transit time',PlanetList[0].ttra)


# Planet index 1
K1_0            = np.float64(6.2)        # replace with planet mass for dynamical fits
m1_0            = np.float64(9)
P1_0            = np.float64(4.3)
e1_0            = np.float64(0.08)
om1_0           = np.float64(30/180*np.pi)   # Standard for circular Orbit
tperi1_0        = np.float64(1.2)#1.28159)
Rp2Rs1_0        = np.float64(0.07)
inc1_0          = np.float64(88./180.*np.pi)
Om1_0            = np.float64(0.)          # Standard for non-Dynamic Orbit, allways fix for first planet 
bounds['K1']    =(0.,1000.) 
#bounds['m1']    =(220,250.) 
#bounds['P1']    =(15,17.2)
bounds['e1']    =(-0.,0.45) 
#bounds['om1']   =(-1.*np.pi,2.5*np.pi)
#bounds['tperi1']=(-29.5,35)
#bounds['Rp2Rs1']=(0.0,0.5)
#bounds['inc1']  =(0.05*np.pi,np.pi*0.51)
if ReStartP[1]:
    PlanetList.append(Planets[1])
    #PlanetList[1].set_orbit('Dynamic')
    #PlanetList[1].set_fixInc(False)
    #PlanetList[1].set_Coplanar(True)
else:
    PlanetList.append(Planet(Mstar=Mstar,Rstar=Rstar,\
                         orbit="Kepler",Transit=True,Coplanar=False,fixOm=True,fixInc=True,\
                         K=K1_0,m=m1_0,P=P1_0,tperi=tperi1_0,e=e1_0,om=om1_0,\
                         Rp2Rs=Rp2Rs1_0,inc=inc1_0,Om=Om1_0,t0=t0,firstcall=True))
print('Transit time',PlanetList[1].ttra)

# Planet index 2
K2_0            = np.float64(5)         # replace with planet mass for dynamical fits
m2_0            = np.float64(12)
P2_0            = np.float64(50.)
e2_0            = np.float64(0.1)
om2_0           = np.float64(200/180*np.pi)   # Standard for circular Orbit
tperi2_0        = np.float64(25)#1.28159)
#Rp2Rs2_0        = np.float64(0.037)
inc2_0          = np.float64(87./180.*np.pi)
Om2_0            = np.float64(0.)          # Standard for non-Dynamic Orbit, allways fix for first planet 
bounds['K2']    =(0.,1000.) 
#bounds['P2']    =(1.935,1.940)
bounds['e2']    =(-0.,0.45) 
#bounds['om2']   =(-1.*np.pi,2.5*np.pi)
#bounds['tperi2']=(1.27,1.29)
#bounds['Rp2Rs2']=(0.0,0.5)
#bounds['inc2']  =(0.45*np.pi,np.pi*0.55)
if ReStartP[2]:
    PlanetList.append(Planets[2])
    #PlanetList[2].set_orbit('Dynamic')
    #PlanetList[2].set_fixInc(False)
    #PlanetList[2].set_Coplanar(True)
else:
    PlanetList.append(Planet(Mstar=Mstar,Rstar=Rstar,\
                         orbit="Kepler",Transit=False,Coplanar=False,fixOm=True,fixInc=True,\
                         K=K2_0,P=P2_0,tperi=tperi2_0,e=e2_0,om=om2_0,\
                         inc=inc2_0,Om=Om2_0,t0=t0,firstcall=True))
print('Transit time',PlanetList[2].ttra)

'''
# Planet index 3
K3_0            = np.float64(3.29)         # replace with planet mass for dynamical fits
m3_0            = np.float64(15.)
P3_0            = np.float64(124.4)
e3_0            = np.float64(0.0)
om3_0           = np.float64(0.5*np.pi)   # Standard for circular Orbit
tperi3_0        = np.float64(0.5)#1.28159)
#Rp2Rs3_0        = np.float64(0.037)
inc3_0          = np.float64(56./180.*np.pi)
Om3_0            = np.float64(0.)          # Standard for non-Dynamic Orbit, allways fix for first planet 
bounds['K3']    =(0.,1000.) 
bounds['P3']    =(123.5,125.0)
bounds['e3']    =(-0.35,0.35) 
#bounds['om3']   =(0.2*np.pi,0.8*np.pi)
#bounds['tperi3']=(1.27,1.29)
#bounds['Rp2Rs3']=(0.0,0.5)
#bounds['inc3']  =(0.45*np.pi,np.pi*0.55)
PlanetList.append(Planet(Mstar=Mstar,Rstar=Rstar,\
                         orbit="Dynamic",Transit=False,Coplanar=True,fixOm=True,\
                         K=K3_0,P=P3_0,tperi=tperi3_0,e=e3_0,om=om3_0,\
                         inc=inc3_0,Om=Om3_0,t0=t0,firstcall=True))
print('Transit time',PlanetList[3].ttra)
'''

try:
    minPer = np.min([l.P for l in PlanetList if (l.P > 0.) & (l.orbit == 'Dynamic')])
except:
    minPer=30.

fitRstar = False
for Plan in PlanetList:
    if Plan.Transit & (not Plan.TransitOnly): fitRstar = True
    
print("")
print("finished defining host star and planet(s)")
print("")

###############################################################################
'''
import celerite
from celerite import terms
import gls
# RV data
# Rotation 70 days, rotation variance 3**2 m/s


# 1st instrument N1 measurements in one epoch of 90 days, N2 next year, scatter in one night
# sigma = 1.2 +-0.3 m/s, Jitter 2 m/s
N1 = 50
N2 = 20
off= 1.5
jitter = 2
sigma = 1.2
sigmae = 0.3
times = np.random.randint(0,90,N1) + 0.5*np.random.randn(N1) + 3.4
times = np.sort(np.append(times,np.random.randint(0,90,N2)+357. + 0.5*np.random.randn(N2)))

error = sigmae*np.random.randn(len(times)) + sigma
velo = PlanetList[0].Kepler(times) + PlanetList[1].Kepler(times) + PlanetList[2].Kepler(times)\
       + np.sqrt(error**2 + jitter**2) * np.random.randn(len(times)) + off


# A non-periodic component
Q = 1.3 #/ np.sqrt(2.0)
w0 = 2*np.pi/70
S0 = 9 / (w0 * Q)
kernel = terms.SHOTerm(log_S0=np.log(S0), log_Q=np.log(Q), log_omega0=np.log(w0))
#a = 9
#c = 0.03
#kernel = terms.RealTerm(log_a=np.log(a), log_c=np.log(c))
gp = celerite.GP(kernel, mean=0)
gp.compute(times, error)
pred_mean, pred_var = gp.predict(3*np.random.randn(len(times)), times, return_var=True)
pred_std = np.sqrt(pred_var)
print(np.std(pred_mean))
plt.plot(times,pred_mean)
plt.show()
glsout = gls.Gls((times,velo+pred_mean,error),verbose=True)
plt.semilogx(1./glsout.freq,glsout.power)
plt.show()

np.savetxt('RV_1.dat',list(zip(times+t0,velo+pred_mean,error)),fmt='%15.6f',delimiter=',')


# 2nd instrument N1 measurements in one epoch of 70 days, N2 next year, scatter in one night
# sigma = 1.2 +-0.3 m/s, Jitter 2 m/s
N1 = 30
N2 = 40
off= -2.5
jitter = 1
sigma = 2.1
sigmae = 0.3
times = np.random.randint(0,90,N1) + 0.5*np.random.randn(N1)+9.7
times = np.sort(np.append(times,np.random.randint(0,90,N2)+377 + 0.5*np.random.randn(N2)))

error = sigmae*np.random.randn(len(times)) + sigma!
velo = PlanetList[0].Kepler(times) + PlanetList[1].Kepler(times) + PlanetList[2].Kepler(times)\
       + np.sqrt(error**2 + jitter**2) * np.random.randn(len(times)) + off

gp.compute(times, error)
pred_mean, pred_var = gp.predict(3*np.random.randn(len(times)), times, return_var=True)
pred_std = np.sqrt(pred_var)
print(np.std(pred_mean))
plt.plot(times,pred_mean)
plt.show()
glsout = gls.Gls((times,velo+pred_mean,error),verbose=True)
plt.semilogx(1./glsout.freq,glsout.power)
plt.show()

np.savetxt('RV_2.dat',list(zip(times+t0,velo+pred_mean,error)),fmt='%15.6f',delimiter=',')

# 3rd instrument N1 measurements in one epoch of 70 days, N2 next year, scatter in one night
# sigma = 1.2 +-0.3 m/s, Jitter 2 m/s
N1 = 10
N2 = 5
off= 15
jitter = 0.2
sigma = 0.5
sigmae = 0.1
times = np.random.randint(0,90,N1) + 0.5*np.random.randn(N1)
times = np.sort(np.append(times,np.random.randint(0,90,N2)+365.25 + 0.5*np.random.randn(N2)))

error = sigmae*np.random.randn(len(times)) + sigma
velo = PlanetList[0].Kepler(times) + PlanetList[1].Kepler(times) + PlanetList[2].Kepler(times)\
       + np.sqrt(error**2 + jitter**2) * np.random.randn(len(times)) + off

gp.compute(times, error)
pred_mean, pred_var = gp.predict(3*np.random.randn(len(times)), times, return_var=True)
pred_std = np.sqrt(pred_var)
print(np.std(pred_mean))
plt.plot(times,pred_mean)
plt.show()
glsout = gls.Gls((times,velo+pred_mean,error),verbose=True)
plt.semilogx(1./glsout.freq,glsout.power)
plt.show()

np.savetxt('RV_3.dat',list(zip(times+t0,velo+pred_mean,error)),fmt='%15.6f',delimiter=',')


# TR data

# 1st instrument continuous measurements 
# sigma = 1.2 +-0.3 m/s, Jitter 2 m/s
N1 = 30
off= 1.e-5
jitter = 0.001
sigma = 0.001
sigmae = 0.0001
u1 = 0.3
u2 = 0.2
times = np.arange(0,N1,2./60./24) + 6.6

error = sigmae*np.random.randn(len(times)) + sigma
flux = PlanetList[0].MandelAgolRV(times,u1,u2) + PlanetList[1].MandelAgolRV(times,u1,u2) - 1\
       + np.sqrt(error**2 + jitter**2) * np.random.randn(len(times)) + off

plt.plot(times%PlanetList[0].P,flux)
plt.plot(times%PlanetList[1].P,flux)
plt.show()
plt.plot(times,flux)

np.savetxt('TR_1.dat',list(zip(times+t0-2457000.,flux,error)),fmt='%15.6f',delimiter=',')

# 2nd instrument continuous measurements 
# sigma = 1.2 +-0.3 m/s, Jitter 2 m/s
N1 = 10
off= 1.e-5
jitter = 0.00005
sigma = 0.00055
sigmae = 0.00005
u1 = 0.1
u2 = 0.1
thirdL = 0.3
times = np.arange(0,N1,2./60./24) + 44.

error = sigmae*np.random.randn(len(times)) + sigma
flux = PlanetList[0].MandelAgolRV(times,u1,u2) + PlanetList[1].MandelAgolRV(times,u1,u2) - 1\
       + np.sqrt(error**2 + jitter**2) * np.random.randn(len(times)) + off + thirdL

plt.plot(times%PlanetList[0].P,flux)
plt.plot(times%PlanetList[1].P,flux)
plt.show()
plt.plot(times,flux)

np.savetxt('TR_2.dat',list(zip(times+t0,flux/(1+thirdL),error)),fmt='%15.6f',delimiter=',')
'''

###############################################################################

# first file is "lead" data set
RVstart= 0
RVdata = ['RV_1.dat','RV_2.dat','RV_3.dat']
RVname = ['TEST1','TEST2','TEST3']
GP     = 'SHO'  # SHO, REAL, NONE, ...
# SHO: S_0, tau, P
# REAL: a, tau
GPpar  = [20.,100.,100.] # SHO   
#GPpar  = [1.3,1.]      # REAL
#boundsgpRV= dict(log_a=(-10., 5.), log_c=(-6., 2.2))
boundsgpRV= dict(log_S0=(-10., 15.), log_Q=(-4., 20.), \
                 log_omega0=(np.log(2.*np.pi/200.),np.log(2.*np.pi/5.)))
Jitter    = [True,True,True]
Jitterpar = [np.exp(-1.),np.exp(-1.),np.exp(-1.),np.exp(-1),np.exp(-1),np.exp(-1)]
boundsJRV = dict(log_sigma=(-5.,3.))
offs      = [1.3,-2.,14.]

tminmax  = []
All_time = []
All_value= []
All_error= []
All_index= []

for i,RV in enumerate(RVdata):
    if ReStartRV[i]:
        DATAList.append(Data[RVstart+i])
        #DATAList[0].Jitterpar = np.exp(-1.)
        #DATAList[i].GP = 'NONE'
        #DATAList[i].GPpar = [2.,2.,16.1]
        #DATAList[i].nGPpar = 0
    else:
        DATAList.append(RVTimeSeries(RV,RVname[i],GP,Jitter=Jitter[i],GPpar=GPpar,\
                               Jitterpar=Jitterpar[i],off=offs[i],t0=t0,\
                               color=INSTCOLORS[i],weight=1.,delimiter=',',binwidth=1))
    tminmax.append([np.min(DATAList[i].time),np.max(DATAList[i].time)])
    All_index= np.append(All_index,np.ones(len(DATAList[i].time))*i)
    All_time = np.append(All_time, DATAList[i].time)
    All_value= np.append(All_value,DATAList[i].value)
    All_error= np.append(All_error,DATAList[i].error)
for i,RV in enumerate(RVdata): DATAList[i].set_Alltimes(All_time,All_value,All_error,All_index=All_index)

###############################################################################
ACstart= len(RVdata)
ACdata = []#'InputFiles/asas-sn_V_no_out_SD.dat'] 
ACname = ['ASAS-SN','CRX','dLW']
AClabels = ['mag','crx (m/s per Np )']
types  = ['PlainCols_ASAS_SD']
binwidths = [1.,]
GP     = ['SHO','SHO']
GPpar  = [0.1,100.,130.]    # a, tau
boundsgpAC= [dict(log_S0=(-10., 5.), log_Q=(-2., 10.), \
                 log_w0=(np.log(2.*np.pi/500.),np.log(2.*np.pi/100.))),\
             dict(log_S0=(-10., 5.), log_Q=(-2., 10.), \
                 log_w0=(np.log(2.*np.pi/500.),np.log(2.*np.pi/100.)))]
Jitter    = [True,False]
Jitterpar = [0.001,1.]
offs      = [0.,0.]

for i,AC in enumerate(ACdata):
    if ReStartAC[i]:
        DATAList.append(Data[ACstart+i])
    else:
        DATAList.append(ACTimeSeries(AC,ACname[i],GP[i],Jitter=Jitter[i],GPpar=GPpar,\
                               Jitterpar=Jitterpar[i],off=offs[i],t0=t0,data='AC',\
                               color=INSTCOLORS[i],filetype=types[i],binwidth=binwidths[i]))

###############################################################################
TRstart = ACstart + len(ACdata)
TRdata = ['TR_1.dat','TR_2.dat']#'InputFiles/Detrended_GJ486_PDC_Spline_025.dat']#,'InputFiles/Detrended_GJ486_PDC_Spline_025.dat']
TRname = ['TEST_1','TEST_2']
types  = ['PlainColsTransit','PlainColsTransit']
detrend= [None,None,None]#,'Linear']
GPs    = ['NONE','NONE','SHO']#,'SHO2']
GPpar  =[[np.float64(1.e-6),np.float64(2.),np.float64(5.)],\
         [np.float64(1.e-6),np.float64(2.),np.float64(5.)],\
         [np.float64(1.e-6),np.float64(2.),np.float64(5.)]]
boundsgpTR= [dict(log_S0=(-19., 5.), log_Q=(-2., 10.), \
                 log_w0=(np.log(2.*np.pi/100.),np.log(2.*np.pi/1.))),\
             dict(log_S0=(-19., 5.), log_Q=(-2., 10.), \
                 log_w0=(np.log(2.*np.pi/100.),np.log(2.*np.pi/1.))),\
             dict(log_S0=(-19., 5.), log_Q=(-2., 10.), \
                 log_w0=(np.log(2.*np.pi/100.),np.log(2.*np.pi/1.)))]                       
            ##dict(log_a=(-10., -4.), log_c=(-6., -4.))]  # has to be adapted to the kernels defined in GPs, all kernels need bounds!!
Jitter    = [False,False]
Jitterpar = [1.e-4,1.e-4,1.e-4]
offs      = [0.,0.,0.]
fixLDs    = [True,True]
t0adds    = [2457000.,0]
u1s       = [0.3,0.1]
u2s       = [0.2,0.1]
fix3s     = [True,False,True]
thirdLs   = [0.0,0.3,0.0]
binwidths = [0.0005,0.0005,0.008]
weights   = [1.,1.,1.]

bounds['u10']=(0.,1.)
bounds['u20']=(0.,1.)
bounds['u11']=(0.,1.)
bounds['u21']=(0.,1.)
bounds['u12']=(0.,1.)
bounds['u22']=(0.,1.)


ttras = []
for i,Plan in enumerate(PlanetList):
    if Plan.Transit: ttras.append((Plan.ttra,Plan.P,0.02,i))

    

All_timeTR  = []
All_valueTR = []
All_indexTR = []
All_TT_time = []
for i,TR in enumerate(TRdata):
    if ReStartTR[i]:
        DATAList.append(Data[TRstart+i])
    else:
        for j,Plan in enumerate(PlanetList):
            if Plan.Transit: 
                TR_input = TRTimeSeries(TR,TRname[i],GPs[i],Jitter=Jitter[i],GPpar=GPpar[i],\
                               Jitterpar=Jitterpar[i],off=offs[i],fixLD=fixLDs[i],u1=u1s[i],u2=u2s[i],\
                               fix3=fix3s[i],thirdL=thirdLs[i],t0=t0,t0add=t0adds[i],filetype=types[i],\
                               delimiter=',',color=INSTCOLORS[i],detrend=detrend[i],data='TR',\
                               binwidth=binwidths[i],ttras=[ttras[j]],weight=weights[i],plot=True,\
                               PlanetList=PlanetList)
                if Plan.orbit == 'Dynamic':  # SD NEW indent
                    for k,nTr in enumerate(TR_input.TransitTimes):
                        TR_store = copy.copy(TR_input)
                        print("cutting out transits",nTr,ttras[j][2])
                        ind = np.where((TR_input.time > nTr - ttras[j][2]) & \
                                       (TR_input.time < nTr + ttras[j][2]) )
                        TR_store.set_Times(TR_input.time[ind],TR_input.value[ind],TR_input.error[ind])
                        TR_store.set_TransitTimes(nTr,j)
                        DATAList.append(TR_store)
                        #All_index= np.append(All_index,np.ones(len(ind[0])*(TRstart+k+\
                        #                     j*len(TR_input.TransitTimes)+i*len(PlanetList))))                
                        All_TT_time += [nTr]
                        #plt.plot(DATAList[-1].time,DATAList[-1].value-0.01)
                        #plt.plot(TR_store.time,TR_store.value)
                        #plt.show()
                        print(i,TRstart,j,len(TR_input.TransitTimes),k,i+TRstart+j*len(TR_input.TransitTimes)+k,len(ind[0]))
                        All_indexTR += [i+TRstart+j*len(TR_input.TransitTimes)+k]*len(ind[0])
                        All_timeTR  += list(TR_store.time)
                        All_valueTR += list(TR_store.value)
                else:
                    DATAList.append(TR_input)
                #All_index= np.append(All_index,np.ones(len(DATAList[TRstart+i].time))*(i+TRstart))

#for i,TR in enumerate(TRdata): DATAList[i+TRstart].set_Alltimes(np.arange(len(All_index)),\
#                      np.zeros(len(All_index)),np.zeros(len(All_index)),All_index=All_index)
#stop   
###############################################################################
# provide the information that only transit light curves are fitted
TransitOnly = False
if RVdata == []: TransitOnly = True
for Plan in PlanetList:
    Plan.set_TransitOnly(TransitOnly)

print("")
print("")
print("finished reading data files")
print("")
###############################################################################
# define model


mod = ModelingClass(PlanetList,DATAList,All_TT_time,All_time,All_index,\
                    All_timeTR,All_indexTR,\
                    bounds,boundsgpRV=boundsgpRV,boundsgpAC=boundsgpAC,\
                    boundsgpTR=boundsgpTR,boundsJRV=boundsJRV,\
                    lin=0.0)

# set time arrays for model output and plot input model
try: 
    timesRV = np.linspace(np.min(tminmax)-10, np.max(tminmax)+10, numt)
except:
    timesRV = np.linspace(0,1)
try:
    timesTR = np.linspace(Plan.ttra-0.03*Plan.P,Plan.ttra+0.03*Plan.P, numt)
except:
    timesTR = np.linspace(0,1)

#mod.plot_input(savename,RVstart,ACstart,TRstart)
PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),Rstar)
mod.plot_result(timesRV,timesTR,savename,RVstart,ACstart,TRstart,mod,'Start',minPer)

#PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),Rstar)
#mod.plot_models(timesRV,timesTR,mod,RVstart,ACstart,TRstart,numt," start model",savename=savename)

###############################################################################
print("")
print("... start minimzation...")
initial_params = mod.gpAll.get_parameter_vector()
bounds         = mod.gpAll.get_parameter_bounds()

# initialize all parameters
x = DATAList[0].time
y = DATAList[0].value
e = DATAList[0].error
for i in range(1,len(DATAList)):
    x = np.concatenate((x,DATAList[i].time),axis=None)
    y = np.concatenate((y,DATAList[i].value),axis=None)
    e = np.concatenate((e,DATAList[i].error),axis=None)
    
inds = np.argsort(x)
x    = x[inds]
y    = y[inds]
e    = e[inds]

mod.gpAll.compute(x, e)

#soln = minimize(mod.neg_log_like, initial_params, 
#                method="Nelder-Mead", args=(y,mod.gpAll))

soln = minimize(mod.neg_log_like, initial_params, 
                method="L-BFGS-B", bounds=bounds, args=(y,mod.gpAll),\
                #method="Nelder-Mead", args=(y,mod.gpAll),\
                options={'ftol': 2.220446049250313e-09, 'gtol': 1e-08, \
                         'eps': 1e-08, 'maxfun': 25000, 'maxiter': 1000})


print("... end minimzation...")
print(mod.gpAll.get_parameter_dict())

#print(initial_params)

ftol  = 2.220446049250313e-09
tmp_i = np.zeros(len(soln.x))
uncertainty_i = np.zeros(len(soln.x))
for i in range(len(soln.x)):
    tmp_i[i] = 1.0
    uncertainty_i[i] = np.sqrt(max(1, abs(soln.fun))*ftol*soln.hess_inv(tmp_i)[i])
    tmp_i[i] = 0.0
    print('{0:12.4e} ± {1:.1e}'.format(soln.x[i], uncertainty_i[i]))

print('######################################################################')
print("Result:",soln)
print("Final log-likelihood optimization: {0}".format(soln.fun))
print("ln L RV only:",mod.log_probability_RV(soln.x))
print("ln L TR only:",mod.log_probability_TR(soln.x))
print('######################################################################')
###############################################################################
# save and show results
      
if fitRstar: Rstar = soln.x[mod.Soff]
PlanetListResult = mod.SaveThePlanet(soln.x,Rstar)
mod.plot_result(timesRV,timesTR,savename,RVstart,ACstart,TRstart,mod,'LM',minPer)
#mod.plot_models(timesRV,timesTR,mod,RVstart,ACstart,TRstart,numt," LM model",savename=savename)

###############################################################################
print("")
print('######################################################################')
print("... start MCMC...")

strt = time.time()
print("Running burn-in...")

initial = np.array(soln.x)
#initial = np.array(initial_params)
ndim    = len(initial)

backend = emcee.backends.HDFBackend("Outputs/"+savename+'burnin.h5')
backend.reset(numWalkers, ndim)

p01 = initial + 1.  * uncertainty_i * np.random.randn(numWalkers//4, ndim)
p02 = initial + 3.  * uncertainty_i * np.random.randn(numWalkers//4, ndim)
p03 = initial + 3.  * uncertainty_i * np.random.randn(numWalkers//4, ndim)
p04 = initial + 1.  * uncertainty_i * np.random.randn(numWalkers//4, ndim)
p0 =  np.array([p01,p02,p03,p04]).reshape(numWalkers,ndim)


for samp in range(10):
    with Pool() as pool:
        sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                  pool=mp.Pool(processes=(cpu_count()//4)),
                  backend=backend)
        start = time.time()
        p0,lp,state = sampler.run_mcmc(p0, numBurn/10, progress=True)
        end = time.time()
        multi_time = end - start
        print("Multiprocessing took {0:.1f} seconds".format(multi_time))
        print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
        print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))


samplesBurn = sampler.flatchain
#p0new = np.copy(p0)
#boundsnew = np.zeros(2*ndim).reshape(ndim,2)
#for k in range(ndim):
#    v=np.percentile(samplesBurn[:,k],[1,50,99])
#    print(p0[np.argmax(lp),k],v,np.mean(np.array([abs(v[0]-v[1]),v[2]-v[1]])))
#    #p0new[0,k] = p0[np.argmax(lp),k] 
#    p0new[0:,k] = p0[np.argmax(lp),k] + np.mean(np.array([abs(v[0]-v[1]),v[2]-v[1]])) \
#                                       * np.random.randn(numWalkers, 1).reshape(-1)
#p0 = np.copy(p0new)
samples = sampler.flatchain
print("run time:",time.time()-strt)
print("First Burn in log-likelihood: {0}".format(-np.max(sampler.flatlnprobability)))
print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(sampler.flatlnprobability)]))
print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(sampler.flatlnprobability)]))
print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
print("parameters{0}".format(samples[np.argmax(sampler.flatlnprobability)]))
################################################################################
# save results 
DATAListResult = mod.SaveTheDatafiles(soln.x,RVstart,ACstart,TRstart)    
STARResult  = Star(Mstar=Mstar/M_sun,Rstar=Rstar/R_sun,MstarE=MstarE/M_sun,RstarE=RstarE/R_sun)

pkl_file = open("Outputs/"+savename+'.pkl', 'wb')
pickle.dump(STARResult,pkl_file)
pickle.dump(PlanetListResult,pkl_file,-1)
pickle.dump(DATAListResult,pkl_file,-1)
pkl_file.close()


PlanetListResult = mod.SaveThePlanet(mod.gpAll.get_parameter_vector(),Rstar)
mod.plot_result(timesRV,timesTR,savename,RVstart,ACstart,TRstart,mod,'BurnIn',minPer)
#mod.plot_models(timesRV,timesTR,mod,RVstart,ACstart,TRstart,numt," first BurnIn model",savename=savename)

backend = emcee.backends.HDFBackend("Outputs/"+savename+'.h5')
backend.reset(numWalkers, ndim)
print("Running MCMC...")
for samp in range(2):
    with Pool() as pool:
        sampler = emcee.EnsembleSampler(numWalkers, ndim, mod.log_probability, 
                  pool=mp.Pool(processes=(cpu_count()//4)),
                  backend=backend)
        start = time.time()
        p0,lp,state = sampler.run_mcmc(p0, numMCMC/2, progress=True)
        end = time.time()
        multi_time = end - start
        print("Multiprocessing took {0:.1f} seconds".format(multi_time))
        print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
        print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(sampler.flatlnprobability)]))
        try:
            reader = emcee.backends.HDFBackend("Outputs/"+savename+'.h5')
            tau = reader.get_autocorr_time()
        except:
            tau = samp*numMCMC/2
        print(tau)
        if np.mean(tau)*50<i*samp*numMCMC/2: break

try:
    reader = emcee.backends.HDFBackend("Outputs/"+savename+'.h5')
    tau    = reader.get_autocorr_time()
    burnin = int(2 * np.max(tau))
    thin   = int(0.5 * np.min(tau))
    
    samples = sampler.get_chain(discard=burnin, thin=thin, flat=True)
    print(flat_samples.shape)
    flatlnprob = sampler.get_log_prob(discard=burnin, flat=True, thin=thin)
except:
    samples    = sampler.flatchain
    flatlnprob = sampler.flatlnprobability

print("Final log-likelihood: {0}".format(-np.max(flatlnprob)))
print("ln L RV only:",mod.log_probability_RV(samples[np.argmax(flatlnprob)]))
print("ln L TR only:",mod.log_probability_TR(samples[np.argmax(flatlnprob)]))
print("Mean acceptance fraction:{0:.3f}".format(np.mean(sampler.acceptance_fraction)))
f = open('Outputs/'+savename+'output.txt','a')
f.write("run time: {0}\n".format(time.time()-strt))
f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(samples[np.argmax(flatlnprob)])))
f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(samples[np.argmax(flatlnprob)])))
f.write("Mean acceptance fraction:{0:.3f}\n".format(np.mean(sampler.acceptance_fraction)))



strt = time.time()
print("Running final LM fit...")
soln.x = samples[np.argmax(flatlnprob)]
print('######################################################################')
print("Result:",soln)
print("Final log-likelihood optimization: {0}".format(soln.fun))
print("ln L RV only:",mod.log_probability_RV(soln.x))
print("ln L TR only:",mod.log_probability_TR(soln.x))
print('######################################################################')
print("Best fit parameter_vector:\n{0}\n".format(soln.x))
print('######################################################################')
f.write('######################################################################\n')
f.write("Result: {0}\n".format(soln))
f.write("Final log-likelihood optimization: {0}\n".format(soln.fun))
f.write("ln L RV only: {0}\n".format(mod.log_probability_RV(soln.x)))
f.write("ln L TR only: {0}\n".format(mod.log_probability_TR(soln.x)))
f.write('######################################################################\n')
f.write("Best fit parameter_vector:\n{0}\n".format(soln.x))
f.write('######################################################################\n')

#######################################################################
# save and show results

if fitRstar: Rstar = soln.x[mod.Soff]
PlanetListResult = mod.SaveThePlanet(soln.x,Rstar)
mod.plot_result(timesRV,timesTR,savename,RVstart,ACstart,TRstart,mod,'',minPer)
#mod.plot_models(timesRV,timesTR,mod,RVstart,ACstart,TRstart,numt,"",savename=savename)

######################################################################
# calculate derived quantities

truth,xx = mod.derived_quantities(samples,soln.x,STAR,mod,RVstart,ACstart,TRstart,ndim)
    
################################################################################
# save results 

DATAListResult = mod.SaveTheDatafiles(soln.x,RVstart,ACstart,TRstart)    
STARResult  = Star(Mstar=Mstar/M_sun,Rstar=Rstar/R_sun,MstarE=MstarE/M_sun,RstarE=RstarE/R_sun)

pkl_file = open("Outputs/"+savename+'.pkl', 'wb')
pickle.dump(STARResult,pkl_file)
pickle.dump(PlanetListResult,pkl_file,-1)
pickle.dump(DATAListResult,pkl_file,-1)
pkl_file.close()

#####################################################################################################
# corner plots
CN_RV_GP_SHO  = ['ln S$_0$','ln Q','ln $omega_0$']
CN_RV_GPm_SHO = ['S$_0 [m^2 s^{-2}]$','$\\tau_\\mathrm{d} [d]$','$P_0$ [d]']

CN_RV_GP_REAL = ['ln a','ln c']
CN_RV_GPm_REAL= ['a [m^2 s^{-2}]','$\\tau_\\mathrm{d}$  [d]']

CN_AC_GP_RVD  = ['ln S$_0$']
CN_AC_GPm_RVD = ['S$_0 [m^2 s^{-2}]$']

CN_P          = ['K [m s$^{-1}$]','m [M$_\oplus$]','P [d]','e','$\omega$ [$^\circ$]',\
                 'T$_\mathrm{peri}$ [d]','R$_{p}$/R$_{*}$','i  [$^\circ$]',\
                 '$\Omega$ [$^\circ$]','a/R$_{*}$',]
CN_P_A        = ['K [m s$^{-1}$]','m [M$_\oplus$]','a [au]','$\lambda$  [$^\circ$]',\
                 'a/R$_{*}$','r [R$_\oplus$]','$\\rho$ [g/cm$^3$]']

#CN_P_C        = ['K [m s$^{-1}$]','P [d]','T$_\mathrm{peri}$ [d]']
#CN_P_K        = ['K [m s$^{-1}$]','P [d]','e','$\omega$ [$^\circ$]','T$_\mathrm{peri}$ [d]']
#CN_P_T        = ['R$_{p}$/R$_{*}$','i  [$^\circ$]']
CN_S          = ['R$_{*}$ [R$_\odot$]']
CN_LS         = ['u$_{1}$','u$_{2}$']
CN_3L         = ['D']
#CN_P_add      = ['m [M$_\oplus$]','a [au]','$\lambda$  [$^\circ$]']
#CN_P_add_T    = ['r [R$_\oplus$]','$\\rho$ [g/cm$^3$]']
CN_J          = ['ln Jitter']
CN_Jm         = ['Jitter [m s$^{-1}$]']
CN_Off        = ['Offset [m s$^{-1}$]']
CN_pol        = ['Trend  [m s$^{-1}$ d$^{-1}$]']

plt.rc('xtick',labelsize=13)
plt.rc('ytick',labelsize=13)

print("soln.x fit parameter_vector:\n{0}\n".format(truth))
print('######################################################################')

nADD = 0
for Plan in PlanetList:
    if Plan.Transit: 
        if Plan.TransitOnly: nADD += 0
        else:                nADD += 6
    else:                    nADD += 3
ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
for jj in range(ndim+nADD):
    v=np.percentile(xx[ii][:,jj],[16,50,84])   
    print(v[1],'^',v[2]-v[1],'_',v[1]-v[0])
    f.write("{0} {1} {2} {3} {4} \n".format(v[1],'& +',v[2]-v[1],'& -',v[1]-v[0]))

offA = ndim
POff = 0
for k,Plan in enumerate(PlanetList):
# still to be modeified
#    if k==0: 
#        xx[:][:,Poff+k*Ppar+4] = (xx[:][:,Poff+k*Ppar+4] - 5) % xx[:][:,Poff+k*Ppar+1] + 5 #- np.median(xx[:][:,Poff+k*Ppar+1])
#        truth[Poff+k*Ppar+4] = (truth[Poff+k*Ppar+4] - 5) % truth[Poff+k*Ppar+1] + 5 #- truth[Poff+k*Ppar+1]
#    if k==0: 
#        xx[:][:,Poff+k*Ppar+3] = (xx[:][:,Poff+k*Ppar+3] - 220.) % 360. + 220. 
#        truth[Poff+k*Ppar+3] = (truth[Poff+k*Ppar+3] - 220.) % 360. + 220.

    
    ind = np.arange(0)
    ind = np.append(ind,np.arange(mod.Poff+POff,mod.Poff+POff+Plan.Ppar))
    nAdd = 0
    if Plan.orbit == 'Circular':
        if Plan.Transit:
            if Plan.TransitOnly: nAdd += 0
            else:                nAdd += 6
        else:                    nAdd += 3
    if Plan.orbit == 'Kepler':
        if Plan.Transit:
            if Plan.TransitOnly: nAdd += 0
            else:                nAdd += 6
        else:                    nAdd += 3
    if Plan.orbit == 'Dynamic':
        if Plan.Transit:
            if Plan.TransitOnly: nAdd += 0
            else:                nAdd += 6
        else:                    nAdd += 3
    #if Plan.Transit: 
    #    if Plan.TransitOnly: nAdd += 0
    #    else:                nAdd += 6
    #else:                    nAdd += 3
    #if (not Plan.TransitOnly) ^ (Plan.orbit == 'Dynamic'): nAdd += 3
    #if (not Plan.TransitOnly) & Plan.Transit:              nAdd += 3
    #ind = np.append(ind,np.arange(offA,offA+nAdd))
    
    print(ind,offA,nAdd,mod.Poff,Plan.Ppar,Plan.orbit,Plan.Transit,Plan.TransitOnly)    
    
    if Plan.orbit == 'Circular':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,5,6,7,9]
            else:                index = [0,2,5,6,7,11,12,13,14,15,16]
            if Plan.fixInc or Plan.Coplanar: index.remove(7)
        else:                    index = [0,2,5,11,12,13]
    if Plan.orbit == 'Kepler':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,3,4,5,6,7,9]
            else:                index = [0,2,3,4,5,6,7,11,12,13,14,15,16]
            if Plan.fixInc or Plan.Coplanar: index.remove(7)
        else:                    index = [0,2,3,4,5,11,12,13]
    if Plan.orbit == 'Dynamic':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,3,4,5,6,7,8,9]
            else:                index = [1,2,3,4,5,6,7,8,10,12,13,14,15,16]
        else:                    index = [1,2,3,4,5,7,8,10,12,13]
        if Plan.fixInc or Plan.Coplanar: index.remove(7)
        if Plan.fixOm:  index.remove(8)
    ind = np.append(ind,np.arange(offA,offA+nAdd))
            
    newnames = []    
    newnames = np.append(newnames,CN_P)
    newnames = np.append(newnames,CN_P_A)
    newnames = newnames[index]
    
    plotrange= [1.]*len(newnames)

    # reject bad samples
    ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
    pp = PdfPages("Outputs/"+savename+"corner_planet"+str(k)+".pdf")
    corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
              labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
              show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
    pp.savefig(plt.gcf()) # This generates pdf page
    plt.show()
    pp.close()
    
    
    if Plan.Transit & (k==0): 
        newnames = []
        ind      = np.arange(0)
        ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
        if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
        for DATA in DATAList[TRstart:]:
            if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
        for DATA in DATAList[TRstart:]:
            if not DATA.fix3: newnames = np.append(newnames,CN_3L)
        plotrange= [1.]*len(newnames)
    
        # reject bad samples
        ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
        pp = PdfPages("Outputs/"+savename+"corner_STAR.pdf")
        corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                      labels=np.asarray(newnames),quantiles=[0.16,0.5,0.84],
                      show_titles=True,title_fmt='.3f',label_kwargs={"fontsize":12},range=(np.asarray(plotrange)))
        pp.savefig(plt.gcf()) # This generates pdf page
        plt.show()
        pp.close()
    
    #if Plan.Transit: 
    #    if Plan.TransitOnly: nAdd += 0
    #    else:                nAdd += 6
    #else:                    nAdd += 3
    offA += nAdd
    POff += Plan.Ppar

offA = ndim
POff = 0
for k,Plan in enumerate(PlanetList):
# still to be modeified
#    if k==0: 
#        xx[:][:,Poff+k*Ppar+4] = (xx[:][:,Poff+k*Ppar+4] - 5) % xx[:][:,Poff+k*Ppar+1] + 5 #- np.median(xx[:][:,Poff+k*Ppar+1])
#        truth[Poff+k*Ppar+4] = (truth[Poff+k*Ppar+4] - 5) % truth[Poff+k*Ppar+1] + 5 #- truth[Poff+k*Ppar+1]
#    if k==0: 
#        xx[:][:,Poff+k*Ppar+3] = (xx[:][:,Poff+k*Ppar+3] - 220.) % 360. + 220. 
#        truth[Poff+k*Ppar+3] = (truth[Poff+k*Ppar+3] - 220.) % 360. + 220.

    
    ind = np.arange(0)
    ind = np.append(ind,np.arange(mod.Poff+POff,mod.Poff+POff+Plan.Ppar))
    nAdd = 0
    
    if Plan.orbit == 'Circular':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,5,6,7,9]
            else:                index = [0,2,5,6,7]
            if Plan.fixInc: index.remove(7)
        else:                    index = [0,2,5]
    if Plan.orbit == 'Kepler':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,3,4,5,6,7,9]
            else:                index = [0,2,3,4,5,6,7]
            if Plan.fixInc: index.remove(7)
        else:                    index = [0,2,3,4,5]
    if Plan.orbit == 'Dynamic':
        if Plan.Transit:
            if Plan.TransitOnly: index = [2,3,4,5,6,7,8,9]
            else:                index = [1,2,3,4,5,6,7,8]
        else:                    index = [1,2,3,4,5,7,8]
        if Plan.fixInc or Plan.Coplanar: index.remove(7)
        if Plan.fixOm:  index.remove(8)
            
    newnames = []    
    newnames = np.append(newnames,CN_P)
    newnames = newnames[index]
    
    if Plan.Transit & (k==0): 
        ind = np.append(ind,np.arange(mod.Soff,mod.Soff+mod.Spar))
        if not Plan.TransitOnly: newnames = np.append(newnames,CN_S)
        for DATA in DATAList[TRstart:]:
            if not DATA.fixLD: newnames = np.append(newnames,CN_LS)
        for DATA in DATAList[TRstart:]:
            if not DATA.fix3: newnames = np.append(newnames,CN_3L)
        plotrange= [1.]*len(newnames)
    
    plotrange= [1.]*len(newnames)

    # reject bad samples
    ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
    pp = PdfPages("Outputs/"+savename+"corner_raw_planet"+str(k)+".pdf")
    corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
              labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
              show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
    pp.savefig(plt.gcf()) # This generates pdf page
    plt.show()
    pp.close()
        
    offA += nAdd
    POff += Plan.Ppar

newnames = []
ind      = np.arange(0)
for i,DATA in enumerate(DATAList):
    if isinstance(DATA,RVTimeSeries) & DATA.Jitter:
        newnames = np.append(newnames,CN_Jm)
        ind = np.append(ind,np.arange(mod.JoffRV+i,mod.JoffRV+i+1))
    if isinstance(DATA,ACTimeSeries) & DATA.Jitter:
        newnames = np.append(newnames,CN_Jm)
        ind = np.append(ind,np.arange(mod.JoffAC[i-ACstart],mod.JoffAC[i-ACstart]+1))
    if isinstance(DATA,TRTimeSeries) & DATA.Jitter:
        newnames = np.append(newnames,CN_Jm)
        ind = np.append(ind,np.arange(mod.JoffTR[i-TRstart],mod.JoffTR[i-TRstart]+1))
ind = np.append(ind,np.arange(mod.OoffRV,mod.OoffRV+mod.nfRV))
newnames = np.append(newnames,CN_Off*mod.nfRV)

ind = np.append(ind,np.arange(mod.OoffAC,mod.OoffAC+mod.nfAC))
newnames = np.append(newnames,CN_Off*mod.nfAC)

ind = np.append(ind,np.arange(mod.OoffTR,mod.OoffTR+mod.nfTR))
newnames = np.append(newnames,CN_Off*mod.nfTR)

if mod.ndeg >= 1:
    ind = np.append(ind,[mod.linpos])
    newnames = np.append(newnames,CN_pol)
if mod.ndeg == 2:
    ind = np.append(ind,[mod.quadpos])
    newnames = np.append(newnames,CN_pol)

plotrange= [1.]*len(newnames)

# reject bad samples
ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
pp = PdfPages("Outputs/"+savename+"corner_Jitter.pdf")
corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
              labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
              show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
pp.savefig(plt.gcf()) # This generates pdf page
plt.show()
pp.close()


newnames = []
ind      = np.arange(0)
for i,DATA in enumerate(DATAList):
    if i == RVstart:
        if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'SHO'): 
            newnames = np.append(newnames,CN_RV_GPm_SHO)
            ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
        if isinstance(DATA,RVTimeSeries) & (DATA.GP == 'REAL'):
            newnames = np.append(newnames,CN_RV_GPm_REAL)
            ind = np.append(ind,np.arange(mod.GoffRV,mod.GoffRV+mod.nGPRV))
    if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'SHO'):
        newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfAC)
        ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
    if isinstance(DATA,ACTimeSeries) & (DATA.GP == 'RV Driver'):
        newnames = np.append(newnames,CN_AC_GPm_RVD*mod.nfAC)
        ind = np.append(ind,np.arange(mod.GoffAC,mod.GoffAC+np.sum(mod.nGPAC)))
    if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'SHO'):
        newnames = np.append(newnames,CN_RV_GPm_SHO*mod.nfTR)
        ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
    if isinstance(DATA,TRTimeSeries) & (DATA.GP == 'REAL'):
        newnames = np.append(newnames,CN_RV_GPm_REAL*mod.nfTR)
        ind = np.append(ind,np.arange(mod.GoffTR,mod.GoffTR+np.sum(mod.nGPTR)))
  
plotrange= [1.]*len(newnames)

if newnames != []:
    # reject bad samples
    ii = np.array(np.where(flatlnprob > np.max(flatlnprob) -20)).reshape(-1)
    pp = PdfPages("Outputs/"+savename+"corner_GP.pdf")
    corner.corner(xx[ii][:,ind], truths=truth[ind],title_kwargs={"fontsize":12},
                  labels=np.asarray(newnames),label_kwargs={"fontsize":12},quantiles=[0.16,0.5,0.84],
                  show_titles=True,title_fmt='.3f',range=(np.asarray(plotrange)))
    pp.savefig(plt.gcf()) # This generates pdf page
    plt.show()
    pp.close()
    
print('######################################################################')

print("Final log-likelihood: {0}".format(-np.max(flatlnprob)))
print("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
print("ln L RV only:",mod.log_probability_RV(sampler.flatchain[np.argmax(flatlnprob)]))
print("ln L TR only:",mod.log_probability_TR(sampler.flatchain[np.argmax(flatlnprob)]))
f.write("Final log-likelihood: {0}\n".format(-np.max(flatlnprob)))
f.write("Best fit parameter_vector:\n{0}\n".format(mod.gpAll.get_parameter_vector()))
f.write("ln L RV only:{0}\n".format(mod.log_probability_RV(sampler.flatchain[np.argmax(flatlnprob)])))
f.write("ln L TR only:{0}\n".format(mod.log_probability_TR(sampler.flatchain[np.argmax(flatlnprob)])))
f.close()

print('######################################################################')

